<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 07/11/2021
 * Time: 20:31
 */

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class StokBapoktingExport implements FromView, WithStyles
{
    public function __construct(array $laporan_stok)
    {
        $this->laporan_stok = $laporan_stok;
    }

    public function styles(Worksheet $sheet)
    {
        $jumlah_data = count($this->laporan_stok['laporan_stok']);
        $jumlah_data = ($jumlah_data > 0)? $jumlah_data + 5 : $jumlah_data + 6;

        $sheet->getStyle('A1:N1')->getAlignment()->setWrapText(true);

        $sheet->getStyle('A1:G'.$jumlah_data)->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ]);

        foreach(range('A',$sheet->getHighestColumn()) as $column) {
            $sheet->getColumnDimension($column)->setAutoSize(true);
        }

        $sheet->getStyle('A1:G1')->getFont()->setBold(true);
        $sheet->getStyle('A1:G1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('A3:G4')->getFont()->setBold(true);
        $sheet->getStyle('A3:G4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A3:G4')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A3:G4')->getAlignment()->setWrapText(false);

        $sheet->getStyle('A5:G5')->getFont()->setSize(9);
        $sheet->getStyle('A5:G5')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    }

    public function view(): View
    {
        return view('dashboard.tabel_laporan_stok_cetak_excel', [
            'laporan_stok' => $this->laporan_stok['laporan_stok'],
        ]);
    }
}