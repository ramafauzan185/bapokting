<?php

namespace App\Exports;

use App\Models\PerusahaanView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PerusahaanExport implements FromView, WithStyles
{

    public function __construct(array $perusahaan)
    {
        $this->perusahaan = $perusahaan;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    /*public function collection()
    {
        return PerusahaanView::all();
    }*/

    public function styles(Worksheet $sheet)
    {
        $jumlah_data = count($this->perusahaan['perusahaan'])+4;

        $sheet->getStyle('A1:N'.$jumlah_data)->applyFromArray([
            'alignment' => [
                'wrapText' => true,
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ]);

        $sheet->getStyle('A1:N1')->getFont()->setBold(true);
        $sheet->getStyle('A1:N1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('A3:N3')->getFont()->setBold(true);
        $sheet->getStyle('A3:N3')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A3:N3')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A3:N3')->getAlignment()->setWrapText(true);

        $sheet->getStyle('A4:N4')->getFont()->setSize(9);
        $sheet->getStyle('A4:N4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    }

    public function view(): View
    {
        return view('dashboard.tabel_perusahaan_cetak_excel', [
            'perusahaan' => $this->perusahaan['perusahaan'],
        ]);
    }
}
