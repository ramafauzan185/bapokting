<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 13/09/2021
 * Time: 19:44
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengadaan extends Model
{
    use SoftDeletes;

    protected $table = 'pengadaan';

    protected $fillable = [
        'kode_pengadaan',
        'id_distribusi_bapokting',
        'id_sumber_pembelian',
        'id_lokasi_pembelian',
        'volume',
        'id_satuan',
        'user_create',
        'user_update',
        'user_delete',
    ];
}