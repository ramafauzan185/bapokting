<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 15:00
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SumberPembelian extends Model
{
    use SoftDeletes;

    protected $table = 'sumber_pembelian';

    protected $fillable = [
        'kode_sumber_pembelian',
        'nama_sumber_pembelian',
        'user_create',
        'user_update',
        'user_delete',
    ];
}