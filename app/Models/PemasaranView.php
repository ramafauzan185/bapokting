<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 14/09/2021
 * Time: 15:18
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PemasaranView extends Model
{
    protected $table = 'view_pemasaran';

    public static function getByFilter($where = 'a.id IS NOT NULL')
    {
        $sql = "SELECT a.*,
                b.id_perusahaan, b.id_bidang_usaha, b.id_jenis_barang, b.tahun, b.bulan, b.nama_perusahaan, b.jenis_barang, b.nama_barang
                FROM view_pemasaran a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE $where";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiPerTahun($tahun)
    {
        $sql = "SELECT b.id_jenis_barang,
                b.nama_barang,
                SUM(a.volume) as jum,
                a.satuan
                FROM view_pemasaran a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE b.tahun='$tahun'
                GROUP BY b.id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiPerTahunDetail($tahun, $id_jenis_barang)
    {
        $sql = "SELECT a.*,
                b.tahun,
                b.bulan,
                b.skala_usaha,
                b.status_laporan,
                b.nib,
                b.nomor_legalitas_usaha,
                b.nama_perusahaan,
                b.alamat_perusahaan,
                b.kode_kbli,
                b.nama_kbli,
                b.jenis_barang,
                b.nama_barang
                FROM view_pemasaran a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE b.tahun='$tahun' AND b.id_jenis_barang=$id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiByKonsumen($id_jenis_barang)
    {
        $sql = "SELECT $id_jenis_barang as id_jenis_barang,
                a.id as id_konsumen,
                a.nama_konsumen as konsumen,
                (
                    SELECT COUNT(*) as jum
                    FROM pemasaran xa
                    INNER JOIN
                    distribusi_bapokting xb
                    ON xa.id_distribusi_bapokting=xb.id
                    WHERE xa.id_konsumen=a.id AND xa.deleted_at IS NULL AND xb.id_jenis_barang=$id_jenis_barang
                    GROUP BY xb.id_jenis_barang
                ) as jum
                FROM konsumen a";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiByKonsumenDetail($id_jenis_barang, $id_konsumen)
    {
        $sql = "SELECT a.*,
                b.id_perusahaan, b.id_bidang_usaha, b.id_jenis_barang, b.tahun, b.bulan, b.status_laporan, b.nomor_legalitas_usaha, b.nama_perusahaan
                FROM view_pemasaran a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE a.id_konsumen=$id_konsumen AND b.id_jenis_barang=$id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiByAsalKonsumen($id_jenis_barang)
    {
        $sql = "SELECT $id_jenis_barang as id_jenis_barang,
                a.id as id_asal_konsumen,
                a.nama_asal_konsumen as asal_konsumen,
                (
                    SELECT COUNT(*) as jum
                    FROM pemasaran xa
                    INNER JOIN
                    distribusi_bapokting xb
                    ON xa.id_distribusi_bapokting=xb.id
                    WHERE xa.id_asal_konsumen=a.id AND xa.deleted_at IS NULL AND xb.id_jenis_barang=$id_jenis_barang
                    GROUP BY xb.id_jenis_barang
                ) as jum
                FROM asal_konsumen a";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiByAsalKonsumenDetail($id_jenis_barang, $id_asal_konsumen)
    {
        $sql = "SELECT a.*,
                b.id_perusahaan, b.id_bidang_usaha, b.id_jenis_barang, b.tahun, b.bulan, b.skala_usaha, b.status_laporan, b.nomor_legalitas_usaha, b.nama_perusahaan
                FROM view_pemasaran a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE a.id_asal_konsumen=$id_asal_konsumen AND b.id_jenis_barang=$id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }
}