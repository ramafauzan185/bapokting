<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 13/09/2021
 * Time: 19:49
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pemasaran extends Model
{
    use SoftDeletes;

    protected $table = 'pemasaran';

    protected $fillable = [
        'kode_pemasaran',
        'id_distribusi_bapokting',
        'id_konsumen',
        'id_asal_konsumen',
        'volume',
        'id_satuan',
        'user_create',
        'user_update',
        'user_delete',
    ];
}