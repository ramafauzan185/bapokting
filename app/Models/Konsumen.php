<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 16:28
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Konsumen extends Model
{
    use SoftDeletes;

    protected $table = 'konsumen';

    protected $fillable = [
        'kode_konsumen',
        'nama_konsumen',
        'user_create',
        'user_update',
        'user_delete',
    ];
}