<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 13/09/2021
 * Time: 16:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DistribusiBapokting extends Model
{
    use SoftDeletes;

    protected $table = 'distribusi_bapokting';

    protected $fillable = [
        'kode_distribusi_bapokting',
        'id_perusahaan',
        'id_bidang_usaha',
        'id_jenis_barang',
        'tahun',
        'bulan',
        'skala_usaha',
        'status_laporan',
        'user_create',
        'user_update',
        'user_delete',
    ];
}