<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 16:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LokasiPembelian extends Model
{
    use SoftDeletes;

    protected $table = 'lokasi_pembelian';

    protected $fillable = [
        'kode_lokasi_pembelian',
        'nama_lokasi_pembelian',
        'user_create',
        'user_update',
        'user_delete',
    ];
}