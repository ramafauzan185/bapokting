<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 10/09/2021
 * Time: 4:17
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pemilik extends Model
{
    use SoftDeletes;

    protected $table = 'pemilik';

    protected $fillable = [
        'kode_pemilik',
        'nik',
        'nama_pemilik',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'alamat',
        'nomor_handphone',
        'tanggal_join',
        'user_create',
        'user_update',
        'user_delete',
    ];
}