<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 10/09/2021
 * Time: 10:51
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerusahaanBidangUsaha extends Model
{
    use SoftDeletes;

    protected $table = 'perusahaan_bidang_usaha';

    protected $fillable = [
        'kode_perusahaan_bidang_usaha',
        'id_perusahaan',
        'id_jenis_barang',
        'status',
        'user_create',
        'user_update',
        'user_delete',
    ];
}