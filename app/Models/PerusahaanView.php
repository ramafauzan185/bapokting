<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 10/09/2021
 * Time: 13:38
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PerusahaanView extends Model
{
    protected $table = 'view_perusahaan';

    public static function getByFilter($where)
    {
        $sql = "SELECT a.*,
                GROUP_CONCAT(' ', b.nama_barang) as nama_barangs
                FROM view_perusahaan a
                LEFT JOIN
                view_perusahaan_bidang_usaha b
                ON a.id=b.id_perusahaan
                WHERE $where
                GROUP BY a.id";

        $query = DB::select($sql);

        return $query;
    }
}