<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 10/09/2021
 * Time: 9:36
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perusahaan extends Model
{
    use SoftDeletes;

    protected $table = 'perusahaan';

    protected $fillable = [
        'kode_perusahaan',
        'id_pemilik',
        'id_user',
        'nib',
        'nomor_legalitas_usaha',
        'nama_perusahaan',
        'alamat_perusahaan',
        'id_kelurahan',
        'nomor_telepon',
        'lokasi_usaha',
        'status_usaha',
        'status_toko',
        'skala_usaha',
        'luas_gudang',
        'koordinat',
        'user_create',
        'user_update',
        'user_delete',
    ];
}