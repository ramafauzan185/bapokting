<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 13/09/2021
 * Time: 20:07
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistribusiBapoktingView extends Model
{
    protected $table = 'view_distribusi_bapokting';
}