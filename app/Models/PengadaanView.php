<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 14/09/2021
 * Time: 11:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PengadaanView extends Model
{
    protected $table = 'view_pengadaan';

    public static function getByFilter($where = 'a.id IS NOT NULL')
    {
        $sql = "SELECT a.*,
                b.id_perusahaan, b.id_bidang_usaha, b.id_jenis_barang, b.tahun, b.bulan, b.nama_perusahaan, b.jenis_barang, b.nama_barang
                FROM view_pengadaan a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE $where";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiPerTahun($tahun)
    {
        $sql = "SELECT b.id_jenis_barang,
                b.nama_barang,
                SUM(a.volume) as jum,
                a.satuan
                FROM view_pengadaan a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE b.tahun='$tahun'
                GROUP BY b.id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiPerTahunDetail($tahun, $id_jenis_barang)
    {
        $sql = "SELECT a.*,
                b.tahun,
                b.bulan,
                b.skala_usaha,
                b.status_laporan,
                b.nib,
                b.nomor_legalitas_usaha,
                b.nama_perusahaan,
                b.alamat_perusahaan,
                b.kode_kbli,
                b.nama_kbli,
                b.jenis_barang,
                b.nama_barang
                FROM view_pengadaan a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE b.tahun='$tahun' AND b.id_jenis_barang=$id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiBySumberPembelian($id_jenis_barang)
    {
        $sql = "SELECT $id_jenis_barang as id_jenis_barang,
                a.id as id_sumber_pembelian,
                a.nama_sumber_pembelian as sumber_pembelian,
                (
                    SELECT COUNT(*) as jum
                    FROM pengadaan xa
                    INNER JOIN
                    distribusi_bapokting xb
                    ON xa.id_distribusi_bapokting=xb.id
                    WHERE xa.id_sumber_pembelian=a.id AND xa.deleted_at IS NULL AND xb.id_jenis_barang=$id_jenis_barang
                    GROUP BY xb.id_jenis_barang
                ) as jum
                FROM sumber_pembelian a";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiBySumberPembelianDetail($id_jenis_barang, $id_sumber_pembelian)
    {
        $sql = "SELECT a.*,
                b.id_perusahaan, b.id_bidang_usaha, b.id_jenis_barang, b.tahun, b.bulan, b.skala_usaha, b.status_laporan, b.nomor_legalitas_usaha, b.nama_perusahaan
                FROM view_pengadaan a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE a.id_sumber_pembelian=$id_sumber_pembelian AND b.id_jenis_barang=$id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiByLokasiPembelian($id_jenis_barang)
    {
        $sql = "SELECT $id_jenis_barang as id_jenis_barang,
                a.id as id_lokasi_pembelian,
                a.nama_lokasi_pembelian as lokasi_pembelian,
                (
                    SELECT COUNT(*) as jum
                    FROM pengadaan xa
                    INNER JOIN
                    distribusi_bapokting xb
                    ON xa.id_distribusi_bapokting=xb.id
                    WHERE xa.id_lokasi_pembelian=a.id AND xa.deleted_at IS NULL AND xb.id_jenis_barang=$id_jenis_barang
                    GROUP BY xb.id_jenis_barang
                ) as jum
                FROM lokasi_pembelian a";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetPerKomoditiByLokasiPembelianDetail($id_jenis_barang, $id_lokasi_pembelian)
    {
        $sql = "SELECT a.*,
                b.id_perusahaan, b.id_bidang_usaha, b.id_jenis_barang, b.tahun, b.bulan, b.skala_usaha, b.status_laporan, b.nomor_legalitas_usaha, b.nama_perusahaan
                FROM view_pengadaan a
                INNER JOIN
                view_distribusi_bapokting b
                ON a.id_distribusi_bapokting=b.id
                WHERE a.id_lokasi_pembelian=$id_lokasi_pembelian AND b.id_jenis_barang=$id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }
}