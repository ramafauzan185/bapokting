<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 10/09/2021
 * Time: 13:45
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PerusahaanBidangUsahaView extends Model
{
    protected $table = 'view_perusahaan_bidang_usaha';

    public static function GetJenisKelaminPedagangPerKomoditi($byIdKelurahan = "")
    {
        $sql = "SELECT z.*,
                ROUND(((z.laki_laki/(z.laki_laki+z.perempuan))*100)) as laki_laki_percent,
                ROUND(((z.perempuan/(z.laki_laki+z.perempuan))*100)) as perempuan_percent
                FROM
                (
                    SELECT a.id as id_jenis_barang,
                    a.nama_barang as category,
                    IFNULL
                    (
                        (
                            SELECT count(*) as laki_laki
                            FROM view_perusahaan_bidang_usaha xa
                            INNER JOIN view_perusahaan xb
                            ON xa.id_perusahaan=xb.id
                            WHERE xa.id_jenis_barang=a.id
                            AND xb.jenis_kelamin='l'
                            $byIdKelurahan
                            GROUP BY xa.id_jenis_barang
                        ), 0
                    ) as laki_laki,
                    IFNULL
                    (
                        (
                            SELECT count(*) as perempuan
                            FROM view_perusahaan_bidang_usaha xa
                            INNER JOIN view_perusahaan xb
                            ON xa.id_perusahaan=xb.id
                            WHERE xa.id_jenis_barang=a.id
                            AND xb.jenis_kelamin='p'
                            $byIdKelurahan
                            GROUP BY xa.id_jenis_barang
                        ), 0
                    ) perempuan
                    FROM jenis_barang a
                ) z";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetJenisKelaminPedagangPerKomoditiDetail($id_jenis_barang, $jenis_kelamin, $byIdKelurahan = "")
    {
        $sql = "SELECT a.id, a.kode_perusahaan_bidang_usaha, a.id_jenis_barang, a.status, a.kode_jenis_barang, a.id_bidang_usaha, a.nama_barang, a.kode_bidang_usaha, a.kode_kbli, a.nama_kbli, a.jenis_barang,
                b.*
                FROM view_perusahaan_bidang_usaha a
                INNER JOIN view_perusahaan b
                ON a.id_perusahaan=b.id
                WHERE a.id_jenis_barang=$id_jenis_barang
                AND b.jenis_kelamin='$jenis_kelamin'
                $byIdKelurahan ";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetRerataLamaUsahaPerKomoditi()
    {
        $sql = "SELECT a.id as id_jenis_barang,
                a.nama_barang,
                (
                    SELECT (SUM(xb.lama_usaha)/COUNT(*)) as lama_usaha
                    FROM view_perusahaan_bidang_usaha xa
                    INNER JOIN
                    view_perusahaan xb
                    ON xa.id_perusahaan=xb.id
                    WHERE xa.id_jenis_barang=a.id
                    GROUP BY xa.id_jenis_barang
                ) as lama_usaha
                FROM jenis_barang a";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetRerataLamaUsahaPerKomoditiDetail($id_jenis_barang)
    {
        $sql = "SELECT a.id, a.kode_perusahaan_bidang_usaha, a.id_jenis_barang, a.status, a.kode_jenis_barang, a.id_bidang_usaha, a.nama_barang, a.kode_bidang_usaha, a.kode_kbli, a.nama_kbli, a.jenis_barang,
                b.*
                FROM view_perusahaan_bidang_usaha a
                INNER JOIN
                view_perusahaan b
                ON a.id_perusahaan=b.id
                WHERE a.id_jenis_barang=$id_jenis_barang";

        $query = DB::select($sql);

        return $query;
    }

    public static function SqlStatusKepemilikanToko($id_jenis_barang, $status_toko)
    {
        $statusToko = ucwords(str_replace('_', ' ', $status_toko));

        $sql = "SELECT $id_jenis_barang as id_jenis_barang,
                '$statusToko' as status_toko,
                (
                    SELECT COUNT(*) as jum
                    FROM view_perusahaan_bidang_usaha a
                    INNER JOIN
                    perusahaan b
                    ON a.id_perusahaan=b.id
                    WHERE a.id_jenis_barang=$id_jenis_barang AND b.status_toko='$status_toko'
                ) as jum";

        return $sql;
    }

    public static function GetStatusKepemilikanTokoPerKomoditi($id_jenis_barang)
    {
        $sql = "
                ".PerusahaanBidangUsahaView::SqlStatusKepemilikanToko($id_jenis_barang, 'milik_sendiri')."
                UNION ALL
                ".PerusahaanBidangUsahaView::SqlStatusKepemilikanToko($id_jenis_barang, 'sewa')."
                UNION ALL
                ".PerusahaanBidangUsahaView::SqlStatusKepemilikanToko($id_jenis_barang, 'lapak_pasar')." ";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetStatusKepemilikanTokoPerKomoditiDetail($id_jenis_barang, $status_toko)
    {
        $sql = "SELECT a.id, a.kode_perusahaan_bidang_usaha, a.id_jenis_barang, a.status, a.kode_jenis_barang, a.id_bidang_usaha, a.nama_barang, a.kode_bidang_usaha, a.kode_kbli, a.nama_kbli, a.jenis_barang,
                b.*
                FROM view_perusahaan_bidang_usaha a
                INNER JOIN
                view_perusahaan b
                ON a.id_perusahaan=b.id
                WHERE a.id_jenis_barang=$id_jenis_barang AND b.status_toko='$status_toko'";

        $query = DB::select($sql);

        return $query;
    }

    public static function SqlSkalaUsaha($id_jenis_barang, $skala_usaha)
    {
        $skalaUsaha = ucwords(str_replace('_', ' ', $skala_usaha));

        $sql = "SELECT $id_jenis_barang as id_jenis_barang,
                '$skalaUsaha' as skala_usaha,
                (
                    SELECT COUNT(*) as jum
                    FROM view_perusahaan_bidang_usaha a
                    INNER JOIN
                    perusahaan b
                    ON a.id_perusahaan=b.id
                    WHERE a.id_jenis_barang=$id_jenis_barang AND b.skala_usaha='$skala_usaha'
                ) as jum";

        return $sql;
    }

    public static function GetSkalaUsahaPerKomoditi($id_jenis_barang)
    {
        $sql = "
                ".PerusahaanBidangUsahaView::SqlSkalaUsaha($id_jenis_barang, 'distributor')."
                UNION ALL
                ".PerusahaanBidangUsahaView::SqlSkalaUsaha($id_jenis_barang, 'sub_distributor')."
                UNION ALL
                ".PerusahaanBidangUsahaView::SqlSkalaUsaha($id_jenis_barang, 'agen')." ";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetSkalaUsahaPerKomoditiDetail($id_jenis_barang, $skala_usaha)
    {
        $sql = "SELECT a.id, a.kode_perusahaan_bidang_usaha, a.id_jenis_barang, a.status, a.kode_jenis_barang, a.id_bidang_usaha, a.nama_barang, a.kode_bidang_usaha, a.kode_kbli, a.nama_kbli, a.jenis_barang,
                b.*
                FROM view_perusahaan_bidang_usaha a
                INNER JOIN
                view_perusahaan b
                ON a.id_perusahaan=b.id
                WHERE a.id_jenis_barang=$id_jenis_barang AND b.skala_usaha='$skala_usaha'";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetLaporanStok($id_perusahaan)
    {
        $sql = "SELECT
                a.id, a.nama_barang,
                b.id as id_distribusi_bapokting, b.tahun, b.bulan,
                c.jumlah_pengadaan,
                d.jumlah_pemasaran
                FROM view_perusahaan_bidang_usaha a
                INNER JOIN
                (
                    SELECT *
                    FROM view_distribusi_bapokting
                    WHERE status_laporan='send'
                    GROUP BY id_perusahaan, id_bidang_usaha, id_jenis_barang, tahun, bulan
                ) b
                ON a.id_perusahaan=b.id_perusahaan AND a.id_jenis_barang=b.id_jenis_barang
                LEFT JOIN
                (
                    SELECT id_distribusi_bapokting, IFNULL(SUM(volume), 0) as jumlah_pengadaan
                    FROM pengadaan
                    GROUP BY id_distribusi_bapokting
                ) c
                ON b.id=c.id_distribusi_bapokting
                LEFT JOIN
                (
                    SELECT id_distribusi_bapokting, IFNULL(SUM(volume), 0) as jumlah_pemasaran
                    FROM pemasaran
                    GROUP BY id_distribusi_bapokting
                ) d
                ON b.id=d.id_distribusi_bapokting
                WHERE a.id_perusahaan=$id_perusahaan
                ORDER BY a.id ASC, b.tahun ASC, b.bulan ASC";

        $query = DB::select($sql);

        return $query;
    }

    public static function GetLaporanStokAllPerusahaan($id_jenis_barang)
    {
        $sql = "SELECT
                a.id, a.id_jenis_barang, a.nama_barang, a.id_perusahaan,
                b.tahun, b.bulan,
                IFNULL(SUM(c.jumlah_pengadaan), 0) as jumlah_pengadaan,
                IFNULL(SUM(d.jumlah_pemasaran), 0) as jumlah_pemasaran
                FROM view_perusahaan_bidang_usaha a
                INNER JOIN
                (
                    SELECT *
                    FROM view_distribusi_bapokting
                    WHERE status_laporan='send'
                    GROUP BY id_perusahaan, id_bidang_usaha, id_jenis_barang, tahun, bulan
                ) b
                ON a.id_perusahaan=b.id_perusahaan AND a.id_jenis_barang=b.id_jenis_barang
                LEFT JOIN
                (
                    SELECT id_distribusi_bapokting, IFNULL(SUM(volume), 0) as jumlah_pengadaan
                    FROM pengadaan
                    GROUP BY id_distribusi_bapokting
                ) c
                ON b.id=c.id_distribusi_bapokting
                LEFT JOIN
                (
                    SELECT id_distribusi_bapokting, IFNULL(SUM(volume), 0) as jumlah_pemasaran
                    FROM pemasaran
                    GROUP BY id_distribusi_bapokting
                ) d
                ON b.id=d.id_distribusi_bapokting
                WHERE a.id_jenis_barang=$id_jenis_barang
                GROUP BY a.id_jenis_barang, b.tahun, b.bulan
                ORDER BY a.id ASC, b.tahun ASC, b.bulan ASC";

        $query = DB::select($sql);

        return $query;
    }
}