<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 14:31
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Satuan extends Model
{
    use SoftDeletes;

    protected $table = 'satuan';

    protected $fillable = [
        'kode_satuan',
        'satuan',
        'nama_satuan',
        'user_create',
        'user_update',
        'user_delete',
    ];
}