<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 10:38
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BidangUsaha extends Model
{
    use SoftDeletes;

    protected $table = 'bidang_usaha';

    protected $fillable = [
        'kode_bidang_usaha',
        'kode_kbli',
        'nama_kbli',
        'jenis_barang',
        'user_create',
        'user_update',
        'user_delete',
    ];
}