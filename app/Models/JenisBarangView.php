<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 14:15
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisBarangView extends Model
{
    protected $table = 'view_jenis_barang';
}