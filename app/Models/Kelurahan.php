<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 13/09/2021
 * Time: 9:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'kelurahan';
}