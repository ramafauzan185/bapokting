<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 09/10/2021
 * Time: 15:06
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

    protected $table = 'slider';

    protected $fillable = [
        'kode_slider',
        'title',
        'caption',
        'image',
        'image_url',
        'link',
        'user_create',
        'user_update',
        'user_delete',
    ];
}