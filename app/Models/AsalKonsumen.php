<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 16:51
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AsalKonsumen extends Model
{
    use SoftDeletes;

    protected $table = 'asal_konsumen';

    protected $fillable = [
        'kode_asal_konsumen',
        'nama_asal_konsumen',
        'user_create',
        'user_update',
        'user_delete',
    ];
}