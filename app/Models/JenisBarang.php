<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 11:53
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JenisBarang extends Model
{
    use SoftDeletes;

    protected $table = 'jenis_barang';

    protected $fillable = [
        'kode_jenis_barang',
        'id_bidang_usaha',
        'nama_barang',
        'user_create',
        'user_update',
        'user_delete',
    ];
}
