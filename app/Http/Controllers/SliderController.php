<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 08/10/2021
 * Time: 14:04
 */

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SliderController extends Controller
{
    var $auth_message;

    function __construct()
    {
        $this->middleware('auth');

        $this->auth_message = 'Maaf, Anda tidak memiliki akses ke data image slider!';
    }

    public function index()
    {
        if(!is_superadmin()) {
            Auth::logout();
            return redirect()->back()->with('error_message', $this->auth_message);
        }

        $slider = Slider::all();

        return view('slider.index', [
            'slider' => $slider,
        ]);
    }

    public function create()
    {
        if(!is_superadmin()) return redirect()->back()->with('error_message', $this->auth_message);

        return view('slider.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'upload_image' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $array = $request->only([
            'title', 'caption', 'image', 'image_url'
        ]);
        $kode_slider = kode_hash();

        if($request->post('upload_image') == 'image') {
            if($image = $request->file('image')) {
                $destinationPath = 'images/slider/';
                $profileImage = date("YmdHis") . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);

                $array['image'] = $profileImage;
            }
            $array['image_url'] = NULL;
        } else {
            $array['image'] = NULL;
        }

        $array['kode_slider'] = $kode_slider;
        $array['user_create'] = $request->user()->id;

        $slider = Slider::create($array);

        return redirect()->route('slider.index')
            ->with('success_message', 'Berhasil menambah image slider baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $slider = Slider::find($id);

        if (!$slider) return redirect()->route('slider.index')
            ->with('error_message', 'Slider dengan ID: '.$id.' tidak ditemukan.');

        return view('slider.edit', [
            'slider' => $slider
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'upload_image' => 'required',
        ]);

        $slider = Slider::find($id);

        $slider->title = $request->title;
        $slider->caption = $request->caption;

        $unlink = function($image) {
            if(!empty($image)) unlink('images/slider/'.$image);
        };

        if($request->upload_image == 'image') {
            if($image = $request->file('image')) {
                $destinationPath = 'images/slider/';
                $profileImage = date("YmdHis") . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);

                $unlink($slider->image);

                $slider->image = $profileImage;
                $slider->image_url = NULL;
            }
        } else {
            if(!empty($request->image_url)) {
                $unlink($slider->image);
                $slider->image = NULL;
            }

            $slider->image_url = $request->image_url;
        }

        $slider->save();

        return redirect()->route('slider.index')
            ->with('success_message', 'Berhasil mengubah image slider.');
    }

    public function destroy(Request $request, $id)
    {
        $slider = Slider::find($id);

        if ($slider) {
            $slider->user_delete = $request->user()->id;

            $slider->save();

            $slider->delete();
        }

        return redirect()->route('slider.index')
            ->with('success_message', 'Berhasil menghapus image slider.');
    }
}