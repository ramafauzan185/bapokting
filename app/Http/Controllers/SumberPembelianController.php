<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:53
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SumberPembelian;

class SumberPembelianController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sumberPembelian = SumberPembelian::all();

        return view('sumber_pembelian.index', [
            'sumber_pembelian' => $sumberPembelian
        ]);
    }

    public function create()
    {
        return view('sumber_pembelian.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_sumber_pembelian' => 'required|unique:sumber_pembelian,nama_sumber_pembelian',
        ]);

        $array = $request->only([
            'nama_sumber_pembelian'
        ]);
        $kode_sumber_pembelian = kode_hash();

        $array['kode_sumber_pembelian'] = $kode_sumber_pembelian;
        $array['user_create'] = $request->user()->id;

        $sumber_pembelian = SumberPembelian::create($array);

        return redirect()->route('sumber-pembelian.index')
            ->with('success_message', 'Berhasil menambah sumber pembelian baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $sumber_pembelian = SumberPembelian::find($id);

        if (!$sumber_pembelian) return redirect()->route('sumber-pembelian.index')
            ->with('error_message', 'Sumber pembelian dengan ID: '.$id.' tidak ditemukan.');

        return view('sumber_pembelian.edit', [
            'sumber_pembelian' => $sumber_pembelian
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_sumber_pembelian' => 'required|unique:sumber_pembelian,nama_sumber_pembelian,'.$id
        ]);

        $sumber_pembelian = SumberPembelian::find($id);

        $sumber_pembelian->nama_sumber_pembelian = $request->nama_sumber_pembelian;
        $sumber_pembelian->user_update = $request->user()->id;

        $sumber_pembelian->save();

        return redirect()->route('sumber-pembelian.index')
            ->with('success_message', 'Berhasil mengubah sumber pembelian.');
    }

    public function destroy(Request $request, $id)
    {
        $sumber_pembelian = SumberPembelian::find($id);

        if ($sumber_pembelian) {
            $sumber_pembelian->user_delete = $request->user()->id;

            $sumber_pembelian->save();

            $sumber_pembelian->delete();
        }

        return redirect()->route('sumber-pembelian.index')
            ->with('success_message', 'Berhasil menghapus sumber pembelian.');
    }
}