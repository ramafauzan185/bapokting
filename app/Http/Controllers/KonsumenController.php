<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 5:04
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Konsumen;

class KonsumenController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $konsumen = Konsumen::all();
        return view('konsumen.index', [
            'konsumen' => $konsumen
        ]);
    }

    public function create()
    {
        return view('konsumen.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_konsumen' => 'required|unique:konsumen,nama_konsumen',
        ]);

        $array = $request->only([
            'nama_konsumen'
        ]);
        $kode_konsumen = kode_hash();

        $array['kode_konsumen'] = $kode_konsumen;
        $array['user_create'] = $request->user()->id;

        $konsumen = Konsumen::create($array);

        return redirect()->route('konsumen.index')
            ->with('success_message', 'Berhasil menambah konsumen baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $konsumen = Konsumen::find($id);

        if (!$konsumen) return redirect()->route('konsumen.index')
            ->with('error_message', 'Konsumen dengan ID: '.$id.' tidak ditemukan.');

        return view('konsumen.edit', [
            'konsumen' => $konsumen
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_konsumen' => 'required|unique:konsumen,nama_konsumen,'.$id,
        ]);

        $konsumen = Konsumen::find($id);

        $konsumen->nama_konsumen = $request->nama_konsumen;
        $konsumen->user_update = $request->user()->id;

        $konsumen->save();

        return redirect()->route('konsumen.index')
            ->with('success_message', 'Berhasil mengubah konsumen.');
    }

    public function destroy(Request $request, $id)
    {
        $konsumen = Konsumen::find($id);

        if ($konsumen) {
            $konsumen->user_delete = $request->user()->id;

            $konsumen->save();

            $konsumen->delete();
        }

        return redirect()->route('konsumen.index')
            ->with('success_message', 'Berhasil menghapus konsumen.');
    }
}