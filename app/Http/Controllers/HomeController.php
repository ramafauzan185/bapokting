<?php

namespace App\Http\Controllers;

use App\Exports\PerusahaanExport;
use App\Exports\StokBapoktingExport;
use App\Models\JenisBarang;
use App\Models\JenisBarangView;
use App\Models\Kecamatan;
use App\Models\PemasaranView;
use App\Models\PengadaanView;
use App\Models\Perusahaan;
use App\Models\PerusahaanBidangUsahaView;
use App\Models\PerusahaanView;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    protected $user;
    protected $perusahaan;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->user = array();
        $this->perusahaan = array();
    }

    private function init()
    {
        $this->user = Auth::user();
        $this->perusahaan = Perusahaan::where('id_user', $this->user->id)->first();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->init();

        $is_superadmin = is_superadmin();
        $is_distributor = is_distributor();
        $is_manajemen = is_manajemen();
        $data['is_superadmin'] = $is_superadmin;
        $data['is_distributor'] = $is_distributor;
        $data['is_manajemen'] = $is_manajemen;

        $tahunNow = date("Y");
        $data['tahun'] = $tahunNow;

        if($is_superadmin || $is_manajemen) {
            $pengadaanGetPerKomoditiPerTahun = PengadaanView::GetPerKomoditiPerTahun($tahunNow);
            $pemasaranGetPerKomoditiPerTahun = PemasaranView::GetPerKomoditiPerTahun($tahunNow);
            $jenisKelaminPedagangPerKomoditi = PerusahaanBidangUsahaView::GetJenisKelaminPedagangPerKomoditi();
            $rerataLamaUsahaPerKomoditi = PerusahaanBidangUsahaView::GetRerataLamaUsahaPerKomoditi();
            $jenis_barang = JenisBarang::all();
            $kecamatan = Kecamatan::all();

            $data['jenis_barang'] = $jenis_barang;
            $data['pengadaan_get_per_komoditi_per_tahun'] = $pengadaanGetPerKomoditiPerTahun;
            $data['pemasaran_get_per_komoditi_per_tahun'] = $pemasaranGetPerKomoditiPerTahun;
            $data['jenis_kelamin_pedagang_per_komoditi'] = $jenisKelaminPedagangPerKomoditi;
            $data['rerata_lama_usaha_per_komoditi'] = $rerataLamaUsahaPerKomoditi;
            $data['kecamatan'] = $kecamatan;
        }

        if($is_distributor) {
            $id_perusahaan = $this->perusahaan->id;
            $getLaporanStok = PerusahaanBidangUsahaView::GetLaporanStok($id_perusahaan);

            $data['laporan_stok'] = $getLaporanStok;
        } else {
            $data['laporan_stok'] = [];
        }

        $slider = Slider::all();
        $data['slider'] = $slider;

        return view('home', $data);
    }

    public function showGrafikStatusKepemilikanTokoPerKomoditi(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;

        $statusKepemilikanTokoPerKomoditi = PerusahaanBidangUsahaView::GetStatusKepemilikanTokoPerKomoditi($id_jenis_barang);

        return response()->json([
            'status_kepemilikan_toko_per_komoditi' => $statusKepemilikanTokoPerKomoditi,
        ]);
    }

    public function showGrafikStatusKepemilikanTokoPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;
        $status_toko = $request->status_toko;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $statusKepemilikanTokoPerKomoditi = PerusahaanBidangUsahaView::GetStatusKepemilikanTokoPerKomoditiDetail($id_jenis_barang, $status_toko);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'status_kepemilikan_toko_per_komoditi' => $statusKepemilikanTokoPerKomoditi,
        ]);
    }

    public function showGrafikSkalaUsahaPerKomoditi(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;

        $skalaUsahaPerKomoditi = PerusahaanBidangUsahaView::GetSkalaUsahaPerKomoditi($id_jenis_barang);

        return response()->json([
            'skala_usaha_per_komoditi' => $skalaUsahaPerKomoditi,
        ]);
    }

    public function showGrafikSkalaUsahaPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;
        $skala_usaha = $request->skala_usaha;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $skalaUsahaPerKomoditi = PerusahaanBidangUsahaView::GetSkalaUsahaPerKomoditiDetail($id_jenis_barang, $skala_usaha);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'skala_usaha_per_komoditi' => $skalaUsahaPerKomoditi,
        ]);
    }

    public function showGrafikPengadaanBySumberPembelianPerKomoditi(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;

        $pengadaanGetPerKomoditiBySumberPembelian = PengadaanView::GetPerKomoditiBySumberPembelian($id_jenis_barang);

        return response()->json([
            'pengadaan_get_per_komoditi_by_sumber_pembelian' => $pengadaanGetPerKomoditiBySumberPembelian,
        ]);
    }

    public function showGrafikPengadaanBySumberPembelianPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;
        $id_sumber_pembelian = $request->id_sumber_pembelian;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $pengadaanPerKomoditiBySumberPembelian = PengadaanView::GetPerKomoditiBySumberPembelianDetail($id_jenis_barang, $id_sumber_pembelian);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'pengadaan_per_komoditi_by_sumber_pembelian' => $pengadaanPerKomoditiBySumberPembelian,
        ]);
    }

    public function showGrafikPengadaanByLokasiPembelianPerKomoditi(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;

        $pengadaanGetPerKomoditiByLokasiPembelian = PengadaanView::GetPerKomoditiByLokasiPembelian($id_jenis_barang);

        return response()->json([
            'pengadaan_get_per_komoditi_by_lokasi_pembelian' => $pengadaanGetPerKomoditiByLokasiPembelian,
        ]);
    }

    public function showGrafikPengadaanByLokasiPembelianPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;
        $id_lokasi_pembelian = $request->id_lokasi_pembelian;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $pengadaanPerKomoditiByLokasiPembelian = PengadaanView::GetPerKomoditiByLokasiPembelianDetail($id_jenis_barang, $id_lokasi_pembelian);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'pengadaan_per_komoditi_by_lokasi_pembelian' => $pengadaanPerKomoditiByLokasiPembelian,
        ]);
    }

    public function showGrafikPemasaranByKonsumenPerKomoditi(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;

        $pemasaranGetPerKomoditiByKonsumen = PemasaranView::GetPerKomoditiByKonsumen($id_jenis_barang);

        return response()->json([
            'pemasaran_get_per_komoditi_by_konsumen' => $pemasaranGetPerKomoditiByKonsumen,
        ]);
    }

    public function showGrafikPemasaranByKonsumenPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;
        $id_konsumen = $request->id_konsumen;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $pemasaranGetPerKomoditiByKonsumen = PemasaranView::GetPerKomoditiByKonsumenDetail($id_jenis_barang, $id_konsumen);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'pemasaran_per_komoditi_by_konsumen' => $pemasaranGetPerKomoditiByKonsumen,
        ]);
    }

    public function showGrafikPemasaranByAsalKonsumenPerKomoditi(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;

        $pemasaranGetPerKomoditiByAsalKonsumen = PemasaranView::GetPerKomoditiByAsalKonsumen($id_jenis_barang);

        return response()->json([
            'pemasaran_get_per_komoditi_by_asal_konsumen' => $pemasaranGetPerKomoditiByAsalKonsumen,
        ]);
    }

    public function showGrafikPemasaranByAsalKonsumenPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;
        $id_asal_konsumen = $request->id_asal_konsumen;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $pemasaranGetPerKomoditiByAsalKonsumen = PemasaranView::GetPerKomoditiByAsalKonsumenDetail($id_jenis_barang, $id_asal_konsumen);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'pemasaran_per_komoditi_by_asal_konsumen' => $pemasaranGetPerKomoditiByAsalKonsumen,
        ]);
    }

    public function showGrafikPengadaanPerKomoditiDetail(Request $request)
    {
        $tahun = $request->tahun;
        $id_jenis_barang = $request->id_jenis_barang;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $pengadaanGetPerKomoditiPerTahun = PengadaanView::GetPerKomoditiPerTahunDetail($tahun, $id_jenis_barang);

        return response()->json([
            'tahun' => $tahun,
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'pengadaan_get_per_komoditi_per_tahun' => $pengadaanGetPerKomoditiPerTahun,
        ]);
    }

    public function showGrafikPemasaranPerKomoditiDetail(Request $request)
    {
        $tahun = $request->tahun;
        $id_jenis_barang = $request->id_jenis_barang;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $pemasaranGetPerKomoditiPerTahun = PemasaranView::GetPerKomoditiPerTahunDetail($tahun, $id_jenis_barang);

        return response()->json([
            'tahun' => $tahun,
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'pemasaran_get_per_komoditi_per_tahun' => $pemasaranGetPerKomoditiPerTahun,
        ]);
    }

    public function showGrafikJenisKelaminPedagangPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;
        $jenis_kelamin = $request->jenis_kelamin;
        $jenis_kelamin = (strtolower($jenis_kelamin) == 'laki_laki')?  'l' : 'p';
        $id_kelurahan = $request->id_kelurahan;
        (!empty($id_kelurahan))? $byIdKelurahan = "AND b.id_kelurahan=$id_kelurahan " : $byIdKelurahan = "";

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $jenisKelaminPedagangPerKomoditi = PerusahaanBidangUsahaView::GetJenisKelaminPedagangPerKomoditiDetail($id_jenis_barang, $jenis_kelamin, $byIdKelurahan);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'jenis_kelamin_pedagang_per_komoditi' => $jenisKelaminPedagangPerKomoditi,
        ]);
    }

    public function showGrafikRerataLamaUsahaPerKomoditiDetail(Request $request)
    {
        $id_jenis_barang = $request->id_jenis_barang;

        $jenis_barang = JenisBarangView::find($id_jenis_barang);

        $rerataLamaUsahaPerKomoditi = PerusahaanBidangUsahaView::GetRerataLamaUsahaPerKomoditiDetail($id_jenis_barang);

        return response()->json([
            'kode_kbli' => $jenis_barang->kode_kbli,
            'nama_kbli' => $jenis_barang->nama_kbli,
            'nama_barang' => $jenis_barang->nama_barang,
            'jenis_barang' => jenis_barangs()[$jenis_barang->jenis_barang],
            'rerata_lama_usaha_per_komoditi' => $rerataLamaUsahaPerKomoditi,
        ]);
    }

    public function getPerusahaanByFilter(Request $request)
    {
        $map = isset($request->map) && $request->map == 'true'? true : false;
        $jenis_barang = $request->jenis_barang;
        $id_jenis_barang = $request->id_jenis_barang;
        $id_kecamatan = $request->id_kecamatan;
        $id_kelurahan = $request->id_kelurahan;
        $is_print = $request->cetak;
        $xls = $request->xls;

        $where = "a.id IS NOT NULL ";

        if($map) $where .= "AND a.koordinat IS NOT NULL ";

        if(!empty($jenis_barang)) $where .= "AND b.jenis_barang='$jenis_barang' ";

        if(!empty($id_jenis_barang)) $where .= "AND b.id_jenis_barang IN($id_jenis_barang) ";

        if(!empty($id_kecamatan)) $where .= "AND a.id_kecamatan=$id_kecamatan ";

        if(!empty($id_kelurahan)) $where .= "AND a.id_kelurahan=$id_kelurahan ";

        $perusahaan = PerusahaanView::getByFilter($where);

        $data = [
            'perusahaan' => $perusahaan,
        ];

        if(isset($is_print) && $is_print == 'true') {
            if(isset($xls) && $xls == 'true') {
                return Excel::download(new PerusahaanExport($data), 'perusahaan_export.xlsx');
            }

            return view('dashboard/tabel_perusahaan_cetak', $data);
        }

        return response()->json($data);
    }

    public function getStokByJenisBarang(Request $request)
    {
        $id_jenis_barang = empty($request->id_jenis_barang)? 0 : $request->id_jenis_barang;
        $is_print = $request->cetak;
        $xls = $request->xls;

        $getLaporanStokAllPerusahaan = PerusahaanBidangUsahaView::GetLaporanStokAllPerusahaan($id_jenis_barang);

        $data = [
            'laporan_stok' => $getLaporanStokAllPerusahaan,
            'bulans' => bulans(),
        ];

        if(isset($is_print) && $is_print == 'true') {
            if(isset($xls) && $xls == 'true') {
                return Excel::download(new StokBapoktingExport($data), 'stok_barang_export.xlsx');
            }

            return view('dashboard/tabel_laporan_stok_cetak', $data);
        }

        return response()->json($data);
    }

    public function showGrafikJenisKelaminPedagangByJenisProduk(Request $request)
    {
        $id_kelurahan = $request->id_kelurahan;
        (!empty($id_kelurahan))? $byIdKelurahan = "AND xb.id_kelurahan=".$id_kelurahan." " : $byIdKelurahan = "";

        $jenisKelaminPedagangPerKomoditi = PerusahaanBidangUsahaView::GetJenisKelaminPedagangPerKomoditi($byIdKelurahan);

        return response()->json([
            'jenis_kelamin_pedagang_per_komoditi' => $jenisKelaminPedagangPerKomoditi,
        ]);
    }
}
