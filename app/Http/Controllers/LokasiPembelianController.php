<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:59
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LokasiPembelian;

class LokasiPembelianController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $lokasiPembelian = LokasiPembelian::all();

        return view('lokasi_pembelian.index', [
            'lokasi_pembelian' => $lokasiPembelian
        ]);
    }

    public function create()
    {
        return view('lokasi_pembelian.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_lokasi_pembelian' => 'required|unique:lokasi_pembelian,nama_lokasi_pembelian',
        ]);

        $array = $request->only([
            'nama_lokasi_pembelian'
        ]);
        $kode_lokasi_pembelian = kode_hash();

        $array['kode_lokasi_pembelian'] = $kode_lokasi_pembelian;
        $array['user_create'] = $request->user()->id;

        $lokasi_pembelian = LokasiPembelian::create($array);

        return redirect()->route('lokasi-pembelian.index')
            ->with('success_message', 'Berhasil menambah lokasi pembelian baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $lokasi_pembelian = LokasiPembelian::find($id);

        if (!$lokasi_pembelian) return redirect()->route('lokasi-pembelian.index')
            ->with('error_message', 'Lokasi pembelian dengan ID: '.$id.' tidak ditemukan.');

        return view('lokasi_pembelian.edit', [
            'lokasi_pembelian' => $lokasi_pembelian
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_lokasi_pembelian' => 'required|unique:lokasi_pembelian,nama_lokasi_pembelian,'.$id,
        ]);

        $lokasi_pembelian = LokasiPembelian::find($id);

        $lokasi_pembelian->nama_lokasi_pembelian = $request->nama_lokasi_pembelian;
        $lokasi_pembelian->user_update = $request->user()->id;

        $lokasi_pembelian->save();

        return redirect()->route('lokasi-pembelian.index')
            ->with('success_message', 'Berhasil mengubah lokasi pembelian.');
    }

    public function destroy(Request $request, $id)
    {
        $lokasi_pembelian = LokasiPembelian::find($id);

        if ($lokasi_pembelian) {
            $lokasi_pembelian->user_delete = $request->user()->id;

            $lokasi_pembelian->save();

            $lokasi_pembelian->delete();
        }
        return redirect()->route('lokasi-pembelian.index')
            ->with('success_message', 'Berhasil menghapus lokasi pembelian.');
    }
}