<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:37
 */

namespace App\Http\Controllers;

Use Illuminate\Http\Request;
Use App\Models\Satuan;

class SatuanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $satuan = Satuan::all();
        return view('satuan.index', [
            'satuan' => $satuan
        ]);
    }

    public function create()
    {
        return view('satuan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'satuan' => 'required|unique:satuan,satuan',
            'nama_satuan' => 'required',
        ]);

        $array = $request->only([
            'satuan', 'nama_satuan'
        ]);
        $kode_satuan = kode_hash();

        $array['kode_satuan'] = $kode_satuan;
        $array['user_create'] = $request->user()->id;

        $satuan = Satuan::create($array);

        return redirect()->route('satuan.index')
            ->with('success_message', 'Berhasil menambah satuan baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $satuan = Satuan::find($id);

        if (!$satuan) return redirect()->route('satuan.index')
            ->with('error_message', 'Satuan dengan ID: '.$id.' tidak ditemukan.');

        return view('satuan.edit', [
            'satuan' => $satuan
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'satuan' => 'required|unique:satuan,satuan,'.$id,
            'nama_satuan' => 'required',
        ]);

        $satuan = Satuan::find($id);

        $satuan->satuan = $request->satuan;
        $satuan->nama_satuan = $request->nama_satuan;
        $satuan->user_update = $request->user()->id;

        $satuan->save();

        return redirect()->route('satuan.index')
            ->with('success_message', 'Berhasil mengubah satuan.');
    }

    public function destroy(Request $request, $id)
    {
        $satuan = Satuan::find($id);

        if ($satuan) {
            $satuan->user_delete = $request->user()->id;

            $satuan->save();

            $satuan->delete();
        }

        return redirect()->route('satuan.index')
            ->with('success_message', 'Berhasil menghapus satuan.');
    }
}