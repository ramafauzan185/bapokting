<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:27
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JenisBarang;
use App\Models\JenisBarangView;
use App\Models\BidangUsaha;

class JenisBarangController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jenisBarang = JenisBarangView::all();
        return view('jenis_barang.index', [
            'jenis_barang' => $jenisBarang
        ]);
    }

    private function form()
    {
        $bidang_usaha = BidangUsaha::all();

        return [
            'bidang_usaha' => $bidang_usaha,
        ];
    }

    public function create()
    {
        $data = [];
        $form = $this->form();

        return view('jenis_barang.create', array_merge($data, $form));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_barang' => 'required',
            'id_bidang_usaha' => 'required',
        ]);

        $array = $request->only([
            'id_bidang_usaha', 'nama_barang'
        ]);
        $kode_jenis_barang = kode_hash();

        $array['kode_jenis_barang'] = $kode_jenis_barang;
        $array['user_create'] = $request->user()->id;

        $jenis_barang = JenisBarang::create($array);

        return redirect()->route('jenis-barang.index')
            ->with('success_message', 'Berhasil menambah jenis barang baru.');

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jenis_barang = JenisBarang::find($id);

        if (!$jenis_barang) return redirect()->route('jenis-barang.index')
            ->with('error_message', 'Jenis barang dengan ID: '.$id.' tidak ditemukan.');

        $data = ['jenis_barang' => $jenis_barang];
        $form = $this->form();

        return view('jenis_barang.edit', array_merge($data, $form));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_barang' => 'required',
            'id_bidang_usaha' => 'required',
        ]);

        $jenis_barang = JenisBarang::find($id);

        $jenis_barang->nama_barang = $request->nama_barang;
        $jenis_barang->id_bidang_usaha = $request->id_bidang_usaha;
        $jenis_barang->user_update = $request->user()->id;

        $jenis_barang->save();

        return redirect()->route('jenis-barang.index')
            ->with('success_message', 'Berhasil mengubah jenis barang.');
    }

    public function destroy(Request $request, $id)
    {
        $jenis_barang = JenisBarang::find($id);

        if ($jenis_barang) {
            $jenis_barang->user_delete = $request->user()->id;

            $jenis_barang->save();

            $jenis_barang->delete();
        }

        return redirect()->route('jenis-barang.index')
            ->with('success_message', 'Berhasil menghapus jenis barang.');
    }

    public function getKomoditiByJenisBarang(Request $request)
    {
        $jenis_barang = $request->jenis_barang;

        $getKomoditi = JenisBarangView::where(['jenis_barang' => $jenis_barang])->get();

        return response()->json([
            'jenis_barang' => $getKomoditi,
        ]);
    }
}