<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 30/08/2021
 * Time: 20:04
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\BidangUsaha;

class BidangUsahaController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $bidangUsaha = BidangUsaha::all();
        return view('bidang_usaha.index', [
            'bidang_usaha' => $bidangUsaha
        ]);
    }

    public function create()
    {
        return view('bidang_usaha.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'kode_kbli' => 'required|unique:bidang_usaha,kode_kbli',
            'nama_kbli' => 'required',
            'jenis_barang' => 'required',
        ]);

        $array = $request->only([
            'kode_kbli', 'nama_kbli', 'jenis_barang'
        ]);
        $kode_bidang_usaha = kode_hash();

        $array['kode_bidang_usaha'] = $kode_bidang_usaha;
        $array['user_create'] = $request->user()->id;

        $bidang_usaha = BidangUsaha::create($array);

        return redirect()->route('bidang-usaha.index')
            ->with('success_message', 'Berhasil menambah bidang usaha baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $bidang_usaha = BidangUsaha::find($id);

        if (!$bidang_usaha) return redirect()->route('bidang-usaha.index')
            ->with('error_message', 'Bidang Usaha dengan ID: '.$id.' tidak ditemukan.');

        return view('bidang_usaha.edit', [
            'bidang_usaha' => $bidang_usaha
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'kode_kbli' => 'required|numeric|unique:bidang_usaha,kode_kbli,'.$id,
            'nama_kbli' => 'required',
            'jenis_barang' => 'required',
        ]);

        $bidang_usaha = BidangUsaha::find($id);

        $bidang_usaha->kode_kbli = $request->kode_kbli;
        $bidang_usaha->nama_kbli = $request->nama_kbli;
        $bidang_usaha->jenis_barang = $request->jenis_barang;
        $bidang_usaha->user_update = $request->user()->id;

        $bidang_usaha->save();

        return redirect()->route('bidang-usaha.index')
            ->with('success_message', 'Berhasil mengubah bidang usaha.');
    }

    public function destroy(Request $request, $id)
    {
        $bidang_usaha = BidangUsaha::find($id);

        if ($bidang_usaha) {
            $bidang_usaha->user_delete = $request->user()->id;

            $bidang_usaha->save();

            $bidang_usaha->delete();
        }

        return redirect()->route('bidang-usaha.index')
            ->with('success_message', 'Berhasil menghapus bidang usaha.');
    }
}