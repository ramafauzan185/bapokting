<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 30/08/2021
 * Time: 13:47
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
        return view('users.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'role' => ['required', Rule::in(roles())],
        ]);

        $array = $request->only([
            'name', 'email', 'password', 'role'
        ]);
        $status = '1';
        $kode_user = kode_hash();

        $array['kode_user'] = $kode_user;
        $array['password'] = bcrypt($array['password']);
        $array['status'] = $status;

        $user = User::create($array);

        return redirect()->route('users.index')
            ->with('success_message', 'Berhasil menambah user baru.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if (!$user) return redirect()->route('users.index')
            ->with('error_message', "User dengan ID: $id tidak ditemukan.");

        return view('users.edit', [
            'user' => $user,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'sometimes|nullable|confirmed',
            'role' => ['required', Rule::in(roles())],
        ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->password) $user->password = bcrypt($request->password);
        $user->role = $request->role;

        $user->save();

        return redirect()->route('users.index')
            ->with('success_message', 'Berhasil mengubah user.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::find($id);

        if ($id == $request->user()->id) return redirect()->route('users.index')
            ->with('error_message', 'Anda tidak dapat menghapus diri sendiri.');

        $user->status = '0';

        $user->save();

        if ($user) $user->delete();

        return redirect()->route('users.index')
            ->with('success_message', 'Berhasil menghapus user.');
    }

    public function status($id)
    {
        $user = User::find($id);
        $status = ($user->status == '1')? '0' : '1';

        $user->status = $status;

        $user->save();

        $buttons = button_status('users', $user).' '.button_edit('users', $user).' '.button_delete('users', $user);

        return response()->json([
            'status' => status($status),
            'buttons' => $buttons,
        ]);
    }

    public function ubahPassword()
    {
        return view('users.ubah-password');
    }

    public function ubahPasswordProses(Request $request)
    {
        $request->validate([
            'password_lama' => 'required',
            'password' => 'required|confirmed|different:password_lama',
        ]);

        if(!Hash::check($request->password_lama, $request->user()->password)) {
            return back()->withErrors([
                'password_lama' => ['Password lama tidak sesuai.']
            ]);
        }

        $auth = Auth::user();
        $id = $auth->id;

        $user = User::find($id);

        $user->password = bcrypt($request->password);
        $user->save();

        $request->session()->passwordConfirmed();

        return redirect()->route('users.ubah-password')->with('success_message', 'Berhasil mengubah password.');
    }
}