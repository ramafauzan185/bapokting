<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 13/09/2021
 * Time: 9:22
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelurahan;

class KelurahanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function showByKecamatan(Request $request)
    {
        $id_kecamatan = $request->id_kecamatan;

        $kelurahan = Kelurahan::where('id_kecamatan', $id_kecamatan)->get();

        return response()->json([
            'kelurahan' => $kelurahan,
        ]);
    }

}