<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:46
 */

namespace App\Http\Controllers;

use App\Models\Kelurahan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Models\JenisBarangView;
use App\Models\Pemilik;
use App\Models\User;
use App\Models\Perusahaan;
use App\Models\PerusahaanBidangUsaha;
use App\Models\PerusahaanView;
use App\Models\PerusahaanBidangUsahaView;
use App\Models\Kecamatan;

class PerusahaanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $perusahaan = PerusahaanView::all();

        return view('perusahaan.index', [
            'perusahaan' => $perusahaan
        ]);
    }

    private function form()
    {
        $jenis_barang = JenisBarangView::all();
        $kecamatan = Kecamatan::all();

        return [
            'jenis_barang' => $jenis_barang,
            'kecamatan' => $kecamatan,
        ];
    }

    public function create()
    {
        $data = ['id_jenis_barangs' => empty(old('id_jenis_barang'))? [] : old('id_jenis_barang')];
        if(!empty(old('id_kecamatan'))) {
            $kelurahans = Kelurahan::where('id_kecamatan', old('id_kecamatan'))->get();
            $data['kelurahans'] = $kelurahans;
        }
        $form = $this->form();

        return view('perusahaan.create', array_merge($data, $form));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nik' => 'required|integer',
            'nama_pemilik' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_handphone' => 'required',
            'tanggal_join' => 'required|date',
            'email' => 'required|email|unique:users,email',
            'nib' => 'required',
            'nomor_legalitas_usaha' => 'required',
            'nama_perusahaan' => 'required',
            'alamat_perusahaan' => 'required',
            'id_kelurahan' => 'required',
            'nomor_telepon' => 'required',
            'lokasi_usaha' => ['required', Rule::in(lokasi_usahas())],
            'status_usaha' => ['required', Rule::in(status_usahas())],
            'status_toko' => ['required', Rule::in(status_tokos())],
            'skala_usaha' => ['required', Rule::in(skala_usahas())],
            'luas_gudang' => 'required|numeric',
            'id_jenis_barang' => 'required|array',
        ]);

        $array_pemilik = $request->only([
            'nik',
            'nama_pemilik',
            'tempat_lahir',
            'tanggal_lahir',
            'jenis_kelamin',
            'alamat',
            'nomor_handphone',
            'tanggal_join',
        ]);

        $array_users = $request->only([
            'email',
            'password',
        ]);

        $array_perusahaan = $request->only([
            'nib',
            'nomor_legalitas_usaha',
            'nama_perusahaan',
            'alamat_perusahaan',
            'id_kelurahan',
            'nomor_telepon',
            'lokasi_usaha',
            'status_usaha',
            'status_toko',
            'skala_usaha',
            'luas_gudang',
            'koordinat',
        ]);

        $id_jenis_barangs = $request->only(['id_jenis_barang']);
        $user_create = $request->user()->id;

        DB::beginTransaction();
        try {
            // add pemilik
            $kode_pemilik = kode_hash();
            $array_pemilik['kode_pemilik'] = $kode_pemilik;
            $array_pemilik['user_create'] = $user_create;
            // save pemilik
            $pemilik = Pemilik::create($array_pemilik);
            $id_pemilik = $pemilik->id;

            // add users
            $kode_user = kode_hash();
            $name = $array_perusahaan['nama_perusahaan'];
            $password = empty($array_users['password'])? bcrypt('12345!@') : bcrypt($array_users['password']);
            $role = 'distributor';
            $status = '1';
            $array_users['kode_user'] = $kode_user;
            $array_users['name'] = $name;
            $array_users['password'] = $password;
            $array_users['role'] = $role;
            $array_users['status'] = $status;
            // save users
            $user = User::create($array_users);
            $id_user = $user->id;

            // add perusahaan
            $kode_perusahaan = kode_hash();
            $array_perusahaan['kode_perusahaan'] = $kode_perusahaan;
            $array_perusahaan['id_pemilik'] = $id_pemilik;
            $array_perusahaan['id_user'] = $id_user;
            $array_perusahaan['user_create'] = $user_create;
            // save perusahaan
            $perusahaan = Perusahaan::create($array_perusahaan);
            $id_perusahaan = $perusahaan->id;

            // add perusahaan bidang usaha
            foreach ($id_jenis_barangs['id_jenis_barang'] as $k=>$v) {
                $kode_perusahaan_bidang_usaha = kode_hash();
                $id_jenis_barang = $v;
                $status = '1';

                $array_perusahaan_bidang_usaha['kode_perusahaan_bidang_usaha'] = $kode_perusahaan_bidang_usaha;
                $array_perusahaan_bidang_usaha['id_perusahaan'] = $id_perusahaan;
                $array_perusahaan_bidang_usaha['id_jenis_barang'] = $id_jenis_barang;
                $array_perusahaan_bidang_usaha['status'] = $status;
                $array_perusahaan_bidang_usaha['user_create'] = $user_create;

                // save
                $perusahaan_bidang_usaha = PerusahaanBidangUsaha::create($array_perusahaan_bidang_usaha);
            }

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return redirect()->route('perusahaan.index')
            ->with('success_message', 'Berhasil menambah perusahaan baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $perusahaan = Perusahaan::find($id);

        if (!$perusahaan) return redirect()->route('perusahaan.index')
            ->with('error_message', 'Perusahaan dengan ID: '.$id.' tidak ditemukan.');

        $pemilik = Pemilik::find($perusahaan->id_pemilik);
        $user = User::find($perusahaan->id_user);
        $kelurahan = Kelurahan::find($perusahaan->id_kelurahan);
        $kelurahans = Kelurahan::where('id_kecamatan', $kelurahan->id_kecamatan)->get();
        $perusahaan_bidang_usaha = PerusahaanBidangUsaha::where(['id_perusahaan' => $id, 'status' => '1'])->get()->toArray();
        $id_jenis_barangs = array_column($perusahaan_bidang_usaha, 'id_jenis_barang');

        $data = [
            'perusahaan' => $perusahaan,
            'pemilik' => $pemilik,
            'user' => $user,
            'kelurahan' => $kelurahan,
            'kelurahans' => $kelurahans,
            'id_jenis_barangs' => $id_jenis_barangs,
        ];
        $form = $this->form();

        return view('perusahaan.edit', array_merge($data, $form));
    }

    public function update(Request $request, $id)
    {
        $perusahaan = Perusahaan::find($id);
        $pemilik = Pemilik::find($perusahaan->id_pemilik);
        $user = User::find($perusahaan->id_user);
        $perusahaan_bidang_usaha = PerusahaanBidangUsaha::where('id_perusahaan', $id)->get()->toArray();

        $request->validate([
            'nik' => 'required|integer',
            'nama_pemilik' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_handphone' => 'required',
            'tanggal_join' => 'required|date',
            'email' => 'required|email|unique:users,email,'.$perusahaan->id_user,
            'nib' => 'required',
            'nomor_legalitas_usaha' => 'required',
            'nama_perusahaan' => 'required',
            'alamat_perusahaan' => 'required',
            'id_kelurahan' => 'required',
            'nomor_telepon' => 'required',
            'lokasi_usaha' => ['required', Rule::in(lokasi_usahas())],
            'status_usaha' => ['required', Rule::in(status_usahas())],
            'status_toko' => ['required', Rule::in(status_tokos())],
            'skala_usaha' => ['required', Rule::in(skala_usahas())],
            'luas_gudang' => 'required|numeric',
            'id_jenis_barang' => 'required|array',
        ]);

        $id_perusahaan_bidang_usahas = array_column($perusahaan_bidang_usaha, 'id');
        $id_jenis_barangs_old = array_column($perusahaan_bidang_usaha, 'id_jenis_barang');
        $id_jenis_barangs_new = $request->only(['id_jenis_barang'])['id_jenis_barang'];
        $user_update = $request->user()->id;
        $user_create = $user_update;
        $user_delete = $user_update;

        DB::beginTransaction();
        try {
            // update perusahaan
            $perusahaan->nib = $request->nib;
            $perusahaan->nomor_legalitas_usaha = $request->nomor_legalitas_usaha;
            $perusahaan->nama_perusahaan = $request->nama_perusahaan;
            $perusahaan->alamat_perusahaan = $request->alamat_perusahaan;
            $perusahaan->id_kelurahan = $request->id_kelurahan;
            $perusahaan->nomor_telepon = $request->nomor_telepon;
            $perusahaan->lokasi_usaha = $request->lokasi_usaha;
            $perusahaan->status_usaha = $request->status_usaha;
            $perusahaan->status_toko = $request->status_toko;
            $perusahaan->skala_usaha = $request->skala_usaha;
            $perusahaan->luas_gudang = $request->luas_gudang;
            $perusahaan->koordinat = $request->koordinat;
            $perusahaan->user_update = $user_update;
            $perusahaan->save();

            // update pemilik
            $pemilik->nik = $request->nik;
            $pemilik->nama_pemilik = $request->nama_pemilik;
            $pemilik->tempat_lahir = $request->tempat_lahir;
            $pemilik->tanggal_lahir = $request->tanggal_lahir;
            $pemilik->jenis_kelamin = $request->jenis_kelamin;
            $pemilik->alamat = $request->alamat;
            $pemilik->nomor_handphone = $request->nomor_handphone;
            $pemilik->tanggal_join = $request->tanggal_join;
            $pemilik->user_update = $user_update;
            $pemilik->save();

            // update user
            $user->name = $request->nama_perusahaan;
            $user->email = $request->email;
            if(!empty($request->password)) $user->password = bcrypt($request->password);
            $user->save();

            // add perusahaan_bidang_usaha
            foreach ($id_jenis_barangs_new as $k=>$v) {
                if(!in_array($v, $id_jenis_barangs_old)) {
                    $kode_perusahaan_bidang_usaha = kode_hash();
                    $id_jenis_barang = $v;
                    $status = '1';

                    $array_perusahaan_bidang_usaha['kode_perusahaan_bidang_usaha'] = $kode_perusahaan_bidang_usaha;
                    $array_perusahaan_bidang_usaha['id_perusahaan'] = $id;
                    $array_perusahaan_bidang_usaha['id_jenis_barang'] = $id_jenis_barang;
                    $array_perusahaan_bidang_usaha['status'] = $status;
                    $array_perusahaan_bidang_usaha['user_create'] = $user_create;

                    // save
                    $perusahaan_bidang_usaha = PerusahaanBidangUsaha::create($array_perusahaan_bidang_usaha);

                } else {

                    $id_perusahaan_bidang_usaha = $id_perusahaan_bidang_usahas[$k];

                    $perusahaan_bidang_usaha = PerusahaanBidangUsaha::find($id_perusahaan_bidang_usaha);

                    // update status
                    $perusahaan_bidang_usaha->user_update = $user_update;
                    $perusahaan_bidang_usaha->status = '1';
                    $perusahaan_bidang_usaha->save();
                }
            }

            // delete or update perusahaan_bidang_usaha
            foreach ($id_jenis_barangs_old as $k=>$v) {
                if(!in_array($v, $id_jenis_barangs_new)) {
                    $id_perusahaan_bidang_usaha = $id_perusahaan_bidang_usahas[$k];

                    $perusahaan_bidang_usaha = PerusahaanBidangUsaha::find($id_perusahaan_bidang_usaha);

                    // update status
                    // $perusahaan_bidang_usaha->user_delete = $user_delete;
                    $perusahaan_bidang_usaha->user_update = $user_update;
                    $perusahaan_bidang_usaha->status = '0';
                    $perusahaan_bidang_usaha->save();

                    // delete
                    // $perusahaan_bidang_usaha->delete();
                }
            }

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return redirect()->route('perusahaan.index')
            ->with('success_message', 'Berhasil mengubah perusahaan.');
    }

    public function destroy(Request $request, $id)
    {
        $perusahaan = Perusahaan::find($id);

        if ($perusahaan) {
            $perusahaan->user_delete = $request->user()->id;

            $perusahaan->save();

            $perusahaan->delete();
        };

        return redirect()->route('perusahaan.index')
            ->with('success_message', 'Berhasil menghapus perusahaan.');
    }

    public function all()
    {
        $perusahaan = Perusahaan::all();

        return response()->json([
            'perusahaan' => $perusahaan
        ]);
    }
}