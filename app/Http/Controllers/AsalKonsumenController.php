<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 5:08
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AsalKonsumen;

class AsalKonsumenController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $asalKonsumen = AsalKonsumen::all();

        return view('asal_konsumen.index', [
            'asal_konsumen' => $asalKonsumen
        ]);
    }

    public function create()
    {
        return view('asal_konsumen.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_asal_konsumen' => 'required|unique:asal_konsumen,nama_asal_konsumen',
        ]);

        $array = $request->only([
            'nama_asal_konsumen'
        ]);
        $kode_asal_konsumen = kode_hash();

        $array['kode_asal_konsumen'] = $kode_asal_konsumen;
        $array['user_create'] = $request->user()->id;

        $asal_konsumen = AsalKonsumen::create($array);

        return redirect()->route('asal-konsumen.index')
            ->with('success_message', 'Berhasil menambah asal konsumen baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $asal_konsumen = AsalKonsumen::find($id);

        if (!$asal_konsumen) return redirect()->route('asal-konsumen.index')
            ->with('error_message', 'Asal konsumen dengan ID: '.$id.' tidak ditemukan.');

        return view('asal_konsumen.edit', [
            'asal_konsumen' => $asal_konsumen
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_asal_konsumen' => 'required|unique:asal_konsumen,nama_asal_konsumen,'.$id,
        ]);

        $asal_konsumen = AsalKonsumen::find($id);

        $asal_konsumen->nama_asal_konsumen = $request->nama_asal_konsumen;
        $asal_konsumen->user_update = $request->user()->id;

        $asal_konsumen->save();

        return redirect()->route('asal-konsumen.index')
            ->with('success_message', 'Berhasil mengubah asal konsumen.');
    }

    public function destroy(Request $request, $id)
    {
        $asal_konsumen = AsalKonsumen::find($id);

        if ($asal_konsumen) {
            $asal_konsumen->user_delete = $request->user()->id;

            $asal_konsumen->save();

            $asal_konsumen->delete();
        }

        return redirect()->route('asal-konsumen.index')
            ->with('success_message', 'Berhasil menghapus asal konsumen.');
    }
}