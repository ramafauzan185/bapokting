<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 6:14
 */

namespace App\Http\Controllers;

use App\Models\AsalKonsumen;
use App\Models\DistribusiBapoktingView;
use App\Models\JenisBarang;
use App\Models\JenisBarangView;
use App\Models\Konsumen;
use App\Models\LokasiPembelian;
use App\Models\Pemasaran;
use App\Models\PemasaranView;
use App\Models\Pengadaan;
use App\Models\PengadaanView;
use App\Models\Perusahaan;
use App\Models\Satuan;
use App\Models\SumberPembelian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Models\DistribusiBapokting;
use App\Models\PerusahaanBidangUsahaView;

class DistribusiBapoktingController extends Controller
{
    protected $user;
    protected $perusahaan;

    function __construct()
    {
        $this->middleware('auth');

        $this->user = array();
        $this->perusahaan = array();
    }

    private function init()
    {
        $this->user = Auth::user();
        $this->perusahaan = Perusahaan::where('id_user', $this->user->id)->first();
    }

    public function index(Request $request)
    {
        $tahun = $request->tahun;
        if(empty($tahun)) $tahun = date("Y");
        $where['tahun'] = $tahun;

        $bulan = $request->bulan;
        if(!empty($bulan)) $where['bulan'] = $bulan;

        $id_perusahaan = $request->id_perusahaan;
        $nama_perusahaan = $request->nama_perusahaan;
        if(!empty($id_perusahaan)) $where['id_perusahaan'] = $id_perusahaan;

        $jenis_barang = $request->jenis_barang;
        if(!empty($jenis_barang)) $where['jenis_barang'] = $jenis_barang;

        $id_jenis_barang = $request->id_jenis_barang;
        if(!empty($id_jenis_barang)) $where['id_jenis_barang'] = $id_jenis_barang;

        $filter = [
            'tahun' => $tahun,
            'bulan' => $bulan,
            'id_perusahaan' => $id_perusahaan,
            'nama_perusahaan' => $nama_perusahaan,
            'jenis_barang' => $jenis_barang,
            'id_jenis_barang' => $id_jenis_barang,
        ];

        if(is_superadmin() || is_manajemen()) {
            $jenis_barang = JenisBarangView::all();

        } else {
            $this->init();

            $jenis_barang = PerusahaanBidangUsahaView::where('id_perusahaan', $this->perusahaan->id)->get();

            $where['id_perusahaan'] = $this->perusahaan->id;
        }

        $distribusiBapokting = DistribusiBapoktingView::where($where)->get();

        return view('distribusi_bapokting.index', [
            'filter' => $filter,
            'distribusi_bapokting' => $distribusiBapokting,
            'jenis_barang' => $jenis_barang,
        ]);
    }

    public function form()
    {
        $this->init();

        $perusahaan = $this->perusahaan;
        $sumber_pembelian = SumberPembelian::all();
        $satuan = Satuan::all();
        $lokasi_pembelian = LokasiPembelian::all();
        $konsumen = Konsumen::all();
        $asal_konsumen = AsalKonsumen::all();
        $perusahaan_bidang_usaha = PerusahaanBidangUsahaView::where('id_perusahaan', $perusahaan->id)->get();

        return [
            'perusahaan' => $perusahaan,
            'sumber_pembelian' => $sumber_pembelian,
            'satuan' => $satuan,
            'lokasi_pembelian' => $lokasi_pembelian,
            'konsumen' => $konsumen,
            'asal_konsumen' => $asal_konsumen,
            'perusahaan_bidang_usaha' => $perusahaan_bidang_usaha,
        ];
    }

    public function create()
    {
        $data = [];
        $form = $this->form();

        return view('distribusi_bapokting.create', array_merge($data, $form));
    }

    private function storePengadaan($request, $id_distribusi_bapokting, $pengadaans)
    {
        if(!empty($pengadaans)) {
            $user_create = $request->user()->id;

            foreach ($pengadaans as $row) {
                $kode_pengadaan = kode_hash();
                $id_sumber_pembelian = $row->id_sumber_pembelian;
                $id_lokasi_pembelian = $row->id_lokasi_pembelian;
                $volume = $row->volume;
                $id_satuan = $row->id_satuan;

                // add pengadaan
                $array_pengadaan['kode_pengadaan'] = $kode_pengadaan;
                $array_pengadaan['id_distribusi_bapokting'] = $id_distribusi_bapokting;
                $array_pengadaan['id_sumber_pembelian'] = $id_sumber_pembelian;
                $array_pengadaan['id_lokasi_pembelian'] = $id_lokasi_pembelian;
                $array_pengadaan['volume'] = $volume;
                $array_pengadaan['id_satuan'] = $id_satuan;
                $array_pengadaan['user_create'] = $user_create;
                // save pengadaan
                $pengadaan = Pengadaan::create($array_pengadaan);

                unset($array_pengadaan);
            }
        }
    }

    private function storePemasaran($request, $id_distribusi_bapokting, $pemasarans)
    {
        if(!empty($pemasarans)) {
            $user_create = $request->user()->id;

            foreach ($pemasarans as $row) {
                $kode_pemasaran = kode_hash();
                $id_konsumen = $row->id_konsumen;
                $id_asal_konsumen = $row->id_asal_konsumen;
                $volume = $row->volume;
                $id_satuan = $row->id_satuan;

                // add pemasaran
                $array_pemasaran['kode_pemasaran'] = $kode_pemasaran;
                $array_pemasaran['id_distribusi_bapokting'] = $id_distribusi_bapokting;
                $array_pemasaran['id_konsumen'] = $id_konsumen;
                $array_pemasaran['id_asal_konsumen'] = $id_asal_konsumen;
                $array_pemasaran['volume'] = $volume;
                $array_pemasaran['id_satuan'] = $id_satuan;
                $array_pemasaran['user_create'] = $user_create;
                // save pemasaran
                $pemasaran = Pemasaran::create($array_pemasaran);

                unset($array_pemasaran);
            }
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'tahun' => 'required|integer',
            'bulan' => 'required|integer',
            'id_jenis_barang' => 'required',
            'skala_usaha' => ['required', Rule::in(skala_usahas())],
            'pengadaan' => 'required|min:5',
            'pemasaran' => 'required|min:5',
        ]);

        $aktivitas = $request->only(['pengadaan', 'pemasaran']);
        $pengadaans = json_decode($aktivitas['pengadaan']);
        $pemasarans = json_decode($aktivitas['pemasaran']);

        $array_distribusi_bapokting = $request->only([
            'id_jenis_barang', 'tahun', 'bulan', 'skala_usaha', 'status_laporan'
        ]);
        $kode_distribusi_bapokting = kode_hash();
        $user_create = $request->user()->id;
        $perusahaan = Perusahaan::where('id_user', $user_create)->first();
        $jenis_barang = JenisBarang::find($array_distribusi_bapokting['id_jenis_barang']);
        $id_perusahaan = $perusahaan->id;
        $id_bidang_usaha = $jenis_barang->id_bidang_usaha;

        $cek = DistribusiBapokting::where([
            'id_perusahaan' => $id_perusahaan,
            'id_bidang_usaha' => $id_bidang_usaha,
            'id_jenis_barang' => $array_distribusi_bapokting['id_jenis_barang'],
            'tahun' => $array_distribusi_bapokting['tahun'],
            'bulan' => $array_distribusi_bapokting['bulan'],
            'deleted_at' => NULL,
        ])->get();

        if(count($cek) > 0) {
            return redirect()->route('distribusi-bapokting.index')
                ->with('warning_message', 'Gagal menambah aktivitas pengadaan dan pemasaran baru. Data distribusi sudah ada !');
        }

        DB::beginTransaction();
        try {
            // add distribusi bapokting
            $array_distribusi_bapokting['kode_distribusi_bapokting'] = $kode_distribusi_bapokting;
            $array_distribusi_bapokting['id_perusahaan'] = $id_perusahaan;
            $array_distribusi_bapokting['id_bidang_usaha'] = $id_bidang_usaha;
            $array_distribusi_bapokting['user_create'] = $request->user()->id;
            // save distribusi bapokting
            $distribusi_bapokting = DistribusiBapokting::create($array_distribusi_bapokting);
            $id_distribusi_bapokting = $distribusi_bapokting->id;

            $this->storePengadaan($request, $id_distribusi_bapokting, $pengadaans);
            $this->storePemasaran($request, $id_distribusi_bapokting, $pemasarans);

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return redirect()->route('distribusi-bapokting.index')
            ->with('success_message', 'Berhasil menambah aktivitas pengadaan dan pemasaran baru.');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $distribusi_bapokting = DistribusiBapokting::find($id);

        if (!$distribusi_bapokting) return redirect()->route('distribusi-bapokting.index')
            ->with('error_message', 'Laporan distribusi bapokting dengan ID: '.$id.' tidak ditemukan.');

        $pengadaan = PengadaanView::where('id_distribusi_bapokting', $id)->get();
        $pemasaran = PemasaranView::where('id_distribusi_bapokting', $id)->get();

        $data = [
            'distribusi_bapokting' => $distribusi_bapokting,
            'pengadaan' => $pengadaan,
            'pemasaran' => $pemasaran,
        ];
        $form = $this->form();

        return view('distribusi_bapokting.edit', array_merge($data, $form));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tahun' => 'required|integer',
            'bulan' => 'required|integer',
            'id_jenis_barang' => 'required',
            'skala_usaha' => ['required', Rule::in(skala_usahas())],
            /*'pengadaan' => 'required|min:5',
            'pemasaran' => 'required|min:5',*/
        ]);

        $aktivitas = $request->only(['pengadaan', 'pemasaran', 'pengadaan_delete', 'pemasaran_delete']);
        $pengadaans = json_decode($aktivitas['pengadaan']);
        $pemasarans = json_decode($aktivitas['pemasaran']);
        $pengadaan_deletes = json_decode($aktivitas['pengadaan_delete']);
        $pemasaran_deletes = json_decode($aktivitas['pemasaran_delete']);

        $user_update = $request->user()->id;
        $user_delete = $user_update;
        $perusahaan = Perusahaan::where('id_user', $user_update)->first();
        $id_perusahaan = $perusahaan->id;
        $jenis_barang = JenisBarang::find($request->id_jenis_barang);
        $id_bidang_usaha = $jenis_barang->id_bidang_usaha;

        $cek = DistribusiBapokting::where([
            'id_perusahaan' => $id_perusahaan,
            'id_bidang_usaha' => $id_bidang_usaha,
            'id_jenis_barang' => $request->id_jenis_barang,
            'tahun' => $request->tahun,
            'bulan' => $request->bulan,
            'deleted_at' => NULL,
            'status_laporan' => 'send',
        ])->get();

        if(count($cek) > 0) {
            return redirect()->route('distribusi-bapokting.index')
                ->with('warning_message', 'Gagal mengubah laporan distribusi bapokting. Data distribusi sudah ada !');
        }

        DB::beginTransaction();
        try {
            $distribusi_bapokting = DistribusiBapokting::find($id);

            $distribusi_bapokting->id_bidang_usaha = $id_bidang_usaha;
            $distribusi_bapokting->id_jenis_barang = $request->id_jenis_barang;
            $distribusi_bapokting->tahun = $request->tahun;
            $distribusi_bapokting->bulan = $request->bulan;
            $distribusi_bapokting->skala_usaha = $request->skala_usaha;
            $distribusi_bapokting->status_laporan = $request->status_laporan;
            $distribusi_bapokting->user_update = $user_update;

            $distribusi_bapokting->save();
            $id_distribusi_bapokting = $id;

            if(!empty($pengadaan_deletes)) {
                foreach ($pengadaan_deletes as $k => $v) {
                    $pengadaan = Pengadaan::find($v);

                    $pengadaan->user_delete = $user_delete;
                    $pengadaan->save();

                    $pengadaan->delete();

                    unset($pengadaan);
                }
            }

            if(!empty($pemasaran_deletes)) {
                foreach ($pemasaran_deletes as $k => $v) {
                    $pemasaran = Pemasaran::find($v);

                    $pemasaran->user_delete = $user_delete;
                    $pemasaran->save();

                    $pemasaran->delete();

                    unset($pemasaran);
                }
            }

            $this->storePengadaan($request, $id_distribusi_bapokting, $pengadaans);
            $this->storePemasaran($request, $id_distribusi_bapokting, $pemasarans);

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw $e;
        }

        return redirect()->route('distribusi-bapokting.index')
            ->with('success_message', 'Berhasil mengubah laporan distribusi bapokting.');
    }

    public function destroy(Request $request, $id)
    {
        $distribusi_bapokting = DistribusiBapokting::find($id);

        if ($distribusi_bapokting) {
            $distribusi_bapokting->user_delete = $request->user()->id;

            $distribusi_bapokting->save();

            $distribusi_bapokting->delete();
        }

        return redirect()->route('distribusi-bapokting.index')
            ->with('success_message', 'Berhasil menghapus laporan distribusi bapokting.');
    }

    public function aktivitas($id)
    {
        $pengadaan = PengadaanView::where('id_distribusi_bapokting', $id)->get();
        $pemasaran = PemasaranView::where('id_distribusi_bapokting', $id)->get();

        return response()->json([
            'pengadaan' => $pengadaan,
            'pemasaran' => $pemasaran,
        ]);
    }

    public function cekDataSama(Request $request)
    {
        $this->init();

        $tahun = $request->tahun;
        $bulan = $request->bulan;
        $id_jenis_barang = $request->id_jenis_barang;
        $jenis_barang = JenisBarang::find($id_jenis_barang);
        if(!empty($jenis_barang)) {
            $id_bidang_usaha = $jenis_barang->id_bidang_usaha;
        } else {
            $id_bidang_usaha = 0;
        }
        $id_perusahaan = $this->perusahaan->id;

        $where = [
            'id_perusahaan' => $id_perusahaan,
            'id_bidang_usaha' => $id_bidang_usaha,
            'id_jenis_barang' => $id_jenis_barang,
            'tahun' => $tahun,
            'bulan' => $bulan,
            'deleted_at' => NULL,
        ];

        if($request->is_form == 'edit') $where = array_merge($where, ['status_laporan' => 'send']);

        $cek = DistribusiBapokting::where($where)->get();

        return response()->json([
            'distribusi_bapokting' => $cek
        ]);
    }
}