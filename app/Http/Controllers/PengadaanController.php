<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 6:19
 */

namespace App\Http\Controllers;

use App\Models\JenisBarangView;
use App\Models\PengadaanView;
use App\Models\Perusahaan;
use App\Models\PerusahaanBidangUsahaView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PengadaanController extends Controller
{
    protected $user;
    protected $perusahaan;

    function __construct()
    {
        $this->middleware('auth');

        $this->user = array();
        $this->perusahaan = array();
    }

    private function init()
    {
        $this->user = Auth::user();
        $this->perusahaan = Perusahaan::where('id_user', $this->user->id)->first();
    }

    public function index(Request $request)
    {
        $where = "a.id IS NOT NULL ";

        $tahun = $request->tahun;
        if(empty($tahun)) $tahun = date("Y");
        $where .= "AND b.tahun=$tahun ";

        $bulan = $request->bulan;
        if(!empty($bulan)) $where .= "AND b.bulan=$bulan ";

        $id_perusahaan = $request->id_perusahaan;
        $nama_perusahaan = $request->nama_perusahaan;
        if(!empty($id_perusahaan)) $where .= "AND b.id_perusahaan=$id_perusahaan ";

        $jenis_barang = $request->jenis_barang;
        if(!empty($jenis_barang)) $where .= "AND b.jenis_barang='$jenis_barang' ";

        $id_jenis_barang = $request->id_jenis_barang;
        if(!empty($id_jenis_barang)) $where .= "AND b.id_jenis_barang=$id_jenis_barang ";

        $filter = [
            'tahun' => $tahun,
            'bulan' => $bulan,
            'id_perusahaan' => $id_perusahaan,
            'nama_perusahaan' => $nama_perusahaan,
            'jenis_barang' => $jenis_barang,
            'id_jenis_barang' => $id_jenis_barang,
        ];

        if(is_superadmin() || is_manajemen()) {
            $jenis_barang = JenisBarangView::all();

        } else {
            $this->init();

            $jenis_barang = PerusahaanBidangUsahaView::where('id_perusahaan', $this->perusahaan->id)->get();

            $where .= "AND b.id_perusahaan=".$this->perusahaan->id." ";
        }

        $pengadaan = PengadaanView::getByFilter($where);
        return view('pengadaan.index', [
            'filter' => $filter,
            'jenis_barang' => $jenis_barang,
            'pengadaan' => $pengadaan
        ]);
    }
}