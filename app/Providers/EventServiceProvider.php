<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen(BuildingMenu::class, function (BuildingMenu $event) {
            // Add some items to the menu
            $menu_dashboard = [
                'text'  => 'Dashboard',
                'url'   => 'home',
                'icon'  => 'fa fa-fw fa-home',
            ];
            $menu_bidang_usaha = [
                'text'  => 'Bidang Usaha',
                'url'   => 'bidang-usaha',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_jenis_barang = [
                'text'  => 'Jenis Barang',
                'url'   => 'jenis-barang',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_satuan = [
                'text'  => 'Satuan',
                'url'   => 'satuan',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_perusahaan = [
                'text'  => 'Pemilik TDPUD',
                'url'   => 'perusahaan',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_sumber_pembelian = [
                'text'  => 'Sumber Pembelian',
                'url'   => 'sumber-pembelian',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_lokasi_pembelian = [
                'text'  => 'Lokasi Pembelian',
                'url'   => 'lokasi-pembelian',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_konsumen = [
                'text'  => 'Konsumen',
                'url'   => 'konsumen',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_asal_konsumen = [
                'text'  => 'Asal Konsumen',
                'url'   => 'asal-konsumen',
                'icon'  => 'fa fa-fw fa-database',
            ];
            $menu_laporan_distribusi = [
                'text'  => 'Distribusi Bapokting',
                'url'   => 'distribusi-bapokting',
                'icon'  => 'fa fa-fw fa-dolly-flatbed',
            ];
            $menu_laporan_pengadaan = [
                'text'  => 'Aktivitas Pengadaan',
                'url'   => 'pengadaan',
                'icon'  => 'fa fa-fw fa-file-import',
            ];
            $menu_laporan_pemasaran = [
                'text'  => 'Aktivitas Pemasaran',
                'url'   => 'pemasaran',
                'icon'  => 'fa fa-fw fa-file-export',
            ];
            $menu_buat_laporan = [
                'text'  => 'Tambah Laporan',
                'url'   => 'distribusi-bapokting/create',
                'icon'  => 'far fa-fw fa-calendar-plus',
            ];
            $menu_pengguna =  [
                'text' => 'Pengguna',
                'url'  => 'users',
                'icon' => 'fas fa-fw fa-user',
            ];
            $menu_ubah_password = [
                'text' => 'Ubah Password',
                'url'  => 'user/ubah-password',
                'icon' => 'fas fa-fw fa-lock',
            ];
            $menu_slider = [
                'text' => 'Image Slider',
                'url'   => 'slider',
                'icon'  => 'fa fa-fw fa-database',
            ];

            $event->menu->add($menu_dashboard);

            if (is_superadmin()) {
                $event->menu->add('DATA MASTER');
                $event->menu->add(
                    $menu_bidang_usaha,
                    $menu_jenis_barang,
                    $menu_satuan,
                    $menu_perusahaan,
                    $menu_sumber_pembelian,
                    $menu_lokasi_pembelian,
                    $menu_konsumen,
                    $menu_asal_konsumen,
                    $menu_slider
                );
                $event->menu->add('LAPORAN');
                $event->menu->add(
                    $menu_laporan_distribusi,
                    $menu_laporan_pengadaan,
                    $menu_laporan_pemasaran
                );
                $event->menu->add('PENGATURAN');
                $event->menu->add(
                    $menu_pengguna,
                    $menu_ubah_password
                );
            }

            if(is_distributor()) {
                $event->menu->add('BUAT PELAPORAN BAPOKTING');
                $event->menu->add(
                    $menu_buat_laporan
                );
                $event->menu->add('HISTORI PELAPORAN');
                $event->menu->add(
                    $menu_laporan_distribusi,
                    $menu_laporan_pengadaan,
                    $menu_laporan_pemasaran
                );
                $event->menu->add('PENGATURAN');
                $event->menu->add(
                    $menu_ubah_password
                );
            }

            if(is_manajemen()) {
                $event->menu->add('HISTORI PELAPORAN');
                $event->menu->add(
                    $menu_laporan_distribusi,
                    $menu_laporan_pengadaan,
                    $menu_laporan_pemasaran
                );
                $event->menu->add('PENGATURAN');
                $event->menu->add(
                    $menu_ubah_password
                );
            }
        });
    }
}
