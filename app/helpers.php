<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 08/09/2021
 * Time: 11:09
 */

if(!function_exists('is_superadmin')) {
    function is_superadmin()
    {
        $role = \Illuminate\Support\Facades\Auth::user()->role;

        if ($role == 'superadmin') return true;
        return false;
    }
}

if(!function_exists('is_distributor')) {
    function is_distributor()
    {
        $role = \Illuminate\Support\Facades\Auth::user()->role;

        if ($role == 'distributor') return true;
        return false;
    }
}

if(!function_exists('is_manajemen')) {
    function is_manajemen()
    {
        $role = \Illuminate\Support\Facades\Auth::user()->role;

        if($role == 'manajemen') return true;
        return false;
    }
}

if(!function_exists('status')) {
    function status($status)
    {
        return ($status == '1')? '<span class="badge badge-info">Aktif</span>' : '<span class="badge badge-warning">Tidak Aktif</span>';
    }
}

if(!function_exists('status_laporan')) {
    function status_laporan($status_laporan)
    {
        return ($status_laporan == 'draft')? '<span class="badge badge-warning">'.ucfirst($status_laporan).'</span>' : '<span class="badge badge-info">'.ucfirst($status_laporan).'</span>';
    }
}

if(!function_exists('roles')) {
    function roles()
    {
        return ['superadmin', 'distributor', 'manajemen'];
    }
}

if(!function_exists('jenis_barangs')) {
    function jenis_barangs()
    {
        return [
            'pokok' => 'Barang Kebutuhan Pokok',
            'penting' => 'Barang Penting',
        ];
    }
}

if(!function_exists('lokasi_usahas')) {
    function lokasi_usahas()
    {
        return ['wilayah_usaha', 'permukiman'];
    }
}

if(!function_exists('status_usahas')) {
    function status_usahas()
    {
        return ['milik_sendiri', 'pekerja', 'konsinyasi'];
    }
}

if(!function_exists('status_tokos')) {
    function status_tokos()
    {
        return ['milik_sendiri', 'sewa', 'lapak_pesan'];
    }
}

if(!function_exists('skala_usahas')) {
    function skala_usahas()
    {
        return ['distributor', 'sub_distributor', 'agen'];
    }
}

if(!function_exists('jenis_kelamins')) {
    function jenis_kelamins()
    {
        return [
            'l' => 'Laki-laki',
            'p' => 'Perempuan',
        ];
    }
}

if(!function_exists('kode_hash')) {
    function kode_hash($pass = '')
    {
        if(empty($pass)) $pass = uniqid();
        $kode = password_hash($pass, PASSWORD_BCRYPT);

        return $kode;
    }
}

if(!function_exists('tahuns')) {
    function tahuns()
    {
        $Y = date("Y");
        $years = [];

        for ($year = $Y; $year >= 2020; $year-- ) {
            array_push($years, $year);
        }

        return $years;
    }
}

if(!function_exists('bulans')) {
    function bulans()
    {
        return [
            1 => "Januari",
            2 => "Februari",
            3 => "Maret",
            4 => "April",
            5 => "Mei",
            6 => "Juni",
            7 => "Juli",
            8 => "Agustus",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Desember",
        ];
    }
}

if(!function_exists('button_add')) {
    function button_add($route)
    {
        return '
            <a href="'. route($route . '.create') .'" class="btn btn-primary mb-2">
                <i class="fa fa-plus"></i> Tambah
            </a>
        ';
    }
}

if(!function_exists('button_save')) {
    function button_save()
    {
        return '
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
        ';
    }
}

if(!function_exists('button_draft')) {
    function button_draft()
    {
        return '
            <button type="button" class="btn btn-warning" onclick="submitDraft()"><i class="fa fa-save"></i> Draft</button>
            &nbsp;&nbsp;
        ';
    }
}

if(!function_exists('button_cancel')) {
    function button_cancel($route)
    {
        return '
            <a href="'. route($route . '.index') .'" class="btn btn-default">
                Batal <i class="fa fa-arrow-right"></i>
            </a>
        ';
    }
}

if(!function_exists('button_edit')) {
    function button_edit($route, $data)
    {
        return '
            <a href="'. route($route . '.edit', $data) .'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Edit data">
                <i class="fa fa-edit"></i> Edit
            </a>
        ';
    }
}

if(!function_exists('button_delete')) {
    function button_delete($route, $data)
    {
        return '
            <a href="'. route($route . '.destroy', $data) .'" onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Hapus data">
                <i class="fa fa-trash"></i> Delete
            </a>
        ';
    }
}

if(!function_exists('button_status')) {
    function button_status($route, $data)
    {
        if($data->status == '1') {
            $status_title = 'Non aktifkan user';
            $btn_class = 'btn-light';
            $btn_icon = 'fa-eye-slash';
        } else {
            $status_title = 'Aktifkan user';
            $btn_class = 'btn-dark';
            $btn_icon = 'fa-eye';
        }

        return '
            <a href="'. route($route . '.status', $data) .'" onclick="updateStatus(event, this)" class="btn '. $btn_class .' btn-xs" title="'. $status_title .'" data-toggle="tooltip" data-placement="bottom">
                <i class="fa '. $btn_icon .'"></i>
            </a>
        ';
    }
}

if(!function_exists('button_aktivitas')) {
    function button_aktivitas($data)
    {
        $data_json = "'".htmlspecialchars(json_encode($data))."'";
        return '
            <a href="'. route('distribusi-bapokting.aktivitas', $data) .'" onclick="showAktivitasDistribusi(event, this, '.$data_json.')" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="bottom" title="Lihat detail aktivitas pengadaan dan pemasaran">
                <i class="fa fa-eye"></i> View
            </a>
        ';
    }
}

if(!function_exists('button_filter')) {
    function button_filter($js = false, $btnId = 'btnFilter')
    {
        if($js) {
            $type = 'button';
        } else {
            $type = 'submit';
        }

        return '
            <button type="'.$type.'" name="btnFilter" id="'.$btnId.'" class="btn btn-primary"><i class="fa fa-check-square"></i> &nbsp; Tampilkan</button>
        ';
    }
}

if(!function_exists('button_pilih')) {
    function button_pilih($data)
    {
        return '
            <a href="javascript:;" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Pilih data" data-param="'.$data.'" onclick="pilihData()">
                <i class="fa fa-check-double"></i>
            </a>
        ';
    }
}