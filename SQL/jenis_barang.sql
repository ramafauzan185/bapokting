CREATE OR REPLACE VIEW view_jenis_barang AS
SELECT a.*, b.kode_bidang_usaha, b.kode_kbli, b.nama_kbli, b.jenis_barang FROM jenis_barang a
INNER JOIN
bidang_usaha b
ON a.id_bidang_usaha=b.id
WHERE a.deleted_at IS NULL;