CREATE OR REPLACE VIEW view_pemasaran AS
SELECT a.*,
b.kode_konsumen, b.nama_konsumen,
c.kode_asal_konsumen, c.nama_asal_konsumen,
d.kode_satuan, d.satuan, d.nama_satuan
FROM pemasaran a
INNER JOIN
konsumen b
ON a.id_konsumen=b.id
INNER JOIN
asal_konsumen c
ON a.id_asal_konsumen=c.id
INNER JOIN
satuan d
ON a.id_satuan=d.id
WHERE a.deleted_at IS NULL;