CREATE OR REPLACE VIEW view_distribusi_bapokting AS
SELECT a.*,
b.kode_perusahaan, b.nib, b.nomor_legalitas_usaha, b.nama_perusahaan, b.alamat_perusahaan,
c.kode_bidang_usaha, c.kode_kbli, c.nama_kbli, c.jenis_barang,
d.kode_jenis_barang, d.nama_barang
FROM distribusi_bapokting a
INNER JOIN
perusahaan b
ON a.id_perusahaan=b.id
INNER JOIN
bidang_usaha c
ON a.id_bidang_usaha=c.id
INNER JOIN
jenis_barang d
ON a.id_jenis_barang=d.id
WHERE a.deleted_at IS NULL;