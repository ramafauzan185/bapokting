CREATE OR REPLACE VIEW view_perusahaan AS
SELECT a.*,
TRIM(SUBSTRING_INDEX(koordinat, ',', 1)) as latitude, TRIM(SUBSTRING_INDEX(koordinat, ',', -1)) as longitude,
b.kode_pemilik, b.nik, b.nama_pemilik, b.jenis_kelamin, b.alamat, b.nomor_handphone, timestampdiff(YEAR, b.tanggal_lahir, CURRENT_DATE()) as umur, timestampdiff(YEAR, b.tanggal_join, CURRENT_DATE()) as lama_usaha,
c.kode_user, c.name, c.email, c.status as status_user,
d.kode_kelurahan, d.nama_kelurahan, d.id_kecamatan, d.kode_kecamatan, d.nama_kecamatan
FROM perusahaan a
INNER JOIN
pemilik b
ON a.id_pemilik=b.id
INNER JOIN
users c
ON a.id_user=c.id
INNER JOIN
view_kelurahan d
ON a.id_kelurahan=d.id
WHERE a.deleted_at IS NULL;