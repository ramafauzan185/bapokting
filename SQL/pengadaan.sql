CREATE OR REPLACE VIEW view_pengadaan AS
SELECT a.*,
b.kode_sumber_pembelian, b.nama_sumber_pembelian,
c.kode_lokasi_pembelian, c.nama_lokasi_pembelian,
d.kode_satuan, d.satuan, d.nama_satuan
FROM pengadaan a
INNER JOIN
sumber_pembelian b
ON a.id_sumber_pembelian=b.id
INNER JOIN
lokasi_pembelian c
ON a.id_lokasi_pembelian=c.id
INNER JOIN
satuan d
ON a.id_satuan=d.id
WHERE a.deleted_at IS NULL