CREATE OR REPLACE VIEW view_perusahaan_bidang_usaha AS
SELECT a.*,
b.kode_jenis_barang, b.id_bidang_usaha, b.nama_barang, b.kode_bidang_usaha, b.kode_kbli, b.nama_kbli, b.jenis_barang
FROM perusahaan_bidang_usaha a
INNER JOIN
view_jenis_barang b
ON a.id_jenis_barang=b.id
WHERE a.deleted_at IS NULL;