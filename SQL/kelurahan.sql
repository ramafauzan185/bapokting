CREATE OR REPLACE VIEW view_kelurahan AS
SELECT a.*,
b.kode_kecamatan, b.nama_kecamatan
FROM kelurahan a
INNER JOIN
kecamatan b
ON a.id_kecamatan=b.id
WHERE a.deleted_at IS NULL;