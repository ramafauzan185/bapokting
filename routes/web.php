<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::resource('users', \App\Http\Controllers\UserController::class);

Route::put('/users/{user}/status', ['uses' => 'App\Http\Controllers\UserController@status', 'as' => 'users.status']);

Route::get('/user/ubah-password', ['uses' => 'App\Http\Controllers\UserController@ubahPassword', 'as' => 'users.ubah-password']);

Route::put('/user/ubah-password-proses', ['uses' => 'App\Http\Controllers\UserController@ubahPasswordProses', 'as' => 'users.ubah-password-proses']);

Route::resource('bidang-usaha', \App\Http\Controllers\BidangUsahaController::class);

Route::resource('jenis-barang', \App\Http\Controllers\JenisBarangController::class);

Route::get('/jenis-barangs/get-komoditi-by-jenis-barang', ['uses' => 'App\Http\Controllers\JenisBarangController@getKomoditiByJenisBarang', 'as' => 'jenis-barang.get-komoditi-by-jenis-barang']);

Route::resource('satuan', \App\Http\Controllers\SatuanController::class);

Route::resource('perusahaan', \App\Http\Controllers\PerusahaanController::class);

Route::get('/perusahaans/all', ['uses' => 'App\Http\Controllers\PerusahaanController@all', 'as' => 'perusahaan.all']);

Route::resource('sumber-pembelian', \App\Http\Controllers\SumberPembelianController::class);

Route::resource('lokasi-pembelian', \App\Http\Controllers\LokasiPembelianController::class);

Route::resource('konsumen', \App\Http\Controllers\KonsumenController::class);

Route::resource('asal-konsumen', \App\Http\Controllers\AsalKonsumenController::class);

Route::resource('distribusi-bapokting', \App\Http\Controllers\DistribusiBapoktingController::class);

Route::get('/distribusi-bapokting/{distribusi_bapokting}/aktivitas', ['uses' => 'App\Http\Controllers\DistribusiBapoktingController@aktivitas', 'as' => 'distribusi-bapokting.aktivitas']);

Route::get('/distribusi-bapoktings/cek-data-sama', ['uses' => 'App\Http\Controllers\DistribusiBapoktingController@cekDataSama', 'as' => 'distribusi-bapokting.cek-data-sama']);

Route::resource('pengadaan', \App\Http\Controllers\PengadaanController::class);

Route::resource('pemasaran', \App\Http\Controllers\PemasaranController::class);

Route::resource('slider', \App\Http\Controllers\SliderController::class);

Route::get('/kelurahan/by-kecamatan', ['uses' => 'App\Http\Controllers\KelurahanController@showByKecamatan', 'as' => 'kelurahan.by-kecamatan']);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home/grafik-status-kepemilikan-toko-per-komoditi', [App\Http\Controllers\HomeController::class, 'showGrafikStatusKepemilikanTokoPerKomoditi'])->name('home.grafik-status-kepemilikan-toko-per-komoditi');

Route::get('/home/grafik-skala-usaha-per-komoditi', [App\Http\Controllers\HomeController::class, 'showGrafikSkalaUsahaPerKomoditi'])->name('home.grafik-skala-usaha-per-komoditi');

Route::get('/home/grafik-pengadaan-by-sumber-pembelian-per-komoditi', [App\Http\Controllers\HomeController::class, 'showGrafikPengadaanBySumberPembelianPerKomoditi'])->name('home.grafik-pengadaan-by-sumber-pembelian-per-komoditi');

Route::get('/home/grafik-pengadaan-by-lokasi-pembelian-per-komoditi', [App\Http\Controllers\HomeController::class, 'showGrafikPengadaanByLokasiPembelianPerKomoditi'])->name('home.grafik-pengadaan-by-lokasi-pembelian-per-komoditi');

Route::get('/home/grafik-pemasaran-by-konsumen-per-komoditi', [App\Http\Controllers\HomeController::class, 'showGrafikPemasaranByKonsumenPerKomoditi'])->name('home.grafik-pemasaran-by-konsumen-per-komoditi');

Route::get('/home/grafik-pemasaran-by-asal-konsumen-per-komoditi', [App\Http\Controllers\HomeController::class, 'showGrafikPemasaranByAsalKonsumenPerKomoditi'])->name('home.grafik-pemasaran-by-asal-konsumen-per-komoditi');

Route::get('/home/grafik-pengadaan-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikPengadaanPerKomoditiDetail'])->name('home.grafik-pengadaan-per-komoditi-detail');

Route::get('/home/grafik-pemasaran-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikPemasaranPerKomoditiDetail'])->name('home.grafik-pemasaran-per-komoditi-detail');

Route::get('/home/grafik-jenis-kelamin-pedagang-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikJenisKelaminPedagangPerKomoditiDetail'])->name('home.grafik-jenis-kelamin-pedagang-per-komoditi-detail');

Route::get('/home/grafik-rerata-lama-usaha-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikRerataLamaUsahaPerKomoditiDetail'])->name('home.grafik-rerata-lama-usaha-per-komoditi-detail');

Route::get('/home/grafik-status-kepemilikan-toko-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikStatusKepemilikanTokoPerKomoditiDetail'])->name('home.grafik-status-kepemilikan-toko-per-komoditi-detail');

Route::get('/home/grafik-skala-usaha-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikSkalaUsahaPerKomoditiDetail'])->name('home.grafik-skala-usaha-per-komoditi-detail');

Route::get('/home/grafik-pengadaan-by-sumber-pembelian-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikPengadaanBySumberPembelianPerKomoditiDetail'])->name('home.grafik-pengadaan-by-sumber-pembelian-per-komoditi-detail');

Route::get('/home/grafik-pengadaan-by-lokasi-pembelian-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikPengadaanByLokasiPembelianPerKomoditiDetail'])->name('home.grafik-pengadaan-by-lokasi-pembelian-per-komoditi-detail');

Route::get('/home/grafik-pemasaran-by-konsumen-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikPemasaranByKonsumenPerKomoditiDetail'])->name('home.grafik-pemasaran-by-konsumen-per-komoditi-detail');

Route::get('/home/grafik-pemasaran-by-asal-konsumen-per-komoditi-detail', [App\Http\Controllers\HomeController::class, 'showGrafikPemasaranByAsalKonsumenPerKomoditiDetail'])->name('home.grafik-pemasaran-by-asal-konsumen-per-komoditi-detail');

Route::get('/home/map-koordinat-perusahaan', [App\Http\Controllers\HomeController::class, 'getPerusahaanByFilter'])->name('home.map-koordinat-perusahaan');

Route::get('/home/tabel-perusahaan', [App\Http\Controllers\HomeController::class, 'getPerusahaanByFilter'])->name('home.tabel-perusahaan');

Route::get('/home/tabel-stok-barang', [App\Http\Controllers\HomeController::class, 'getStokByJenisBarang'])->name('home.tabel-stok-barang');

Route::get('/home/grafik-jenis-kelamin-pedagang-by-jenis-produk', [App\Http\Controllers\HomeController::class, 'showGrafikJenisKelaminPedagangByJenisProduk'])->name('home.grafik-jenis-kelamin-pedagang-by-jenis-produk');
