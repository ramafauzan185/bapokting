-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 17, 2021 at 06:24 AM
-- Server version: 10.2.40-MariaDB
-- PHP Version: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pelaporan_bapokting`
--

-- --------------------------------------------------------

--
-- Table structure for table `asal_konsumen`
--

CREATE TABLE IF NOT EXISTS `asal_konsumen` (
  `id` bigint(20) NOT NULL,
  `kode_asal_konsumen` varchar(100) NOT NULL,
  `nama_asal_konsumen` varchar(50) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `asal_konsumen`
--

INSERT INTO `asal_konsumen` (`id`, `kode_asal_konsumen`, `nama_asal_konsumen`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$K.ItikXzbWuIZ0X20gZgEudY6xADO3QlOWrDtpYTEGwclSVInnwPu', 'Dalam Kelurahan', 1, 1, NULL, '2021-09-09 19:32:32', '2021-11-11 07:32:16', NULL),
(2, '$2y$10$hArAPVIwhvoy7.s2EqReKeN6AI6Dnqbu25Yyaklfz5jtpr8jEqqp2', 'Luar Kelurahan Dalam Kecamatan', 1, 1, NULL, '2021-09-09 19:32:49', '2021-11-11 07:32:03', NULL),
(3, '$2y$10$jnCidyKjBvAJx3Pe.rXcDOcMo4afbTCVdld4xfygctgPmVL3IeMZu', 'Dalam Negeri 2 3', 1, 1, 1, '2021-09-09 19:33:02', '2021-09-09 19:40:28', '2021-09-09 19:40:28'),
(4, '$2y$10$FOk1YSmtAlzOmdrG9Wb4fe2kLqh.wpbMjN7XrKQzfg9ov3vs/IdxK', 'Luar Kecamatan Dalam Kota Bogor', 1, 1, NULL, '2021-10-05 14:24:58', '2021-11-11 07:31:18', NULL),
(5, '$2y$10$Hu.wN.VK7C/fD4sdrciBpepl9CDpOYMSwKvONOeMB/q0f187xoGY6', 'Luar Kota Bogor', 1, 1, NULL, '2021-10-05 14:25:08', '2021-11-11 07:30:52', NULL),
(6, '$2y$10$8yN5DbifCiLcRQG1IOx2JeIIATGpUc3ys2GQAQaDaA47hbBk4aPhi', 'Luar Provinsi Jawa Barat', 1, 1, NULL, '2021-10-05 14:25:25', '2021-11-11 07:32:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bidang_usaha`
--

CREATE TABLE IF NOT EXISTS `bidang_usaha` (
  `id` bigint(20) NOT NULL,
  `kode_bidang_usaha` varchar(100) NOT NULL,
  `kode_kbli` int(11) NOT NULL,
  `nama_kbli` varchar(255) NOT NULL,
  `jenis_barang` enum('pokok','penting') NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bidang_usaha`
--

INSERT INTO `bidang_usaha` (`id`, `kode_bidang_usaha`, `kode_kbli`, `nama_kbli`, `jenis_barang`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$o86.9IwTZmTLWDn.c1KebuHYCQX9QjJOXrqFDJ3O2DOk6hq9jD8Dq', 46311, 'Perdagangan Besar Beras', 'pokok', 1, 1, NULL, '2021-09-09 03:49:39', '2021-10-05 02:01:55', NULL),
(2, '$2y$10$cdv/VQfc.Vy7UcXfzyrgAe0le6/awu/wGj.MQnRFLG9y3EUXgJBoS', 2432426, 'ewer iewpripwe uweyrieywi djhkhkds', 'pokok', 1, 1, 1, '2021-09-09 03:52:14', '2021-09-09 04:45:00', '2021-09-09 04:45:00'),
(3, '$2y$10$M8z6odfqqb5zmOxRaArLGuyrPWztr803ZjGlsg7z1CDSmT1./J4Um', 243242225, 'dsdsd222 irpwoierpwoe x,cmn,xmldsksjs', 'penting', 1, 1, 1, '2021-09-09 03:54:52', '2021-09-09 04:43:45', '2021-09-09 04:43:45'),
(4, '$2y$10$/DWEv6vk0Q/CW2fHDHhYZ.rrqWAl/OvpeX7yHmySVAQesdEia4Hv2', 46631, 'Perdagangan Besar Barang Logam untuk Bahan Konstruksi', 'penting', 1, 1, NULL, '2021-09-09 07:06:11', '2021-10-05 02:02:30', NULL),
(5, '$2y$10$SGyimXucigAPRT4sbpU.Nu8aQnMrbzke7mg8YDeVVrMV9ovF2MUsG', 46201, 'Perdagangan Besar Padi dan Palawija', 'pokok', 1, NULL, NULL, '2021-10-05 02:02:56', '2021-10-05 02:02:56', NULL),
(6, '$2y$10$RraCMdGBNc5ZeKZd20kLne5jQxfZaM9adtTKUB0x160jnFpuuu2vK', 46315, 'Perdagangan Besar Minyak dan Lemak Nabati', 'pokok', 1, 1, NULL, '2021-10-05 02:03:18', '2021-10-05 03:01:48', NULL),
(7, '$2y$10$syPMUlpxPNCfYdU0yOnfc.M9sBfe5E55eZ4uwbJAHI8IiWekBlyza', 46321, 'Perdagangan Besar Daging Sapi dan Daging Sapi Olahan', 'pokok', 1, NULL, NULL, '2021-10-05 02:03:43', '2021-10-05 02:03:43', NULL),
(8, '$2y$10$eGC.rI/yAoeG7PeoSelXGuE4juFVewUsrjFS/PibgXcPUJyf2o8Aa', 46322, 'Perdagangan Besar Daging Ayam dan Daging Ayam Olahan', 'pokok', 1, NULL, NULL, '2021-10-05 02:04:12', '2021-10-05 02:04:12', NULL),
(9, '$2y$10$s5mN9aEyjX8MyCUKv4tsueWpl6lug1ItC6VILb3.k/fDmZz7knDEm', 46325, 'Perdagangan Besar Telur dan Hasil Olahan Telur', 'pokok', 1, NULL, NULL, '2021-10-05 02:04:44', '2021-10-05 02:04:44', NULL),
(10, '$2y$10$o/1ObvhepGqCj35nrHBm8efPhfFgbLVFrg7OfLJainYL1IkonpYdu', 46331, 'Perdagangan Besar Gula, Coklat dan Kembang  Gula', 'pokok', 1, NULL, NULL, '2021-10-05 02:05:09', '2021-10-05 02:05:09', NULL),
(11, '$2y$10$1lUlaNerAvuDNp7llOOLLuXTsyusuW1D4C7LE92/MfkGEeJXIaF/S', 46206, 'Perdagangan Besar Hasil Perikanan', 'pokok', 1, NULL, NULL, '2021-10-05 02:06:55', '2021-10-05 02:06:55', NULL),
(12, '$2y$10$dlAgeP9C7VuqtFZFK3ZFFO0rLqtIw44Q6i/wRBiD4Zm9c.c0tZEZG', 46319, 'Perdagangan Besar Bahan Makanan dan Minuman Hasil Pertanian Lainnya', 'pokok', 1, NULL, NULL, '2021-10-05 02:49:46', '2021-10-05 02:49:46', NULL),
(13, '$2y$10$T74OCGPvwmIj8Vzhw.fpEO.XQZCbBfINb/Z7.XL5tfhRNhBdv0iq2', 46339, 'Perdagangan Besar Makanan dan Minuman Lainnya', 'pokok', 1, NULL, NULL, '2021-10-05 02:50:12', '2021-10-05 02:50:12', NULL),
(14, '$2y$10$CrM41fGQn4sEkiaBTAwM6OFOdm5ToU6DjPMlKIcoDg/3TCSv9PiGG', 46209, 'Perdagangan Besar Hasil Pertanian dan Hewan Hidup Lainnya', 'pokok', 1, NULL, NULL, '2021-10-05 02:50:38', '2021-10-05 02:50:38', NULL),
(15, '$2y$10$iiw46JqPyWxh.9wyUqWH0.uPNAFjsOXm2IgBCMOxE5me6dtnSDB0.', 46633, 'Perdagangan Besar Genteng, Batu Bata, Ubin, dan Sejenisnya dari Tanah Liat, Kapur, Semen atau Kaca', 'penting', 1, NULL, NULL, '2021-10-05 02:51:32', '2021-10-05 02:51:32', NULL),
(16, '$2y$10$zedsGFFgBkaE/obHa5v8KOH2JjLPEt/hVshUOL/55Xf/rP87zfLIC', 46634, 'Perdagangan Besar Semen, Kapur, Pasir, dan Batu', 'penting', 1, NULL, NULL, '2021-10-05 02:51:57', '2021-10-05 02:51:57', NULL),
(17, '$2y$10$c.LlUgCIRM23WshwOp9reeqSzqXJgVnJOXI./lWSPemuGqsEm5KxC', 46636, 'Perdagangan Besar Bahan Konstruksi dari Kayu', 'penting', 1, NULL, NULL, '2021-10-05 02:52:31', '2021-10-05 02:52:31', NULL),
(18, '$2y$10$pT/8bWD.zimNSEfSGv4p2OldUay5uPNTh3D4zyaw8NgMTopuyz4/W', 46638, 'Perdagangan Besar berbagai Macam Material Bangunan', 'penting', 1, NULL, NULL, '2021-10-05 02:53:01', '2021-10-05 02:53:01', NULL),
(19, '$2y$10$vVNa74KcSGKsWDklcB7e9O/1XMERYFEurfNdsFD5GQvB14hpfw60e', 46639, 'Perdagangan Besar Bahan Konstruksi Lainnya', 'penting', 1, NULL, NULL, '2021-10-05 02:53:25', '2021-10-05 02:53:25', NULL),
(20, '$2y$10$G9uihUKbt1pk6kDZJwMeAuGdn2EYTfok6qaovHjmq7tR3Hs..a4h.', 46652, 'Perdagangan Besar Pupuk dan Agrokimia', 'penting', 1, NULL, NULL, '2021-10-05 02:53:52', '2021-10-05 02:53:52', NULL),
(21, '$2y$10$5QTbE19jviyOICygw8d1gOrcrGpwlsRYEO4dFTpKU08WXqnA22wcq', 46610, 'Perdagangan Besar Bahan Bakar Padat, Cair, Gas, dan Produk YBDI', 'penting', 1, NULL, NULL, '2021-10-05 02:54:21', '2021-10-05 02:54:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `distribusi_bapokting`
--

CREATE TABLE IF NOT EXISTS `distribusi_bapokting` (
  `id` bigint(20) NOT NULL,
  `kode_distribusi_bapokting` varchar(100) NOT NULL,
  `id_perusahaan` bigint(20) NOT NULL,
  `id_bidang_usaha` bigint(20) NOT NULL,
  `id_jenis_barang` bigint(20) NOT NULL,
  `tahun` int(11) NOT NULL,
  `bulan` int(11) NOT NULL,
  `skala_usaha` enum('distributor','sub_distributor','agen') NOT NULL,
  `status_laporan` enum('draft','send') NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `distribusi_bapokting`
--

INSERT INTO `distribusi_bapokting` (`id`, `kode_distribusi_bapokting`, `id_perusahaan`, `id_bidang_usaha`, `id_jenis_barang`, `tahun`, `bulan`, `skala_usaha`, `status_laporan`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '$2y$10$N7P.zVz31ZE894fpHKhT4OE.rUNoBp7.n9Mw670MfTCwuq4kKzxLG', 1, 9, 2, 2021, 8, 'distributor', 'send', 12, 12, NULL, '2021-09-13 12:12:52', '2021-10-05 09:56:21', NULL),
(3, '$2y$10$DJ8uquFgn1E78QoyD7EO1uF/AT35.8/K8DclwgPrwNUJfm6QMJjGG', 1, 9, 2, 2021, 9, 'distributor', 'send', 12, 12, NULL, '2021-09-13 12:53:24', '2021-10-05 09:55:41', NULL),
(9, '$2y$10$9ptrmvY9haGLamny61.j4.IG1XowaVjx2nHWwqfQoHPKowvzLn4Bq', 1, 1, 1, 2021, 10, 'agen', 'send', 12, NULL, NULL, '2021-10-16 08:29:37', '2021-10-16 08:29:37', NULL),
(12, '$2y$10$wz.b30nWF0H/vr8x.miuMufvjgWJ812o4jJzebbLS3zQJEHllNWm2', 1, 1, 1, 2021, 11, 'agen', 'send', 12, NULL, NULL, '2021-11-07 04:17:59', '2021-11-07 04:17:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE IF NOT EXISTS `jenis_barang` (
  `id` bigint(20) NOT NULL,
  `kode_jenis_barang` varchar(100) NOT NULL,
  `id_bidang_usaha` bigint(20) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`id`, `kode_jenis_barang`, `id_bidang_usaha`, `nama_barang`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$C62ZEh/UT6CC4pxoOU17J.pZxw0O4gaNb8nfP47SjJBtY.yvU0Qta', 1, 'Beras', 1, 1, 1, '2021-09-09 07:05:45', '2021-10-05 02:56:01', NULL),
(2, '$2y$10$PZnTz3IIJde7sztH555HVOwUePZyvyfVXKQqjJIrjPb95.2rO5EVC', 9, 'Telur Ayam Ras', 1, 1, 1, '2021-09-09 07:18:37', '2021-10-08 06:03:11', NULL),
(3, '$2y$10$7fJl4eKBbFNelijwafNqqOesvwhGYQ2YhREajgRbG/jd3xOuqdkdi', 7, 'Daging Sapi', 1, 1, NULL, '2021-10-04 09:28:59', '2021-10-05 02:56:46', NULL),
(4, '$2y$10$lrENdRbFGzqI8Rr1RQljGO4hKBt8gBWWGdLmCpVtDoI.d.gdlAM6C', 8, 'Daging Ayam Ras', 1, 1, NULL, '2021-10-04 09:29:06', '2021-10-08 06:02:58', NULL),
(5, '$2y$10$v9tkY.Sm2yG1k6SaqGoedu.MxUl9zvLKSDnlDhW0bMiISJpXgD3Ii', 14, 'Sembako', 1, 1, 1, '2021-10-04 09:29:23', '2021-10-08 06:23:43', '2021-10-08 06:23:43'),
(6, '$2y$10$ztL9pIytmcPDW6ZgHZLnUePxvEAkx2UQyt3RoTQC0FI7nSbzdwuHO', 16, 'Pasir', 1, 1, 1, '2021-10-04 09:29:29', '2021-10-08 06:25:22', '2021-10-08 06:25:22'),
(7, '$2y$10$a4KbwOjbyP1MTruParZVK.IK7jJwuIzz.JYoROfn5d8Qme/JACH5m', 21, 'Tabung Gas Elpiji', 1, 1, 1, '2021-10-04 09:29:38', '2021-10-08 06:13:44', '2021-10-08 06:13:44'),
(8, '$2y$10$1T0U9JmCuBRGQPlvc5E.5.qilMtJDCwXL1g2M.GgFHeR5vhzUkLG2', 21, 'Gas Elpiji 3 Kg', 1, 1, NULL, '2021-10-04 09:29:59', '2021-10-08 06:11:05', NULL),
(9, '$2y$10$wEDY7iKnHKREAW8sex8On.ft/RGLMCe829XvPqHGZqi7ZZFka6oaS', 13, 'Aqua Gelas & Galon', 1, 1, 1, '2021-10-04 09:30:13', '2021-10-08 06:23:48', '2021-10-08 06:23:48'),
(10, '$2y$10$pRMppSrR0CG.omT8mLpraOnavrYVqKmskIjX6KUjvMtkct5S8qoZi', 6, 'Minyak Kelapa', 1, 1, 1, '2021-10-04 09:30:30', '2021-10-08 04:00:47', '2021-10-08 04:00:47'),
(11, '$2y$10$FSwO79YcsJe.vFfj/FR3o.b.ICRYcU2S/LQ9urfc3P826K05f2mkq', 11, 'Ikan Asin', 1, 1, 1, '2021-10-04 09:30:36', '2021-10-08 06:06:38', '2021-10-08 06:06:38'),
(12, '$2y$10$INedpblTi7qSvv4tqe.iIOWvzbfEVu20QrbxikTWfk2YcyJVKYgFe', 18, 'Bahan Bangunan', 1, 1, 1, '2021-10-04 09:30:43', '2021-10-08 06:26:03', '2021-10-08 06:26:03'),
(13, '$2y$10$fXWGtTnw973uFspcCANVpOXTYdMNzO6e/HKDAwFNDEuaChSsPEi0W', 21, 'LPG', 1, 1, 1, '2021-10-04 09:30:55', '2021-10-08 06:13:49', '2021-10-08 06:13:49'),
(14, '$2y$10$ugyI2CrnSD/5qqdPgdn59O53YHbPBr4JTwsPMo8fxhtRxV/ssXo6G', 14, 'Tepung Terigu', 1, 1, NULL, '2021-10-04 09:31:01', '2021-10-08 06:02:24', NULL),
(15, '$2y$10$ZkpfB8QYJhZ6DXtX1fuuE.McoHn9QMYq/6by5pEfTmneUrJLqiLmW', 10, 'Gula', 1, 1, NULL, '2021-10-04 09:31:09', '2021-10-08 03:58:01', NULL),
(16, '$2y$10$y/9QD3sscuu/VYeTGGIu0O5HVA8lH126SoqF2rIqckuA.L9bBQSfu', 14, 'Kedelai Bahan Baku Tahu dan Tempe', 1, 1, NULL, '2021-10-04 09:31:24', '2021-10-08 03:57:12', NULL),
(17, '$2y$10$.D8nWiKE6rVqYaLeGOYK4uXkyxMtr9zfBbafjSvsRnKA3G6pmneVO', 6, 'Minyak Goreng', 1, 1, NULL, '2021-10-04 09:31:47', '2021-10-08 03:58:21', NULL),
(18, '$2y$10$fJ/0ivxZyYib89Kf2xamq.S0tZGGSlu3z7iHTKQzhfVscp.CYXbC.', 14, 'Bawang Merah', 1, 1, NULL, '2021-10-04 09:31:54', '2021-10-05 03:02:09', NULL),
(19, '$2y$10$1wdLeNN0R18V5/SbNEDWruq.G2zaD.84AHcSCmko8ZJXuskvqCEQm', 9, 'Telur', 1, 1, 1, '2021-10-04 09:32:10', '2021-10-08 06:04:55', '2021-10-08 06:04:55'),
(20, '$2y$10$HsnZgZ9TIN2cmMsP05XcnOzHflTDjAqaJW2BgPmVpTtdMyYyEigq2', 21, 'Hasil Bumi', 1, 1, 1, '2021-10-04 09:32:17', '2021-10-08 06:26:43', '2021-10-08 06:26:43'),
(21, '$2y$10$6KiXP0ju0LI4f8xDvMhCieIsqULVYxH/W6SzL4gcv63AGyg5/AZ5m', 14, 'Cabe', 1, 1, NULL, '2021-10-04 09:32:32', '2021-10-08 03:57:26', NULL),
(22, '$2y$10$f980U0DwBP6hp9r64IE.RuoNgpoxWiBgUP4rSiK.R5HtdAcEIZChG', 14, 'Bawang Putih', 1, 1, 1, '2021-10-04 09:32:39', '2021-10-08 06:30:26', '2021-10-08 06:30:26'),
(23, '$2y$10$3kVSL1MNR3MWHYBP6gzXMOb/5i3/g6rTEFNQgcxLu6nobhQPLMPpW', 16, 'Semen', 1, 1, NULL, '2021-10-04 09:32:56', '2021-10-08 06:15:07', NULL),
(24, '$2y$10$rGmigjxIx6Qx1GyjV2FR7.4TTe/9YvA6np/HILZafRsh4QXeae1vO', 11, 'Ikan Segar', 1, 1, NULL, '2021-10-04 14:15:03', '2021-10-05 03:03:25', NULL),
(25, '$2y$10$hBJVmWTqw3EyDMlT.b.ia.8eue3EgsIcHQ1ytVOOweT.u9trpFk3y', 20, 'Benih', 1, 1, NULL, '2021-10-08 06:09:15', '2021-10-08 06:09:44', NULL),
(26, '$2y$10$Pgejiqxo.sIJAn9A132sBuCknTqMSfhQJBq7SX0mU4r1rgLZUVTU6', 20, 'Pupuk', 1, NULL, NULL, '2021-10-08 06:10:22', '2021-10-08 06:10:22', NULL),
(27, '$2y$10$fSx0Yk3PADvtGz5WYiqLiuvRgokk3C244ZxIkNKxcApXEXzakbsbi', 17, 'Triplek', 1, 1, NULL, '2021-10-08 06:14:41', '2021-10-08 06:15:55', NULL),
(28, '$2y$10$xwZjsrNGjOzXexXX5dYByuUTgnaquaB1n9C1xE.pXWlsEKr/fvVQu', 19, 'Besi Baja Konstruksi', 1, NULL, NULL, '2021-10-08 06:15:45', '2021-10-08 06:15:45', NULL),
(29, '$2y$10$uGE9stzUWvahHyNmGWKa0ufUJFBkzliT3E.Wv0SKuR9pjvaoRgDuK', 18, 'Baja Ringan', 1, NULL, NULL, '2021-10-08 06:16:39', '2021-10-08 06:16:39', NULL),
(30, '$2y$10$fQEGPuEeW1R./A7hiM5rOeyXiJXg34vi8ovz8ujD.KpiRTff2.In2', 14, 'Bawang Putih', 1, NULL, NULL, '2021-11-08 08:01:19', '2021-11-08 08:01:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE IF NOT EXISTS `kecamatan` (
  `id` bigint(20) NOT NULL,
  `kode_kecamatan` varchar(100) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `id_kota_kab` bigint(20) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=327107 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `kode_kecamatan`, `nama_kecamatan`, `id_kota_kab`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(327101, '$2y$10$5YXOZMvzJuAxbq.Im6Jlc.VaQDe1gXOW/5HYBugUkW9VmhLD5dddi', 'Bogor Selatan', 3271, 1, NULL, NULL, '2021-08-13 03:42:57', NULL, NULL),
(327102, '$2y$10$Dew9Uf9EoAHrzxnY7GhAX.Wc6gP7NnAP1JW6jdDBa/2fjlG0rVg.K', 'Bogor Timur', 3271, 1, NULL, NULL, '2021-08-13 03:42:58', NULL, NULL),
(327103, '$2y$10$XHawfuHb4tGeO0SlaPJq2uik6XeJa2ZJIRL4DfHS.0c3lDzubByEG', 'Bogor Tengah', 3271, 1, NULL, NULL, '2021-08-13 03:42:58', NULL, NULL),
(327104, '$2y$10$Nq1acze/MozZ5t5OU41QSuSgCGErIKCX1lSvLULtGHb5/ko9XAjJu', 'Bogor Barat', 3271, 1, NULL, NULL, '2021-08-13 03:42:58', NULL, NULL),
(327105, '$2y$10$fgLs1cXagouoI61TaxJELuSkgP6epfpaGWc5lka78DXHnYA9frm5K', 'Bogor Utara', 3271, 1, NULL, NULL, '2021-08-13 03:42:58', NULL, NULL),
(327106, '$2y$10$/eLG21Yv5RsaRtlks07sE.qkvQt.k8gV1FinEW11VqKwAMlfIl.nm', 'Tanah Sareal', 3271, 1, NULL, NULL, '2021-08-13 03:42:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE IF NOT EXISTS `kelurahan` (
  `id` bigint(20) NOT NULL,
  `kode_kelurahan` varchar(100) NOT NULL,
  `nama_kelurahan` varchar(50) NOT NULL,
  `id_kecamatan` bigint(20) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3271061012 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `kode_kelurahan`, `nama_kelurahan`, `id_kecamatan`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3271011001, '$2y$10$HMe2HhJP86vX.ZeJixJ26.LOa0r1Jh.ODyF0X4vCzV4V7PRRJaAsS', 'Batu Tulis', 327101, 1, NULL, NULL, '2021-08-13 03:43:28', NULL, NULL),
(3271011002, '$2y$10$c/bGgijxScBejxHZ/NBzCORtkX7RZkFV0Q8L9CuTuZRcfVSEu8/OW', 'Bondongan', 327101, 1, NULL, NULL, '2021-08-13 03:43:28', NULL, NULL),
(3271011003, '$2y$10$9TS4leISIPIoIAt6F2JQJ.cPdB1i4qz7uR9x0dNINMup1gEJs7VMu', 'Empang', 327101, 1, NULL, NULL, '2021-08-13 03:43:28', NULL, NULL),
(3271011004, '$2y$10$AyQVgxSa4OqY7H1LcYxZteXDIB3bqRmQmtcdk.HeaBipr9b/ft4c2', 'Lawanggintung', 327101, 1, NULL, NULL, '2021-08-13 03:43:29', NULL, NULL),
(3271011005, '$2y$10$99/516z6eFpa7kFMj1.ZaOhw4id8HiaMyjOGqQF2cC71gWWC574MC', 'Pamoyanan', 327101, 1, NULL, NULL, '2021-08-13 03:43:29', NULL, NULL),
(3271011006, '$2y$10$Hz05zpCIDHtbHYyAPNkr9OeNpWSwL5wHsUrGby9KgbOa.8UrdxQbK', 'Ranggamekar', 327101, 1, NULL, NULL, '2021-08-13 03:43:29', NULL, NULL),
(3271011007, '$2y$10$ULzdR1c6fmM.DLUoQHun3.qU0r8mYHNZfo.6Az321CVlK/tKedyOm', 'Mulyaharja', 327101, 1, NULL, NULL, '2021-08-13 03:43:29', NULL, NULL),
(3271011008, '$2y$10$L/YPBJruNFUOH6yuMrdMYO2z20AK5bUktItdBSalESdWMmcAZ1zMW', 'Cikaret', 327101, 1, NULL, NULL, '2021-08-13 03:43:29', NULL, NULL),
(3271011009, '$2y$10$BsjJoM7ixv5k59tbPLb69.71JidtshBJCO09/yBZYAprzDT2oiXs6', 'Bojongkerta', 327101, 1, NULL, NULL, '2021-08-13 03:43:30', NULL, NULL),
(3271011010, '$2y$10$86h.AwkaG48mp5p8Go9X/uidEjWpyWFU5sm8k6bR2Q7HMS/wfuEyq', 'Rancamaya', 327101, 1, NULL, NULL, '2021-08-13 03:43:30', NULL, NULL),
(3271011011, '$2y$10$pa9M0J2sYqgWUQFCwGhRgeMBxFdK/4l1Pj.yC2Nkq9ZIa8CQCRmwC', 'Kertamaya', 327101, 1, NULL, NULL, '2021-08-13 03:43:30', NULL, NULL),
(3271011012, '$2y$10$7wpaEy13pHw/4KO/lzSUPO2jVOXVAt4wPGk9K6c2bOaiFzEI6M7r6', 'Harjasari', 327101, 1, NULL, NULL, '2021-08-13 03:43:30', NULL, NULL),
(3271011013, '$2y$10$AOgAp0gGHjvcKq75b6RDp.ypO3D4eC7enI5biwBNXwCBW/EDxiZa.', 'Muarasari', 327101, 1, NULL, NULL, '2021-08-13 03:43:30', NULL, NULL),
(3271011014, '$2y$10$G5myQWtV9eLcOBF1kp5FQOKDhXWiiq6ZAcU8vlkMlxsRyXZqCssAG', 'Genteng', 327101, 1, NULL, NULL, '2021-08-13 03:43:30', NULL, NULL),
(3271011015, '$2y$10$BoeCg6ipPv3o1rk1PH/dqOtPdRsOeVNPLg8bMZeM9uLGXswjYoVF2', 'Pakuan', 327101, 1, NULL, NULL, '2021-08-13 03:43:31', NULL, NULL),
(3271011016, '$2y$10$KdEm0hpZSg3TYUg1COyyv.6isWGGG83ODK8F9MOhWP6vV5fUmZla6', 'Cipaku', 327101, 1, NULL, NULL, '2021-08-13 03:43:31', NULL, NULL),
(3271021001, '$2y$10$nG23uz3y9ds.BqPz2rH0CupzmK11sHQWfqmKU2rTKhrp091WuAAVa', 'Sukasari', 327102, 1, NULL, NULL, '2021-08-13 03:43:31', NULL, NULL),
(3271021002, '$2y$10$8ja2ALZvIOGKP4cQT70VXuYcjTHEBX9MfLOsBCAz1rM/SvUkgnBFS', 'Baranangsiang', 327102, 1, NULL, NULL, '2021-08-13 03:43:31', NULL, NULL),
(3271021003, '$2y$10$LuGvi7LUnTTz0wjg5Eih3OIPFCwl1wy0vuRYGU5ATkOFI2B0ykH2G', 'Katulampa', 327102, 1, NULL, NULL, '2021-08-13 03:43:32', NULL, NULL),
(3271021004, '$2y$10$vJBoerbe/vY0FDscq52H9egLOtZrlWjPABY3o0qocFTZQoZdcafym', 'Sindangsari', 327102, 1, NULL, NULL, '2021-08-13 03:43:32', NULL, NULL),
(3271021005, '$2y$10$clOS1ne1iZkCF.aN7RPrJ.Dr9U4HSQIb0e/5FMYidRN5.xIBmCXya', 'Sindangrasa', 327102, 1, NULL, NULL, '2021-08-13 03:43:32', NULL, NULL),
(3271021006, '$2y$10$DV29Nk7bzHS6tz7A4UuGjumJYs17GAt/bXNwVNDNKWy6jHUa3HIrO', 'Tajur', 327102, 1, NULL, NULL, '2021-08-13 03:43:32', NULL, NULL),
(3271031001, '$2y$10$45X8U7IVoYdTI5w0sQkfYu5DGsdi75A0vxKz6O8RGFO/AoWrd2fmu', 'Gudang', 327103, 1, NULL, NULL, '2021-08-13 03:43:32', NULL, NULL),
(3271031002, '$2y$10$e6KyTO.PVq0DkfcAcSjVD.GG0NYNu7IJXMSUyfS2RDdo8TnpXd8jy', 'Paledang', 327103, 1, NULL, NULL, '2021-08-13 03:43:32', NULL, NULL),
(3271031003, '$2y$10$jTJN3vXv4rfpI49.U34EfepmtYjbYW0DgRl9OWbkJ1objmgi78wve', 'Pabaton', 327103, 1, NULL, NULL, '2021-08-13 03:43:33', NULL, NULL),
(3271031004, '$2y$10$Vdt4BSdTY4qLzYhp619V9ekbtlHL9dueftFMDbpupOcj.VRLkoMR.', 'Cibogor', 327103, 1, NULL, NULL, '2021-08-13 03:43:33', NULL, NULL),
(3271031005, '$2y$10$Rwe1bCpRAt7EXBN6W.AbLeDpniBOxcWeiwiwaY0ad9WedroCJq0Zm', 'Babakan', 327103, 1, NULL, NULL, '2021-08-13 03:43:34', NULL, NULL),
(3271031006, '$2y$10$SItbrYDF3GZos.LVm.kRD.DCw/.7SOVm/dMJKWbdIa.Pz6.JWeFMy', 'Sempur', 327103, 1, NULL, NULL, '2021-08-13 03:43:34', NULL, NULL),
(3271031007, '$2y$10$U8o18dU6Kz/Ow9KF9fz4e.5zRHdXFRGcdfxM5CkhTXtalMiuqU1am', 'Tegallega', 327103, 1, NULL, NULL, '2021-08-13 03:43:34', NULL, NULL),
(3271031008, '$2y$10$Mc3.uV.yv.xYBcfrXOPV5.byzy29KLPaA5GYQ26EW4GHkLKsVdxW2', 'Babakan Pasar', 327103, 1, NULL, NULL, '2021-08-13 03:43:34', NULL, NULL),
(3271031009, '$2y$10$eRJ4n4YMvG/Y70OzNEJTYO.EB42fmq239CxIgdL2G39eVzkBMmzXi', 'Panaragan', 327103, 1, NULL, NULL, '2021-08-13 03:43:34', NULL, NULL),
(3271031010, '$2y$10$nBXV7aXMJs6ogPbkYqo10Oz7EQq2Z7eZ/jTM12vbw5MwCWSb1cx.K', 'Ciwaringin', 327103, 1, NULL, NULL, '2021-08-13 03:43:34', NULL, NULL),
(3271031011, '$2y$10$MiGaFhQZbUvBy41pbZIBEugmADNmKoqj4S/MCDg9voOQ4nWQo21aa', 'Kebon Kalapa', 327103, 1, NULL, NULL, '2021-08-13 03:43:34', NULL, NULL),
(3271041001, '$2y$10$y1iyz15pVTZzl/v27BsiXeI8nIxi1AoKOS/wIBXdnEFujYmFN9jfa', 'Menteng', 327104, 1, NULL, NULL, '2021-08-13 03:43:35', NULL, NULL),
(3271041002, '$2y$10$cNasOW.ZJMzbhdHppwm/1eChfN0foGOFRuQ.ylAvQLyIkmoheT4R6', 'Sindang Barang', 327104, 1, NULL, NULL, '2021-08-13 03:43:35', NULL, NULL),
(3271041003, '$2y$10$WA34rAmRFfeaPV2ib9V9weCkNh8BkP3XumLmIwPI4ZD5PopSDalaO', 'Bubulak', 327104, 1, NULL, NULL, '2021-08-13 03:43:35', NULL, NULL),
(3271041004, '$2y$10$eLOoEQoMWBA8tdSyV7q8ruRBCaX5IBbmw4PxRrDddX8UTrim8JkvC', 'Margajaya', 327104, 1, NULL, NULL, '2021-08-13 03:43:35', NULL, NULL),
(3271041005, '$2y$10$2UigBXuO4QftdYCX4QXO2ueNrWLL7lWze1H9YSvwdOp5u0gcOCp7C', 'Balumbang Jaya', 327104, 1, NULL, NULL, '2021-08-13 03:43:35', NULL, NULL),
(3271041006, '$2y$10$L6lYyF8ZPKsUXm1ydMdYNexI6gryvxf29v7k61zK8u7bhzG7jPP.i', 'Situ Gede', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041007, '$2y$10$jju5hc2Oti9wVItBUqBnBe9k46qLEiT7XMsDWDTyxJHR/Au1Igu02', 'Semplak', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041008, '$2y$10$IV2mvOWsv2VL6ZPd5XHNj./Z3iqlumbd8NZ4gUGQ5AgUtR3/vv7Hm', 'Cilendek Barat', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041009, '$2y$10$gjsfI2acbWcwa0zyEKHcJOE7LoCnSaF7GyiqTXmhFrRwIOf.iLgxi', 'Cilendek Timur', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041010, '$2y$10$0j/V8eRkidWWxgCrx3lvT..k/f/rwCcsaJhQ3jCSmqxWjCmIEvN8m', 'Curug Mekar', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041011, '$2y$10$PDZhfFHxAJqvgFzUsVto9uUa8Ajr8qo2Q7DER.E35AUcJLTCHjWMK', 'Curug', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041012, '$2y$10$kBzjJXcpFl.8qM3mOEc/sujWuAMBx1rq6pvxFp3yzAtp1mGN.GlmG', 'Pasir Jaya', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041013, '$2y$10$BUU8jF7fFpn4gI8YikT8Tu2psNa4YCZ8DuOtiNsrUGuUx6l82TZeO', 'Pasir Kuda', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041014, '$2y$10$ugEiBs5WfXCVELZG8lfogeV/OnzjMEwGtExjpocUuAeR41RvVon9m', 'Pasir Mulya', 327104, 1, NULL, NULL, '2021-08-13 03:43:36', NULL, NULL),
(3271041015, '$2y$10$V78rP5cE5opFUSh98yqlG.NL7R6miAfobnpfREA/dYt7Ig.9SI92S', 'Gunung Batu', 327104, 1, NULL, NULL, '2021-08-13 03:43:37', NULL, NULL),
(3271041016, '$2y$10$0GNFOqLddirn8NtmmEsTn.NAzwibm020xWYJKIMVgP6hweT0pSVDq', 'Loji', 327104, 1, NULL, NULL, '2021-08-13 03:43:37', NULL, NULL),
(3271051001, '$2y$10$gjDCVVUIRlv1oisd7BsuYuqVyzdSjkco8BSa/.fXpY7EHmq8opTGS', 'Bantarjati', 327105, 1, NULL, NULL, '2021-08-13 03:43:37', NULL, NULL),
(3271051002, '$2y$10$zXoQAwbZPtczlpG8qtYREeAIDnbxqCR0UacOG/gz.CuRizA.0T6li', 'Tegal Gundil', 327105, 1, NULL, NULL, '2021-08-13 03:43:37', NULL, NULL),
(3271051003, '$2y$10$BGXf83fhzrsW6IhHUZRhr.gvT1KDQwTOkRFT6I3qRfQylk3slW3X6', 'Kedung Halang', 327105, 1, NULL, NULL, '2021-08-13 03:43:37', NULL, NULL),
(3271051004, '$2y$10$jeBM33svUfbXiYR.eI.Hsu1CYZAcsodljQv2B.c6CMAzhPLJRVYue', 'Ciparigi', 327105, 1, NULL, NULL, '2021-08-13 03:43:38', NULL, NULL),
(3271051005, '$2y$10$AFYGg8Ljr6HgCpoY64tenuNi0IKuAzbI1qxCEcGpt5L3akX/a/Z0e', 'Cibuluh', 327105, 1, NULL, NULL, '2021-08-13 03:43:38', NULL, NULL),
(3271051006, '$2y$10$ir7atMQ13ANtT1pqCyjooOvI6KYIELctwMmWF4VK2HM8/xwb5wpnK', 'Ciluar', 327105, 1, NULL, NULL, '2021-08-13 03:43:38', NULL, NULL),
(3271051007, '$2y$10$7BdUnD4/DJknVc/Rx9h3vu/ddqQ24BQda9gO/RQ58GWKkSrHCCmIu', 'Tanah Baru', 327105, 1, NULL, NULL, '2021-08-13 03:43:38', NULL, NULL),
(3271051008, '$2y$10$ajsuSw16NXTvOc0Sj/CU7uNTo4wu4PQKUzQ5YUiE5EIYMoZWi9lCm', 'Cimahpar', 327105, 1, NULL, NULL, '2021-08-13 03:43:39', NULL, NULL),
(3271061001, '$2y$10$p0sPD5qB8wBrD8TWr31cD.RoCQ.BoVPjg41sSZrV7qSd8KolBN58e', 'Tanah Sareal', 327106, 1, NULL, NULL, '2021-08-13 03:43:39', NULL, NULL),
(3271061002, '$2y$10$cN68BlXVCYZZAxu1fAcukeCJUlhXCECCsFeR7lhvJDkQJS/5RDSPu', 'Kebon Pedes', 327106, 1, NULL, NULL, '2021-08-13 03:43:39', NULL, NULL),
(3271061003, '$2y$10$33UEnsyzITBOpsBQZNtKO.ROxuD9CtRFQcSrYTG2jEvGiKUrd.3T.', 'Kedung Badak', 327106, 1, NULL, NULL, '2021-08-13 03:43:39', NULL, NULL),
(3271061004, '$2y$10$tWs5BXm/d89EJojmziriXuOal1GgkMVpoP1AG2Bj3rnuETnmBfeLK', 'Sukaresmi', 327106, 1, NULL, NULL, '2021-08-13 03:43:40', NULL, NULL),
(3271061005, '$2y$10$mqtfmG5B1bxGSE09hm/pn.G6Bwd/v4jpFsRyZc0FDWUXlfRs0gEZa', 'Kedung Waringin', 327106, 1, NULL, NULL, '2021-08-13 03:43:40', NULL, NULL),
(3271061006, '$2y$10$Y7tw8WZWduGUvdAQwZlUMekO8bbFH2fdq88Efffar.rSh4AaeOblO', 'Kedung Jaya', 327106, 1, NULL, NULL, '2021-08-13 03:43:40', NULL, NULL),
(3271061007, '$2y$10$yacyMHSuiPmvjgeIDUzLPu7khh/.jKOwDGIZmjgTBvZIXExt464L6', 'Sukadamai', 327106, 1, NULL, NULL, '2021-08-13 03:43:40', NULL, NULL),
(3271061008, '$2y$10$Kg9UgAsfMz7Jvj5TK/3JEOf5BoyfDxlQj3AopFCzZfxNHK33L8DkO', 'Mekar Wangi', 327106, 1, NULL, NULL, '2021-08-13 03:43:40', NULL, NULL),
(3271061009, '$2y$10$y0VaHrtv./rGx0JNE0dXseq02hf1aFkqMsHQBJUViwB3plFW5Ylk.', 'Kencana', 327106, 1, NULL, NULL, '2021-08-13 03:43:41', NULL, NULL),
(3271061010, '$2y$10$HlUpK2l7r.ce.zlpbnGL4eswmeWFTzwZfkyLIESx/jJhFzwxcAjM.', 'Kayu Manis', 327106, 1, NULL, NULL, '2021-08-13 03:43:41', NULL, NULL),
(3271061011, '$2y$10$sQ/mNEL8GUusG8py8MSYMOoTbKn5L7hvNmMOo9Ir11Nw56CG2MNMm', 'Cibadak', 327106, 1, NULL, NULL, '2021-08-13 03:43:41', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `konsumen`
--

CREATE TABLE IF NOT EXISTS `konsumen` (
  `id` bigint(20) NOT NULL,
  `kode_konsumen` varchar(100) NOT NULL,
  `nama_konsumen` varchar(50) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `konsumen`
--

INSERT INTO `konsumen` (`id`, `kode_konsumen`, `nama_konsumen`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$D8H471oWRZTtybIm1JG.qenWVglHytoXbRvyjbtZLtMphP5qyhPrW', 'Pedagang Pasar Induk', 1, 1, NULL, '2021-09-09 09:43:43', '2021-10-05 14:21:46', NULL),
(2, '$2y$10$GOZ2mpkBsaUmqKJg50x9TeJrK03YlI9G6pjVTBaLcQXBsWkK3Ii62', 'Pedagang Pasar Kecamatan', 1, 1, NULL, '2021-09-09 09:44:04', '2021-10-05 14:22:05', NULL),
(3, '$2y$10$QP40o1FmCPQDMuqOfy0shOgRPz5lxqogAoT/PbzHuJXBkd4vrB/By', 'Warga2 3', 1, 1, 1, '2021-09-09 09:44:11', '2021-09-09 09:49:27', '2021-09-09 09:49:27'),
(4, '$2y$10$0HiXWXrTvVdwguKhE04ka.7vXiKDdgW7kSLCi90boH7Q6lKPoY3Cy', 'Pedagang Pasar Desa / Kelurahan', 1, 1, NULL, '2021-10-05 14:22:17', '2021-11-11 07:29:07', NULL),
(5, '$2y$10$RawPH83D191rqIf30jqNPOWRev43qvklkBbTVNedjNGmQlnWyZk7O', 'Pedagang Pengecer di Lingkungan Konsumen', 1, NULL, NULL, '2021-10-05 14:22:36', '2021-10-05 14:22:36', NULL),
(6, '$2y$10$NWQK3laTsLoxFNrwOGW/.eh5l//aXOI3grrNXIwaAtDkoebvyGOZy', 'Konsumen Langsung', 1, NULL, NULL, '2021-10-05 14:23:37', '2021-10-05 14:23:37', NULL),
(7, '$2y$10$aJ6qqCUBUXVXHHXBc/LT..pRQa8vFWHRMyKcmcrAuNQ9DYBR5E/9q', 'Rumah Makan/Warung/Restoran/PKL', 1, NULL, NULL, '2021-10-05 14:24:04', '2021-10-05 14:24:04', NULL),
(8, '$2y$10$1JdF9NTfqhgJnC6lk.fVaOF7aK3oC7.YeGgmFI6fAdB14tB7MbsOm', 'Hotel', 1, NULL, NULL, '2021-10-05 14:24:11', '2021-10-05 14:24:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `log_activity_user`
--

CREATE TABLE IF NOT EXISTS `log_activity_user` (
  `id_user` bigint(20) NOT NULL,
  `waktu` datetime NOT NULL,
  `activity` text NOT NULL,
  `ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lokasi_pembelian`
--

CREATE TABLE IF NOT EXISTS `lokasi_pembelian` (
  `id` bigint(20) NOT NULL,
  `kode_lokasi_pembelian` varchar(100) NOT NULL,
  `nama_lokasi_pembelian` varchar(50) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lokasi_pembelian`
--

INSERT INTO `lokasi_pembelian` (`id`, `kode_lokasi_pembelian`, `nama_lokasi_pembelian`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$yElN96q8Y8.GR0F/iXCR6OqyCeptgO68VOutFRCtxaBbO5HcPohgK', 'Dalam Desa /Kota', 1, 1, NULL, '2021-09-09 09:16:38', '2021-10-06 03:49:15', NULL),
(2, '$2y$10$WO8mtfXoBqJwlQbw/aDSB./mXpK1neAwI3ZkdnW.hJNgLsNUKaJiG', 'Luar Desa Dalam Kecamatan', 1, 1, NULL, '2021-09-09 09:17:20', '2021-10-05 14:20:09', NULL),
(3, '$2y$10$ov4j9FOH6Hx6YFu7LL3d5evafpGM.QOLlg2Kl3LEhCrh80c5YgqF6', 'Luar Negeri 2', 1, 1, 1, '2021-09-09 09:17:28', '2021-09-09 09:24:24', '2021-09-09 09:24:24'),
(4, '$2y$10$Wh7chxiO9Om6pcRkh4sB0OG/ZT5Kel4Jdnry.3UmIQppf5tuUVXI6', 'Luar Kecamatan Dalam Kabupaten', 1, NULL, NULL, '2021-10-05 14:20:45', '2021-10-05 14:20:45', NULL),
(5, '$2y$10$eZrgRwNxWKXjUhpbAo4o/eQNbADsPV4RsK4Ntpw9VQctUZKKX2kZa', 'Luar Kabupaten', 1, NULL, NULL, '2021-10-05 14:20:58', '2021-10-05 14:20:58', NULL),
(6, '$2y$10$2cTviEsdl6OQbge.MCON6ugidLpxROMZJfIJxdnKa6d3PU4t.rt/m', 'Luar Provinsi', 1, NULL, NULL, '2021-10-05 14:21:08', '2021-10-05 14:21:08', NULL),
(7, '$2y$10$BLRvCL0pG3cU3TCWfPN0FerCF9lFxIPyNADRYoL.K5dfCuALQS2ey', 'Luar Negeri', 1, NULL, NULL, '2021-10-05 14:21:26', '2021-10-05 14:21:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pemasaran`
--

CREATE TABLE IF NOT EXISTS `pemasaran` (
  `id` bigint(20) NOT NULL,
  `kode_pemasaran` varchar(100) NOT NULL,
  `id_distribusi_bapokting` bigint(20) NOT NULL,
  `id_konsumen` bigint(20) NOT NULL,
  `id_asal_konsumen` bigint(20) NOT NULL,
  `volume` decimal(10,0) NOT NULL,
  `id_satuan` bigint(20) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemasaran`
--

INSERT INTO `pemasaran` (`id`, `kode_pemasaran`, `id_distribusi_bapokting`, `id_konsumen`, `id_asal_konsumen`, `volume`, `id_satuan`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$vKTL/mICjRxGkTZEHoL6V.epTmalnd4fy7vVPwYp92zlQ15Y7gXS.', 3, 2, 2, '30', 1, 12, NULL, NULL, '2021-09-13 12:53:24', '2021-09-13 12:53:24', NULL),
(2, '$2y$10$N/AxFiKsieORxEG/OaSQBu905gNQqJ8l1NajIAD/2LxlcfQPO036a', 3, 1, 1, '50', 1, 12, NULL, NULL, '2021-09-13 12:53:24', '2021-09-13 12:53:24', NULL),
(3, '$2y$10$U5dy6Nmp4GrXzAi1Yj1zDOgNjyO8eRPB3LrSO1/i.jyZ5R0u4ZAm6', 2, 1, 2, '60', 1, 12, NULL, NULL, '2021-10-05 09:56:21', '2021-10-05 09:56:21', NULL),
(4, '$2y$10$Ob2Hlnco8LZS8ldavzyYxucZtyb21uP9QQFBWx8l0AfhY8eo9IJBe', 2, 1, 1, '20', 1, 12, NULL, NULL, '2021-10-05 09:56:21', '2021-10-05 09:56:21', NULL),
(6, '$2y$10$PhasZOTvuJaW2O.Z4LfgK.4O7E3dfNb5dmC/ah95Oe5tfjAKIG.sG', 9, 1, 1, '10', 1, 12, NULL, NULL, '2021-10-16 08:29:37', '2021-10-16 08:29:37', NULL),
(7, '$2y$10$ElXu9uz3MRTDogzQ2Kp.rujtwteoQH8JTjJtArbXshYFR.7W/uDY.', 12, 4, 1, '10', 5, 12, NULL, NULL, '2021-11-07 04:17:59', '2021-11-07 04:17:59', NULL),
(8, '$2y$10$XrQ8pauosqrBCg/OUU7hNu9X2o5BR0JNy/CXETtvh9.jwZ64BrjNu', 12, 4, 1, '20', 6, 12, NULL, NULL, '2021-11-07 04:17:59', '2021-11-07 04:17:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pemilik`
--

CREATE TABLE IF NOT EXISTS `pemilik` (
  `id` bigint(20) NOT NULL,
  `kode_pemilik` varchar(100) NOT NULL,
  `nik` varchar(25) NOT NULL,
  `nama_pemilik` varchar(100) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('l','p') NOT NULL,
  `alamat` text NOT NULL,
  `nomor_handphone` varchar(15) NOT NULL,
  `tanggal_join` date NOT NULL COMMENT 'Tanggal join atau masuk di perushaaan',
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemilik`
--

INSERT INTO `pemilik` (`id`, `kode_pemilik`, `nik`, `nama_pemilik`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `nomor_handphone`, `tanggal_join`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$9bf4BZrpJdGF4L7pDul4xu6vAAl8nbUk6/AsR.eQ6nEymOGCCnTRq', '1', 'Agen Beras Cek Din Jaya', '-', '1985-09-14', 'l', '-', '0896', '2000-01-01', 1, 1, NULL, '2021-09-13 12:07:50', '2021-10-04 09:45:11', NULL),
(2, '$2y$10$mswyTKIdgegSkbKnHldh2uPENTn6gxPIq.2ns4FpC8M91hu9QGHfq', '534543', '-', '-', '2021-10-12', 'p', '-', '0896', '2021-06-01', 1, NULL, NULL, '2021-10-04 12:57:30', '2021-10-04 12:57:30', NULL),
(3, '$2y$10$fnMcU0IcB325dg5r4s/B/uYMLa1b3wjYr/pgpM2fRFvp3/y02uK8O', '674545543', '-', '-', '2021-10-01', 'l', '-', '0251', '2018-01-01', 1, NULL, NULL, '2021-10-04 12:59:42', '2021-10-04 12:59:42', NULL),
(4, '$2y$10$1IAilCTnd/Tbc0U194E/O.AbcBMfuQol17EvfJtBnQAJaPgembwui', '64654654', '-', '-', '1993-10-22', 'l', '-', '0896', '2017-01-01', 1, NULL, NULL, '2021-10-04 13:02:16', '2021-10-04 13:02:16', NULL),
(5, '$2y$10$l17VLzPgJRSlMijItFG1cuU/hxjGayK64ECCJVWSJS7GRu0cFyVzK', '4543534', '-', '-', '1993-10-21', 'p', '-', '0896', '2020-01-01', 1, NULL, NULL, '2021-10-04 13:04:12', '2021-10-04 13:04:12', NULL),
(6, '$2y$10$mjJ2s4VPdyELaCcJ86/D1up/3731vlmHTyctkioZzuz/V5xunQt5W', '75656794564', '-', '-', '1993-10-30', 'l', '-', '0896', '2010-02-01', 1, NULL, NULL, '2021-10-04 13:06:08', '2021-10-04 13:06:08', NULL),
(7, '$2y$10$y/YR7VBYM0Sn83zkDfRF2.oO4i2nyM6kZYGtZKU3wUSttBlnFlcOu', '43535232', '-', '-', '1993-10-19', 'l', '-', '0896', '2001-01-01', 1, 1, NULL, '2021-10-04 13:07:41', '2021-10-04 13:11:25', NULL),
(8, '$2y$10$IMlPsW7UaBUFPbZ.UJr.n.tJ7lA7xQ/MUbU03WGsUiN9/j5RnugC2', '768967453', '-', '-', '1993-10-08', 'l', '-', '0896', '2011-01-01', 1, 1, NULL, '2021-10-04 13:09:23', '2021-10-08 06:20:12', NULL),
(9, '$2y$10$FS8tg8BJd0mEFbYS9J1mkOgELZKEGoAVC8oQq9CbTQWxab5dnhubm', '75756756', '-', '-', '1993-10-08', 'p', '-', '0896', '2001-01-01', 1, 1, NULL, '2021-10-04 13:11:07', '2021-10-08 06:20:54', NULL),
(10, '$2y$10$Un2iUtNK6IdXKfywkTkFe.dVOtRw1sIae4vyj5i0R/FNem9Z7LmTe', '7656753543', '-', '-', '1993-10-21', 'p', '-', '0896', '2016-01-01', 1, 1, NULL, '2021-10-04 13:13:32', '2021-10-08 06:25:10', NULL),
(11, '$2y$10$X0kfrhcp3cSADM.e.NzpruVOX8OzlqLOG9HEJwXEvIajHoz.W2ALe', '54648797843', '-', '-', '1993-10-12', 'l', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 13:15:09', '2021-10-08 06:21:02', NULL),
(12, '$2y$10$VX.0YdLyPcr5uiGHmJ8BlufWJoBqG11kRl4vMGO/uo6qGtcErubq2', '978907686', '-', '-', '1993-10-23', 'p', '-', '0896', '2021-01-01', 1, NULL, NULL, '2021-10-04 13:16:42', '2021-10-04 13:16:42', NULL),
(13, '$2y$10$Z9jAlN7uiG5fOyFMZrrvNebnweuv8QNXAzpiEGvY6XF4MxudiMW16', '768696734532', '-', '-', '1993-10-19', 'l', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 13:18:32', '2021-10-08 06:23:02', NULL),
(14, '$2y$10$3xl0R7uM7db7AXjv8dliLOegxJ/MmxwmBJhGsy4Rtm4pD9NbVKFZ2', '567567978943', '-', '-', '1993-10-19', 'p', '-', '0896', '2021-01-01', 1, NULL, NULL, '2021-10-04 13:20:03', '2021-10-04 13:20:03', NULL),
(15, '$2y$10$athyVbDeZYhc.g5z9E5wbeycdvcDdaf0.TwMDWoXXEFCABMQIjGg6', '546458997823412', '-', '-', '1993-10-02', 'l', '-', '0896', '2021-01-01', 1, NULL, NULL, '2021-10-04 13:21:34', '2021-10-04 13:21:34', NULL),
(16, '$2y$10$lvETBND./x7DvU/PZlC72OtfT/UcbE1hMhvUhlfr353pYKt5Ztbgm', '78697834232', '-', '-', '1993-10-30', 'l', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 13:23:04', '2021-10-08 06:25:52', NULL),
(17, '$2y$10$7Iw9G/E3pM0zTy8x/kZZv.Z75xGFW2sLkmImUlimqa3bSY21EItvi', '45648766783', 'Benyamin Setiady', '-', '1993-10-14', 'l', '-', '02518393018', '2013-01-01', 1, 1, NULL, '2021-10-04 13:25:10', '2021-11-08 07:43:37', NULL),
(18, '$2y$10$soQIpvBB4XZ8hGx4wGjGwuaveP0EoCDWbIeuFOtgcpg3f9rl63rvW', '54645787667', '-', '-', '1993-10-19', 'l', '-', '0896', '2020-01-01', 1, NULL, NULL, '2021-10-04 13:26:35', '2021-10-04 13:26:35', NULL),
(19, '$2y$10$BHxu5RXNkmaAx.Rv2AUF6OhL3KBT7wpIeKptrgVEeZ1QaQdPm6nri', '5675690534', '-', '-', '1993-10-12', 'l', '-', '0896', '2013-01-01', 1, NULL, NULL, '2021-10-04 13:36:01', '2021-10-04 13:36:01', NULL),
(20, '$2y$10$NDKCcSr4Fwslv.M8oaWTmeQ.BG0ypXgazwhmJWbT/EEyqXIPUNgMa', '5464596862423', '-', '-', '1993-10-19', 'l', '-', '0896', '2011-01-01', 1, NULL, NULL, '2021-10-04 13:37:42', '2021-10-04 13:37:42', NULL),
(21, '$2y$10$c16hmBFzPBYGHAubFSDdlusi3cBJs/3DV8/fyZpnSmYt11RHaD0RS', '54359768353', '-', '-', '1993-10-18', 'l', '-', '0896', '2013-02-01', 1, 1, NULL, '2021-10-04 13:39:19', '2021-10-08 06:20:02', NULL),
(22, '$2y$10$b42TNVdG.Uaret9LKkwXJeD9/0xa4iQ2wHcwLAIRwRLlER2WwY8c6', '5465497802342', 'Sudarseh', '-', '1993-10-10', 'p', '-', '08159351352', '2006-01-01', 1, 1, NULL, '2021-10-04 13:40:41', '2021-11-08 07:38:53', NULL),
(23, '$2y$10$UJ94rxjBRyun2JTHS.3jbeleKM6h8Uidde8qiDla5Tm1a2bAxCXSq', '565467867896312', '-', '-', '1993-10-25', 'l', '-', '0896', '2001-01-01', 1, NULL, NULL, '2021-10-04 13:42:20', '2021-10-04 13:42:20', NULL),
(24, '$2y$10$G0M40WHmdkgX.K2/mzKDpekHQY0xd7nt52YtYK9MeQtxuAvy.xJKe', '897842342', '-', '-', '1993-10-18', 'l', '-', '0896', '1991-01-01', 1, NULL, NULL, '2021-10-04 13:43:47', '2021-10-04 13:43:47', NULL),
(25, '$2y$10$iT8qClREe1phb7e3H53/Ru.FOwk/DOi0DFi5eN4nVK6pcK0uVqk9e', '87978353453', '-', '-', '1993-10-16', 'p', '-', '0896', '1991-01-01', 1, 1, NULL, '2021-10-04 13:45:20', '2021-10-08 06:19:14', NULL),
(26, '$2y$10$MuaQVj1oqVVCPDZCk/QBwu5BCGrUB88Qo2W90U7wxVPzIfIco4v8q', '567587978974', '-', '-', '1993-10-07', 'l', '-', '0896', '1981-01-01', 1, NULL, NULL, '2021-10-04 13:46:44', '2021-10-04 13:46:44', NULL),
(27, '$2y$10$uEt5JWhKQ5xxuoKk.O0Kauc912Vt5qNqNlc8he1vLq5KDLUs9FF5S', '4645687678763', '-', '-', '1993-10-25', 'p', '-', '0896', '2006-01-01', 1, NULL, NULL, '2021-10-04 13:48:11', '2021-10-04 13:48:11', NULL),
(28, '$2y$10$KUh0oxF/OC.sd.JbKQRXNe529suf7gzQtNPMw1DzI1hIJL0mej0X2', '87978456422', '-', '-', '1993-10-08', 'l', '-', '0896', '2017-01-01', 1, NULL, NULL, '2021-10-04 13:49:32', '2021-10-04 13:49:32', NULL),
(29, '$2y$10$rcEJwq6P0Si7fboMqehhD.qk4pxN50tgbOWRZfP8VXrGBtz/IXzg6', '65789789780342', '-', '-', '1993-10-26', 'l', '-', '0896', '1971-01-01', 1, NULL, NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL),
(30, '$2y$10$k/ifRwepDnemY6wSYBWgnek3r8dd3.2StFsckXQhUhfEffWn5cz.K', '789897435322', '-', '-', '1993-10-17', 'l', '-', '0896', '1981-01-01', 1, NULL, NULL, '2021-10-04 13:53:16', '2021-10-04 13:53:16', NULL),
(31, '$2y$10$Ln/bQbDwZ01LyZrWB0QOj.yhkEEAgOdC90TXC0CS/viJBgdH.zBeG', '897897867533', '-', '-', '1993-10-24', 'l', '-', '0896', '1981-01-01', 1, NULL, NULL, '2021-10-04 13:54:59', '2021-10-04 13:54:59', NULL),
(32, '$2y$10$XZGWf2ATTKJCNDCW0qrxiOFz3FIKmVaZ.L3PDbMeqchecggQ79EVC', '789797865756', '-', '-', '1993-10-25', 'l', '-', '0896', '1985-01-01', 1, NULL, NULL, '2021-10-04 13:56:26', '2021-10-04 13:56:26', NULL),
(33, '$2y$10$b8OGhR.yZxNF8Z74o/.AyOw5FUtPkcJ3zZgxeckeARDJhCjK.tYJe', '89794564522', '-', '-', '1993-10-23', 'l', '-', '0896', '2009-01-01', 1, NULL, NULL, '2021-10-04 13:57:57', '2021-10-04 13:57:57', NULL),
(34, '$2y$10$ZAIJywqxZ1e1g/hZND4Kge/J2FW5kJO.oMa4770EbpkL/2TDM44pu', '890895464643', '-', '-', '1993-10-07', 'p', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 13:59:18', '2021-10-08 06:18:45', NULL),
(35, '$2y$10$OWxCEvbA7MpF.77bwfap0OiWpwMPb35q14My3vhaZA4ApG6MW0jJC', '789789567573', '-', '-', '1993-10-21', 'l', '-', '0896', '2001-01-01', 1, 1, NULL, '2021-10-04 14:00:42', '2021-10-08 06:19:33', NULL),
(36, '$2y$10$A9LQiHiHRd3ylJoJbLNQXO7901k7o/EO4lqDfUMis6SAWMcrJr6ea', '8978972342346', '-', '-', '1993-10-01', 'p', '-', '0896', '2021-01-01', 1, NULL, NULL, '2021-10-04 14:02:10', '2021-10-04 14:02:10', NULL),
(37, '$2y$10$7F4hohbd2WfORIL2iTfNnOtvYmEL4PX1cdC612D/NnrHnQ8Gbd2vm', '7689090743534', '-', '-', '1993-10-14', 'l', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 14:03:30', '2021-10-08 06:19:43', NULL),
(38, '$2y$10$NAMURB.K.Swz8WTR0I6MSOd43iTAbxcQdKhdz5/1EawNB4SbqgwSG', '67863423413123', '-', '-', '1993-10-22', 'l', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 14:04:53', '2021-10-08 06:18:59', NULL),
(39, '$2y$10$EORksPhtsocgn/c3k1o/UedcWTiuvXAfIHy5uHWg7xfTWSAcEM1DK', '789783424222', '-', '-', '1993-10-08', 'l', '-', '0896', '2021-01-01', 1, NULL, NULL, '2021-10-04 14:06:28', '2021-10-04 14:06:28', NULL),
(40, '$2y$10$qfPV8a6/SKiFrAVHUYC53.Xow0yV.VDF.GzVboHd67hr3412u8KBS', '879784534242', '-', '-', '1993-10-27', 'l', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 14:07:48', '2021-10-08 06:19:52', NULL),
(41, '$2y$10$HeLYnQr.1TU7qL7Cv0Uh6.9tJDEwCgk/33YQlOlRyUlrszvs6V462', '2423456546786', '-', '-', '1993-10-25', 'p', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 14:09:07', '2021-10-08 06:20:33', NULL),
(42, '$2y$10$2cI/yXaNwQoPod8DXu/jbeHhIhSCU3s12CYHSGrCmut6gXznVJxVO', '8797896575673', '-', '-', '1993-10-29', 'l', '-', '0896', '2021-01-01', 1, 1, NULL, '2021-10-04 14:10:33', '2021-10-08 06:26:26', NULL),
(43, '$2y$10$FT45O5pMDZNpz0nB38aSmu4EIiQ1mfK0zQqtkbLBN8E1eqbxotEjC', '54642342879878', '-', '-', '1993-10-30', 'l', '-', '0896', '2016-01-01', 1, 1, NULL, '2021-10-04 14:12:12', '2021-10-08 06:18:28', NULL),
(44, '$2y$10$AErEKRPIv0t2Z00dDQv2rOWTALpMgAUuvRn5zc16jteIUSQ5CmbfG', '897843534522', '-', '-', '1993-10-21', 'l', '-', '0896', '2006-01-01', 1, NULL, NULL, '2021-10-04 14:13:38', '2021-10-04 14:13:38', NULL),
(45, '$2y$10$OjczZrsADtm22j./WJbKreFZ.L82kXd42UxHVDRkitixjYa/XV4/W', '78978342345656', '-', '-', '1993-08-01', 'p', '-', '0896', '2021-08-01', 1, NULL, NULL, '2021-10-04 14:16:52', '2021-10-04 14:16:52', NULL),
(46, '$2y$10$QHrKXgTIlRVkSc01pYPNWeN6e2TYV5av9/f6gEZalCZPVcJUaCvqK', '8980453533', '-', '-', '1993-10-01', 'p', '-', '0896', '2001-01-01', 1, NULL, NULL, '2021-10-04 14:18:46', '2021-10-04 14:18:46', NULL),
(47, '$2y$10$B183Gjjfnxtz3jJoSOOgVeJeQ54tV2hqZ/GzbUIew8EoEVKqNevXi', '78678645345356', '-', '-', '1993-10-20', 'p', '-', '0896', '2013-01-01', 1, 1, NULL, '2021-10-04 14:20:15', '2021-10-04 14:21:54', NULL),
(48, '$2y$10$XZvBRR6lngEfRKyBgMMpwufcirYDGUUgN3AJPVRLajqUSO2jCjUBS', '765768797844', '-', '-', '1993-10-25', 'p', '-', '0896', '2001-01-01', 1, NULL, NULL, '2021-10-04 14:21:42', '2021-10-04 14:21:42', NULL),
(49, '$2y$10$kMVq2F9PEFvUghYrOj4xxOeHDA8NhyprdcVRq5pGvfydLhWY6tb9G', '56758687654354344', '-', '-', '1993-10-27', 'l', '-', '0896', '2006-01-01', 1, NULL, NULL, '2021-10-04 14:23:19', '2021-10-04 14:23:19', NULL),
(50, '$2y$10$FNPU7EuDADh/G9nF89sYa.9ZEfTsFeQFsJvLbvHUkujM9yXpkDRTy', '9783423342676', '-', '-', '1993-10-30', 'p', '-', '0896', '2011-01-01', 1, NULL, NULL, '2021-10-04 14:24:45', '2021-10-04 14:24:45', NULL),
(51, '$2y$10$Rm86p71Cb8ipnMPJk.kW5.dcATA7hX/HuFhXB1vs7voYVkWzmMNp6', '45873475933222', '-', '-', '1993-01-01', 'l', '-', '0251', '1996-01-01', 1, NULL, NULL, '2021-10-13 03:32:11', '2021-10-13 03:32:11', NULL),
(52, '$2y$10$SzfezutFABprXvw8Vw.LgeDryzNg94SszYMgFxQCvdCa8x17tJmSS', '389483749822', '-', '-', '1993-01-01', 'l', '-', '0251', '2016-01-01', 1, NULL, NULL, '2021-10-13 03:34:46', '2021-10-13 03:34:46', NULL),
(53, '$2y$10$h3jxnnV.m8c8puQZ4Q1EWulflT/6Dhp2OKspSsmmvDexKGC98DJUO', '398204892387922', '-', '-', '1993-01-01', 'p', '-', '0251', '2017-01-01', 1, NULL, NULL, '2021-10-13 03:36:31', '2021-10-13 03:36:31', NULL),
(54, '$2y$10$RevzAZx6PFSuur3LOWP/nOY.ndQOgNMj.XXO7m9o7IWZfkmQBH3jG', '435839796644', '-', '-', '1993-01-01', 'l', '-', '0251', '2015-01-01', 1, NULL, NULL, '2021-10-13 03:38:11', '2021-10-13 03:38:11', NULL),
(55, '$2y$10$DZK.XWb/nbFeteIz2cM5hus0A3UVZTTQHWGBgganx3uN.xGcqTDiS', '843958379333', '-', '-', '1993-01-01', 'l', '-', '0251', '2013-01-01', 1, NULL, NULL, '2021-10-13 03:39:38', '2021-10-13 03:39:38', NULL),
(56, '$2y$10$FOGH1.G6IeQBumGb6vtcx.391hqoCtTLdx5LvxkQUTCNyUXgWY1eu', '3847958433833', '-', '-', '1993-01-01', 'l', '-', '0251', '2011-01-01', 1, NULL, NULL, '2021-10-13 03:41:06', '2021-10-13 03:41:06', NULL),
(57, '$2y$10$ho7stWu/I1DKvVp4WZW/HuHcoBp7yUiurrquKUVyDEzhUWuG6W/Lm', '48359672617281', '-', '-', '1993-01-01', 'p', '-', '0251', '2004-01-01', 1, NULL, NULL, '2021-10-13 03:42:34', '2021-10-13 03:42:34', NULL),
(58, '$2y$10$qVWM7hLrUFb/fVBtStAsGeGppQk2rAfcExJpoJmm2qATI9hf7k7J6', '8938473983334', '-', '-', '1993-01-01', 'l', '-', '0251', '2004-01-01', 1, NULL, NULL, '2021-10-13 03:44:24', '2021-10-13 03:44:24', NULL),
(59, '$2y$10$keVpoWueW0cUqv2UkF2KPebJizZlIWvMaC2twBR1b4AIqugrr7J26', '9484874644222', '-', '-', '1993-01-01', 'l', '-', '0251', '2006-01-01', 1, NULL, NULL, '2021-10-13 03:45:42', '2021-10-13 03:45:42', NULL),
(60, '$2y$10$emZn96CijOrLcmTRgZmD5ODa3cWoaoaCkWMMnKAur15iDQ8st1Qcy', '984746453455', '-', '-', '1993-01-01', 'l', '-', '0251', '2003-01-01', 1, NULL, NULL, '2021-10-13 03:47:00', '2021-10-13 03:47:00', NULL),
(61, '$2y$10$LpzqVEnHwf4.ytc1Ury/9.AWE/Q92OB1VySvnfTbIBVJOLGOT8x1K', '73638736332', '-', '-', '1993-01-01', 'l', '-', '0251', '2006-01-01', 1, NULL, NULL, '2021-10-13 03:48:21', '2021-10-13 03:48:21', NULL),
(62, '$2y$10$2eWhBVENW3rfbzg2s8VFT.jWYhBB500sGZqhBLRujjox2NnkDbOju', '983738373932', '-', '-', '1993-01-01', 'p', '-', '0251', '2013-01-01', 1, NULL, NULL, '2021-10-13 03:49:30', '2021-10-13 03:49:30', NULL),
(63, '$2y$10$raajHz2.VELh/dKMV9cfZefyq4zHSwU7./vgs.VrQ8Lz77DRJeOu6', '78738299222', '-', '-', '1993-01-01', 'l', '-', '0251', '2006-01-01', 1, NULL, NULL, '2021-10-13 03:50:46', '2021-10-13 03:50:46', NULL),
(64, '$2y$10$Lt8qYvz.T5jAsapYWMH4cOwk4bCxPzDADuExTUMPQPAYI8RfDKE4.', '7484658392234', '-', '-', '1993-01-01', 'l', '-', '0251', '2008-01-01', 1, NULL, NULL, '2021-10-13 03:53:26', '2021-10-13 03:53:26', NULL),
(65, '$2y$10$UvzSt8WWfxYHW/yzgLvITOgqH4QILFcHn0wAGkAPTwuIuZIvIRtvS', '493849384399', '-', '-', '1993-01-01', 'p', '-', '0251', '2012-01-01', 1, NULL, NULL, '2021-10-13 03:54:48', '2021-10-13 03:54:48', NULL),
(66, '$2y$10$qfhzblD4pX53U4coF6tjv.aLHxYvJ/iFxLnlRNNalrJBwXUsVjitW', '498349587394854', '-', '-', '1993-01-01', 'p', '-', '0251', '2017-01-01', 1, NULL, NULL, '2021-10-13 03:56:15', '2021-10-13 03:56:15', NULL),
(67, '$2y$10$U/YCna/etC2CY8sL9sumlOmAS9fJ0rI4vz3IOZGKiAvq6G5YQLPFa', '894785978444', '-', '-', '1993-01-01', 'p', '-', '0251', '2001-01-01', 1, NULL, NULL, '2021-10-13 03:57:57', '2021-10-13 03:57:57', NULL),
(68, '$2y$10$Ja/Qo/YBZLesLawp/1Ud2O80k/evmn/0BcbRt.kuzEV4nPmcy58Ua', '484759849944', '-', '-', '1993-01-01', 'l', '-', '0251', '2001-01-01', 1, NULL, NULL, '2021-10-13 03:59:24', '2021-10-13 03:59:24', NULL),
(69, '$2y$10$l93SBeg0UNjA8mzMdVjYl.CHEaBX3YDTYbMFUWePFOIIR9TbyjJLO', '8484748347933', '-', '-', '1993-01-01', 'l', '-', '0251', '2002-01-01', 1, NULL, NULL, '2021-10-13 04:00:53', '2021-10-13 04:00:53', NULL),
(70, '$2y$10$IFJhT5rcXW3pe106Acvife9PzdPBzEiSvFoazoGfChTKQD3IR/cMi', '83498574888474', '-', '-', '1993-01-01', 'l', '-', '0251', '2001-01-01', 1, NULL, NULL, '2021-10-13 04:02:23', '2021-10-13 04:02:23', NULL),
(71, '$2y$10$rLWCuTi0lIOmsugxEC6AK.KaXGntKqeDUhWBY1qaHY/FCW4b/iZQa', '4859498889994', '-', '-', '1993-01-01', 'l', '-', '0251', '2001-01-01', 1, NULL, NULL, '2021-10-13 04:03:36', '2021-10-13 04:03:36', NULL),
(72, '$2y$10$FTz8YPo67vTZvZQl1VmVfueCUdSM7YwYljJesKol0bwSogGCZ88I6', '945834598494', '-', '-', '1993-01-01', 'l', '-', '0251', '1994-01-01', 1, NULL, NULL, '2021-10-13 04:04:57', '2021-10-13 04:04:57', NULL),
(73, '$2y$10$xW2H9dfIBU5XK3Ip6x1Z.eXvGZg6.ZpZgShefFljlPmN3xGIuSnPa', '93443857948444', '-', '-', '1993-01-01', 'p', '-', '0251', '2001-01-01', 1, NULL, NULL, '2021-10-13 04:06:22', '2021-10-13 04:06:22', NULL),
(74, '$2y$10$lPizcOZy0t0AHLW2NaKEMu2lciEdo6ilU2sSV.Lr3eqhXRPtqm4ha', '49349384793321', '-', '-', '1993-01-01', 'p', '-', '0251', '1993-10-13', 1, NULL, NULL, '2021-10-13 04:08:17', '2021-10-13 04:08:17', NULL),
(75, '$2y$10$JcgbaJBYAYI8Y3Ria9ltd.s/NnvjoigJlyaCiowcaMHiHchGNvxla', '3383736633', '-', '-', '1993-01-01', 'p', '-', '0251', '1993-10-13', 1, NULL, NULL, '2021-10-13 04:09:37', '2021-10-13 04:09:37', NULL),
(76, '$2y$10$dG4fmd9xXn7fcl10FdobpeUTnhWaNZuGPfsJAGKSwsYnCIA0CEYNK', '338877363536', '-', '-', '1993-01-01', 'p', '-', '0251', '2021-10-13', 1, NULL, NULL, '2021-10-13 04:11:15', '2021-10-13 04:11:15', NULL),
(77, '$2y$10$PopnkEDcvGjo4hPn7b3tFeQzcJpSdlX.vIRvl5ZhBJcbk5f75SMY2', '0', 'Putera', '-', '1997-12-08', 'l', 'Leuwiliang Bogor', '089604261465', '2001-01-01', 1, NULL, NULL, '2021-11-08 04:19:03', '2021-11-08 04:19:03', NULL),
(78, '$2y$10$9AO3/2egvuOQLjJkB03BG.M0X4UX8kWmm7w6xfmTpuEHVcqJfycNO', '0', 'Zaki', '-', '1981-01-01', 'l', 'Gang Masjid no 27 RT 01/18,\r\nCilendek Timur', '085771660466', '2016-01-01', 1, NULL, NULL, '2021-11-08 04:33:00', '2021-11-08 04:33:00', NULL),
(79, '$2y$10$RyfAQWoNDF0zUoVGRkG1juQAEo76NoBTK.mwCYPpJ0jVQizA7xwze', '0', 'Surya Atmaja', '-', '1956-01-01', 'l', 'Jl. Pasar Dramaga RT 03/03', '0', '2014-01-01', 1, 1, NULL, '2021-11-08 04:50:48', '2021-11-08 06:37:04', NULL),
(80, '$2y$10$YiMB.RfucMpVUqwGJOXjm.8Tv2/YYY3mMmJscdoYj3EpRF/yaXQPy', '0', 'Triah', '-', '1998-01-01', 'p', 'Jl. Cibeureum Sinar Sari RT 02/01', '0', '2011-01-01', 1, NULL, NULL, '2021-11-08 06:15:21', '2021-11-08 06:15:21', NULL),
(81, '$2y$10$PIvaCVAQuEG82W69G5Mog.tyVBrqJMqZ6fKabpAdI9piBX6j5PNdm', '0', 'Fauzi', '-', '1981-01-01', 'l', 'Jl. Cikampek', '085776933385', '2016-01-01', 1, NULL, NULL, '2021-11-08 06:23:08', '2021-11-08 06:23:08', NULL),
(82, '$2y$10$DnjilMZBYkHjKqLk9O1M1OGPEhYY41HuWxk6w.qGl.uioCHsCa/xW', '0', 'Sandi', '-', '1997-01-01', 'l', 'Surya Kencana', '089516133212', '2016-01-01', 1, 1, NULL, '2021-11-08 06:35:33', '2021-11-08 06:36:41', NULL),
(83, '$2y$10$O.eyfBQAY6urMb.wl0yWqeqEgdq7/IWc9ADZ610hAPzhbAav2ajNK', '0', 'Age', '-', '1992-01-01', 'l', 'Majulaya, Bandung', '083819708102', '1994-01-01', 1, NULL, NULL, '2021-11-08 06:43:11', '2021-11-08 06:43:11', NULL),
(84, '$2y$10$zmZP3AkIArPRT4k6xHphQegWIEW/QkbO72SdXsQ5.dJGK7fRKTdqq', '0', 'H. Udi', '-', '1976-01-01', 'l', 'Jl. Babakan Peundel Baranangsiang', '081223352404', '2017-01-01', 1, NULL, NULL, '2021-11-08 06:55:27', '2021-11-08 06:55:27', NULL),
(85, '$2y$10$9oN8SJ4QYQ3l12pA4uutzuft.SZJP34efwSbYlpc80oy5gGxkSyMG', '0', 'Awang', '-', '1979-01-01', 'l', 'Sindangbarang Jero, Situ Gede', '0', '1996-01-01', 1, 1, NULL, '2021-11-08 07:01:29', '2021-11-11 07:17:11', NULL),
(86, '$2y$10$pJnLxgWQLVFXPLGTpbQ..eM2eSYGq/LN1LFd8MdERctK5TK1UUasK', '0', 'Nasution', '-', '1963-01-01', 'l', 'Jl. Sindangbarang', '0', '2019-01-01', 1, NULL, NULL, '2021-11-08 07:06:43', '2021-11-08 07:06:43', NULL),
(87, '$2y$10$Iwoh06ZCCyZbc9cG6aG8NePLiPeU/5XKRxzCRj/eGy79cQbdR6y3W', '0', 'Fauzi', '-', '1981-01-01', 'l', 'Jl. Cikampek', '085776933385', '2016-01-01', 1, 1, NULL, '2021-11-08 07:58:07', '2021-11-08 08:02:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pengadaan`
--

CREATE TABLE IF NOT EXISTS `pengadaan` (
  `id` bigint(20) NOT NULL,
  `kode_pengadaan` varchar(100) NOT NULL,
  `id_distribusi_bapokting` bigint(20) NOT NULL,
  `id_sumber_pembelian` bigint(20) NOT NULL,
  `id_lokasi_pembelian` bigint(20) NOT NULL,
  `volume` decimal(10,0) NOT NULL,
  `id_satuan` bigint(20) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengadaan`
--

INSERT INTO `pengadaan` (`id`, `kode_pengadaan`, `id_distribusi_bapokting`, `id_sumber_pembelian`, `id_lokasi_pembelian`, `volume`, `id_satuan`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$XRctaL3D12EKX3D8CJxhKOHQ3CznejXGZtK8MtAoGB3buU.D2IbkS', 3, 1, 2, '80', 1, 12, NULL, NULL, '2021-09-13 12:53:24', '2021-09-13 12:53:24', NULL),
(2, '$2y$10$yDHXsfqUZb8y2p7Ci4UwlOHSu7qTZwnuwFt./D0ovGToUOv2Lo0AK', 3, 1, 1, '40', 1, 12, NULL, NULL, '2021-09-13 12:53:24', '2021-09-13 12:53:24', NULL),
(3, '$2y$10$GAT0Tbwo8HU/3LDOxIxXXO5NyejqyzDw1nrSuVajC9Pf5hxeJO9RS', 2, 1, 2, '60', 1, 12, NULL, NULL, '2021-10-05 09:56:21', '2021-10-05 09:56:21', NULL),
(4, '$2y$10$LJXA4bud4oTY70j6.7E4qey/IPzaHkb.BmNZcR3bds4DfSud3HJmK', 2, 1, 1, '50', 1, 12, NULL, NULL, '2021-10-05 09:56:21', '2021-10-05 09:56:21', NULL),
(7, '$2y$10$i5CPbK3ILiMFSXVa52QhC.l4fI6bm0eurQUmn7zzil6IWXQ19FHyq', 9, 1, 1, '20', 1, 12, NULL, NULL, '2021-10-16 08:29:37', '2021-10-16 08:29:37', NULL),
(9, '$2y$10$cXiBfST.ZBvwDrn9E28j3O7HpwxIbeIZEhmr51Blnjz4iVH98XL.W', 12, 9, 1, '30', 5, 12, NULL, NULL, '2021-11-07 04:17:59', '2021-11-07 04:17:59', NULL),
(10, '$2y$10$2U4DpdVylbGgroskxlU4u.yymDYSa1ciFU6ye9KmUSlVdDfcRmFb.', 12, 9, 1, '20', 1, 12, NULL, NULL, '2021-11-07 04:17:59', '2021-11-07 04:17:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan`
--

CREATE TABLE IF NOT EXISTS `perusahaan` (
  `id` bigint(20) NOT NULL,
  `kode_perusahaan` varchar(100) NOT NULL,
  `id_pemilik` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `nib` varchar(25) NOT NULL COMMENT 'Nomor induk berusahaa',
  `nomor_legalitas_usaha` varchar(50) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `alamat_perusahaan` text NOT NULL,
  `id_kelurahan` bigint(20) NOT NULL,
  `nomor_telepon` varchar(15) NOT NULL,
  `lokasi_usaha` enum('wilayah_usaha','permukiman') NOT NULL,
  `status_usaha` enum('milik_sendiri','pekerja','konsinyasi') NOT NULL,
  `status_toko` enum('milik_sendiri','sewa','lapak_pesan') NOT NULL,
  `skala_usaha` enum('distributor','sub_distributor','agen') NOT NULL,
  `luas_gudang` decimal(10,0) NOT NULL,
  `koordinat` varchar(50) DEFAULT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `perusahaan`
--

INSERT INTO `perusahaan` (`id`, `kode_perusahaan`, `id_pemilik`, `id_user`, `nib`, `nomor_legalitas_usaha`, `nama_perusahaan`, `alamat_perusahaan`, `id_kelurahan`, `nomor_telepon`, `lokasi_usaha`, `status_usaha`, `status_toko`, `skala_usaha`, `luas_gudang`, `koordinat`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$lkqGNLGXlAAyo7xlrr18QO2WY0rukzJnYUdvmwUhaUmDb9dBueIEO', 1, 12, '-', '-', 'Agen Beras Cek Din Jaya', 'Jl. DR. Semeru RT 01/01, Cilendek Barat', 3271041001, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'agen', '0', '-6.577966,106.771201', 1, 1, NULL, '2021-09-13 12:07:51', '2021-10-04 09:45:11', NULL),
(2, '$2y$10$ZI3Wk56agqZOb4Pt9r8qE.33zJ2ZqgoeYvLwoRnynlSetENT4NRES', 2, 13, '-', '-', 'Agen Telur Anugerah', 'Jl. Sindangbarang', 3271041002, '-', 'permukiman', 'milik_sendiri', 'sewa', 'agen', '0', '-6.580323,106.76328', 1, NULL, NULL, '2021-10-04 12:57:30', '2021-10-04 12:57:30', NULL),
(3, '$2y$10$8UhxzjzXmmfT1cb7zDEQNe6Hx4eyt6n/H0H/5ZPaCzLfpoo2uJzHy', 3, 14, '-', '-', 'Agen Telur Putera Mandiri', 'Jl. Sindangbarang', 3271041002, '-', 'permukiman', 'pekerja', 'sewa', 'agen', '0', '-6.576895,106.756448', 1, NULL, NULL, '2021-10-04 12:59:42', '2021-10-04 12:59:42', NULL),
(4, '$2y$10$y2p25cXWA/5b8/KPbKdFgedOOX0u9vNWCNYZkjDvk5h8FPgGWsfdS', 4, 15, '-', '-', 'Beras Kusuma', 'Gang Masjid, Cilendek Timur', 3271041009, '-', 'permukiman', 'milik_sendiri', 'sewa', 'agen', '0', '-6.575136,106.776341', 1, NULL, NULL, '2021-10-04 13:02:16', '2021-10-04 13:02:16', NULL),
(5, '$2y$10$QMDTVBxTb2dqva9yFvBoreM58G8K6NnsGbNsuj9lbLJm9pHl09u66', 5, 16, '-', '-', 'PT. Sumber Gizi Utama (SGU)', 'Jl. Raya Cifor no 77 RT 03/08, Bubulak', 3271041003, '-', 'permukiman', 'pekerja', 'milik_sendiri', 'agen', '0', '-6.568746,106.754433', 1, NULL, NULL, '2021-10-04 13:04:13', '2021-10-04 13:04:13', NULL),
(6, '$2y$10$Fa8qtOfc.TN0j758mf8wQuZcN3jWbQ5CCwkURO68YzSg6e2C0gG9G', 6, 17, '-', '-', 'RPH Bubulak Bogor', 'JL. HM. Syarifudin no 38 RT 02/01, Bubulak', 3271041003, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '0', '-6.566262,106.758401', 1, NULL, NULL, '2021-10-04 13:06:09', '2021-10-04 13:06:09', NULL),
(7, '$2y$10$eCroY9Eqvm02LU8TS4CEAeAYG.Kt4a1VwzQwW1irtQnX50Avtk7g6', 7, 18, '-', '-', 'Tiga Putera Perkasa (Iga)', 'Jl. Wijaya Kusuma Raya no 98, Taman Yasmin', 3271041009, '-', 'permukiman', 'pekerja', 'milik_sendiri', 'agen', '9', '-6.566087,106.773986', 1, 1, NULL, '2021-10-04 13:07:41', '2021-10-04 13:11:25', NULL),
(8, '$2y$10$UGf6pY1.71R.K8AmCdUz/u3ApbFmDycm8wlVqKeNQhGMGOfDC2oBa', 8, 19, '-', '-', 'Toko 85', 'Merdeka 85', 3271031011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.590769,106.787697', 1, 1, NULL, '2021-10-04 13:09:23', '2021-10-08 06:20:12', NULL),
(9, '$2y$10$vNz6lIi6mTg1C2Jh3kN.POkXKoNMfHOKqN4vVQaAPazJelh9CImh2', 9, 20, '-', '-', 'Toko Kuruta', 'Jl. Sindangbarang no 24 RT 01/05', 3271041002, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '150', '-6.578641,106.758742', 1, 1, NULL, '2021-10-04 13:11:07', '2021-10-08 06:20:54', NULL),
(10, '$2y$10$iQMKkw.r4rgZD9oLzc4WP.ApZfIxifwvwVzPzjq8lKKSb14vnaXZ6', 10, 21, '-', '-', 'Zaki Jaya', 'Gang Masjid no 27 RT 01/18, Cilendek Timur', 3271041009, '-', 'permukiman', 'pekerja', 'milik_sendiri', 'agen', '200', '-6.575861,106.774247', 1, 1, NULL, '2021-10-04 13:13:32', '2021-10-08 06:25:10', NULL),
(11, '$2y$10$d7ayaGNYfx0E9BnZN7CifOt6u3DqfwjvER4IFnLiCrtftjLGKYmxW', 11, 22, '-', '-', 'Toko Kurnia', 'Jl. Sindangbarang No.24 RT.1/5', 3271041002, '-', 'wilayah_usaha', 'pekerja', 'sewa', 'agen', '0', '-6.580488,106.764366', 1, 1, NULL, '2021-10-04 13:15:09', '2021-10-08 06:21:02', NULL),
(12, '$2y$10$DXtWP1JhY/z/GRT.j0H8QO8dy2KMXZHYx3lYSlmjFXiNWS5puXoNG', 12, 23, '-', '-', 'Co Tiong Ho (Novianty Raharja)', 'Jl.Raya Tunggal RT.003 RW.003 ', 3271011001, '-', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.620457,106.806304', 1, NULL, NULL, '2021-10-04 13:16:42', '2021-10-04 13:16:42', NULL),
(13, '$2y$10$k9ggXsblfjqFLIyzPVdeguFzVQ.2nxPpmpACwM2QBkUC2nwU4odI.', 13, 24, '-', '-', 'Benyamin Setiadi ', 'Jl.Darlia Raya No.10 RT. 001/003', 3271011015, '-', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.628736,106.822231', 1, 1, NULL, '2021-10-04 13:18:32', '2021-10-08 06:23:02', NULL),
(14, '$2y$10$QEILi8ebUUQ/Kgu84RO4/.RcX975l49ArzAKdymLAZseugUdxoABC', 14, 25, '-', '-', 'Yoyong (Toko 11)', 'Jl. Soemantadireja RT.01/O8', 3271011005, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.639931,106.80806', 1, NULL, NULL, '2021-10-04 13:20:03', '2021-10-04 13:20:03', NULL),
(15, '$2y$10$QNT/9OjYD3zu.risDa0PV.wF6Yiic2wIokwb4ghJ/h69MhkHnrsQG', 15, 26, '-', '-', 'Monika M. (Rizal)', 'RT.01/02', 3271011005, '-', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.632315,106.806198', 1, NULL, NULL, '2021-10-04 13:21:34', '2021-10-04 13:21:34', NULL),
(16, '$2y$10$i30Xh36.vpQxiCKGfYnwt.NuD.lUQ..THJc2YaF40ZcAffvmFwzzi', 16, 27, '-', '-', 'Winata (Toko Mekar Jaya)', 'Jl.Raya Tunggal RT.004 RW.003', 3271011001, '-', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.61948,106.80728', 1, 1, NULL, '2021-10-04 13:23:04', '2021-10-08 06:25:52', NULL),
(17, '$2y$10$CsXUatLTev9dR1vh8kQLSOcJvqftsdTZB3Y206p4.ADo4zM2mnqcq', 17, 28, '-', '541/11/45-295/2014', 'Saq & Gas', 'Dahlia Raya No 10', 3271011015, '02518393018', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'sub_distributor', '300', '-6.629063,106.822385', 1, 1, NULL, '2021-10-04 13:25:10', '2021-11-08 07:43:37', NULL),
(18, '$2y$10$yGmipnzGLVEUXw2BrSX3OOET.bYqtO9KkPfLrB0If6sI.Xa7TqdOW', 18, 29, '-', '-', 'Agen Telur Amanah', 'Pasar Anyar, Blok CD', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '0', '-6.591414,106.791868', 1, NULL, NULL, '2021-10-04 13:26:36', '2021-10-04 13:26:36', NULL),
(19, '$2y$10$E0q30ybAzxDRU9rWmjVwNeOzaiYFWQ3BOhhLu8GcnBDFae.q5wgJy', 19, 30, '-', '-', 'Bintang Plastik', 'Pasar Bogor', 3271031008, '-', 'wilayah_usaha', 'pekerja', 'sewa', 'sub_distributor', '6', '-6.603315,106.801058', 1, NULL, NULL, '2021-10-04 13:36:01', '2021-10-04 13:36:01', NULL),
(20, '$2y$10$I.cjaBfIaM9FQiodV9xoie6JhE5spHXC.J9SCYsAs9lUljwN.IueG', 20, 31, '-', '9120009540876', 'Mustika Jaya', 'Jl. RE Martadinata No. 43', 3271031010, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '300', '-6.583113,106.789114', 1, NULL, NULL, '2021-10-04 13:37:42', '2021-10-04 13:37:42', NULL),
(21, '$2y$10$d1Vooz8i2AKPo31pQ17xmuD1YgDzRlUAeylQXK6MGL07UGTQscMte', 21, 32, '-', '517/113/PM/B/BPPTPM/XI/2016', 'PT. Mitra Pratama Subur Jaya', 'Jl. MA. Salmun No 40 C dan D, RT 01/13', 3271031010, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '200', '-6.590197,106.789055', 1, 1, NULL, '2021-10-04 13:39:20', '2021-10-08 06:20:02', NULL),
(22, '$2y$10$1/8ZQcKPh2DhhIiA.c2S/.dUnqSP/swYGA9QqYMPwCYWO0RbhPuvm', 22, 33, '-', '-', 'Rumah Telur', 'Pasar Anyar Block CD No 141-142', 3271031004, '08159351352', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '0', '-6.591532,106.792091', 1, 1, NULL, '2021-10-04 13:40:41', '2021-11-08 07:38:53', NULL),
(23, '$2y$10$bVlpfoREKjKjSOnO/8seVuRNSzLxI8tSvdTR1iBeZv2/1Q/tGw532', 23, 34, '-', '-', 'Super Telur', 'Pasar Anyar, Blok CD', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '0', '-6.591679,106.791953', 1, NULL, NULL, '2021-10-04 13:42:20', '2021-10-04 13:42:20', NULL),
(24, '$2y$10$8WOpSDLXqApLLmqtJ3wB6ul105A/8XeCpok6tcj8BrWJUlDyl0S/6', 24, 35, '-', '-', 'Toko Aa', 'Jl. MA Salmun, Pasar Anyar', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '0', '-6.590472,106.790565', 1, NULL, NULL, '2021-10-04 13:43:48', '2021-10-04 13:43:48', NULL),
(25, '$2y$10$.in.7asCX9IKpnF6MeAiEOB5z9g/oQ63p5lNlYy6XK9mxNUiu3O1W', 25, 36, '-', '-', 'Toko Alun', 'Jl.Klenteng RT.002 RW.004', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '200', '-6.603798,106.801027', 1, 1, NULL, '2021-10-04 13:45:20', '2021-10-08 06:19:14', NULL),
(26, '$2y$10$tAspg6XvKjzYe.NlKx/66O.eETBAg0o5dZeAGps0HM/Y2Z8n6kyGO', 26, 37, '-', '-', 'Toko Bayangan', 'Pasar Anyar, Blok CD', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '0', '-6.591655,106.792151', 1, NULL, NULL, '2021-10-04 13:46:44', '2021-10-04 13:46:44', NULL),
(27, '$2y$10$BjRLtLwLuFWL1Yy4bAeMU.hH1ilkce4jVfK6r1a4p2WqnqlAcHe1q', 27, 38, '-', '-', 'Toko Iming', 'Jl. Pasar Bogor Ujung', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '100', '-6.603191,106.801015', 1, NULL, NULL, '2021-10-04 13:48:12', '2021-10-04 13:48:12', NULL),
(28, '$2y$10$I2kWSsKlB04w8acT/0S5NuywCQrtC.jIphSelH6o7EMNAJ9EdPngS', 28, 39, '-', '-', 'Toko Kencong Telur', 'Jl. Roda No. 78', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'agen', '25', '-6.608311,106.806285', 1, NULL, NULL, '2021-10-04 13:49:32', '2021-10-04 13:49:32', NULL),
(29, '$2y$10$v./s0tnqtWlnCX2H4ZSgleu3W3YYnBB2yyJX3rEV2KOj56g4j0b5y', 29, 40, '-', '-', 'Toko Kertajaya', 'Jl. MA. Salmun, Pasar Anyar', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '0', '-6.590486,106.790779', 1, NULL, NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL),
(30, '$2y$10$zI1/akAiuN5LFOlecPh19OzhsKK5xZUnO22eV0HER5suvAHHtAH5e', 30, 41, '-', '-', 'Toko Makmur', 'Jl. MA. Salmun no 24 RT 03/06, Pasar Anyar', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '0', '-6.59056,106.791081', 1, NULL, NULL, '2021-10-04 13:53:16', '2021-10-04 13:53:16', NULL),
(31, '$2y$10$mKefweoQHBjC/5O2uOJIJulcxbXLZ086FKf1l5Ng3UEc4LKUWg53a', 31, 42, '-', '-', 'Toko Santo', 'Pasar Merdeka', 3271031011, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'sub_distributor', '0', '-6.592385,106.787102', 1, NULL, NULL, '2021-10-04 13:55:00', '2021-10-04 13:55:00', NULL),
(32, '$2y$10$7FevTnbLEMmtWBlJ0fg4kO8MlNlvxYGGitJEQ0JhbPwozhTjD13Ha', 32, 43, '-', '-', 'UD. Hendra Mandiri', 'Pasar Anyar, Blok CD', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '0', '-6.591431,106.791889', 1, NULL, NULL, '2021-10-04 13:56:26', '2021-10-04 13:56:26', NULL),
(33, '$2y$10$6dpl7YK1TUFRu8RM1yha9eGUXamubtzWfALjiGa76eUKFGwOPVLCe', 33, 44, '-', '-', 'Akin', 'Pasar Bogor', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'sub_distributor', '20', '-6.603349,106.800871', 1, NULL, NULL, '2021-10-04 13:57:57', '2021-10-04 13:57:57', NULL),
(34, '$2y$10$7ba.0jlJS2ptTkXZWQjk9uHSOG36KWNlX26f1OmcvMLleSbCQ98hu', 34, 45, '-', '-', 'Lienun Suryajaya', 'Jl. MA. Salmun No 22', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'agen', '0', '-6.590574,106.791159', 1, 1, NULL, '2021-10-04 13:59:18', '2021-10-08 06:18:45', NULL),
(35, '$2y$10$y1.f/VQWGnsIh4WhKg7SrORLUrtUuSYNZeL54d.11L8dJgTWyRGVC', 35, 46, '-', '-', 'Herlina Anggawikara', 'Jl. Kelenteng no 33', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'agen', '40', '-6.603261,106.800559', 1, 1, NULL, '2021-10-04 14:00:42', '2021-10-08 06:19:33', NULL),
(36, '$2y$10$r0T9ZR8eve7rxo1j78mZIeORXTqC7D./.1HQOnaBesjIwRTW8t0Mm', 36, 47, '-', '-', 'Agen Telur Kencana', 'Jl. Roda', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.605367,106.802062', 1, NULL, NULL, '2021-10-04 14:02:10', '2021-10-04 14:02:10', NULL),
(37, '$2y$10$RfKAz8T/nX/fkQHXvfBMQum9reqGe9cWFubsf/oGBfoRPEg33BHSS', 37, 48, '-', '-', 'Dany(Iming)', 'Jl.Klenteng RT.03 RW.04', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.603203,106.801177', 1, 1, NULL, '2021-10-04 14:03:30', '2021-10-08 06:19:43', NULL),
(38, '$2y$10$WaQiww1ND26RXNozaCHUYeDpTay9WzQ0pqG.UJm8Z6F6aDDF1vCPq', 38, 49, '-', '-', 'Toko Kota Jaya', 'Jl. MA Salmun No.22 RT.04/RW.06', 3271031004, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.590658,106.790916', 1, 1, NULL, '2021-10-04 14:04:53', '2021-10-08 06:18:59', NULL),
(39, '$2y$10$W1yK0eEaWRTHMfRRHFeSLOe8lUrVGLV/0HinmU/EGJ1QvwD5erXMq', 39, 50, '-', '-', 'Susanto', 'Jl.Re.MaRTadinata.RT.01/09', 3271031010, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.582949,106.789189', 1, NULL, NULL, '2021-10-04 14:06:28', '2021-10-04 14:06:28', NULL),
(40, '$2y$10$wpJx2xIlptYyaSay9yAXneQ864GkNhSYb/QRHnsrVTms/Oo4tEIjm', 40, 51, '-', '-', 'Toko Herlina Angga Wikara ', 'Jl.Klenteng RT.003 RW.003', 3271031008, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.603237,106.800563', 1, 1, NULL, '2021-10-04 14:07:49', '2021-10-08 06:19:52', NULL),
(41, '$2y$10$d4X94w8/X2OYwYu7FDfbs.qQznTVAj1ssRkGOxFWkqtQ3NhUByLO.', 41, 52, '-', '-', 'Budiono Djapara (Trijaya)', 'Jl.Merdeka 85 RT.03/04', 3271031011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.59078,106.787837', 1, 1, NULL, '2021-10-04 14:09:08', '2021-10-08 06:20:33', NULL),
(42, '$2y$10$hXflXXSDEDeLaHfNsmnISuTFeqUyt4XuHbtkVR.KoQOvS.vD.PYUK', 42, 53, '-', '-', 'Toko Andre', 'Jl.Merdeka 89 RT.03/04', 3271031011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.590493,106.787542', 1, 1, NULL, '2021-10-04 14:10:33', '2021-10-08 06:26:26', NULL),
(43, '$2y$10$ej/lhi81l780QUkmmKSlx.VeBZl1H8rRlbKwttodVs5C6vJOdC5XG', 43, 54, '-', '-', 'Toko Sebelas', 'Jl. Lawang Seketeng No 11', 3271031001, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '0', '-6.605098,106.797691', 1, 1, NULL, '2021-10-04 14:12:12', '2021-10-08 06:18:28', NULL),
(44, '$2y$10$lPp9a2PWJTtylBIBHkWL4.sYy4zBjzfA3jUZ2J0YE/jWzVLk4wcaO', 44, 55, '-', '-', 'Rizal', 'Lw. Seketeng', 3271031001, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '220', '-6.605136,106.797823', 1, NULL, NULL, '2021-10-04 14:13:38', '2021-10-04 14:13:38', NULL),
(45, '$2y$10$7cOab3Wdku4zLOJgQJZPNO19E1uxHhWAwK231Y/eGs0VOO9ikSxG2', 45, 56, '-', '-', 'Harahap Jaya', 'Ciheuleut', 3271021002, '-', 'permukiman', 'milik_sendiri', 'sewa', 'agen', '12', '-6.602938,106.813504', 1, NULL, NULL, '2021-10-04 14:16:52', '2021-10-04 14:16:52', NULL),
(46, '$2y$10$cTzbjN.MhtcD3OtrfMEqgOvUCSlblV5LilJ1awkf6Z6fYZhAsB.le', 46, 57, '-', '-', 'Ibu Jukasih', 'Pasar Gembrong', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '4', '-6.619101,106.814428', 1, NULL, NULL, '2021-10-04 14:18:46', '2021-10-04 14:18:46', NULL),
(47, '$2y$10$qXvqBtQm/gWVBEIjz4MUb.aVmYA9DdUPImLI6n15KTJrf8lRfsava', 47, 58, '-', '-', 'Kios Sri', 'Pasar Gembrong', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '9', '-6.619266,106.81401', 1, 1, NULL, '2021-10-04 14:20:15', '2021-10-04 14:21:54', NULL),
(48, '$2y$10$OjBoAbIcH9ExkUENBceq.O66f84t/zUYYbWAz1rHrKOKL5.HDbzdK', 48, 59, '-', '-', 'Sukasih', 'Pasar Gembrong', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '4', '-6.618074,106.812532', 1, NULL, NULL, '2021-10-04 14:21:42', '2021-10-04 14:21:42', NULL),
(49, '$2y$10$ASXJruCo29heRo4Y7Fkmb.e.bqxsFU8Qd9N..r/ddN2cx/YqzKdii', 49, 60, '-', '-', 'Sumber Beras Utama', 'Jl. R3, Katulampa, Al-Busro', 3271021003, '-', 'permukiman', 'milik_sendiri', 'sewa', 'agen', '30', '-6.621747,106.829094', 1, NULL, NULL, '2021-10-04 14:23:19', '2021-10-04 14:23:19', NULL),
(50, '$2y$10$QWq0vSk4HJdY5EAJengn5.hiiEY/plECbT9RWTqzgL4J1ifJhG7s6', 50, 61, '-', '-', 'Telur Berkah', 'Jl. Raya Parung Banteng RT 07/01', 3271021003, '-', 'permukiman', 'pekerja', 'sewa', 'agen', '16', '-6.622598,106.829135', 1, NULL, NULL, '2021-10-04 14:24:45', '2021-10-04 14:24:45', NULL),
(51, '$2y$10$k1/rlUV3HBG7KU/rAtdVZ.1yM8sjUEBgwRnuMofLsWxOkFXPC6R6e', 51, 62, '-', '-', 'Toko Ading Sayur', 'Pasar Gembrong', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '9', '-6.617808,106.813172', 1, NULL, NULL, '2021-10-13 03:32:12', '2021-10-13 03:32:12', NULL),
(52, '$2y$10$Q79ZFkezmMW5nXfA1absOOpFqmU.tihDWZpXhkOrllOkZkqLaxP2W', 52, 63, '-', '-', 'Toko Berkah', 'Bantarkemang RW 13', 3271021002, '-', 'permukiman', 'milik_sendiri', 'sewa', 'agen', '16', '-6.614979,106.819617', 1, NULL, NULL, '2021-10-13 03:34:47', '2021-10-13 03:34:47', NULL),
(53, '$2y$10$411Mg6/q.CvDrKiSESMbeuqRXDxlNMFiXJgjEaWRnM4GgpUmSWwAG', 53, 64, '-', '-', 'Toko Intan', 'Jl. Raya Parung Banteng RT 02/07, Katulampa', 3271021003, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'agen', '16', '-6.620456,106.822078', 1, NULL, NULL, '2021-10-13 03:36:31', '2021-10-13 03:36:31', NULL),
(54, '$2y$10$p27XdsVEVGEnTyvgi7wDHOiO6af9U7hX3im1m/UsBeO9zq4Sywefa', 54, 65, '-', '-', 'Toko Kerny', 'Pasar Gembrong, Sukasari', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '8', '-6.618898,106.814043', 1, NULL, NULL, '2021-10-13 03:38:11', '2021-10-13 03:38:11', NULL),
(55, '$2y$10$XPrrkrRJPty9iviSf2kMyeB2MDt90pLBLjjk29UORTeOGyvoMxtH2', 55, 66, '-', '-', 'Toko Sri', 'Pasar Gembrong', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '9', '-6.618429,106.813124', 1, NULL, NULL, '2021-10-13 03:39:38', '2021-10-13 03:39:38', NULL),
(56, '$2y$10$0MbuPEy7tJRRejC87i73o.bruOh6fXRGF9HNWa.lSwXIgshKF0LDe', 56, 67, '-', '-', 'Toko Yuni', 'Pasar Gembrong', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'lapak_pesan', 'agen', '9', '-6.619124,106.81375', 1, NULL, NULL, '2021-10-13 03:41:06', '2021-10-13 03:41:06', NULL),
(57, '$2y$10$vUSzKpb3yP8A62crJ2DUTORLAmRcBMtLEMAlfit7OLI3INVrMBVKi', 57, 68, '-', '-', 'Dhany', 'Jl. Kelengkeng', 3271021001, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '202', '-6.612645,106.809165', 1, NULL, NULL, '2021-10-13 03:42:34', '2021-10-13 03:42:34', NULL),
(58, '$2y$10$fUcO6pe5JR4fZec98hyCBeyx2RL6x09v8yKalAYTsgzbIp3O8mFai', 58, 69, '-', '-', 'Lapak UD. Saepul', 'Pasar Induk Warung Jambu no 34', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'sub_distributor', '30', '-6.570398,106.807393', 1, NULL, NULL, '2021-10-13 03:44:24', '2021-10-13 03:44:24', NULL),
(59, '$2y$10$T06uB6ndGKjubogUgYj.yuh58X7oDvzR0ZdVd0Na15SL9rqWOkWwG', 59, 70, '-', '-', 'Tiga Sodara', 'Pasar Induk Warung Jambu', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'sub_distributor', '20', '-6.570359,106.80722', 1, NULL, NULL, '2021-10-13 03:45:42', '2021-10-13 03:45:42', NULL),
(60, '$2y$10$TZu/vyJL6G8F9ULW3101nOEjokSyGc8D7Kycw0Z.uoE1Pvm2QzMNe', 60, 71, '-', '-', 'Toko Al Jawad', 'Pasar Induk Warung Jambu no 4', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '50', '-6.570202,106.807311', 1, NULL, NULL, '2021-10-13 03:47:00', '2021-10-13 03:47:00', NULL),
(61, '$2y$10$b32ydeI8j00Ba0LgArQWAeNj0BR22ZBbI4VL.12LfhJMYUy7lGoGG', 61, 72, '-', '-', 'Toko Beras Madina', 'Pasar Induk Warung Jambu', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '30', '-6.570763,106.807203', 1, NULL, NULL, '2021-10-13 03:48:21', '2021-10-13 03:48:21', NULL),
(62, '$2y$10$klt6k3aYJCBAN7u9mAYGKuS5ZSPso7535fV6T.4uZdgUNyqEY5bbq', 62, 73, '-', '-', 'Toko Maura', 'Pasar Induk Warung Jambu Lapak no 83', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'sub_distributor', '40', '-6.570956,106.806936', 1, NULL, NULL, '2021-10-13 03:49:30', '2021-10-13 03:49:30', NULL),
(63, '$2y$10$MaW.yD5ycZn1qQdrZiQFLe8DcPvMNJva5BZdY2ONnpubhJuMBnIW6', 63, 74, '-', '-', 'Toko Paul', 'Pasar Induk Warung Jambu No 47', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'sub_distributor', '20', '-6.570354,106.807187', 1, NULL, NULL, '2021-10-13 03:50:46', '2021-10-13 03:50:46', NULL),
(64, '$2y$10$InpshouH4ranwyZ54jZX0.xWU.Ae4S0ERKDRYkyg9DSbl7VsxDaxa', 64, 75, '-', '-', 'Toko Tiga Sodara', 'Pasar Induk Warung Jambu no 2', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'agen', '40', '-6.571073,106.806854', 1, NULL, NULL, '2021-10-13 03:53:26', '2021-10-13 03:53:26', NULL),
(65, '$2y$10$CZ8cw1bgbnIf5Q.6jsMWqO4CkCPNjULyE.BtfTRsha0XjsV5I8fRK', 65, 76, '-', '-', 'Ud. Sumatera', 'Pasar Induk Warung Jambu lapak 104', 3271061001, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'sub_distributor', '20', '-6.570842,106.807073', 1, NULL, NULL, '2021-10-13 03:54:48', '2021-10-13 03:54:48', NULL),
(66, '$2y$10$1m25zwDY0YuNrvGhg7DUWO4HH7SX2o.JKHHBV7Wka/3Wygj2UwtN6', 66, 77, '-', '-', 'Ud. Usaha Dagang Sejahtera', 'Pasar Induk Warung Jambu', 3271061001, '-', 'wilayah_usaha', 'pekerja', 'sewa', 'sub_distributor', '0', '-6.570107,106.807192', 1, NULL, NULL, '2021-10-13 03:56:15', '2021-10-13 03:56:15', NULL),
(67, '$2y$10$mYnUdUl5l1FopChaG3w5sekARt9hZagxZQ0CNtq10t98NmeFdJ2LW', 67, 78, '-', '-', 'Klp. Nusantara', 'Pasar Induk Kemang Bogor', 3271061011, '-', 'wilayah_usaha', 'pekerja', 'lapak_pesan', 'sub_distributor', '1000', '-6.53941,106.769836', 1, NULL, NULL, '2021-10-13 03:57:57', '2021-10-13 03:57:57', NULL),
(68, '$2y$10$tzPc4SZqFcvArcJq7Wc8E.Nb6r29xTrZe8QUmtMiWson5MOzpQyCq', 68, 79, '-', '-', 'Pak Opel', 'Pasar Kemang TU C9 no 24', 3271061011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '30', '-6.539643,106.769911', 1, NULL, NULL, '2021-10-13 03:59:25', '2021-10-13 03:59:25', NULL),
(69, '$2y$10$TAh6zw9oQ/dpKr5haf5fnu.I6n8vALuCluy3HzGytM0gHWGmgjitq', 69, 80, '-', '-', 'Toko A. Barokah', 'Pasar Kemang TU no 45', 3271061011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '30', '-6.540024,106.769069', 1, NULL, NULL, '2021-10-13 04:00:53', '2021-10-13 04:00:53', NULL),
(70, '$2y$10$c2uPf0ji1wTMtKlGEqAyyeeU5MhivUnoWO4NEKAynk3DK.K8FMJt.', 70, 81, '-', '-', 'Toko Hamim', 'Pasa Kemang TU C9', 3271061011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '30', '-6.540037,106.769821', 1, NULL, NULL, '2021-10-13 04:02:23', '2021-10-13 04:02:23', NULL),
(71, '$2y$10$.XVjFS.4ydRUeapfRBL2tOcImAQzIgvR8T6sG2Ojuza16FSULJK9C', 71, 82, '-', '-', 'Toko Pak Abbas', 'Pasa Kemang TU C12', 3271061011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '30', '-6.540534,106.769942', 1, NULL, NULL, '2021-10-13 04:03:36', '2021-10-13 04:03:36', NULL),
(72, '$2y$10$3k45D368tBKxyhn3U4m42.j2Rz9Zw.G5lQT5QUAUacCXjckSeE29.', 72, 83, '-', '-', 'Toko Pak Age', 'Pasa Kemang TU C9 no 25', 3271061011, '-', 'wilayah_usaha', 'milik_sendiri', 'milik_sendiri', 'distributor', '30', '-6.539169,106.769827', 1, NULL, NULL, '2021-10-13 04:04:57', '2021-10-13 04:04:57', NULL),
(73, '$2y$10$efjAqysdSOAPs5yw4/Rfnu3yqsVFdAwJ/b6WZ2nbYM43Rqx0tSXt2', 73, 84, '-', '-', 'Toko. H. Ucup', 'Pasa Kemang TU No 19', 3271061011, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '30', '-6.538871,106.768901', 1, NULL, NULL, '2021-10-13 04:06:22', '2021-10-13 04:06:22', NULL),
(74, '$2y$10$XjBTZWmHGuR993.NHXlMUutq0KTM2JZpi8iUeLV6yMsYgYmHG3ZnW', 74, 85, '-', '-', 'H.Hairul', 'Jl. Pool Binamarga RT.001/001', 3271061010, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.530411,106.769585', 1, NULL, NULL, '2021-10-13 04:08:17', '2021-10-13 04:08:17', NULL),
(75, '$2y$10$7SysZG1YBG8gocvlA32K2e7HaHGslHjLrAS/S70oIGSOu9K59cAsy', 75, 86, '-', '-', 'PT. Proman Kasa', 'Jl. Sholeh Iskandar RT. 001/001', 3271061011, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.541823,106.771693', 1, NULL, NULL, '2021-10-13 04:09:37', '2021-10-13 04:09:37', NULL),
(76, '$2y$10$4FNPE07qB/0c39daKhzCJ.n9Odxrcu3IGdYJgBbjx2EqMxQf9I5ru', 76, 87, '-', '-', 'Hantoso Suteja (Gudang 45)', 'RT.002/001', 3271061003, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.561515,106.804494', 1, NULL, NULL, '2021-10-13 04:11:15', '2021-10-13 04:11:15', NULL),
(77, '$2y$10$6sOLhACGqNrvYWqPTxuer.MAi938PjCZ80pvviuyX2VaM1fMK5nea', 77, 90, '-', '-', '-', 'Pasar Anyar, Blok CD', 3271031004, '089604261465', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.59175545575181,106.7921680212021', 1, NULL, NULL, '2021-11-08 04:19:03', '2021-11-08 04:19:03', NULL),
(78, '$2y$10$EuF3yZ.CdnrKSTTcNgRPGuU/SI.XSIP8j15Z/IxZhCUBql1waHSHa', 78, 91, '-', '-', 'Zaki Jaya', 'Gang Masjid no 27 RT 01/18,\r\nCilendek Timur', 3271041009, '085771660466', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'distributor', '12', '-6.575267393704666,106.77538812160493', 1, NULL, NULL, '2021-11-08 04:33:01', '2021-11-08 04:33:01', NULL),
(79, '$2y$10$10/XdMrlAU8lITchRAv98Op3bshKTeykSf1kOIfhrH8xnyiHSu1Hy', 79, 92, '-', '-', 'Aah Sayur', 'Jl. Pasar Dramaga RT 03/03', 3271041003, '-', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'distributor', '0', '-6.568179597058648,106.75726175308228', 1, 1, NULL, '2021-11-08 04:50:49', '2021-11-08 06:37:04', NULL),
(80, '$2y$10$lXYp2z2SKQBiMdEfBpLes.ky/lfOKCWA0L5LuZKrW6IcjCYkIjqiC', 80, 93, '-', '-', '-', 'Jl. Pasar Dramaga', 3271041003, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '12', '-6.568520666277924,106.75734758377077', 1, NULL, NULL, '2021-11-08 06:15:22', '2021-11-08 06:15:22', NULL),
(81, '$2y$10$.i6d3GoLfdqWYtI9I1IYneMwtMftQCc9JNxinb0ZFRZlLl941ngIS', 81, 94, '-', '-', 'Sayuran Fauzi', 'Jl. Pasar Dramaga', 3271041003, '0857769333855', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '12', '-6.568520666277924,106.75734758377077', 1, NULL, 1, '2021-11-08 06:23:08', '2021-11-08 07:53:00', '2021-11-08 07:53:00'),
(82, '$2y$10$2cG9Ewsv2jDcaysbZf6K7.ZGZ6bc2WAI5A35kbew083M3M41G0xGi', 82, 95, '-', '-', 'Toko Mandiri Berkah', 'Pasar Induk Warung Jambu', 3271051001, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.568520666277924,106.75734758377077', 1, 1, NULL, '2021-11-08 06:35:33', '2021-11-08 06:36:41', NULL),
(83, '$2y$10$o3wXdIM0Y/Y7f3b0vums1OSYLvSi5W8iLNJSqjvpo4vVREvyRmZrW', 83, 96, '-', '-', 'Toko Pak Age', 'Pasa Kemang TU C9 no 25', 3271061011, '0', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.551893269964722,106.78378343582153', 1, NULL, NULL, '2021-11-08 06:43:11', '2021-11-08 06:43:11', NULL),
(84, '$2y$10$4csfrtFhr./IvEdlH.rlxORjVDug.jpFPzjGmmIlH4Ij.gRz21zoW', 84, 97, '-', '-', '-', 'Pasar Bogor Blok 11-13 A', 3271031005, '-', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.568520666277924,106.75734758377077', 1, NULL, NULL, '2021-11-08 06:55:27', '2021-11-08 06:55:27', NULL),
(85, '$2y$10$mSb15rEasFKEPL2/z.5deOMO0S.MXaHezHGWvY9a/oC16aTGuagnG', 85, 98, '-', '-', '-', 'Pasar Anyar, Blok CD', 3271031004, '0', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.591723482018142,106.7920985620846', 1, 1, NULL, '2021-11-08 07:01:29', '2021-11-11 07:17:11', NULL),
(86, '$2y$10$Q95.8lxwX3s3C5m3EdvSkum2NxVyIHyhuzW0w11C5AtSd.6aztFkW', 86, 99, '-', '-', '-', 'Jl. Sindangbarang', 3271041002, '0', 'permukiman', 'milik_sendiri', 'milik_sendiri', 'distributor', '12', '-6.568520666277924,106.75734758377077', 1, NULL, NULL, '2021-11-08 07:06:44', '2021-11-08 07:06:44', NULL),
(87, '$2y$10$sClNOuMeAFkipmUR6C4XX.APRhehki5ustakIFWR4LxxWmvzb84k6', 87, 100, '-', '-', 'Sayuran Fauzi', 'Jl. Pasar Dramaga', 3271041003, '085776933385', 'wilayah_usaha', 'milik_sendiri', 'sewa', 'distributor', '0', '-6.568520666277924,106.75734758377077', 1, 1, NULL, '2021-11-08 07:58:08', '2021-11-08 08:02:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan_bidang_usaha`
--

CREATE TABLE IF NOT EXISTS `perusahaan_bidang_usaha` (
  `id` bigint(20) NOT NULL,
  `kode_perusahaan_bidang_usaha` varchar(100) NOT NULL,
  `id_perusahaan` bigint(20) NOT NULL,
  `id_jenis_barang` bigint(20) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `perusahaan_bidang_usaha`
--

INSERT INTO `perusahaan_bidang_usaha` (`id`, `kode_perusahaan_bidang_usaha`, `id_perusahaan`, `id_jenis_barang`, `status`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$0rXOdP/QvzLqNmjuEt3qR.AibR6UeuAogrBendtT8wzzEcgbTnH7G', 1, 1, '1', 1, 1, NULL, '2021-09-13 12:07:51', '2021-10-01 09:44:58', NULL),
(2, '$2y$10$KwrSvOQ6z0Ngq/n0d4cmXOQ3m86CYlTmnYXFCemz6qS6KkRQk0Qhu', 1, 2, '0', 1, 1, NULL, '2021-09-13 12:07:51', '2021-10-04 09:45:11', NULL),
(3, '$2y$10$oTMrt1HvqH8OmyC6Ywj99utVeve9tfS0TFbjxq2OxCqvAZ40A3/GK', 2, 2, '1', 1, NULL, NULL, '2021-10-04 12:57:30', '2021-10-04 12:57:30', NULL),
(4, '$2y$10$CvXMQHgWvC75qOTMTjAc6.8gX6PGyjSR8uxXv3lv9J8O9xxB0TWTO', 3, 2, '1', 1, NULL, NULL, '2021-10-04 12:59:42', '2021-10-04 12:59:42', NULL),
(5, '$2y$10$V.A3TkfhUreHQBBazREE9.yclLNty1vdrZSGvgM09Vn/y9QLXdbq.', 4, 1, '1', 1, NULL, NULL, '2021-10-04 13:02:17', '2021-10-04 13:02:17', NULL),
(6, '$2y$10$pPNaHoeygSjEv283iquDOuNzUlROkB7zsXtIxOBX/lSTIYJMw1EGy', 5, 3, '1', 1, NULL, NULL, '2021-10-04 13:04:13', '2021-10-04 13:04:13', NULL),
(7, '$2y$10$hXxpPWpc1cw.wtgb3v3TJeBG7MrV5BhJamuj3Pg4BMv3Fq6mesgbe', 6, 3, '1', 1, NULL, NULL, '2021-10-04 13:06:09', '2021-10-04 13:06:09', NULL),
(8, '$2y$10$T0tWs/pw3qOBxxwVGGZXguABoRZYySI320BnIWTZi7PFzM8GR2JZq', 7, 4, '1', 1, 1, NULL, '2021-10-04 13:07:41', '2021-10-04 13:11:25', NULL),
(9, '$2y$10$as6zhgqoq.gsDcQChZAnouBI6XSakcdwplUo.2NbgbYHpy1D7pTiK', 8, 5, '0', 1, 1, NULL, '2021-10-04 13:09:23', '2021-10-08 06:20:12', NULL),
(10, '$2y$10$m6tdrA9wEkkRtlXoLjo27OedyR7f.9rg6QfsMTrR9f2WCBZBLqXgC', 9, 5, '0', 1, 1, NULL, '2021-10-04 13:11:07', '2021-10-08 06:20:54', NULL),
(11, '$2y$10$P5vL7nLwSB3iPaLDa6Pg3uck9sjffYYznyD3M9gCIy1ALPnhp5IUO', 10, 6, '0', 1, 1, NULL, '2021-10-04 13:13:32', '2021-10-08 06:25:10', NULL),
(12, '$2y$10$k/JHGu5IkqfZaa7t9oFx7eAo.L4iCZYsOnmvsCbP5NvYKOHMJ0frC', 11, 5, '0', 1, 1, NULL, '2021-10-04 13:15:09', '2021-10-08 06:21:02', NULL),
(13, '$2y$10$mVutZcd1lDPCDp7j5Q7Mzu62WdOODhmiEQIjvd4g7XV3pX4ctpsaW', 12, 8, '1', 1, NULL, NULL, '2021-10-04 13:16:42', '2021-10-04 13:16:42', NULL),
(14, '$2y$10$sfbNxTyVuKQ6rKmk8LIZJuccS85lWwckNFTnUO0eDGwNS.5JEgLbG', 13, 8, '1', 1, 1, NULL, '2021-10-04 13:18:32', '2021-10-08 06:23:02', NULL),
(15, '$2y$10$SKGtKIhS3jYLcAZSLG2hWetCLXvL/yI1EPJiPq5Hkf48mcoTsm9LC', 13, 9, '0', 1, 1, NULL, '2021-10-04 13:18:32', '2021-10-08 06:23:02', NULL),
(16, '$2y$10$kYKUfyb1nMriuXeb0UcTr.3MERvVBv2NKTKXS2JaV4O8DKX9ujHCa', 14, 17, '1', 1, NULL, NULL, '2021-10-04 13:20:04', '2021-10-04 13:20:04', NULL),
(17, '$2y$10$aS9oe6tZnMfAjBy8ZtepOe8CO.wGhuN3rTkvajLE3HCphz8xv.Vwa', 15, 24, '1', 1, NULL, NULL, '2021-10-04 13:21:34', '2021-10-04 13:21:34', NULL),
(18, '$2y$10$FYuxLQRXHTzi5xSbOtPdUeDO2v2Vshsc0ZZDMboyMrdNyCT547wUm', 16, 12, '0', 1, 1, NULL, '2021-10-04 13:23:04', '2021-10-08 06:25:52', NULL),
(19, '$2y$10$2G5ejyA3vbyy9zD3g4N/4ugeEJj7flMN8MaBjE.X777spqRLVEERa', 17, 2, '1', 1, 1, NULL, '2021-10-04 13:25:10', '2021-11-08 07:43:37', NULL),
(20, '$2y$10$IuNv4nEquTbndjhIUF5pxevdN5XvHi.U0w3MTHEh6HQgiuLSWcYCK', 18, 2, '1', 1, NULL, NULL, '2021-10-04 13:26:36', '2021-10-04 13:26:36', NULL),
(21, '$2y$10$kVZ48UpH5I1GTEogGPdTp./bs6RNHeuabkTHW3eJiuAmzUY8v8gYq', 19, 15, '1', 1, NULL, NULL, '2021-10-04 13:36:01', '2021-10-04 13:36:01', NULL),
(22, '$2y$10$XcQqLMw.2D2ial0g1gw/T.LeR9QGhfhNpNkY2/GbNKbldOm9/cLj2', 19, 14, '1', 1, NULL, NULL, '2021-10-04 13:36:01', '2021-10-04 13:36:01', NULL),
(23, '$2y$10$Iqj3HWvgFwHLIO97XLZ6IOlMOXRdF6lfCxETB7QHRo/yENrF8G6jy', 20, 16, '1', 1, NULL, NULL, '2021-10-04 13:37:42', '2021-10-04 13:37:42', NULL),
(24, '$2y$10$MOXs8CmCqkxxF7Sq41lxQO2ur88y/nEkKHi.XSAdz/R.yh8e8mF1m', 21, 5, '0', 1, 1, NULL, '2021-10-04 13:39:20', '2021-10-08 06:20:02', NULL),
(25, '$2y$10$FOdrqh.ivEpPKDqrDyv5vezrlcp4X22998bnDlXpb30exF9HiNvUC', 22, 2, '1', 1, 1, NULL, '2021-10-04 13:40:41', '2021-11-08 07:38:53', NULL),
(26, '$2y$10$RmJ4o9g1odXVQZZzNqS.I.WFsvvlste9S0uGOi1Z2tPFXhY9U2f96', 23, 2, '1', 1, NULL, NULL, '2021-10-04 13:42:20', '2021-10-04 13:42:20', NULL),
(27, '$2y$10$pWBTAwrBW28A.CnYXqGb8.sAz/sjFJGsixOtFWLbq0M7J6wRHNhLu', 24, 1, '1', 1, NULL, NULL, '2021-10-04 13:43:48', '2021-10-04 13:43:48', NULL),
(28, '$2y$10$LAs05tcPzHGIR14NDCHhJuWHMjgC250PbEyp1BAvaAdX94SpOoQmG', 25, 5, '0', 1, 1, NULL, '2021-10-04 13:45:20', '2021-10-08 06:19:14', NULL),
(29, '$2y$10$xm9RWPfv/P43Y19XiQMsjeQGNsHD/VsEVgU/v.mnSXQg.ogaGT5/y', 26, 2, '1', 1, NULL, NULL, '2021-10-04 13:46:44', '2021-10-04 13:46:44', NULL),
(30, '$2y$10$t8VqJu7kPiSnUCRmwU2t9OBieViXZZMF8z28BxE/TcGq1S.iPqO4m', 27, 14, '1', 1, NULL, NULL, '2021-10-04 13:48:12', '2021-10-04 13:48:12', NULL),
(31, '$2y$10$AhdlXOYIn.N7lycBQDywsOl5ASyqUsjna/fqtXh4UsUEQzZSGf0Ay', 27, 17, '1', 1, NULL, NULL, '2021-10-04 13:48:12', '2021-10-04 13:48:12', NULL),
(32, '$2y$10$/MgNErpsKZCDq2GKFZTFaOLN4ZFw8HnaLwymoPMhilW13xSGSQYSO', 28, 2, '1', 1, NULL, NULL, '2021-10-04 13:49:32', '2021-10-04 13:49:32', NULL),
(33, '$2y$10$d9DINIycVsYEja7qGzQlnOtEr6RMtPzvwD647.sNMdmMcsLMOafcq', 29, 2, '1', 1, NULL, NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL),
(34, '$2y$10$hsaimnCd81lSfcDDeK/c7Og8jUbrrrziOyf4z.vfKPnUKkVsVZn7m', 29, 15, '1', 1, NULL, NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL),
(35, '$2y$10$Wna9doYwav4RcW4agEdDA.Ijlx8xg.eMqEAaBLVvtOlQev6fwIYNG', 29, 1, '1', 1, NULL, NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL),
(36, '$2y$10$iuw0avk8qQs/wa0yKOIaE.gm/w5LwL37f86M2WTuDz7hpsgX9ZA66', 29, 14, '1', 1, NULL, NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL),
(37, '$2y$10$re5klRz6uFhqtYQZYi0VSeOiqLc0bXqx/l3Qpxo/S5x6f2xFPBSAi', 29, 17, '1', 1, NULL, NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL),
(38, '$2y$10$Wh3tTNnoUiQ1vYaItv0/reLGlzt80N3.I4eW4INZd3qFdjv4plz7K', 30, 15, '1', 1, NULL, NULL, '2021-10-04 13:53:16', '2021-10-04 13:53:16', NULL),
(39, '$2y$10$On3rgUXpIeHWqJWwYrAUQu1BVcKSOWpodqepjz50Nnzzdo5.lFy/m', 30, 14, '1', 1, NULL, NULL, '2021-10-04 13:53:16', '2021-10-04 13:53:16', NULL),
(40, '$2y$10$TeLafbSKbYEmjzjqSaDYve5f5nwquESjKmuNcRnevoDJ.giaeXK7W', 30, 17, '1', 1, NULL, NULL, '2021-10-04 13:53:16', '2021-10-04 13:53:16', NULL),
(41, '$2y$10$eNkiFACKmnpTmzXMPl.pIuVMI/8QXHqaECH2WtxzedOdha9Vh.VOG', 31, 15, '1', 1, NULL, NULL, '2021-10-04 13:55:00', '2021-10-04 13:55:00', NULL),
(42, '$2y$10$4aXwQl7R8PAzpetRI/ZmSO7Js6bq06rBd/QHdqrYrh5/vYVZ2M87K', 31, 14, '1', 1, NULL, NULL, '2021-10-04 13:55:00', '2021-10-04 13:55:00', NULL),
(43, '$2y$10$k/MtYe5REPDNwJr4hRTwf.P6olPz.XoDLMDGjX4.0klA4QaVgbF.u', 31, 17, '1', 1, NULL, NULL, '2021-10-04 13:55:00', '2021-10-04 13:55:00', NULL),
(44, '$2y$10$IkTreZlx2eknFxulbAOuF.XV1kjHJaJeNv2z.sIzKjlg3lB3IiXrC', 32, 4, '1', 1, NULL, NULL, '2021-10-04 13:56:26', '2021-10-04 13:56:26', NULL),
(45, '$2y$10$8Ywc2Tt0lwuZSgM21hJrsOPr8RR7.56rcJLhSCCbJ25Z/fhfrDvG6', 33, 18, '1', 1, NULL, NULL, '2021-10-04 13:57:57', '2021-10-04 13:57:57', NULL),
(46, '$2y$10$/mt2V04.h3L7krvDqd97juYWSUT9XbUas7TyxNOvUf8C3BUZJDJZi', 34, 5, '0', 1, 1, NULL, '2021-10-04 13:59:18', '2021-10-08 06:18:45', NULL),
(47, '$2y$10$HsotEHoBMd8QTC.DmQ5aOObAtESYOV974XlbQZ90zOYcGFZ8/5ut.', 35, 5, '0', 1, 1, NULL, '2021-10-04 14:00:42', '2021-10-08 06:19:33', NULL),
(48, '$2y$10$Jfub9lo/l3hfWPor5PC.xe6jxqoOgObqhD9GsRDinaOSqfVzD1bnW', 36, 2, '1', 1, NULL, NULL, '2021-10-04 14:02:10', '2021-10-04 14:02:10', NULL),
(49, '$2y$10$MfxBh5zVZyuJZFHH9LCIIekS1k5xQkKAtZe7zChExy9F1skeNqleO', 37, 5, '0', 1, 1, NULL, '2021-10-04 14:03:30', '2021-10-08 06:19:43', NULL),
(50, '$2y$10$nOVMXRSeFOyzUd7MLmFfFeOKAJugiyRi0a6vOtoP7nuMdElAvn8Jm', 38, 5, '0', 1, 1, NULL, '2021-10-04 14:04:53', '2021-10-08 06:18:59', NULL),
(51, '$2y$10$qr2X0rt8R5HAQ01r7sGvlOVncfghPas8tZwuFhJhilRPpbbV0qgnC', 39, 16, '1', 1, NULL, NULL, '2021-10-04 14:06:28', '2021-10-04 14:06:28', NULL),
(52, '$2y$10$4Km7AFWwDWwHwTqpHjunv.CZA8nwvr5NG8FKNLjBGZYfBOPODhY2a', 40, 5, '0', 1, 1, NULL, '2021-10-04 14:07:49', '2021-10-08 06:19:52', NULL),
(53, '$2y$10$Wh7rIGPFlMxsckVBn4bQoexBtqRZ7ETGAhadyN4LazSF.sigw9LHy', 41, 5, '0', 1, 1, NULL, '2021-10-04 14:09:08', '2021-10-08 06:20:33', NULL),
(54, '$2y$10$8ckjRCLe8GOfhijvsufqDOVD9niKnYgMLvmJ5yAZyAav6nL91HbvC', 41, 1, '1', 1, NULL, NULL, '2021-10-04 14:09:08', '2021-10-04 14:09:08', NULL),
(55, '$2y$10$RAW.AMZOCsHdIhBHvtZh5eUmxo19wg4V2ousus188o.BJvshH6ySq', 42, 20, '0', 1, 1, NULL, '2021-10-04 14:10:33', '2021-10-08 06:26:26', NULL),
(56, '$2y$10$hEf/i2lmZ6SgEuTFAEbHweSLHvyjO.DjTJLePcMAjWKKMd8vOkp12', 43, 5, '0', 1, 1, NULL, '2021-10-04 14:12:12', '2021-10-08 06:18:28', NULL),
(57, '$2y$10$paHwc58Rs/B5RvIpBwir3OocJxG0JR77QHM87q.iNc3kOvYdoleiG', 44, 24, '1', 1, NULL, NULL, '2021-10-04 14:13:38', '2021-10-04 14:13:38', NULL),
(58, '$2y$10$4crNR0Tb9SEz41aq9VgYz.G60ZrZytyQHfVNklTfyjsMZVSVndSIy', 45, 2, '1', 1, NULL, NULL, '2021-10-04 14:16:52', '2021-10-04 14:16:52', NULL),
(59, '$2y$10$lfMjLUejPHoM/Nv0aC1Ah.MXeRJPMthxbntiXPnYDLU9pMBdMuZeW', 45, 1, '1', 1, NULL, NULL, '2021-10-04 14:16:52', '2021-10-04 14:16:52', NULL),
(60, '$2y$10$iwCmpTviaVUOHFoA21hwa.ffWMixXru1IyGlrzfF9C7.5TclU33/C', 46, 24, '1', 1, NULL, NULL, '2021-10-04 14:18:46', '2021-10-04 14:18:46', NULL),
(61, '$2y$10$CxGWP6PKI8cdTEL3JR1qRON9FOaHJhklYHzkgTrzS1pecZSDZKxzW', 47, 18, '1', 1, 1, NULL, '2021-10-04 14:20:15', '2021-10-04 14:21:54', NULL),
(62, '$2y$10$85WidgKKh7fZPspJ9ldlt.0EeP1FhFhRvas76XTITnEDsIV2vXCmy', 48, 4, '1', 1, NULL, NULL, '2021-10-04 14:21:42', '2021-10-04 14:21:42', NULL),
(63, '$2y$10$9fXup7azMdku4lTaYUGl5uRcHkeHsq/cMZ5aK7dKlQOeKVzy7OHvi', 49, 1, '1', 1, NULL, NULL, '2021-10-04 14:23:19', '2021-10-04 14:23:19', NULL),
(64, '$2y$10$g0GT5CceXUqarjSZtjD8WOpPbYYfZo98/5nTcpYID2KhgEcLrOiL.', 50, 2, '1', 1, NULL, NULL, '2021-10-04 14:24:45', '2021-10-04 14:24:45', NULL),
(65, '$2y$10$x9UG2r6YclXkUfWmiNh0quNJcx1kbP4KzcEuGNzk96oiaDzk2W876', 43, 1, '1', 1, NULL, NULL, '2021-10-08 06:18:28', '2021-10-08 06:18:28', NULL),
(66, '$2y$10$29zC2WlCeN49gAPr7F8cJuWNzF5HrzbV1zNEvZUA96CQsdzYiclSG', 34, 1, '1', 1, NULL, NULL, '2021-10-08 06:18:45', '2021-10-08 06:18:45', NULL),
(67, '$2y$10$YUgKK8wlWkKO8AgP39wtt.bzm2/3HNn0g1EZiQ3O54Bcwhp56XSLa', 38, 1, '1', 1, NULL, NULL, '2021-10-08 06:18:59', '2021-10-08 06:18:59', NULL),
(68, '$2y$10$/nJNhTQinauDhcxjk3SkHucjMB.SlGQGmVojtX87ybyWtMtTwsCfa', 25, 1, '1', 1, NULL, NULL, '2021-10-08 06:19:14', '2021-10-08 06:19:14', NULL),
(69, '$2y$10$Xbim3wfRH3Y5NFUmVUnkkuFyquZqtmwaXEyRHWz/6Rvpdb4LaO5jm', 35, 1, '1', 1, NULL, NULL, '2021-10-08 06:19:33', '2021-10-08 06:19:33', NULL),
(70, '$2y$10$peHqXBMCrWk4dQ.prPs.U.pHT/kjOLwvN7S2HxCUxn4GW9QAOGW32', 37, 1, '1', 1, NULL, NULL, '2021-10-08 06:19:43', '2021-10-08 06:19:43', NULL),
(71, '$2y$10$sOnbAoa.PUrBA5FJZ0BgN.4j8aLkkJUvNbSJ6U4QAQjwt9ju.C5uG', 40, 1, '1', 1, NULL, NULL, '2021-10-08 06:19:52', '2021-10-08 06:19:52', NULL),
(72, '$2y$10$yZ03DxwvAEimR0vrAmtCOeqUgk2kkgdmFlB5tjQ7DZBLaBHAbtrse', 21, 1, '1', 1, NULL, NULL, '2021-10-08 06:20:02', '2021-10-08 06:20:02', NULL),
(73, '$2y$10$ZKhxCe41f9D9ACXapIOZX.oqZvogYNmWG08hC1v2VYymcKnTskYEy', 8, 1, '1', 1, NULL, NULL, '2021-10-08 06:20:12', '2021-10-08 06:20:12', NULL),
(74, '$2y$10$TSOqoFAw3lqFQilQ.Cxm/urOBAJ5jhJTYR1hf6/NTbqE4Q/VtUffy', 41, 2, '1', 1, NULL, NULL, '2021-10-08 06:20:33', '2021-10-08 06:20:33', NULL),
(75, '$2y$10$1FCD7ww0MDLf3H5g7ehtWO.MwRbNWGzmy3Y3MAmkdMYF5e.qOHypi', 9, 1, '1', 1, NULL, NULL, '2021-10-08 06:20:54', '2021-10-08 06:20:54', NULL),
(76, '$2y$10$W8TQN.kHekkCnRTGEvI3xu8Kae9uWossiwxwxk4hdVsa1Jwvpr.mi', 11, 1, '1', 1, NULL, NULL, '2021-10-08 06:21:02', '2021-10-08 06:21:02', NULL),
(77, '$2y$10$6mnPf6FZUqUgMNBo4qgfS.BU3.ixx6kYpusJ2HFQ7vc7HL1uVZSya', 10, 23, '1', 1, NULL, NULL, '2021-10-08 06:25:10', '2021-10-08 06:25:10', NULL),
(78, '$2y$10$gJpBv74MKI6R5Xkp6fXO6.6A41VE/u8KMQJPBnzKLtR3A4ojFgcra', 16, 27, '1', 1, NULL, NULL, '2021-10-08 06:25:52', '2021-10-08 06:25:52', NULL),
(79, '$2y$10$FDVDZ.11k9tS4Rwd/uF41OPFQ/vlzplrbe2IXWvcVYIZoquqbCL92', 42, 8, '1', 1, NULL, NULL, '2021-10-08 06:26:26', '2021-10-08 06:26:26', NULL),
(80, '$2y$10$s8jQaj9JN/mK1T3Hlq/.qeuninAcSHUYfK0iF1smPOWn83DsZSn1O', 51, 18, '1', 1, NULL, NULL, '2021-10-13 03:32:12', '2021-10-13 03:32:12', NULL),
(81, '$2y$10$TsVSbFGvPvl4seViRTIM9uw0tEkTtgYcByPvSDRLGUoMEB30h.A1.', 51, 21, '1', 1, NULL, NULL, '2021-10-13 03:32:12', '2021-10-13 03:32:12', NULL),
(82, '$2y$10$xje.K.4vw9liHKxDWGcpjeMfkYrJYPv9WkQ4x/XcwrzmcmWZrXana', 52, 1, '1', 1, NULL, NULL, '2021-10-13 03:34:47', '2021-10-13 03:34:47', NULL),
(83, '$2y$10$pfJX0eVCZuQawNUc7ekg/ugizs6mRhwCDzbD55UHUsRw1fXboBeXi', 52, 2, '1', 1, NULL, NULL, '2021-10-13 03:34:47', '2021-10-13 03:34:47', NULL),
(84, '$2y$10$82Jg9B45Ek/qlVUnHqgJu.jT9xGaxOwsjK78vS5oIHdHIm75hdutW', 53, 1, '1', 1, NULL, NULL, '2021-10-13 03:36:31', '2021-10-13 03:36:31', NULL),
(85, '$2y$10$sGpyIQ.p1S2SenPWtWFApuzvPchpdtiIIwBGI5tbsy0EnGMOa0Kv6', 53, 2, '1', 1, NULL, NULL, '2021-10-13 03:36:31', '2021-10-13 03:36:31', NULL),
(86, '$2y$10$YTkCH0WWB4CPYByw92nb7Oj59UgBJ1FQ/tuJisNxCi6IMur34tnk2', 54, 2, '1', 1, NULL, NULL, '2021-10-13 03:38:11', '2021-10-13 03:38:11', NULL),
(87, '$2y$10$1aKVfJ90lwF8tKbD5pAiB.HqUGQo9F4oy.88cmfLq4yVFvZKXKKca', 55, 21, '1', 1, NULL, NULL, '2021-10-13 03:39:38', '2021-10-13 03:39:38', NULL),
(88, '$2y$10$tJAxXLLXV4YOERAa2vJYyOIdBo5a75TElBDPvMer5YtHAcNR/hfDy', 56, 18, '1', 1, NULL, NULL, '2021-10-13 03:41:06', '2021-10-13 03:41:06', NULL),
(89, '$2y$10$gt7q126OEE77Dh1XDO4MGeRXP1AL/7Qh5Nc9OlN6Gx8HRKgamb9d6', 56, 21, '1', 1, NULL, NULL, '2021-10-13 03:41:06', '2021-10-13 03:41:06', NULL),
(90, '$2y$10$FePI20pkVkciH8sKWYwWwOu3XwPR2Dkrk5RFbmyplXhofmMLuc16i', 57, 1, '1', 1, NULL, NULL, '2021-10-13 03:42:34', '2021-10-13 03:42:34', NULL),
(91, '$2y$10$C7X6coIFaJ5rUStesjPGo.lvsRjRDBaL3FKVOPyCOBRSZxf9Cxqwy', 58, 18, '1', 1, NULL, NULL, '2021-10-13 03:44:24', '2021-10-13 03:44:24', NULL),
(92, '$2y$10$mbC1bKYBIUdCS2QYMkG/G.2neOhWJ.ZFepIiXBL.b/PX.S/BqT7OS', 59, 21, '1', 1, NULL, NULL, '2021-10-13 03:45:42', '2021-10-13 03:45:42', NULL),
(93, '$2y$10$DRDl33ixskNvr.VxFiwE/OWpWCo1i5kh7uh4YW/XL3z81VSNRWM6m', 60, 1, '1', 1, NULL, NULL, '2021-10-13 03:47:00', '2021-10-13 03:47:00', NULL),
(94, '$2y$10$crMhXSECn2RkkLIhz9JVP.rfDpjrjCYj2iAG2OfrR2M355iK7pLn6', 61, 1, '1', 1, NULL, NULL, '2021-10-13 03:48:21', '2021-10-13 03:48:21', NULL),
(95, '$2y$10$Lxw1.B8rffIXGirfGLaHEOclzM8ZMoq8p0.WZxKfb5lbHjA7pZOru', 62, 1, '1', 1, NULL, NULL, '2021-10-13 03:49:30', '2021-10-13 03:49:30', NULL),
(96, '$2y$10$3EwJc8D4m8kKDxt2wCwBduiEdP/XLQjSt/SiNpr8TyEaNKqoMqxNO', 63, 2, '1', 1, NULL, NULL, '2021-10-13 03:50:46', '2021-10-13 03:50:46', NULL),
(97, '$2y$10$GADPUVR3Sq/zW/zsQZEYiOOBxrOkFPq5m/J3.cKbWm/Iq/5wjX6wy', 64, 1, '1', 1, NULL, NULL, '2021-10-13 03:53:26', '2021-10-13 03:53:26', NULL),
(98, '$2y$10$ONKtcoZCVgNQMpiFHWR5JebLziw2d1QysKISJjQ7gWWkazIUHBmaG', 65, 2, '1', 1, NULL, NULL, '2021-10-13 03:54:48', '2021-10-13 03:54:48', NULL),
(99, '$2y$10$iAWtIVuagPKAnr26NdLnDObH.bPKb.Z0zWa0m7JLFbz.ymRJgMwHm', 66, 18, '1', 1, NULL, NULL, '2021-10-13 03:56:15', '2021-10-13 03:56:15', NULL),
(100, '$2y$10$1Abccta7/x5MIs.KXVpYOuNx0Cf2/FW.IHPFZjiHg2etfmEo5.Cg2', 67, 18, '1', 1, NULL, NULL, '2021-10-13 03:57:57', '2021-10-13 03:57:57', NULL),
(101, '$2y$10$yeWWBRZpPfVn621eY2b2hOv5YGVT/nkGB4h6t8JycXzpNfiFRSTse', 68, 21, '1', 1, NULL, NULL, '2021-10-13 03:59:25', '2021-10-13 03:59:25', NULL),
(102, '$2y$10$XNpFm7ppzm8WI5r9m87ilOca9LENEZkIhg9fpMgFmuxuAxlG/fT9m', 69, 21, '1', 1, NULL, NULL, '2021-10-13 04:00:53', '2021-10-13 04:00:53', NULL),
(103, '$2y$10$ane74QpdxDf41DALcmWfXukkEZyHwqdXU7QNxqiEGyER3TpvqHav6', 70, 18, '1', 1, NULL, NULL, '2021-10-13 04:02:23', '2021-10-13 04:02:23', NULL),
(104, '$2y$10$KpIVX.q0AofZKv9mAz/moewGSKxxr66AFGFxs5XzdX5WQwfxsonpi', 71, 18, '1', 1, NULL, NULL, '2021-10-13 04:03:36', '2021-10-13 04:03:36', NULL),
(105, '$2y$10$Ke/gZK6aVRJRDRKGdd9ZYONO0strsiS9NLZe2hq4bHsn9K2zIX79q', 72, 18, '1', 1, NULL, NULL, '2021-10-13 04:04:57', '2021-10-13 04:04:57', NULL),
(106, '$2y$10$weVjMD0tCRjlpS.OEs7LL.rHEYwYpyHIde7vA1GJXBVhdcbM8izs.', 73, 18, '1', 1, NULL, NULL, '2021-10-13 04:06:22', '2021-10-13 04:06:22', NULL),
(107, '$2y$10$rF4kqrx3y10X4wnuwU6oR.w1qnWdgueWjDEMXEnN1WNKtOznBYK5a', 74, 8, '1', 1, NULL, NULL, '2021-10-13 04:08:17', '2021-10-13 04:08:17', NULL),
(108, '$2y$10$RmJcR2hVEt8NSpAfI0EOPOLTaIQYoBo6QR6SrGph9u2TtlkAwIt1y', 75, 23, '1', 1, NULL, NULL, '2021-10-13 04:09:37', '2021-10-13 04:09:37', NULL),
(109, '$2y$10$uCpD6sAhQvSfMB48cUzCYuyjGXH4k5tAZx.vH/cSx/r65kDT8Mq56', 76, 1, '1', 1, NULL, NULL, '2021-10-13 04:11:15', '2021-10-13 04:11:15', NULL),
(110, '$2y$10$llSE7FFXSTSmdjcggr8yw.G9jjWmlI3g45TvRxupoacTh.a4612ES', 77, 3, '1', 1, NULL, NULL, '2021-11-08 04:19:03', '2021-11-08 04:19:03', NULL),
(111, '$2y$10$BLssUOxEcJpherBKrlpgjusfrEI2nkIPKOJB5owV8C69X.eAc8PTy', 78, 2, '1', 1, NULL, NULL, '2021-11-08 04:33:01', '2021-11-08 04:33:01', NULL),
(112, '$2y$10$tRLK/dNh1FRris7pMXJ7BOk7p9G6E0G8aah/G/7zLFsJfF7J/zKmW', 79, 21, '1', 1, 1, NULL, '2021-11-08 04:50:49', '2021-11-08 06:37:04', NULL),
(113, '$2y$10$ANOPIL2neRQIz68zJsCjUuSoOVpwn1rIYViJNgtRzWDY5F01Bf4oq', 80, 21, '1', 1, NULL, NULL, '2021-11-08 06:15:22', '2021-11-08 06:15:22', NULL),
(114, '$2y$10$rQKL/35iQlyX.pTEEHqIfube/sIqbn9QYG/8kUHhDZonA4gNN/rd6', 81, 21, '1', 1, NULL, NULL, '2021-11-08 06:23:08', '2021-11-08 06:23:08', NULL),
(117, '$2y$10$G2EIsySGo1JzHnFH8PbpUOS3B5tuoUR04A1QHMSM/aw.RxMAVyu0a', 82, 18, '1', 1, 1, NULL, '2021-11-08 06:35:33', '2021-11-08 06:36:41', NULL),
(118, '$2y$10$F4K2wXDpi5Rxtadl5lO4guuWDuhr6lnbr4yiqlA7g8kmH9oSzziBO', 83, 21, '1', 1, NULL, NULL, '2021-11-08 06:43:11', '2021-11-08 06:43:11', NULL),
(120, '$2y$10$8WRMIlpax1NFYUgPkPNzK.SsS.e0cPJ1GhDbfZ8WBcr.Kr35xtWvW', 84, 14, '1', 1, NULL, NULL, '2021-11-08 06:55:27', '2021-11-08 06:55:27', NULL),
(121, '$2y$10$0jqBEOJ55sK2KvWLXpcgEOsBJETq8VQsemPfAfk3vR1xc4hdaOnBq', 85, 1, '1', 1, 1, NULL, '2021-11-08 07:01:29', '2021-11-11 07:17:11', NULL),
(122, '$2y$10$xuK6f1KVp6vLzMnjt8PJRelIxJmmFdeG3GAdsMHA3zcvsDy/w9lZ6', 86, 1, '1', 1, NULL, NULL, '2021-11-08 07:06:44', '2021-11-08 07:06:44', NULL),
(125, '$2y$10$4C.FR6nR0g9hOSEnfITkVOxKfyXA/LCn4sVm9FGF3Lq0qCz9oW2eS', 87, 18, '1', 1, 1, NULL, '2021-11-08 07:58:08', '2021-11-08 08:02:24', NULL),
(126, '$2y$10$FPSmOLEycAszsPXo1aVKfeT1sPTe53nZkfyLziLGEygzDJiptWczy', 87, 21, '1', 1, 1, NULL, '2021-11-08 07:58:08', '2021-11-08 08:02:24', NULL),
(127, '$2y$10$rlyoAg6L5dXlmNOJGHe80e.kTGhNkjzxInxGctrrkwDlfoK1jN9oW', 87, 30, '1', 1, NULL, NULL, '2021-11-08 08:02:24', '2021-11-08 08:02:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id` bigint(20) NOT NULL,
  `kode_satuan` varchar(100) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `nama_satuan` varchar(35) NOT NULL COMMENT 'Nama atau kepanjangan dari satuan, misal Kg=Kilogram',
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `kode_satuan`, `satuan`, `nama_satuan`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$F7B0hiUJCFBMUNqP/ephmutZkVD4wUa8Z4OuEmOb/MvQhGitTq4xu', 'kg', 'Kilogram', 1, 1, NULL, '2021-09-09 07:47:01', '2021-10-06 04:01:23', NULL),
(2, '$2y$10$Kac7pYbEvGsTbZqeg1Ibxu4Khy.N9YDobGiHpW61IfpPngbdftqLW', 'M', 'Meter', 1, NULL, NULL, '2021-09-09 07:47:22', '2021-09-09 07:47:22', NULL),
(3, '$2y$10$QRGqlWMvPRSEjaTAOLh7wedEKlTV9za2F5S1GbLaf7EDsyAnC23UW', 'Pcs', 'Piece', 1, 1, NULL, '2021-09-09 07:47:31', '2021-09-09 07:52:17', NULL),
(4, '$2y$10$NzYYGuweit3hUQ4yQ7b3yuh5HKwXdQzuCBWaI1APwSA7qQqwYXpDG', 'sdsd', 'dsds', 1, NULL, 1, '2021-09-09 07:54:12', '2021-09-09 07:54:19', '2021-09-09 07:54:19'),
(5, '$2y$10$leIPIrKT4/4ReLYJDFONeOcsMfZPW43ySRZEz2fL0PZ2oTpBDM6rm', 'ton', 'Ton', 1, NULL, NULL, '2021-10-06 04:01:46', '2021-10-06 04:01:46', NULL),
(6, '$2y$10$MY.An8Ze89ZIrKjNcsTZyuFOSQHOqU6Kd5Bt1JeW1uaRTBqdtMiYW', 'liter', 'Liter', 1, NULL, NULL, '2021-10-06 04:02:32', '2021-10-06 04:02:32', NULL),
(7, '$2y$10$7LWFPWtI1vkGKpkLTmhAmOOY20dC9RTaFPwXg1JPjwlhXItTPOiEy', 'Batang', 'batang', 1, NULL, NULL, '2021-11-11 07:01:21', '2021-11-11 07:01:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` bigint(20) NOT NULL,
  `kode_slider` varchar(100) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `caption` text DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image_url` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `kode_slider`, `title`, `caption`, `image`, `image_url`, `link`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$owRIO3WXO51.QpESSDTCtOWNKDNjsHzLPZM0ey.HAopn66AwHvxXC', 'Komoditas Cabe', 'Para petani cabe kota bogor', NULL, 'https://assets.corteva.com/is/image/Corteva/ar4-17mei21?$image_desktop$', NULL, 1, NULL, NULL, '2021-10-10 13:38:20', '2021-10-10 13:38:20', NULL),
(2, '$2y$10$2Y70ush4nPrTxqVOgAJYXOTELu36OT9HSuBs7H0ci//5SEUwBwJYi', 'Komoditas Telur Ayam Ras', 'Para petani telur ayam ras kota bogor', NULL, 'https://dhanangclosedhouse.com/wp-content/uploads/2017/12/closed-house-ayam-petelur-01.jpg', NULL, 1, NULL, NULL, '2021-10-10 13:38:45', '2021-10-10 13:38:45', NULL),
(3, '$2y$10$CEdy11oHoDF3ePtSe76KVeoUhZvLrxV7AksBMmg0UR5Ho8tUUKB8W', 'Gas Elpiji 3 Kg', 'Barang kebutuhan penting gas epliji 3 kilogram', NULL, 'https://cdn0-production-images-kly.akamaized.net/c6tccnDi0T6Jm3pcZ2IegQ5jsqc=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2715897/original/097509300_1548756189-20190129-Subsidi-Energi-2019--TALLO-7.jpg', NULL, 1, NULL, NULL, '2021-10-10 13:39:11', '2021-10-10 13:39:11', NULL),
(4, '$2y$10$UI0GWtW93DM2YumWrnBRjerbOLCx5JhOZnauMyK1fdEbQ1ZHurwjq', 'beras', NULL, '20211012120910.jpg', NULL, NULL, 1, NULL, NULL, '2021-10-12 12:09:10', '2021-10-12 12:09:10', NULL),
(5, '$2y$10$7Ll6eIBp4a53daaOngLjUuBY6SyJmLxcPv0lJFO5yjymq1r9aUHmG', 'Baja ringan', NULL, '20211012125500.jpg', NULL, NULL, 1, NULL, NULL, '2021-10-12 12:55:00', '2021-10-12 12:55:00', NULL),
(6, '$2y$10$3XSUmHNmPaNdIoXEAY0IZ.dNnMfT3WMVGKsSrIGMKJMRxFTgFvQoC', 'Ganjar Gunawan, A.P.', 'Kepala Dinas Perdagangan dan Perindustrian Kota Bogor', '20211014055256.jpg', NULL, NULL, 1, NULL, NULL, '2021-10-14 05:52:56', '2021-11-09 06:16:25', NULL),
(7, '$2y$10$aQ3roVnkSN24zfPQJOSAq.jK/pNkaXy21SNGlKvh2KnjqVXLRZ/p2', 'Ganjar Gunawan', 'KADIS Perdagangan dan Perindustrian Kota Bogor', '20211014060405.JPG', NULL, NULL, 1, NULL, 1, '2021-10-14 06:04:05', '2021-10-14 06:57:06', '2021-10-14 06:57:06'),
(8, '$2y$10$XcN3n2GygEfJmFnCAfXfVOuAxvQwZWn0NnwOKa1Hc1Nb5ShzfRNQi', 'Mohamad Soleh. ST', 'Kabid Pengembangan Perdagangan Dalam Negeri, Perlindungan Konsumen dan Tertib Niaga.', '20211108033423.PNG', NULL, NULL, 1, NULL, 1, '2021-11-08 03:34:23', '2021-11-08 03:36:08', '2021-11-08 03:36:08'),
(9, '$2y$10$bP9hDdBYlb5ROQpcfwplle9ntr8g4A0omKTiInXzLpGL7WmNppotK', 'KABID PPDNPKTN', 'KABID PPDNPKTN', '20211109055802.jpeg', NULL, NULL, 1, NULL, 1, '2021-11-09 05:58:02', '2021-11-09 05:59:27', '2021-11-09 05:59:27'),
(10, '$2y$10$616DGi.10Zc2baeZwYyoJe61DG6rELL9udshyUgnqELgLRxPvDBCC', 'Mohamad Soleh, S.T.', 'Kepala Bidang Pengembangan Perdagangan dalam Negeri Perlindungan Konsumen dan Tertib Niaga', '20211109060812.jpg', NULL, NULL, 1, NULL, NULL, '2021-11-09 06:08:12', '2021-11-09 06:10:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sumber_pembelian`
--

CREATE TABLE IF NOT EXISTS `sumber_pembelian` (
  `id` bigint(20) NOT NULL,
  `kode_sumber_pembelian` varchar(100) NOT NULL,
  `nama_sumber_pembelian` varchar(50) NOT NULL,
  `user_create` bigint(20) NOT NULL,
  `user_update` bigint(20) DEFAULT NULL,
  `user_delete` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sumber_pembelian`
--

INSERT INTO `sumber_pembelian` (`id`, `kode_sumber_pembelian`, `nama_sumber_pembelian`, `user_create`, `user_update`, `user_delete`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '$2y$10$NRsNxE4WRGyqaA57awza/.UFu4VZGd/e7L5Gsu.cvwB/5pU8p05Mm', 'Kelompok Tani', 1, 1, NULL, '2021-09-09 08:11:08', '2021-10-05 14:16:02', NULL),
(2, '$2y$10$HXHw4c9.dndLgniN88WvQ.FHID9902EKZ28Z3jC715bUT6AosFTfy', 'Test sumber pembelian 2 3', 1, 1, 1, '2021-09-09 08:11:40', '2021-09-09 08:24:33', '2021-09-09 08:24:33'),
(3, '$2y$10$J92QH/IY8pA.hTMnZiYh3.nYgnPqVvNGszP9i9o/jgWo5mIJhaa/W', 'Pengepul Bahan Pokok di Daerah Lain', 1, NULL, NULL, '2021-10-05 14:16:29', '2021-10-05 14:16:29', NULL),
(4, '$2y$10$fYNUzqIjtToN0GlbIBvKGuN5PKFnf3KySRw/kxLHedMYUqM/JqjJG', 'Pedagang di Pasar Induk', 1, NULL, NULL, '2021-10-05 14:16:45', '2021-10-05 14:16:45', NULL),
(5, '$2y$10$uHm0x9y5rsFRhZfY.kgDCOZG/VPTjqkqxs5EaNKrTW/edfuIPQHn6', 'Pedagang di Pasar Kecamatan', 1, NULL, NULL, '2021-10-05 14:17:26', '2021-10-05 14:17:26', NULL),
(6, '$2y$10$gXGXVLwE/XUYc47A8AA06u5/2hjtO6YPfWTUOGnoKezkabYw0a1wm', 'Pedagang di Pasar Desa / Kelurahan', 1, 1, NULL, '2021-10-05 14:17:39', '2021-11-11 07:28:27', NULL),
(7, '$2y$10$FCMJk8yzaMWrIoFLWViFOeb92DPLOUxl1kHNO7cf1WmbRMJW0idqG', 'Pedagang Pengecer', 1, NULL, NULL, '2021-10-05 14:17:52', '2021-10-05 14:17:52', NULL),
(8, '$2y$10$pP5S2N0VPZ634e3kNDlkr.u.9bBiG8UcStznhkr6fRgbI.48EaHcq', 'Peternak atau Kelompok Peternak', 1, NULL, NULL, '2021-10-05 14:18:06', '2021-10-05 14:18:06', NULL),
(9, '$2y$10$1DEtonT708VmVwesmm9UY.bNTTG.W3YJEObExJgo3GzdaGxnbEv8O', 'Pemotongan RPH', 1, NULL, NULL, '2021-10-05 14:18:18', '2021-10-05 14:18:18', NULL),
(10, '$2y$10$eSp3vYt3gdQJe8Ng9SpyZOzBGxW2QAtepQjfQgf/xxDP/n0bu4Mim', 'Pemotongan Tradisional', 1, NULL, NULL, '2021-10-05 14:18:30', '2021-10-05 14:18:30', NULL),
(11, '$2y$10$Gn67qkkO2nZCVQYlLGN5cOwuzBsinGTHDQvnLdiEIQr4J2vEgmU16', 'Perusahaan Penyuplai', 1, NULL, NULL, '2021-10-05 14:18:43', '2021-10-05 14:18:43', NULL),
(12, '$2y$10$Co3TJvfJA3bQiwBInwp5Tu6WWdRzzF0Cordnfgnl49N0i38olcjFS', 'Importir', 1, NULL, NULL, '2021-10-05 14:18:55', '2021-10-05 14:18:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL,
  `kode_user` varchar(100) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `role` enum('superadmin','distributor','manajemen') NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `kode_user`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `role`, `status`) VALUES
(1, NULL, 'Admin', 'admin@gmail.com', NULL, '$2y$10$Vc8MIelRQrmd4ZgrpMYURumuvl3pv2S/8hWtJ0xQ8CBi98fuQZiqy', 'Vf3vNQrmccztAesXUt016Q0knjCrMg7BjWir1dE7zVJfHVetCMV3iVgL5SD8', '2021-09-08 04:00:54', '2021-09-08 08:02:02', NULL, 'superadmin', '1'),
(2, NULL, 'Distributor', 'distributor1@gmail.com', NULL, '$2y$10$fTJqlRKt6ryWMaouLm4jPOo113a6693usG1BowErqmWFzBMpxct5O', NULL, '2021-09-08 04:01:29', '2021-10-05 14:11:31', NULL, 'distributor', '1'),
(3, NULL, 'Nama test', 'test@gmail.com', NULL, '$2y$10$emXy3X5VuKAeSebxH9yx5eTxWb01PXEz5rc2T2BmCx5501hCJD6Qi', NULL, '2021-09-08 04:44:40', '2021-10-05 14:27:45', '2021-10-05 14:27:45', 'superadmin', '0'),
(4, NULL, 'dsfdsfsd', 'fwerwe@gmail.com', NULL, '$2y$10$ZVI9tVpQgPoCEpqYR.cEWOAZg.j2V8ktfg72yMk1nqZ/AabEvQXtq', NULL, '2021-09-08 04:47:34', '2021-10-05 14:27:42', '2021-10-05 14:27:42', 'superadmin', '0'),
(5, NULL, 'dsdsd', 'dsdsds@gmail.com', NULL, '$2y$10$F.CN3QCrSucYTsf6vlWEfOzD7xh37YJPkF1ptlI3EGVIqov9eENha', NULL, '2021-09-08 05:00:01', '2021-10-05 14:27:39', '2021-10-05 14:27:39', 'superadmin', '0'),
(6, NULL, 'dsd', 'dsdsds2@gmail.com', NULL, '$2y$10$oHwlXohkuF7HRvfMhlIWuusnVdPu6FcIn31PqMtWQu.A2wK34qk5e', NULL, '2021-09-08 05:12:56', '2021-10-05 14:27:34', '2021-10-05 14:27:34', 'superadmin', '0'),
(7, NULL, 'erertre', 'fdgdfgfdg@gmail.com', NULL, '$2y$10$vo.csKdJMJ8WJDOOZrhaD.lmxss45cTkmE2CqnR0nJhqHHEFHKuwe', NULL, '2021-09-08 05:16:39', '2021-10-05 14:27:31', '2021-10-05 14:27:31', 'superadmin', '0'),
(8, NULL, 'dsdsdsd', 'dsdsweds@gmail.com', NULL, '$2y$10$tg01qgEgT.bLBAxObr6sE.8CGtyRkomL9JEOuCPyo8KIs05Y5UceC', NULL, '2021-09-08 05:20:40', '2021-10-05 14:27:27', '2021-10-05 14:27:27', 'superadmin', '0'),
(9, '6138652356f8b', 'poeirpweirpw', 'pwriepo@gmail.com', NULL, '$2y$10$jqq0rYkKt8Bx6TlZI7ndPulFX8EEMbdHNwVcubez0BwyH7S2ff3pW', NULL, '2021-09-08 07:24:19', '2021-09-08 08:27:16', '2021-09-08 08:27:16', 'distributor', '1'),
(10, '$2y$10$P/SgbbBlWPUOEIe3.rTLRe3Slg6Ysu/edLq9OHe83Xm43176yUOdG', 'dfwrerw2', 'sdfsfds2@gmail.com', NULL, '$2y$10$E4n.Wc41CJIJjhadpZi6m.hkxlykdWvKiThnSUz.JQ9.XaOmZG.oy', NULL, '2021-09-08 07:29:48', '2021-09-08 08:26:49', '2021-09-08 08:26:49', 'manajemen', '1'),
(12, '$2y$10$eqW..JTsQL9Iv4BawBac7edeqJORpttwafM5q1Qpa0lhSts54u6K.', 'Agen Beras Cek Din Jaya', 'distributor@gmail.com', NULL, '$2y$10$Tu0TuB3H1n0G7OAgQiXhueIeMtt5f5jf3NMANvIwBf7ep.zQlBIFu', NULL, '2021-09-13 12:07:50', '2021-10-05 14:11:57', NULL, 'distributor', '1'),
(13, '$2y$10$9dj5nuXnh4SJXe.S/a/OS.yuDHcwp6j13KSkOnSLsasyrPODOtjOu', 'Agen Telur Anugerah', 'b@gmail.com', NULL, '$2y$10$sMQLirllKEs1Q8sTEKxd7.TQQswnJgc0sUYbT2R0ITU8PB1nDcu/a', NULL, '2021-10-04 12:57:30', '2021-10-06 03:55:03', NULL, 'distributor', '1'),
(14, '$2y$10$NZkpyxYifPpHsf/P7.P0sOTjx2UAE8/es7lB5Zw5R8QyMyH4THQPS', 'Agen Telur Putera Mandiri', 'c@gmail.com', NULL, '$2y$10$TFnR8AKc4q6OLA.SK5dGOO9JvF/jkC6VsUVkQnQzTGOHAaPMBLryC', NULL, '2021-10-04 12:59:42', '2021-10-04 12:59:42', NULL, 'distributor', '1'),
(15, '$2y$10$b1y7KHlv0XKEKVXJbN7lcObWGLbrezQkcjJGQJKwuDytcB0ItOpGe', 'Beras Kusuma', 'd@gmail.com', NULL, '$2y$10$2zBLMBcpBX1x6KrDc5djYeiKvhOsE1F76tJ400I95dZg8C8g2A0Fi', NULL, '2021-10-04 13:02:16', '2021-10-04 13:02:16', NULL, 'distributor', '1'),
(16, '$2y$10$3nNzPtrhME07pT114X0/V.QsvpYlInNv6UXoo1jkazolqZ3BS7CtO', 'PT. Sumber Gizi Utama (SGU)', 'e@gmail.com', NULL, '$2y$10$XgKXTeX/Jy71.damHk28.OM9hc7dF9a/yAgjx.LbUJnqg6gGvmMJ2', NULL, '2021-10-04 13:04:13', '2021-10-04 13:04:13', NULL, 'distributor', '1'),
(17, '$2y$10$9RTlcByXZG/8AXQ.3a/mAeCaxtwR0hhqwsSZAnclR1AjeLdU.GUJu', 'RPH Bubulak Bogor', 'f@gmail.com', NULL, '$2y$10$p3z7eCJQ.NgrSGMhKrGA2.pc5/7tq7G4hst4vesHmqpeWKTwjNECi', NULL, '2021-10-04 13:06:09', '2021-10-04 13:06:09', NULL, 'distributor', '1'),
(18, '$2y$10$vmNNCQYQfmXJVDjuQR0qqOdEpGZrSvOkVfNtkuSiaS.6Xv/xkl4ra', 'Tiga Putera Perkasa (Iga)', 'g@gmail.com', NULL, '$2y$10$XuIQpLY.OMcOXNm59ThLAehYnMbh8Lj4Kdkx0gBcejV3Za62UnOBy', NULL, '2021-10-04 13:07:41', '2021-10-04 13:07:41', NULL, 'distributor', '1'),
(19, '$2y$10$0x47EASJpgR8F046b/1GaOic8MLT7xwnnCyGWNBlIjlpaQ2dRldfW', 'Toko 85', 'h@gmail.com', NULL, '$2y$10$4BCRyV70jBLm03rfFK2/pO453I8bFo0WT/tZkxcwazN0pxyUX4BXS', NULL, '2021-10-04 13:09:23', '2021-10-04 13:09:23', NULL, 'distributor', '1'),
(20, '$2y$10$/ORkqqwn3lJAZOfdvVg6ieMQ2jLQTYnB.fvd72ufW05MWOynN86jq', 'Toko Kuruta', 'i@gmail.com', NULL, '$2y$10$WAX4CFAs2aPliXvdCLLZGefek9OSfZ5d9WZhAcp.ummttfKk8oCOa', NULL, '2021-10-04 13:11:07', '2021-10-04 13:11:07', NULL, 'distributor', '1'),
(21, '$2y$10$Z0RuemtPQU7PqnFpvCX/0upDZW3VRkRyHj3dl5CrFKB61ZNq7RVJK', 'Zaki Jaya', 'j@gmail.com', NULL, '$2y$10$RDUGZTtHeT/V/WDqW9oXx.fipgwbyiWQdWwNlQNZbkMHe/nSvbqW2', NULL, '2021-10-04 13:13:32', '2021-10-04 13:13:32', NULL, 'distributor', '1'),
(22, '$2y$10$h0xvm5Ode9iTl3DcqceGrOsPD2VMvVTRrd7ZnMmOWpK8KPfoeAX7.', 'Toko Kurnia', 'k@gmail.com', NULL, '$2y$10$KfhNoLXibe.ARQePJFiqTeIgC5zAQ4PDy7fJty4EO0zkfD5vOR4HS', NULL, '2021-10-04 13:15:09', '2021-10-04 13:15:09', NULL, 'distributor', '1'),
(23, '$2y$10$eqmJTrNxAZiR4KFHkwjs9emWeR741wrujSP6dM4TJdH5xKeusTKUK', 'Co Tiong Ho (Novianty Raharja)', 'l@gmail.com', NULL, '$2y$10$cbQ.f2tMgmmcdyZQys2/PORjCwu0OX0piv/CU3wcflki81kGuCHXq', NULL, '2021-10-04 13:16:42', '2021-10-04 13:16:42', NULL, 'distributor', '1'),
(24, '$2y$10$F3COClQyGv4AgpIzbLDWxeGRjiNbQ7D8VarUA.afpqp.87VwP.YCy', 'Benyamin Setiadi ', 'm@gmail.com', NULL, '$2y$10$iQIJRitT9i8bpGhLTBWyVOVy9FKN8TAVFtNVsL8h4MKawvPtV3XI6', NULL, '2021-10-04 13:18:32', '2021-10-04 13:18:32', NULL, 'distributor', '1'),
(25, '$2y$10$622Fw9u4.WraXWt02VUoXO8XTZsKVH9YyE21smF28r37yS2u1mBW6', 'Yoyong (Toko 11)', 'n@gmail.com', NULL, '$2y$10$3oQyeZxPI3NsmrmN5m71cuekzy3SlPnHSnZ5rEFicbuxxPZOwIj6S', NULL, '2021-10-04 13:20:03', '2021-10-04 13:20:03', NULL, 'distributor', '1'),
(26, '$2y$10$QWuJzY56NwAkJPOpbfQ2LuolpC8.b1lnoVfAf7jX8dXunvEguuCoe', 'Monika M. (Rizal)', 'o@gmail.com', NULL, '$2y$10$v6lO0jl.EE8BH9IuQdXR8evIA.qN9H/I9.3qTF9o8vDclgpfKMlm.', NULL, '2021-10-04 13:21:34', '2021-10-04 13:21:34', NULL, 'distributor', '1'),
(27, '$2y$10$s8tWrYzvjaI.WX4w89rQnOBt40eK4O6sXmUBjX0ddMm9YwvSxN6uG', 'Winata (Toko Mekar Jaya)', 'p@gmail.com', NULL, '$2y$10$7A/UT5adoOqWkSlmjALpje6mXSQcVZZlOKd3eu5laiWyCxLss5jQi', NULL, '2021-10-04 13:23:04', '2021-10-04 13:23:04', NULL, 'distributor', '1'),
(28, '$2y$10$TFdhYSASUHe1SXdloDVJlurFmY2IfO9SchtuLyRvFlhWlNk32cdsa', 'Saq & Gas', 'q@gmail.com', NULL, '$2y$10$FFs40gk388SwTjikyhf.ZueyOvL1I5YgAmjIY5PClDmCwCxw2t7qa', NULL, '2021-10-04 13:25:10', '2021-10-04 13:25:10', NULL, 'distributor', '1'),
(29, '$2y$10$TXZMyemBtrZb3D2NGEO9eOsPtAwLLMJfGmrHRmStq91gLY5gMn0NK', 'Agen Telur Amanah', 'r@gmail.com', NULL, '$2y$10$CRAwIP1N.f2UfLmUpdewKOvSW4fBHZYwt7etj/niwKbgJ2TuyWEKy', NULL, '2021-10-04 13:26:36', '2021-10-04 13:26:36', NULL, 'distributor', '1'),
(30, '$2y$10$1G.AdFAvF7QJ8HQJJBVxluRVVaISnnAHok5e133n3w8QxVtTXgpY2', 'Bintang Plastik', 's@gmail.com', NULL, '$2y$10$rmVWekuw0.2CUcuvUooZmunJO2A/zg3oKxfsUDdAkVqY7u6soj.0y', NULL, '2021-10-04 13:36:01', '2021-10-04 13:36:01', NULL, 'distributor', '1'),
(31, '$2y$10$iNcNKEzP3RXUHx4jghFDfuRkhGToDOZvNYZ64IUl8AJKOs8FOGRTK', 'Mustika Jaya', 't@gmail.com', NULL, '$2y$10$ZPGlV/8ZfPD0WoDBu2AP4uv.qJ3LWN7TH/XtfvvVmv.tf7XpwPCry', NULL, '2021-10-04 13:37:42', '2021-10-04 13:37:42', NULL, 'distributor', '1'),
(32, '$2y$10$44dqm5wUfcyIGQqJqOpqceRVKDbm0gpWL/BIlrn1gtHMY2EumTba2', 'PT. Mitra Pratama Subur Jaya', 'u@gmail.com', NULL, '$2y$10$fTerV70EcC5WYRn5Bz5kG.cDTxEwcl4P.EdqK/DuwRkZjDIxSqPl6', NULL, '2021-10-04 13:39:20', '2021-10-04 13:39:20', NULL, 'distributor', '1'),
(33, '$2y$10$yXXQA5AkywITBDE9/hT2UO5cgoQOsUMhiPNSpx8MiEeDRu8CBLbEW', 'Rumah Telur', 'v@gmail.com', NULL, '$2y$10$CwqQ0Zd.Sghj92XpQSgW6uUYo85CtckefSr0z.tcRIyfMWJp4GEcK', NULL, '2021-10-04 13:40:41', '2021-10-04 13:40:41', NULL, 'distributor', '1'),
(34, '$2y$10$XOjSH1fNyGu5oFZP/2mMbed0qh6zZI26lYVeta2yStl3fPx4jDfaa', 'Super Telur', 'w@gmail.com', NULL, '$2y$10$tfIhZFwKqMOvLGgyjDuqKuANHl0DN6.DA7pbzfYmBoUP9uZ9G5CU.', NULL, '2021-10-04 13:42:20', '2021-10-04 13:42:20', NULL, 'distributor', '1'),
(35, '$2y$10$OONAQ07wyjHgyilkFN/LQO2BYh/7BZ1Hdu8.vydxgxsF9cRRrwBRi', 'Toko Aa', 'x@gmail.com', NULL, '$2y$10$/bYVCVBRQU22HbYF.wu5w.YILbL0fqsWU.YIFp0skXr9hi5mNsuKK', NULL, '2021-10-04 13:43:48', '2021-10-04 13:43:48', NULL, 'distributor', '1'),
(36, '$2y$10$xTxlJucAeZdXGcYnhL1e5e29rbyJL.0cZTsadQEWrq8R8vhdUb8ZK', 'Toko Alun', 'y@gmail.com', NULL, '$2y$10$TE6G5utN4VykkhXa/puHyOLccXrUSqdrHbMxxZ2p3lvySpLdENyTa', NULL, '2021-10-04 13:45:20', '2021-10-04 13:45:20', NULL, 'distributor', '1'),
(37, '$2y$10$copzO3FPuXnpMRjJgQ4Zx.zKz7TcjW8k4Q1qsn3aaFwhmIyEIZl42', 'Toko Bayangan', 'z@gmail.com', NULL, '$2y$10$tsvIfA4LVyQtZdu2mvPfXu9lvFTATKYh8nbsc8qkkiT0/vAO/rRnq', NULL, '2021-10-04 13:46:44', '2021-10-04 13:46:44', NULL, 'distributor', '1'),
(38, '$2y$10$Sh8IBpIFa4XtfQNmXYXb1eXIhqgI93N9UV07q4HkYyQ2Weq0R0zVW', 'Toko Iming', 'aa@gmail.com', NULL, '$2y$10$ufnAHXsCRInFyeqYpx4GqeJDuwFeB01BGpHA6Pdx4oXcRAVyhyRGa', NULL, '2021-10-04 13:48:12', '2021-10-04 13:48:12', NULL, 'distributor', '1'),
(39, '$2y$10$lAplX5srxsLgF0VaFCenEuW2VFgVlQH56fe7ujZim8LYXzcP39IdC', 'Toko Kencong Telur', 'ab@gmail.com', NULL, '$2y$10$oPsblBd7IYp6nD7scDFbVOk7aObplsQTJ.nF/V1nWJqWSnsx8DqA6', NULL, '2021-10-04 13:49:32', '2021-10-04 13:49:32', NULL, 'distributor', '1'),
(40, '$2y$10$P7TsdaB4FFbDV9xOjLi/5ulbAz3NvsSS2j2bF6v9lpDovSiQu4E8q', 'Toko Kertajaya', 'ac@gmail.com', NULL, '$2y$10$n5FGgTqQN2stpPOa7BxiGeyaqQ1bHlWhrJHwow/6U2Xsid7jeTYxy', NULL, '2021-10-04 13:51:35', '2021-10-04 13:51:35', NULL, 'distributor', '1'),
(41, '$2y$10$N453libk6hgpFhJpJNAnaurzPpry1t4NINr7HdMY2UMyLErRu9KDS', 'Toko Makmur', 'ad@gmail.com', NULL, '$2y$10$TLiVkoTizxaQr223lns.geGK/3.yn77lpW3FpQj4qOq39tVcXGMJi', NULL, '2021-10-04 13:53:16', '2021-10-04 13:53:16', NULL, 'distributor', '1'),
(42, '$2y$10$Eep7DscSc8V8gfxp269sAOtsg7Gx6dOkcxb.URw/k6HfQ1u4SZW9.', 'Toko Santo', 'ae@gmail.com', NULL, '$2y$10$KQlMQFlu2GN4MB29CgVIQeYywcWY2kCice0EM72zVy.lb2F3zMFV2', NULL, '2021-10-04 13:55:00', '2021-10-04 13:55:00', NULL, 'distributor', '1'),
(43, '$2y$10$Pycx8N4jHDSkG.h6Iukgmeo8q1PeGfenw3iMgIuk9xLv6YBip5dbq', 'UD. Hendra Mandiri', 'af@gmail.com', NULL, '$2y$10$3xagoCZLo1xAfIPOqTtl..c7AWRzjNzz/V29I.XfitjXtFtI0iPOS', NULL, '2021-10-04 13:56:26', '2021-10-04 13:56:26', NULL, 'distributor', '1'),
(44, '$2y$10$ssMz34dUhtL7h.mD1P0cUONAo8/lM1rUunf4BZuhzHQ9ZwPfJAcay', 'Akin', 'ag@gmail.com', NULL, '$2y$10$i1va9AWFNjMyWx5nNILYZu.1h18v7zYp/.9ijAK69urgN5sFtlrNK', NULL, '2021-10-04 13:57:57', '2021-10-04 13:57:57', NULL, 'distributor', '1'),
(45, '$2y$10$VLC3pZejYtyqclboZmpVi.JyTc.ItPEwOjny0RpyIRxiDO/CLR8l.', 'Lienun Suryajaya', 'ah@gmail.com', NULL, '$2y$10$mCZE7oX1FWlwEvvIz3HdDeTFsBI9qP2KLKFaj70XRXduFbk1zALWq', NULL, '2021-10-04 13:59:18', '2021-10-04 13:59:18', NULL, 'distributor', '1'),
(46, '$2y$10$2Ai6.hOr6HBIUJMxBrQdM.LY2PKLy2uW.BFkCLWSlSWEvQkzV.3MC', 'Herlina Anggawikara', 'ai@gmail.com', NULL, '$2y$10$YFUqAzNVWPR6i3vhMihLFOtBNEWNaX8KUR0qC9JTPklZE/vdOMDMa', NULL, '2021-10-04 14:00:42', '2021-10-04 14:00:42', NULL, 'distributor', '1'),
(47, '$2y$10$UfmKN82IK1vCQH85C62F.O6kanOCquWPZT/VE6ee3imCckrl0jZ6C', 'Agen Telur Kencana', 'aj@gmail.com', NULL, '$2y$10$/oYGmbXjvlcF.i.TX7pQ.eHU7I98xmCxay3vIqRuFMlt78QWhR6we', NULL, '2021-10-04 14:02:10', '2021-10-04 14:02:10', NULL, 'distributor', '1'),
(48, '$2y$10$omk6YBcTxSnrP3UM9ABsv.hkn0fIsVspk7fHZZWEfJ2yrDRIcDKNu', 'Dany(Iming)', 'ak@gmail.com', NULL, '$2y$10$4Q..bdRbyp1V4t/S45GtpuppYXarB2XByKGcTvjDorqqGTHf2AjqK', NULL, '2021-10-04 14:03:30', '2021-10-04 14:03:30', NULL, 'distributor', '1'),
(49, '$2y$10$ds4ciXvyPKSUHcB.c709CuJVEavzoGItLstPmfiM14fDLS.mRyXwe', 'Toko Kota Jaya', 'al@gmail.com', NULL, '$2y$10$kM1RIHuPKeVuvP2G0EWvSenbyUhI5zxQMJTVoSRj0uAUIUYoIigC2', NULL, '2021-10-04 14:04:53', '2021-10-04 14:04:53', NULL, 'distributor', '1'),
(50, '$2y$10$ugc1OB9Gi.dP3Q2LJ3lU7eQpC/K0ynGmEWikxwRsxga0MI0xGvaZS', 'Susanto', 'am@gmail.com', NULL, '$2y$10$F2k/.g93rnUXHQOUTXNouuGOy.erLi7M6ZGOni1JJbytyBZSGXwRO', NULL, '2021-10-04 14:06:28', '2021-10-04 14:06:28', NULL, 'distributor', '1'),
(51, '$2y$10$nAOXMjuZNeqawxcZ8CEE..QEhuV9PGN9zqDJLBH5TiimuI0yrpPQu', 'Toko Herlina Angga Wikara ', 'an@gmail.com', NULL, '$2y$10$xw0BqGy/FtnYANPSTsaOw.HL57nmgPIhdayLvMGm8wZkDS6lgD1q2', NULL, '2021-10-04 14:07:49', '2021-10-04 14:07:49', NULL, 'distributor', '1'),
(52, '$2y$10$aSYaiZzO46jd.NsDVxnVnutHLPOk/iu.V.sQN3Zf4Y14sP/reFB8C', 'Budiono Djapara (Trijaya)', 'ao@gmail.com', NULL, '$2y$10$OtRtHS/xW4bF7XU1C2mPmelufTAtAI.pF46RdUJzpx140Q5nud8UC', NULL, '2021-10-04 14:09:08', '2021-10-04 14:09:08', NULL, 'distributor', '1'),
(53, '$2y$10$d3tkplq6ye0z2y92354pOuIs8VPD34YAbN0Yxy72t02/ehRZf8KSG', 'Toko Andre', 'ap@gmail.com', NULL, '$2y$10$Vlriac4kinuDtdfmgKNzjeONdea2haq8R5Fw8lNc09wrFdp9eoIlC', NULL, '2021-10-04 14:10:33', '2021-10-04 14:10:33', NULL, 'distributor', '1'),
(54, '$2y$10$wRIb.O7lH.BdBtla5i5oyuHKvGAN9GnQ7JYz7IKXkOrkQTubHITyW', 'Toko Sebelas', 'aq@gmail.com', NULL, '$2y$10$rDFwwt50pbb7DSWRse6veeEY7VV56qINR7ke7alluSJDK2aR1G0yu', NULL, '2021-10-04 14:12:12', '2021-10-04 14:12:12', NULL, 'distributor', '1'),
(55, '$2y$10$07y5EzWqn94fK8fn4cB7SeosPZPn/ZhplzVzr38URzNoao2oQOOVi', 'Rizal', 'ar@gmail.com', NULL, '$2y$10$ETkyTZNBfyLJLWhqA1jhT.sADC6FmZKFtT3snsWmqQyTn3dW9.wXy', NULL, '2021-10-04 14:13:38', '2021-10-04 14:13:38', NULL, 'distributor', '1'),
(56, '$2y$10$2P/JVcopH1B.WIV52uqW4OTHGiCmlLZfvOjk4cuMG4vsh3VwWQfQa', 'Harahap Jaya', 'as@gmail.com', NULL, '$2y$10$B63DPkHhKBx.FPlGDMB4FeCn2ELOPvEoFSuliXhlBwjHMjAj/xL1a', NULL, '2021-10-04 14:16:52', '2021-10-04 14:16:52', NULL, 'distributor', '1'),
(57, '$2y$10$ra5pKSbIsFfT9Wj.ptGCyeEzrPe1/4Cymq7tLm1g6.eNQJVM5uWIq', 'Ibu Jukasih', 'at@gmail.com', NULL, '$2y$10$NWmL8T3vJD2/nvIbdtBTz.yUy108fQssk7ogmZ62mkoH72gpm/F4C', NULL, '2021-10-04 14:18:46', '2021-10-04 14:18:46', NULL, 'distributor', '1'),
(58, '$2y$10$wTJszT8CpRF5chrKHhvrLO5oR36VY8PIo1I6oXNCApjCnotT1wozy', 'Kios Sri', 'au@gmail.com', NULL, '$2y$10$k5GMpeUGwqpxe.i250rZv.n4B12AedHUXdXYsu1KaaGAUYpjFFOta', NULL, '2021-10-04 14:20:15', '2021-10-04 14:20:15', NULL, 'distributor', '1'),
(59, '$2y$10$.VCalyIhT5QF3i2yxolj0eNEo8I42iwFqmq7Ce9mPmhAtL9LMYafa', 'Sukasih', 'av@gmail.com', NULL, '$2y$10$3RO4AEq6nlY0qLxcd64A8eI.YpiHf2Q4jkQExKj2NJMQmrlur.L5e', NULL, '2021-10-04 14:21:42', '2021-10-04 14:21:42', NULL, 'distributor', '1'),
(60, '$2y$10$NKfzEjrbD4BJfMskH4/sxOo0Vke5/uqW2lv9GuovfWXcb2VGLMJiS', 'Sumber Beras Utama', 'aw@gmail.com', NULL, '$2y$10$zMUSmxZFi/tRDtPPYBLE/uHKsdrT9BtpLF4qitn4t.XHbUTWl95.q', NULL, '2021-10-04 14:23:19', '2021-10-04 14:23:19', NULL, 'distributor', '1'),
(61, '$2y$10$cIlOjPf5Jh4jBJgVWsFdi.8Oz7F9T130EC9HghAqPJCI1NneN3VwO', 'Telur Berkah', 'ax@gmail.com', NULL, '$2y$10$NR4Y9RfqDjLi10A2npeYwuQKh5qi.qPo0tywc0bxxpxRFX6Oa4UdW', NULL, '2021-10-04 14:24:45', '2021-10-04 14:24:45', NULL, 'distributor', '1'),
(62, '$2y$10$4J71JIf82p2pqZO1oytFL.rdvBde4QtaT3paI0msAl4ZEgzE2jMVW', 'Toko Ading Sayur', 'ay@gmail.com', NULL, '$2y$10$I/hgsRz06IQ9MyewT8JkY.7Eylz9V7L7P5LHobOmoKs0vUYOqyPeG', NULL, '2021-10-13 03:32:12', '2021-10-13 03:32:12', NULL, 'distributor', '1'),
(63, '$2y$10$PO/5/PG8O2qQGXiTx0pv3uDPBPb4atZKgKX7RRRsT/Fk2IvDJf0YK', 'Toko Berkah', 'az@gmail.com', NULL, '$2y$10$zjBhoDEXiQ2yxkH9hWoEX.WqcAR0IjVc08aahgx6GtiHp5xSpWfg6', NULL, '2021-10-13 03:34:47', '2021-10-13 03:34:47', NULL, 'distributor', '1'),
(64, '$2y$10$qBY8ANVJzAwD5A6mgrStuuZajd5ZUT0DjNc5Sgc16Npzn8kTP5iC2', 'Toko Intan', 'ba@gmail.com', NULL, '$2y$10$rjO/mg3bzy2rIeyNqFpGT.rKRDGMUMILazwpDnwZH5o/2ITyOLi9G', NULL, '2021-10-13 03:36:31', '2021-10-13 03:36:31', NULL, 'distributor', '1'),
(65, '$2y$10$kTqLVjgiEmoB3Qr4QbLzoewTth7ZJiwTqXFe29jydoJt8VyvamAF6', 'Toko Kerny', 'bb@gmail.com', NULL, '$2y$10$q4yszwOZv7xbOUIL6BwY9eTKjFnPsjWR4GLqZP7hUe5OWqjwOXr.m', NULL, '2021-10-13 03:38:11', '2021-10-13 03:38:11', NULL, 'distributor', '1'),
(66, '$2y$10$ypvwjZZnqgz/FaDmLdUTdujZoJd2ItogiRu60/Eh19s6TkrdhtWBK', 'Toko Sri', 'bc@gmail.com', NULL, '$2y$10$v1u2jzRK5nzAhMpbvwV6.ehVL9XhXKMmmADBt5f4p.hiCyOLkDfOS', NULL, '2021-10-13 03:39:38', '2021-10-13 03:39:38', NULL, 'distributor', '1'),
(67, '$2y$10$Pba02aSlz6CWwLDiMoADTug03fo44vpU5w6jQQzsGuzTudgKj8axS', 'Toko Yuni', 'bd@gmail.com', NULL, '$2y$10$eeoUEWGkuCq5apGWegwrGuQbPTULvXBqRX7pCkZgH.lyJ2Jvc5EJi', NULL, '2021-10-13 03:41:06', '2021-10-13 03:41:06', NULL, 'distributor', '1'),
(68, '$2y$10$y7p175B1VoULMLE4YHvjCOVRLz8CCjDEc6WS1x4aNN8mumU6nn7se', 'Dhany', 'be@gmail.com', NULL, '$2y$10$dv1pSQdI2TPm4GPNlLnDReOl3x6bRAFtGvhvObpuK96cjfZlo2crO', NULL, '2021-10-13 03:42:34', '2021-10-13 03:42:34', NULL, 'distributor', '1'),
(69, '$2y$10$.UndLpUYl3.8ZuHENdJRU.LSLgXsKAdXVjKJ30QIElvx/pxrYAqw2', 'Lapak UD. Saepul', 'bf@gmail.com', NULL, '$2y$10$ImRNuizAA4qsnECWK6QhWebxslVigMc6XSiwYUo8WN9pbuibGMufm', NULL, '2021-10-13 03:44:24', '2021-10-13 03:44:24', NULL, 'distributor', '1'),
(70, '$2y$10$fpAe4CVNnYhpIxv368gYVObFGi6RIUmNsIWNhYy6J3uHBa3CB9MJC', 'Tiga Sodara', 'bg@gmail.com', NULL, '$2y$10$msGX9Vp8HRKazYQ./wM0eeImEPCzyXqHPk5IPV7d3I4CpDPatFVm2', NULL, '2021-10-13 03:45:42', '2021-10-13 03:45:42', NULL, 'distributor', '1'),
(71, '$2y$10$7jg3me7Awa6QySmNO09KS.54Dw16ZnxHt/4aN/.ldt8OhhvSIkfB2', 'Toko Al Jawad', 'bh@gmail.com', NULL, '$2y$10$.1pqRbwQgjMKY8mV2C9Sv.CZ7GQZEd4/QIi0WcgdPTLrU5393ZxjS', NULL, '2021-10-13 03:47:00', '2021-10-13 03:47:00', NULL, 'distributor', '1'),
(72, '$2y$10$DDT62txdyBglpG6tDJ9j6uVknRPb7NZZVAXMstlTV9jd8HscXGnqW', 'Toko Beras Madina', 'bi@gmail.com', NULL, '$2y$10$6S2vgg6J56BP66CsV.GmoOE1aNRlrYPgzxu78vqy7oC.W/DMdc2o6', NULL, '2021-10-13 03:48:21', '2021-10-13 03:48:21', NULL, 'distributor', '1'),
(73, '$2y$10$.gpIAQ.p1pvcK51bIbzd/Oit4BlwjGvEizZNtJL6OfzhbisdPgBcO', 'Toko Maura', 'bj@gmail.com', NULL, '$2y$10$pjJV4qxDpnEOvmN25w/ZUOZbXPLHgVeI2wokOoH335vbgTDL21tUm', NULL, '2021-10-13 03:49:30', '2021-10-13 03:49:30', NULL, 'distributor', '1'),
(74, '$2y$10$/JSghOZOi0QsXNuOeNZ.oOxtmQCTna9PBMGbUMu.lA3VjW15QwMBK', 'Toko Paul', 'bk@gmail.com', NULL, '$2y$10$1ZOFS3WM7gWfHrCJlanb4.t6rK7EdmT3k5PAqfUW0f.ZIzFlLErTq', NULL, '2021-10-13 03:50:46', '2021-10-13 03:50:46', NULL, 'distributor', '1'),
(75, '$2y$10$h/kRkD2qjhymHPGJMIsLg.luECNIr4avzsVUMU1pkSHc.ZZ.9WUES', 'Toko Tiga Sodara', 'bl@gmail.com', NULL, '$2y$10$d1BMWgO9IiNBVXXPnG2aSeuHxP3jroN7.HBDvFN6PDOfgI2y5lnsC', NULL, '2021-10-13 03:53:26', '2021-10-13 03:53:26', NULL, 'distributor', '1'),
(76, '$2y$10$ceY0hZua0LnJdDYFYoa6gObqo9rMHZi2dVHLVITf/dVWjWeaq.Wb6', 'Ud. Sumatera', 'bm@gmail.com', NULL, '$2y$10$bfXocZNT6bof2fkR6ZqgsOoQMIODvwHountrTmVjqBGRxROpWhhU6', NULL, '2021-10-13 03:54:48', '2021-10-13 03:54:48', NULL, 'distributor', '1'),
(77, '$2y$10$UXFqXVVSHMyA.uzYlvnpgu.v8hSf3VsFIU.BdwnRrilLPDB1NEx5m', 'Ud. Usaha Dagang Sejahtera', 'bn@gmail.com', NULL, '$2y$10$J8L5j1vqHJRU5.bTpBNjOeuvJ384hQdtza//.Boi9d0L.PkAmFGgq', NULL, '2021-10-13 03:56:15', '2021-10-13 03:56:15', NULL, 'distributor', '1'),
(78, '$2y$10$UiiL02IXwRi.c9Zp/P5m2OnmmZJOwki1mcOcEThTS9gNcKCvVp6m2', 'Klp. Nusantara', 'bo@gmail.com', NULL, '$2y$10$8Eq4WsWmT8RFlccgw0GNgOs7N3/mmK/BgxSqmzWWso7kLQTI1O8LS', NULL, '2021-10-13 03:57:57', '2021-10-13 03:57:57', NULL, 'distributor', '1'),
(79, '$2y$10$W.mBgbCF197Va7NctIKXm.FODAJMTfKZdl7hLhtR1XHy4A1XkKH5y', 'Pak Opel', 'bp@gmail.com', NULL, '$2y$10$4t.Fzir.l.K9HsPx914Jke.sugISmb6qpcwVEfZACKPwIPuw0g1wW', NULL, '2021-10-13 03:59:25', '2021-10-13 03:59:25', NULL, 'distributor', '1'),
(80, '$2y$10$oS/xJt8VsPKnBgqP6/8.MutrhTu7wGsmHge.37acYPKeq4/N9p5vG', 'Toko A. Barokah', 'bq@gmail.com', NULL, '$2y$10$C/f3BSvnYyIpaBvzGrvOM.CVreaSCAN.tIW/lrd17P7qaIvzDXgV.', NULL, '2021-10-13 04:00:53', '2021-10-13 04:00:53', NULL, 'distributor', '1'),
(81, '$2y$10$bB2J2kpUJwdEVNdKN2PQxeo0l9Ka2hL75G8YUj8K.phJU3m7TBp5G', 'Toko Hamim', 'br@gmail.com', NULL, '$2y$10$l0geioVtbx7stmJNWA64k.93oEWGLOib4Y8uTl5cqHnCnOBUcdhve', NULL, '2021-10-13 04:02:23', '2021-10-13 04:02:23', NULL, 'distributor', '1'),
(82, '$2y$10$Jf.5kAuF4NRO6i.IiWjobevVCxKRm.9fMPu6015xn8GH.CCHwQ7U6', 'Toko Pak Abbas', 'bs@gmail.com', NULL, '$2y$10$5cICnxDuIVZh9cr1/c9f9eGcQ86R2eFGblFxTjImvh/xU3pGwG8uW', NULL, '2021-10-13 04:03:36', '2021-10-13 04:03:36', NULL, 'distributor', '1'),
(83, '$2y$10$MWSRWTeLvs8mWuPyNK.NXuuTXosAxg8wiIWjh1CC/iIChWiJcJ2qO', 'Toko Pak Age', 'bt@gmail.com', NULL, '$2y$10$GTvn29wrfCyO0Lch6V.IBu6Z/h9laXpmTtcwlzFdjyOlpl4N.vJ/a', NULL, '2021-10-13 04:04:57', '2021-10-13 04:04:57', NULL, 'distributor', '1'),
(84, '$2y$10$4.XAV2v0CALDzErQVrOTK.saLgvVhdBnBBCOxAYOvrYaqj5V5459i', 'Toko. H. Ucup', 'bu@gmail.com', NULL, '$2y$10$87w1mhGx.W5cChWRxJGppeu/OiMPRfpHd2X.yDjYazrQud75i9cq2', NULL, '2021-10-13 04:06:22', '2021-10-13 04:06:22', NULL, 'distributor', '1'),
(85, '$2y$10$WZ9KChNuIGYIMliu5A2WQ.Ggqr64.2zQomt7U91p2S/JQJpRZQdTq', 'H.Hairul', 'bv@gmail.com', NULL, '$2y$10$e7sjysGlR0dHjOV/9oIwqeZhXvbQki81KqCSLOoOIiSad7s6.BWli', NULL, '2021-10-13 04:08:17', '2021-10-13 04:08:17', NULL, 'distributor', '1'),
(86, '$2y$10$1msYdKWb87iDwI3fHZsrKe//0Yw5CzZdZQ0I9n2gxicuI4j.Nx9Qm', 'PT. Proman Kasa', 'bw@gmail.com', NULL, '$2y$10$wim6k6Cs1Arjhr4wHV2qdu/yoKUTarm/lkOOQ3jf1wPQcJBiZM6Pm', NULL, '2021-10-13 04:09:37', '2021-10-13 04:09:37', NULL, 'distributor', '1'),
(87, '$2y$10$b7jYZZB4A8l8GcZhFRH9COwlRpoT5xUeTa7iN3MC1yWrRXwNa.J2i', 'Hantoso Suteja (Gudang 45)', 'bx@gmail.com', NULL, '$2y$10$q/rpthe4C/bBBu9M7JU.SeHR7uXx2D/dJ1p9lsq8zJW0nTIuJe5PW', NULL, '2021-10-13 04:11:15', '2021-10-13 04:11:15', NULL, 'distributor', '1'),
(88, '$2y$10$Fu40eJrs4zuEhgcFJ44eWeuJ0n7KI8/HO9Zt3tB3ZiIcRwk6VqwPS', 'Manajemen', 'manajemen@gmail.com', NULL, '$2y$10$hx.FrTHlENHT.p9uTP3MCOcrZpvxU76CU6iZQcTrtVlfUce7uxqu6', NULL, '2021-10-13 08:39:55', '2021-10-13 08:39:55', NULL, 'manajemen', '1'),
(89, '$2y$10$E4pDNyRy/hO3pn0LXgx1T.F1uBLtUE.lpmZ742oE/veBJlbHFQeUu', 'Bayu', 'bayu@gmail.com', NULL, '$2y$10$u6PHuDC9kghMyrulIV5z/uhKNfO79UBlF786MBHlSKc4hErSVqYfC', NULL, '2021-10-13 08:42:40', '2021-10-13 08:48:34', NULL, 'manajemen', '1'),
(90, '$2y$10$wCL8KK.5UtJv48uOszfnZ.KK.IA5YwHgbOi2kKkY/RdZ8eM2bVkfu', '-', 'putera@gmail.com', NULL, '$2y$10$RITA5JkMlqlh/IFGyVOHMO1nwiavgf8HZIn7twpzF06xy6XkosWXy', NULL, '2021-11-08 04:19:03', '2021-11-08 04:19:03', NULL, 'distributor', '1'),
(91, '$2y$10$gtS5W1MO3m5/5wbk75qadOpIR37SYhp9T3ZBYKiW1dhnLC1fyRkx.', 'Zaki Jaya', 'zaki@gmail.com', NULL, '$2y$10$yZAtCEfPXmtHRobGGDygA.gR6fffXWzuaGGAeD/fUv6yYwWk/tIgS', NULL, '2021-11-08 04:33:01', '2021-11-08 04:33:01', NULL, 'distributor', '1'),
(92, '$2y$10$OzD1Sn0YHAlIOYVSs0I62OqHTiPX3b.Bzk4iNeyODi5ViWW/vDL7u', 'Aah Sayur', 'surya@gmail.com', NULL, '$2y$10$pPhHw3dfv5AWayzrhev/OuT4chVxEPKHQ.vvJpVRDkSaGkJY2AQR2', NULL, '2021-11-08 04:50:49', '2021-11-08 04:50:49', NULL, 'distributor', '1'),
(93, '$2y$10$sBfkSeCGd.ZtGS.YFQe.6eCYvGRNmQD9h7.ltjEJ77HuxqiJZx9ie', '-', 'triah@gmail.com', NULL, '$2y$10$pV47gSH/UbsdS63AVOShV.LlZw2TdU13an0Ckn1Hsrw2IpUxuMyti', NULL, '2021-11-08 06:15:22', '2021-11-08 06:15:22', NULL, 'distributor', '1'),
(94, '$2y$10$45GhH.r4xstDm4aZB1sOX.zNuSqOQFdU0POqJrqsEa..LgsIfozV2', 'Sayuran Fauzi', 'fauzi@gmail.com', NULL, '$2y$10$YdaCKUgx/D40YPWVcvlCTePO4ZMStawS7PdQoZUfuUl5MV5FtOa1u', NULL, '2021-11-08 06:23:08', '2021-11-08 06:23:08', NULL, 'distributor', '1'),
(95, '$2y$10$UvdCEfB7yhnW2nEMieBdm.Lsg8q5LsGt6JynE3aLO0SnRaX40g8q.', 'Toko Mandiri Berkah', 'sandi@gmail.com', NULL, '$2y$10$STk.2xy5Npx3zqGqUNpPAuv2GrA2owC9IIQZKCSmOPBC7PSP.URXe', NULL, '2021-11-08 06:35:33', '2021-11-08 06:35:33', NULL, 'distributor', '1'),
(96, '$2y$10$3/QG4ZTWYNnTRRsdh0trc.YDWh5n7rCcS9yWHmFoGUBoW8hW5hmjS', 'Toko Pak Age', 'age@gmail.com', NULL, '$2y$10$d3uNwBhddQiz.vu6/KxEaeA5.zSLbQmdT.OQtQO4E.7lk6aca8v4W', NULL, '2021-11-08 06:43:11', '2021-11-08 06:43:11', NULL, 'distributor', '1'),
(97, '$2y$10$LcXgId1KCXvGo2MX3iO8DeArCKlruJPrZRrYw5MyBgyZZeHJ.vCNu', '-', 'udi@gmail.com', NULL, '$2y$10$KTAuDvyGnhu3teU1e/ifdO2I4Y8BMi8Hgi82VAxbU3xlK.JJo6MJ.', NULL, '2021-11-08 06:55:27', '2021-11-08 06:55:27', NULL, 'distributor', '1'),
(98, '$2y$10$/eYi4b3TGbhQiCNS0HB63e31lvwx2NodLJfnXhs4WNtPFVZmgfEHi', '-', 'Awang@gmail.com', NULL, '$2y$10$8Z5kOtFwofL8zYQ5XwgdcONCzgiU9f9uQDQXQNve6ePqmO0.oQ5X.', NULL, '2021-11-08 07:01:29', '2021-11-08 07:01:29', NULL, 'distributor', '1'),
(99, '$2y$10$IH4WtGnSGUI5WEc2QmBugum0JxZDxXIUL0XdQHjBPcV6CL6BCqYma', '-', 'nasution@gmail.com', NULL, '$2y$10$0MIKo12fXOhId4HHFdU3duSpy4vc6tE9tU54p2jk062yZfrKbczPO', NULL, '2021-11-08 07:06:44', '2021-11-08 07:06:44', NULL, 'distributor', '1'),
(100, '$2y$10$GhEAie907twbPRtzhmdLuOaqxHdjWut6lXF1Z329H1CsNOTHm79Cy', 'Sayuran Fauzi', 'fauzi21@gmail.com', NULL, '$2y$10$ZmlInDciuyt68KdDrrFYm.D.kmEN4VLrf0VO1Av/kTnFykyH6Mo1W', NULL, '2021-11-08 07:58:07', '2021-11-08 07:58:07', NULL, 'distributor', '1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_distribusi_bapokting`
--
CREATE TABLE IF NOT EXISTS `view_distribusi_bapokting` (
`id` bigint(20)
,`kode_distribusi_bapokting` varchar(100)
,`id_perusahaan` bigint(20)
,`id_bidang_usaha` bigint(20)
,`id_jenis_barang` bigint(20)
,`tahun` int(11)
,`bulan` int(11)
,`skala_usaha` enum('distributor','sub_distributor','agen')
,`status_laporan` enum('draft','send')
,`user_create` bigint(20)
,`user_update` bigint(20)
,`user_delete` bigint(20)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
,`kode_perusahaan` varchar(100)
,`nib` varchar(25)
,`nomor_legalitas_usaha` varchar(50)
,`nama_perusahaan` varchar(255)
,`alamat_perusahaan` text
,`kode_bidang_usaha` varchar(100)
,`kode_kbli` int(11)
,`nama_kbli` varchar(255)
,`jenis_barang` enum('pokok','penting')
,`kode_jenis_barang` varchar(100)
,`nama_barang` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jenis_barang`
--
CREATE TABLE IF NOT EXISTS `view_jenis_barang` (
`id` bigint(20)
,`kode_jenis_barang` varchar(100)
,`id_bidang_usaha` bigint(20)
,`nama_barang` varchar(100)
,`user_create` bigint(20)
,`user_update` bigint(20)
,`user_delete` bigint(20)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
,`kode_bidang_usaha` varchar(100)
,`kode_kbli` int(11)
,`nama_kbli` varchar(255)
,`jenis_barang` enum('pokok','penting')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kelurahan`
--
CREATE TABLE IF NOT EXISTS `view_kelurahan` (
`id` bigint(20)
,`kode_kelurahan` varchar(100)
,`nama_kelurahan` varchar(50)
,`id_kecamatan` bigint(20)
,`user_create` bigint(20)
,`user_update` bigint(20)
,`user_delete` bigint(20)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
,`kode_kecamatan` varchar(100)
,`nama_kecamatan` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pemasaran`
--
CREATE TABLE IF NOT EXISTS `view_pemasaran` (
`id` bigint(20)
,`kode_pemasaran` varchar(100)
,`id_distribusi_bapokting` bigint(20)
,`id_konsumen` bigint(20)
,`id_asal_konsumen` bigint(20)
,`volume` decimal(10,0)
,`id_satuan` bigint(20)
,`user_create` bigint(20)
,`user_update` bigint(20)
,`user_delete` bigint(20)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
,`kode_konsumen` varchar(100)
,`nama_konsumen` varchar(50)
,`kode_asal_konsumen` varchar(100)
,`nama_asal_konsumen` varchar(50)
,`kode_satuan` varchar(100)
,`satuan` varchar(20)
,`nama_satuan` varchar(35)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pengadaan`
--
CREATE TABLE IF NOT EXISTS `view_pengadaan` (
`id` bigint(20)
,`kode_pengadaan` varchar(100)
,`id_distribusi_bapokting` bigint(20)
,`id_sumber_pembelian` bigint(20)
,`id_lokasi_pembelian` bigint(20)
,`volume` decimal(10,0)
,`id_satuan` bigint(20)
,`user_create` bigint(20)
,`user_update` bigint(20)
,`user_delete` bigint(20)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
,`kode_sumber_pembelian` varchar(100)
,`nama_sumber_pembelian` varchar(50)
,`kode_lokasi_pembelian` varchar(100)
,`nama_lokasi_pembelian` varchar(50)
,`kode_satuan` varchar(100)
,`satuan` varchar(20)
,`nama_satuan` varchar(35)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_perusahaan`
--
CREATE TABLE IF NOT EXISTS `view_perusahaan` (
`id` bigint(20)
,`kode_perusahaan` varchar(100)
,`id_pemilik` bigint(20)
,`id_user` bigint(20)
,`nib` varchar(25)
,`nomor_legalitas_usaha` varchar(50)
,`nama_perusahaan` varchar(255)
,`alamat_perusahaan` text
,`id_kelurahan` bigint(20)
,`nomor_telepon` varchar(15)
,`lokasi_usaha` enum('wilayah_usaha','permukiman')
,`status_usaha` enum('milik_sendiri','pekerja','konsinyasi')
,`status_toko` enum('milik_sendiri','sewa','lapak_pesan')
,`skala_usaha` enum('distributor','sub_distributor','agen')
,`luas_gudang` decimal(10,0)
,`koordinat` varchar(50)
,`user_create` bigint(20)
,`user_update` bigint(20)
,`user_delete` bigint(20)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
,`latitude` varchar(50)
,`longitude` varchar(50)
,`kode_pemilik` varchar(100)
,`nik` varchar(25)
,`nama_pemilik` varchar(100)
,`jenis_kelamin` enum('l','p')
,`alamat` text
,`nomor_handphone` varchar(15)
,`umur` bigint(21)
,`lama_usaha` bigint(21)
,`kode_user` varchar(100)
,`name` varchar(255)
,`email` varchar(255)
,`status_user` enum('0','1')
,`kode_kelurahan` varchar(100)
,`nama_kelurahan` varchar(50)
,`id_kecamatan` bigint(20)
,`kode_kecamatan` varchar(100)
,`nama_kecamatan` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_perusahaan_bidang_usaha`
--
CREATE TABLE IF NOT EXISTS `view_perusahaan_bidang_usaha` (
`id` bigint(20)
,`kode_perusahaan_bidang_usaha` varchar(100)
,`id_perusahaan` bigint(20)
,`id_jenis_barang` bigint(20)
,`status` enum('0','1')
,`user_create` bigint(20)
,`user_update` bigint(20)
,`user_delete` bigint(20)
,`created_at` datetime
,`updated_at` datetime
,`deleted_at` datetime
,`kode_jenis_barang` varchar(100)
,`id_bidang_usaha` bigint(20)
,`nama_barang` varchar(100)
,`kode_bidang_usaha` varchar(100)
,`kode_kbli` int(11)
,`nama_kbli` varchar(255)
,`jenis_barang` enum('pokok','penting')
);

-- --------------------------------------------------------

--
-- Structure for view `view_distribusi_bapokting`
--
DROP TABLE IF EXISTS `view_distribusi_bapokting`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_distribusi_bapokting` AS select `a`.`id` AS `id`,`a`.`kode_distribusi_bapokting` AS `kode_distribusi_bapokting`,`a`.`id_perusahaan` AS `id_perusahaan`,`a`.`id_bidang_usaha` AS `id_bidang_usaha`,`a`.`id_jenis_barang` AS `id_jenis_barang`,`a`.`tahun` AS `tahun`,`a`.`bulan` AS `bulan`,`a`.`skala_usaha` AS `skala_usaha`,`a`.`status_laporan` AS `status_laporan`,`a`.`user_create` AS `user_create`,`a`.`user_update` AS `user_update`,`a`.`user_delete` AS `user_delete`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,`b`.`kode_perusahaan` AS `kode_perusahaan`,`b`.`nib` AS `nib`,`b`.`nomor_legalitas_usaha` AS `nomor_legalitas_usaha`,`b`.`nama_perusahaan` AS `nama_perusahaan`,`b`.`alamat_perusahaan` AS `alamat_perusahaan`,`c`.`kode_bidang_usaha` AS `kode_bidang_usaha`,`c`.`kode_kbli` AS `kode_kbli`,`c`.`nama_kbli` AS `nama_kbli`,`c`.`jenis_barang` AS `jenis_barang`,`d`.`kode_jenis_barang` AS `kode_jenis_barang`,`d`.`nama_barang` AS `nama_barang` from (((`distribusi_bapokting` `a` join `perusahaan` `b` on(`a`.`id_perusahaan` = `b`.`id`)) join `bidang_usaha` `c` on(`a`.`id_bidang_usaha` = `c`.`id`)) join `jenis_barang` `d` on(`a`.`id_jenis_barang` = `d`.`id`)) where `a`.`deleted_at` is null;

-- --------------------------------------------------------

--
-- Structure for view `view_jenis_barang`
--
DROP TABLE IF EXISTS `view_jenis_barang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jenis_barang` AS select `a`.`id` AS `id`,`a`.`kode_jenis_barang` AS `kode_jenis_barang`,`a`.`id_bidang_usaha` AS `id_bidang_usaha`,`a`.`nama_barang` AS `nama_barang`,`a`.`user_create` AS `user_create`,`a`.`user_update` AS `user_update`,`a`.`user_delete` AS `user_delete`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,`b`.`kode_bidang_usaha` AS `kode_bidang_usaha`,`b`.`kode_kbli` AS `kode_kbli`,`b`.`nama_kbli` AS `nama_kbli`,`b`.`jenis_barang` AS `jenis_barang` from (`jenis_barang` `a` join `bidang_usaha` `b` on(`a`.`id_bidang_usaha` = `b`.`id`)) where `a`.`deleted_at` is null;

-- --------------------------------------------------------

--
-- Structure for view `view_kelurahan`
--
DROP TABLE IF EXISTS `view_kelurahan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kelurahan` AS select `a`.`id` AS `id`,`a`.`kode_kelurahan` AS `kode_kelurahan`,`a`.`nama_kelurahan` AS `nama_kelurahan`,`a`.`id_kecamatan` AS `id_kecamatan`,`a`.`user_create` AS `user_create`,`a`.`user_update` AS `user_update`,`a`.`user_delete` AS `user_delete`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,`b`.`kode_kecamatan` AS `kode_kecamatan`,`b`.`nama_kecamatan` AS `nama_kecamatan` from (`kelurahan` `a` join `kecamatan` `b` on(`a`.`id_kecamatan` = `b`.`id`)) where `a`.`deleted_at` is null;

-- --------------------------------------------------------

--
-- Structure for view `view_pemasaran`
--
DROP TABLE IF EXISTS `view_pemasaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pemasaran` AS select `a`.`id` AS `id`,`a`.`kode_pemasaran` AS `kode_pemasaran`,`a`.`id_distribusi_bapokting` AS `id_distribusi_bapokting`,`a`.`id_konsumen` AS `id_konsumen`,`a`.`id_asal_konsumen` AS `id_asal_konsumen`,`a`.`volume` AS `volume`,`a`.`id_satuan` AS `id_satuan`,`a`.`user_create` AS `user_create`,`a`.`user_update` AS `user_update`,`a`.`user_delete` AS `user_delete`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,`b`.`kode_konsumen` AS `kode_konsumen`,`b`.`nama_konsumen` AS `nama_konsumen`,`c`.`kode_asal_konsumen` AS `kode_asal_konsumen`,`c`.`nama_asal_konsumen` AS `nama_asal_konsumen`,`d`.`kode_satuan` AS `kode_satuan`,`d`.`satuan` AS `satuan`,`d`.`nama_satuan` AS `nama_satuan` from (((`pemasaran` `a` join `konsumen` `b` on(`a`.`id_konsumen` = `b`.`id`)) join `asal_konsumen` `c` on(`a`.`id_asal_konsumen` = `c`.`id`)) join `satuan` `d` on(`a`.`id_satuan` = `d`.`id`)) where `a`.`deleted_at` is null;

-- --------------------------------------------------------

--
-- Structure for view `view_pengadaan`
--
DROP TABLE IF EXISTS `view_pengadaan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pengadaan` AS select `a`.`id` AS `id`,`a`.`kode_pengadaan` AS `kode_pengadaan`,`a`.`id_distribusi_bapokting` AS `id_distribusi_bapokting`,`a`.`id_sumber_pembelian` AS `id_sumber_pembelian`,`a`.`id_lokasi_pembelian` AS `id_lokasi_pembelian`,`a`.`volume` AS `volume`,`a`.`id_satuan` AS `id_satuan`,`a`.`user_create` AS `user_create`,`a`.`user_update` AS `user_update`,`a`.`user_delete` AS `user_delete`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,`b`.`kode_sumber_pembelian` AS `kode_sumber_pembelian`,`b`.`nama_sumber_pembelian` AS `nama_sumber_pembelian`,`c`.`kode_lokasi_pembelian` AS `kode_lokasi_pembelian`,`c`.`nama_lokasi_pembelian` AS `nama_lokasi_pembelian`,`d`.`kode_satuan` AS `kode_satuan`,`d`.`satuan` AS `satuan`,`d`.`nama_satuan` AS `nama_satuan` from (((`pengadaan` `a` join `sumber_pembelian` `b` on(`a`.`id_sumber_pembelian` = `b`.`id`)) join `lokasi_pembelian` `c` on(`a`.`id_lokasi_pembelian` = `c`.`id`)) join `satuan` `d` on(`a`.`id_satuan` = `d`.`id`)) where `a`.`deleted_at` is null;

-- --------------------------------------------------------

--
-- Structure for view `view_perusahaan`
--
DROP TABLE IF EXISTS `view_perusahaan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_perusahaan` AS select `a`.`id` AS `id`,`a`.`kode_perusahaan` AS `kode_perusahaan`,`a`.`id_pemilik` AS `id_pemilik`,`a`.`id_user` AS `id_user`,`a`.`nib` AS `nib`,`a`.`nomor_legalitas_usaha` AS `nomor_legalitas_usaha`,`a`.`nama_perusahaan` AS `nama_perusahaan`,`a`.`alamat_perusahaan` AS `alamat_perusahaan`,`a`.`id_kelurahan` AS `id_kelurahan`,`a`.`nomor_telepon` AS `nomor_telepon`,`a`.`lokasi_usaha` AS `lokasi_usaha`,`a`.`status_usaha` AS `status_usaha`,`a`.`status_toko` AS `status_toko`,`a`.`skala_usaha` AS `skala_usaha`,`a`.`luas_gudang` AS `luas_gudang`,`a`.`koordinat` AS `koordinat`,`a`.`user_create` AS `user_create`,`a`.`user_update` AS `user_update`,`a`.`user_delete` AS `user_delete`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,trim(substring_index(`a`.`koordinat`,',',1)) AS `latitude`,trim(substring_index(`a`.`koordinat`,',',-1)) AS `longitude`,`b`.`kode_pemilik` AS `kode_pemilik`,`b`.`nik` AS `nik`,`b`.`nama_pemilik` AS `nama_pemilik`,`b`.`jenis_kelamin` AS `jenis_kelamin`,`b`.`alamat` AS `alamat`,`b`.`nomor_handphone` AS `nomor_handphone`,timestampdiff(YEAR,`b`.`tanggal_lahir`,curdate()) AS `umur`,timestampdiff(YEAR,`b`.`tanggal_join`,curdate()) AS `lama_usaha`,`c`.`kode_user` AS `kode_user`,`c`.`name` AS `name`,`c`.`email` AS `email`,`c`.`status` AS `status_user`,`d`.`kode_kelurahan` AS `kode_kelurahan`,`d`.`nama_kelurahan` AS `nama_kelurahan`,`d`.`id_kecamatan` AS `id_kecamatan`,`d`.`kode_kecamatan` AS `kode_kecamatan`,`d`.`nama_kecamatan` AS `nama_kecamatan` from (((`perusahaan` `a` join `pemilik` `b` on(`a`.`id_pemilik` = `b`.`id`)) join `users` `c` on(`a`.`id_user` = `c`.`id`)) join `view_kelurahan` `d` on(`a`.`id_kelurahan` = `d`.`id`)) where `a`.`deleted_at` is null;

-- --------------------------------------------------------

--
-- Structure for view `view_perusahaan_bidang_usaha`
--
DROP TABLE IF EXISTS `view_perusahaan_bidang_usaha`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_perusahaan_bidang_usaha` AS select `a`.`id` AS `id`,`a`.`kode_perusahaan_bidang_usaha` AS `kode_perusahaan_bidang_usaha`,`a`.`id_perusahaan` AS `id_perusahaan`,`a`.`id_jenis_barang` AS `id_jenis_barang`,`a`.`status` AS `status`,`a`.`user_create` AS `user_create`,`a`.`user_update` AS `user_update`,`a`.`user_delete` AS `user_delete`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at`,`b`.`kode_jenis_barang` AS `kode_jenis_barang`,`b`.`id_bidang_usaha` AS `id_bidang_usaha`,`b`.`nama_barang` AS `nama_barang`,`b`.`kode_bidang_usaha` AS `kode_bidang_usaha`,`b`.`kode_kbli` AS `kode_kbli`,`b`.`nama_kbli` AS `nama_kbli`,`b`.`jenis_barang` AS `jenis_barang` from (`perusahaan_bidang_usaha` `a` join `view_jenis_barang` `b` on(`a`.`id_jenis_barang` = `b`.`id`)) where `a`.`deleted_at` is null;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asal_konsumen`
--
ALTER TABLE `asal_konsumen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `bidang_usaha`
--
ALTER TABLE `bidang_usaha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `distribusi_bapokting`
--
ALTER TABLE `distribusi_bapokting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `distribusi_bapokting_ibfk_1` (`id_perusahaan`),
  ADD KEY `distribusi_bapokting_ibfk_2` (`id_bidang_usaha`),
  ADD KEY `distribusi_bapokting_ibfk_3` (`id_jenis_barang`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jenis_barang_ibfk_1` (`id_bidang_usaha`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelurahan_ibfk_1` (`id_kecamatan`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `log_activity_user`
--
ALTER TABLE `log_activity_user`
  ADD KEY `log_activity_user_ibfk_1` (`id_user`);

--
-- Indexes for table `lokasi_pembelian`
--
ALTER TABLE `lokasi_pembelian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `pemasaran`
--
ALTER TABLE `pemasaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pemasaran_ibfk_1` (`id_distribusi_bapokting`),
  ADD KEY `pemasaran_ibfk_2` (`id_konsumen`),
  ADD KEY `pemasaran_ibfk_3` (`id_asal_konsumen`),
  ADD KEY `pemasaran_ibfk_4` (`id_satuan`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `pemilik`
--
ALTER TABLE `pemilik`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `pengadaan`
--
ALTER TABLE `pengadaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengadaan_ibfk_1` (`id_distribusi_bapokting`),
  ADD KEY `pengadaan_ibfk_2` (`id_sumber_pembelian`),
  ADD KEY `pengadaan_ibfk_3` (`id_lokasi_pembelian`),
  ADD KEY `pengadaan_ibfk_4` (`id_satuan`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perusahaan_ibfk_1` (`id_kelurahan`),
  ADD KEY `perusahaan_ibfk_2` (`id_user`),
  ADD KEY `perusahaan_ibfk_3` (`id_pemilik`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `perusahaan_bidang_usaha`
--
ALTER TABLE `perusahaan_bidang_usaha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perusahaan_bidang_usaha_ibfk_1` (`id_perusahaan`),
  ADD KEY `perusahaan_bidang_usaha_ibfk_2` (`id_jenis_barang`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_delete` (`user_delete`);

--
-- Indexes for table `sumber_pembelian`
--
ALTER TABLE `sumber_pembelian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_create` (`user_create`),
  ADD KEY `user_update` (`user_update`),
  ADD KEY `user_deleted` (`user_delete`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asal_konsumen`
--
ALTER TABLE `asal_konsumen`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bidang_usaha`
--
ALTER TABLE `bidang_usaha`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `distribusi_bapokting`
--
ALTER TABLE `distribusi_bapokting`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=327107;
--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3271061012;
--
-- AUTO_INCREMENT for table `konsumen`
--
ALTER TABLE `konsumen`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `lokasi_pembelian`
--
ALTER TABLE `lokasi_pembelian`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pemasaran`
--
ALTER TABLE `pemasaran`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pemilik`
--
ALTER TABLE `pemilik`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `pengadaan`
--
ALTER TABLE `pengadaan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `perusahaan_bidang_usaha`
--
ALTER TABLE `perusahaan_bidang_usaha`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sumber_pembelian`
--
ALTER TABLE `sumber_pembelian`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `asal_konsumen`
--
ALTER TABLE `asal_konsumen`
  ADD CONSTRAINT `asal_konsumen_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asal_konsumen_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `asal_konsumen_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bidang_usaha`
--
ALTER TABLE `bidang_usaha`
  ADD CONSTRAINT `bidang_usaha_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bidang_usaha_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bidang_usaha_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `distribusi_bapokting`
--
ALTER TABLE `distribusi_bapokting`
  ADD CONSTRAINT `distribusi_bapokting_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `perusahaan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `distribusi_bapokting_ibfk_2` FOREIGN KEY (`id_bidang_usaha`) REFERENCES `bidang_usaha` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `distribusi_bapokting_ibfk_3` FOREIGN KEY (`id_jenis_barang`) REFERENCES `jenis_barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `distribusi_bapokting_ibfk_4` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `distribusi_bapokting_ibfk_5` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `distribusi_bapokting_ibfk_6` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD CONSTRAINT `jenis_barang_ibfk_1` FOREIGN KEY (`id_bidang_usaha`) REFERENCES `bidang_usaha` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jenis_barang_ibfk_2` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jenis_barang_ibfk_3` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jenis_barang_ibfk_4` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `kecamatan_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kecamatan_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kecamatan_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD CONSTRAINT `kelurahan_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelurahan_ibfk_2` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelurahan_ibfk_3` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelurahan_ibfk_4` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD CONSTRAINT `konsumen_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `konsumen_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `konsumen_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log_activity_user`
--
ALTER TABLE `log_activity_user`
  ADD CONSTRAINT `log_activity_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lokasi_pembelian`
--
ALTER TABLE `lokasi_pembelian`
  ADD CONSTRAINT `lokasi_pembelian_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lokasi_pembelian_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lokasi_pembelian_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemasaran`
--
ALTER TABLE `pemasaran`
  ADD CONSTRAINT `pemasaran_ibfk_1` FOREIGN KEY (`id_distribusi_bapokting`) REFERENCES `distribusi_bapokting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasaran_ibfk_2` FOREIGN KEY (`id_konsumen`) REFERENCES `konsumen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasaran_ibfk_3` FOREIGN KEY (`id_asal_konsumen`) REFERENCES `asal_konsumen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasaran_ibfk_4` FOREIGN KEY (`id_satuan`) REFERENCES `satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasaran_ibfk_5` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasaran_ibfk_6` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasaran_ibfk_7` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemilik`
--
ALTER TABLE `pemilik`
  ADD CONSTRAINT `pemilik_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemilik_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemilik_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengadaan`
--
ALTER TABLE `pengadaan`
  ADD CONSTRAINT `pengadaan_ibfk_1` FOREIGN KEY (`id_distribusi_bapokting`) REFERENCES `distribusi_bapokting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengadaan_ibfk_2` FOREIGN KEY (`id_sumber_pembelian`) REFERENCES `sumber_pembelian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengadaan_ibfk_3` FOREIGN KEY (`id_lokasi_pembelian`) REFERENCES `lokasi_pembelian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengadaan_ibfk_4` FOREIGN KEY (`id_satuan`) REFERENCES `satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengadaan_ibfk_5` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengadaan_ibfk_6` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengadaan_ibfk_7` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD CONSTRAINT `perusahaan_ibfk_1` FOREIGN KEY (`id_kelurahan`) REFERENCES `kelurahan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_ibfk_3` FOREIGN KEY (`id_pemilik`) REFERENCES `pemilik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_ibfk_4` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_ibfk_5` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_ibfk_6` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `perusahaan_bidang_usaha`
--
ALTER TABLE `perusahaan_bidang_usaha`
  ADD CONSTRAINT `perusahaan_bidang_usaha_ibfk_1` FOREIGN KEY (`id_perusahaan`) REFERENCES `perusahaan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_bidang_usaha_ibfk_2` FOREIGN KEY (`id_jenis_barang`) REFERENCES `jenis_barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_bidang_usaha_ibfk_3` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_bidang_usaha_ibfk_4` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perusahaan_bidang_usaha_ibfk_5` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `satuan`
--
ALTER TABLE `satuan`
  ADD CONSTRAINT `satuan_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `satuan_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `satuan_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slider`
--
ALTER TABLE `slider`
  ADD CONSTRAINT `slider_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slider_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slider_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sumber_pembelian`
--
ALTER TABLE `sumber_pembelian`
  ADD CONSTRAINT `sumber_pembelian_ibfk_1` FOREIGN KEY (`user_create`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sumber_pembelian_ibfk_2` FOREIGN KEY (`user_update`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sumber_pembelian_ibfk_3` FOREIGN KEY (`user_delete`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
