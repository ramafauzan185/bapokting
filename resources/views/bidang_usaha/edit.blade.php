<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 10:57
 */

$title = 'Edit Bidang Usaha';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('bidang-usaha.update', $bidang_usaha)}}" method="post">
        @method('PUT')
        @csrf
        @include('bidang_usaha/form');
    </form>
@stop
