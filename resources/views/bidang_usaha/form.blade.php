<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 9:54
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="kode_kbli">Kode KBLI</label>
                    <input type="number" class="form-control @error('kode_kbli') is-invalid @enderror" id="kode_kbli" placeholder="Kode KBLI" name="kode_kbli" value="{{ isset($bidang_usaha)? $bidang_usaha->kode_kbli : old('kode_kbli') }}" required>
                    @error('kode_kbli') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="nama_kbli">Nama KBLI</label>
                    <input type="text" class="form-control @error('nama_kbli') is-invalid @enderror" id="nama_kbli" placeholder="Nama KBLI" name="nama_kbli" value="{{ isset($bidang_usaha)? $bidang_usaha->nama_kbli : old('nama_kbli') }}" required>
                    @error('nama_kbli') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="jenis_barang">Jenis Barang</label>
                    <select class="form-control @error('jenis_barang') is-invalid @enderror" id="jenis_barang" name="jenis_barang" required>
                        <option value="">-- Pilih Jenis Barang --</option>
                        @foreach(jenis_barangs() as $k=>$v)
                            <option value="{{ $k }}" {{ (isset($bidang_usaha) && $bidang_usaha->jenis_barang == $k || old('jenis_barang') == $k)? 'selected' : '' }}>{{ $v }}</option>
                        @endforeach
                    </select>
                    @error('jenis_barang') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('bidang-usaha') !!}
            </div>
        </div>
    </div>
</div>
