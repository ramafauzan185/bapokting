<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 30/08/2021
 * Time: 20:07
 */

$title = 'Pengelolaan Data Bidang Usaha';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {!! button_add('bidang-usaha') !!}
                </div>

                <div class="card-body">
                    <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Kode KBLI</th>
                            <th>Nama KBLI</th>
                            <th>Jenis Barang</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bidang_usaha as $key => $row)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$row->kode_kbli}}</td>
                                <td>{{$row->nama_kbli}}</td>
                                <td>{{ ucfirst($row->jenis_barang) }}</td>
                                <td class="text-center">
                                    {!! button_edit('bidang-usaha', $row) !!}
                                    {!! button_delete('bidang-usaha', $row) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    @include('layouts/js')
@endpush
