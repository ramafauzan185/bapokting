<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 30/08/2021
 * Time: 20:19
 */

$title = 'Tambah Bidang Usaha';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('bidang-usaha.store')}}" method="post">
        @csrf
        @include('bidang_usaha/form')
    </form>
@stop
