<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 16:45
 */

$title = 'Edit Konsumen';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('konsumen.update', $konsumen)}}" method="post">
        @method('PUT')
        @csrf
        @include('konsumen/form');
    </form>
@stop