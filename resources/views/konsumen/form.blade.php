<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 16:38
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="nama_konsumen">Konsumen</label>
                    <input type="text" class="form-control @error('nama_konsumen') is-invalid @enderror" id="nama_konsumen" placeholder="Nama konsumen" name="nama_konsumen" value="{{ isset($konsumen)? $konsumen->nama_konsumen : old('nama_konsumen') }}" required>
                    @error('nama_konsumen') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('konsumen') !!}
            </div>
        </div>
    </div>
</div>
