<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 5:12
 */

$title = 'Tambah Asal Konsumen';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('asal-konsumen.store')}}" method="post">
        @csrf
        @include('asal_konsumen/form')
    </form>
@stop