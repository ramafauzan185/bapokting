<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 10/09/2021
 * Time: 2:28
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="nama_asal_konsumen">Asal Konsumen</label>
                    <input type="text" class="form-control @error('nama_asal_konsumen') is-invalid @enderror" id="nama_asal_konsumen" placeholder="Asal konsumen" name="nama_asal_konsumen" value="{{ isset($asal_konsumen)? $asal_konsumen->nama_asal_konsumen : old('nama_asal_konsumen') }}" required>
                    @error('nama_asal_konsumen') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('asal-konsumen') !!}
            </div>
        </div>
    </div>
</div>
