<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 5:10
 */

$title = 'Pengelolaan Data Asal Konsumen';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {!! button_add('asal-konsumen') !!}
                </div>
                <div class="card-body">
                    <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Asal Konsumen</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($asal_konsumen as $key => $row)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$row->nama_asal_konsumen}}</td>
                                <td class="text-center">
                                    {!! button_edit('asal-konsumen', $row) !!}
                                    {!! button_delete('asal-konsumen', $row) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    @include('layouts/js')
@endpush