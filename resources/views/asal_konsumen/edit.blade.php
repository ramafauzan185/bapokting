<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 10/09/2021
 * Time: 2:34
 */

$title = 'Edit Asal Konsumen';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('asal-konsumen.update', $asal_konsumen)}}" method="post">
        @method('PUT')
        @csrf
        @include('asal_konsumen/form');
    </form>
@stop