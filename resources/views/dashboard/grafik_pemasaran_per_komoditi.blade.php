<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 16/09/2021
 * Time: 14:47
 */

?>

<div class="card card-outline card-blue">
    <div class="card-header">{!! __('Pemasaran Per Komoditi <small>Di Tahun 2021</small>') !!}</div>

    <div class="card-body" style="overflow: auto;">
        <div style="width: 100%; height: 400px; display: flex; align-content: center;">
            <div id="chartKomoditiPemasaranPertahun" style="width: 100%; height: 100%;"></div>
        </div>
    </div>
</div>

<x-adminlte-modal id="modalGrafikPemasaranPerKomoditi" title="Grafik Pemasaran Per Komoditi" size="lg" theme="light" v-centered static-backdrop scrollable>
    <div style="height: auto;" id="modalGrafikPemasaranPerKomoditiContent">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped">
                    <tr>
                        <th width="150px" class="bg-light">Tahun</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailTahun"></span></td>

                        <th width="150px" class="bg-light">Kode KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailKodeKbli"></span></td>
                    </tr>
                    <tr>
                        <th width="150px" class="bg-light">Nama KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td colspan="4"><span id="detailNamaKbli"></span></td>
                    </tr>
                    <tr>
                        <th class="bg-light">Jenis Barang</th>
                        <th class="bg-light">:</th>
                        <td colspan="4"><span id="detailNamaBarang"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped simpleDatatable">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Bulan</th>
                        <th>Nama Perusahaan</th>
                        <th>Skala Usaha</th>
                        <th>Volume</th>
                        <th>Konsumen</th>
                        <th>Asal Konsumen</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@push('js')
    <script>
        am4core.ready(function() {
            am4core.useTheme(am4themes_animated);

            // Create chart instance
            var chartKomoditiPemasaranPertahun = am4core.create("chartKomoditiPemasaranPertahun", am4charts.PieChart);

            chartKomoditiPemasaranPertahun.exporting.menu = new am4core.ExportMenu();
            chartKomoditiPemasaranPertahun.exporting.menu.items = [{
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "pdf", "label": "PDF" },
                    { "type": "print", "label": "Print", },
                ]
            }];

            // Create pie series
            var series = chartKomoditiPemasaranPertahun.series.push(new am4charts.PieSeries());
            series.dataFields.value = "jum";
            series.dataFields.category = "nama_barang";
            series.dataFields.id_jenis_barang = "id_jenis_barang";
            series.dataFields.satuan = "satuan";

            series.labels.template.text = "{category}:\n{value} {satuan}";
            series.slices.template.tooltipText = "{category}\n{value} {satuan}";
            series.legendSettings.valueText = "{value} {satuan}";

            series.labels.template.maxWidth = 100;
            series.labels.template.wrap = true;
            series.labels.template.fontSize = 11;

            // Add data
            chartKomoditiPemasaranPertahun.data = <?php if(!empty($pemasaran_get_per_komoditi_per_tahun)){ echo json_encode($pemasaran_get_per_komoditi_per_tahun); } else { ?> [] <?php } ;?>;
            /*chartKomoditiPemasaranPertahun.data = [{
                "id_jenis_barang": "1",
                "nama_barang": "Beras",
                "jum": 501.9,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "2",
                "nama_barang": "Ikan Asin",
                "jum": 301.9,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "3",
                "nama_barang": "LPG",
                "jum": 201.1,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "4",
                "nama_barang": "Ikan Segar",
                "jum": 165.8,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "5",
                "nama_barang": "Minyak Curah",
                "jum": 139.9,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "6",
                "nama_barang": "Kedelai",
                "jum": 128.3,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "7",
                "nama_barang": "Gula Pasir",
                "jum": 99,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "8",
                "nama_barang": "Terigu",
                "jum": 60,
                "satuan": "Kg"
            }, {
                "id_jenis_barang": "9",
                "nama_barang": "Bawang Putih",
                "jum": 50,
                "satuan": "Kg"
            }];*/

            // And, for a good measure, let's add a legend
            chartKomoditiPemasaranPertahun.legend = new am4charts.Legend();

            chartKomoditiPemasaranPertahun.legend.maxHeight = 100;
            chartKomoditiPemasaranPertahun.legend.scrollable = true;
            chartKomoditiPemasaranPertahun.legend.labels.template.text = "[bold {color}]{category}";

            series.slices.template.events.on("hit", function(ev) {
                let seriesItem = ev.target.dataItem.component;
                seriesItem.slices.each(function(item) {
                    if (item.isActive && item != ev.target) {
                        item.isActive = false;
                    }
                    if (!item.isActive && item == ev.target) {
                        item.isActive = true;
                    }

                    if(item == ev.target) {
                        showGrafikPemasaranPerKomoditiDetail(ev.target.dataItem.id_jenis_barang);
                    }
                });
            });
        });

        function showGrafikPemasaranPerKomoditiDetail(id_jenis_barang) {
            const modal = $('#modalGrafikPemasaranPerKomoditi');
            const table = $('#modalGrafikPemasaranPerKomoditiContent .simpleDatatable tbody');
            const detailNamaBarang = $('#modalGrafikPemasaranPerKomoditiContent #detailNamaBarang');
            const detailTahun = $('#modalGrafikPemasaranPerKomoditiContent #detailTahun');
            const detailKodeKbli = $('#modalGrafikPemasaranPerKomoditiContent #detailKodeKbli');
            const detailNamaKbli = $('#modalGrafikPemasaranPerKomoditiContent #detailNamaKbli');

            $.ajax({
                data: { id_jenis_barang: id_jenis_barang, tahun: '<?=$tahun;?>' },
                url: '<?=route('home.grafik-pemasaran-per-komoditi-detail');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    const pemasaran_get_per_komoditi_per_tahun = data.pemasaran_get_per_komoditi_per_tahun;
                    const kode_kbli = data.kode_kbli;
                    const nama_kbli = data.nama_kbli;
                    const nama_barang = data.nama_barang;
                    const jenis_barang = data.jenis_barang;
                    const tahun = data.tahun;

                    detailTahun.html(tahun);
                    detailKodeKbli.html(kode_kbli);
                    detailNamaKbli.html(nama_kbli);
                    detailNamaBarang.html(nama_barang + ' - ' + jenis_barang);

                    let tableBody;
                    pemasaran_get_per_komoditi_per_tahun.forEach((item, index) => {
                        tableBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${bulans()[item.bulan]}</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.skala_usaha.replaceAll('_', ' ')}</td>
                                <td>${item.volume} ${item.satuan}</td>
                                <td>${item.nama_konsumen}</td>
                                <td>${item.nama_asal_konsumen}</td>
                            </tr>
                        `;
                    });

                    table.html('');
                    table.append(tableBody);

                    simpleDatatable();

                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush
