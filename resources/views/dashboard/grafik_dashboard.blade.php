<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 15/11/2021
 * Time: 20:54
 */

?>

<div class="row justify-content-center">
    <div class="col-md-12">
        @include('dashboard/grafik_jenis_kelamin_pedagang_by_jenis_produk')
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-6">
        @include('dashboard/grafik_pengadaan_per_komoditi')
    </div>
    <div class="col-md-6">
        @include('dashboard/grafik_pemasaran_per_komoditi')
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-12">
        @include('dashboard/grafik_rerata_lama_usaha_pedagang_by_jenis_produk')
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-6">
        @include('dashboard/grafik_status_kepemilikan_toko_per_komoditi')
    </div>
    <div class="col-md-6">
        @include('dashboard/grafik_status_usaha_per_komoditi')
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-6">
        @include('dashboard/grafik_pengadaan_per_komoditi_by_sumber_pembelian')
    </div>
    <div class="col-md-6">
        @include('dashboard/grafik_pengadaan_per_komoditi_by_lokasi_pembelian')
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-6">
        @include('dashboard/grafik_pemasaran_per_komoditi_by_konsumen')
    </div>
    <div class="col-md-6">
        @include('dashboard/grafik_pemasaran_per_komoditi_by_asal_konsumen')
    </div>
</div>
