<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 16/09/2021
 * Time: 16:11
 */

?>

<div class="card card-outline card-blue">
    <div class="card-header">{!! __('Jenis Kelamin Pedagang <small>Berdasarkan jenis produk yang dijual</small>') !!}</div>

    <div class="card-body" style="overflow: auto;">
        <form action="" id="formFilterChart" method="get">
            <div class="form-group row">
                <label for="filter_id_kecamatan" class="col-sm-2 col-form-label">Pilih Kecamatan</label>
                <div class="col-sm-4">
                    <select class="form-control @error('id_kecamatan') is-invalid @enderror" id="filter_id_kecamatan" name="id_kecamatan">
                        <option value="">All</option>
                        @foreach($kecamatan as $row)
                            <option value="{{ $row->id }}">{{ $row->nama_kecamatan }}</option>
                        @endforeach
                    </select>
                </div>

                <label for="filter_id_kelurahan" class="col-sm-2 col-form-label">Pilih Kelurahan</label>
                <div class="col-sm-4">
                    <select class="form-control @error('id_kelurahan') is-invalid @enderror" id="filter_id_kelurahan" name="id_kelurahan" disabled>
                        <option value="">All</option>
                    </select>
                </div>
            </div>
        </form>

        <div id="chartJenisKelaminPedagangByJenisProduk" style="width: 100%; height: 400px;"></div>
    </div>
</div>

<x-adminlte-modal id="modalGrafikJenisKelaminPedagangByJenisProduk" title="Grafik Pedagang Per Jenis Produk" size="lg" theme="light" v-centered static-backdrop scrollable>
    <div style="height: auto;" id="modalGrafikJenisKelaminPedagangByJenisProdukContent">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped">
                    <tr>
                        <th width="150px" class="bg-light">Kode KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailKodeKbli"></span></td>
                    </tr>
                    <tr>
                        <th width="150px" class="bg-light">Nama KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailNamaKbli"></span></td>
                    </tr>
                    <tr>
                        <th class="bg-light">Jenis Barang</th>
                        <th class="bg-light">:</th>
                        <td><span id="detailNamaBarang"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-stripped simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nomor Legalitas Usaha</th>
                            <th>Nama Perusahaan</th>
                            <th>NIK</th>
                            <th>Pemilik</th>
                            <th>Jenis Kelamin</th>
                            <th>Alamat</th>
                            <th>Umur</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@push('js')
    <script>
        am4core.ready(function() {
            showGrafikJenisKelaminPedagangByJenisProduk(<?php if(!empty($jenis_kelamin_pedagang_per_komoditi)){ echo json_encode($jenis_kelamin_pedagang_per_komoditi); } else { ?> [] <?php } ?>);
        });

        function showGrafikJenisKelaminPedagangByJenisProduk(data) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chartJenisKelaminPedagangByJenisProduk = am4core.create('chartJenisKelaminPedagangByJenisProduk', am4charts.XYChart);
            chartJenisKelaminPedagangByJenisProduk.colors.step = 2;

            chartJenisKelaminPedagangByJenisProduk.exporting.menu = new am4core.ExportMenu();
            chartJenisKelaminPedagangByJenisProduk.exporting.menu.items = [{
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "pdf", "label": "PDF" },
                    { "type": "print", "label": "Print", },
                ]
            }];

            chartJenisKelaminPedagangByJenisProduk.legend = new am4charts.Legend();
            chartJenisKelaminPedagangByJenisProduk.legend.position = 'top';
            chartJenisKelaminPedagangByJenisProduk.legend.paddingBottom = 20;
            chartJenisKelaminPedagangByJenisProduk.legend.labels.template.maxWidth = 95;

            var xAxis = chartJenisKelaminPedagangByJenisProduk.xAxes.push(new am4charts.CategoryAxis());
            xAxis.dataFields.category = 'category';
            xAxis.renderer.cellStartLocation = 0.1;
            xAxis.renderer.cellEndLocation = 0.9;
            xAxis.renderer.grid.template.location = 0;

            xAxis.renderer.minGridDistance = 30;
            xAxis.renderer.labels.template.horizontalCenter = "right";
            xAxis.renderer.labels.template.verticalCenter = "middle";
            xAxis.renderer.labels.template.rotation = 340;
            // categoryAxis.tooltip.disabled = true;
            xAxis.renderer.minHeight = 110;

            xAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                if (target.dataItem && target.dataItem.index & 2 == 2) {
                    return 0;//dy + 25;
                }
                return 0;//dy;
            });

            var yAxis = chartJenisKelaminPedagangByJenisProduk.yAxes.push(new am4charts.ValueAxis());
            yAxis.min = 0;

            function createSeries(value, name) {
                var series = chartJenisKelaminPedagangByJenisProduk.series.push(new am4charts.ColumnSeries());
                series.dataFields.valueY = value;
                series.dataFields.categoryX = 'category';
                series.dataFields.id_jenis_barang = 'id_jenis_barang';
                series.dataFields.percent = `${value}_percent`;
                series.name = name;

                series.columns.template.column.cornerRadiusTopLeft = 5;
                series.columns.template.column.cornerRadiusTopRight = 5;
                series.columns.template.tooltipText = `${name}: [bold]{valueY}[/] orang`;

                series.events.on("hidden", arrangeColumns);
                series.events.on("shown", arrangeColumns);

                var bullet = series.bullets.push(new am4charts.LabelBullet());
                bullet.interactionsEnabled = false;
                bullet.dy = 10;
                bullet.label.text = '{valueY}';
                bullet.label.fill = am4core.color('#ffffff');
                bullet.label.fontSize = 12;

                series.columns.template.events.on("hit", function(ev) {
                    showGrafikJenisKelaminPedagangByJenisProdukDetail(ev.target.dataItem.id_jenis_barang, value);
                }, this);

                return series;
            }

            chartJenisKelaminPedagangByJenisProduk.data = data;

            createSeries('laki_laki', 'Laki-Laki');
            createSeries('perempuan', 'Perempuan');

            function arrangeColumns() {
                var series = chartJenisKelaminPedagangByJenisProduk.series.getIndex(0);

                var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                if (series.dataItems.length > 1) {
                    var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                    var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                    var delta = ((x1 - x0) / chartJenisKelaminPedagangByJenisProduk.series.length) * w;
                    if (am4core.isNumber(delta)) {
                        var middle = chartJenisKelaminPedagangByJenisProduk.series.length / 2;

                        var newIndex = 0;
                        chartJenisKelaminPedagangByJenisProduk.series.each(function(series) {
                            if (!series.isHidden && !series.isHiding) {
                                series.dummyData = newIndex;
                                newIndex++;
                            }
                            else {
                                series.dummyData = chartJenisKelaminPedagangByJenisProduk.series.indexOf(series);
                            }
                        });
                        var visibleCount = newIndex;
                        var newMiddle = visibleCount / 2;

                        chartJenisKelaminPedagangByJenisProduk.series.each(function(series) {
                            var trueIndex = chartJenisKelaminPedagangByJenisProduk.series.indexOf(series);
                            var newIndex = series.dummyData;

                            var dx = (newIndex - trueIndex + middle - newMiddle) * delta;

                            series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                            series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                        });
                    }
                }
            }

            // Cursor
            chartJenisKelaminPedagangByJenisProduk.cursor = new am4charts.XYCursor();
        }

        function getJenisKelaminPedagangByJenisProduk(id_kelurahan = '') {
            $.ajax({
                data: { id_kelurahan },
                url: '<?=route('home.grafik-jenis-kelamin-pedagang-by-jenis-produk');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const jenis_kelamin_pedagang_per_komoditi = data.jenis_kelamin_pedagang_per_komoditi;

                    showGrafikJenisKelaminPedagangByJenisProduk(jenis_kelamin_pedagang_per_komoditi);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function showGrafikJenisKelaminPedagangByJenisProdukDetail(id_jenis_barang, jenis_kelamin) {
            const modal = $('#modalGrafikJenisKelaminPedagangByJenisProduk');
            const table = $('#modalGrafikJenisKelaminPedagangByJenisProdukContent .simpleDatatable tbody');
            const detailNamaBarang = $('#modalGrafikJenisKelaminPedagangByJenisProdukContent #detailNamaBarang');
            const detailKodeKbli = $('#modalGrafikJenisKelaminPedagangByJenisProdukContent #detailKodeKbli');
            const detailNamaKbli = $('#modalGrafikJenisKelaminPedagangByJenisProdukContent #detailNamaKbli');
            const id_kelurahan = $('#formFilterChart #filter_id_kelurahan').val();

            $.ajax({
                data: { id_jenis_barang, jenis_kelamin, id_kelurahan },
                url: '<?=route('home.grafik-jenis-kelamin-pedagang-per-komoditi-detail');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const jenis_kelamin_pedagang_per_komoditi = data.jenis_kelamin_pedagang_per_komoditi;
                    const kode_kbli = data.kode_kbli;
                    const nama_kbli = data.nama_kbli;
                    const nama_barang = data.nama_barang;
                    const jenis_barang = data.jenis_barang;

                    detailKodeKbli.html(kode_kbli);
                    detailNamaKbli.html(nama_kbli);
                    detailNamaBarang.html(nama_barang + ' - ' + jenis_barang);

                    let tableBody;
                    jenis_kelamin_pedagang_per_komoditi.forEach((item, index) => {
                        tableBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${item.nomor_legalitas_usaha}</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.nik}</td>
                                <td>${item.nama_pemilik}</td>
                                <td>${get_jenis_kelamin(item.jenis_kelamin)}</td>
                                <td>${item.alamat}</td>
                                <td>${item.umur} tahun</td>
                            </tr>
                        `;
                    });

                    table.html('');
                    table.append(tableBody);

                    simpleDatatable();

                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        $('#formFilterChart #filter_id_kecamatan').on('change', function () {
            getKelurahanByKecamatan('formFilterChart', this.value);
        });

        $('#formFilterChart #filter_id_kelurahan').on('change', function () {
            getJenisKelaminPedagangByJenisProduk(this.value);
        })
    </script>
@endpush