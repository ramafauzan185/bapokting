<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 01/10/2021
 * Time: 2:47
 */

?>

<div class="row">
    <div class="col-12">
        <x-adminlte-card title="Tampilkan Peta Berdasarkan" icon="fas fa-sm fa-filter" theme="lightblue" collapsible="{{ !isset($_GET['btnFilter'])? 'collapsed' : '' }}">
            <form action="" id="formFilterMap" method="get">
                @include('dashboard/form_filter')
            </form>
        </x-adminlte-card>
    </div>
</div>

<div class="card card-outline card-blue">
    {{--<div class="card-header">{!! __('Titik Koordinat Perusahaan') !!}</div>--}}

    <div class="card-body" style="padding: 0;">
        <div id="mapKoordinatPerusahaan" style="height: 450px;"></div>
    </div>
</div>

@push('js')
    <script>
        const koordinatCenter = [-6.596506266337799, 106.79968302699291];
        var map = L.map('mapKoordinatPerusahaan').setView(koordinatCenter, 12);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 22,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: '{{ env('MAPBOX_ACCESS_TOKEN') }}'
        }).addTo(map);

        var markers = (koordinat) => {
            return L.marker([koordinat.lat, koordinat.lng]).addTo(map);
        };
        var markersPopup = (marker, content) => {
            marker.bindPopup(content);
        };
        var marks;
        marks = L.layerGroup([ markers({ lat: koordinatCenter[0], lng: koordinatCenter[1]}) ]).addTo(map);

        function showMap(data)
        {
            $.ajax({
                data: data,
                url: '<?=route('home.map-koordinat-perusahaan');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const perusahaan = data.perusahaan;
                    let marker,
                        content,
                        nama_perusahaan,
                        alamat_perusahaan,
                        skala_usaha,
                        latitude,
                        longitude,
                        nama_pemilik;

                    marks.clearLayers();

                    data_marker = [];
                    perusahaan.forEach((item) => {
                        nama_perusahaan = item.nama_perusahaan;
                        alamat_perusahaan = item.alamat_perusahaan;
                        skala_usaha = item.skala_usaha;
                        latitude = item.latitude;
                        longitude = item.longitude;
                        nama_pemilik = item.nama_pemilik;

                        marker = markers({ lat: latitude, lng: longitude});
                        content = `${nama_perusahaan}<br>
                               ${alamat_perusahaan}<br>
                               ${skala_usaha.replaceAll('_', ' ')}
                               <hr class="mt-1 mb-1">
                               Pemilik : ${nama_pemilik}
                               `;
                        markersPopup(marker, content);

                        data_marker.push(marker);
                    });

                    marks = L.layerGroup(data_marker).addTo(map);

                    hideLoader();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        showMap({ map: true });

        $('#formFilterMap #filter_jenis_barang').on('change', function() {
            getKomoditiByJenisBarang('formFilterMap', this.value);
        });

        $('#formFilterMap #filter_id_kecamatan').on('change', function() {
            getKelurahanByKecamatan('formFilterMap', this.value);
        });

        $('#formFilterMap #btnFilter').on('click', () => {
            const jenis_barang = $('#formFilterMap #filter_jenis_barang').val();
            const id_jenis_barang = $('#formFilterMap #filter_id_jenis_barang').val();
            const id_kecamatan = $('#formFilterMap #filter_id_kecamatan').val();
            const id_kelurahan = $('#formFilterMap #filter_id_kelurahan').val();

            const data = {
                map: true,
                jenis_barang,
                id_jenis_barang,
                id_kecamatan,
                id_kelurahan,
            };

            showLoader();

            showMap(data);
        });
    </script>
@endpush