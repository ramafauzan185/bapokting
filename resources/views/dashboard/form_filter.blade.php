<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 04/10/2021
 * Time: 14:55
 */

?>

<div class="form-row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="jenis_barang">Jenis Barang</label>
            <select class="form-control @error('jenis_barang') is-invalid @enderror" id="filter_jenis_barang" name="jenis_barang">
                <option value="">All</option>
                @foreach(jenis_barangs() as $k=>$v)
                    <option value="{{ $k }}" {{ (isset($bidang_usaha) && $bidang_usaha->jenis_barang == $k || old('jenis_barang') == $k)? 'selected' : '' }}>{{ $v }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <label for="filter_id_jenis_barang">Komoditi</label>
            <select class="form-control @error('id_jenis_barang') is-invalid @enderror" id="filter_id_jenis_barang" name="id_jenis_barang" disabled>
                <option value="">All</option>
            </select>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <label for="filter_id_kecamatan">Kecamatan</label>
            <select class="form-control @error('id_kecamatan') is-invalid @enderror" id="filter_id_kecamatan" name="id_kecamatan">
                <option value="">All</option>
                @foreach($kecamatan as $row)
                    <option value="{{ $row->id }}">{{ $row->nama_kecamatan }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <label for="filter_id_kelurahan">Kelurahan</label>
            <select class="form-control @error('id_kelurahan') is-invalid @enderror" id="filter_id_kelurahan" name="id_kelurahan" disabled>
                <option value="">All</option>
            </select>
        </div>
    </div>

    <div class="col-md-2" style="padding-top: 30px;">
        {!! button_filter(true, 'btnFilter') !!}
    </div>
</div>

@push('js')
    <script>
        function getKomoditiByJenisBarang(id, jenis_barang) {
            $.ajax({
                data: { jenis_barang: jenis_barang },
                url: '<?=route('jenis-barang.get-komoditi-by-jenis-barang');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const komoditiForm = $(`#${id} #filter_id_jenis_barang`);
                    komoditiForm.prop('disabled', false);
                    komoditiForm.html('');

                    komoditiForm.append(`<option value="">All</option>`);
                    data.jenis_barang.forEach((item) => {
                        komoditiForm.append(`<option value="${item.id}">${item.nama_barang}</option>`);
                    });
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function getKelurahanByKecamatan(id, id_kecamatan) {
            $.ajax({
                data: { id_kecamatan: id_kecamatan },
                url: '<?=route('kelurahan.by-kecamatan');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const kelurahanForm = $(`#${id} #filter_id_kelurahan`);
                    kelurahanForm.prop('disabled', false);
                    kelurahanForm.html('');

                    kelurahanForm.append(`<option value="">All</option>`);
                    data.kelurahan.forEach((item) => {
                        kelurahanForm.append(`<option value="${item.id}">${item.nama_kelurahan}</option>`);
                    })
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush