<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 17/09/2021
 * Time: 10:22
 */

$id_jenis_barang = '';

?>

<div class="card card-outline card-blue">
    <div class="card-header">{!! __('Skala Usaha Pedagang <small>Per komoditi</small>') !!}</div>

    <div class="card-body" style="overflow: auto;">
        <div class="form-group row">
            <label for="id_jenis_barang" class="col-sm-4 col-form-label">Pilih Komoditi</label>
            <div class="col-sm-8">
                <select class="form-control @error('id_jenis_barang') is-invalid @enderror" id="id_jenis_barang" name="id_jenis_barang" onchange="getPerusahaanByIdJenisBarangStatusUsaha(this.value)">
                    @foreach($jenis_barang as $key => $row)
                        @if($key == 0) @php $id_jenis_barang = $row->id @endphp @endif
                        <option value="{{ $row->id }}">{{ $row->nama_barang }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div style="width: 100%; height: 400px; display: flex; align-content: center;">
            <div id="chartStatusUsahaPerKomoditi" style="width: 100%; height: 100%;"></div>
        </div>
    </div>
</div>

<x-adminlte-modal id="modalGrafikStatusUsahaPerKomoditi" title="Grafik Skala Usaha Per Komoditi" size="lg" theme="light" v-centered static-backdrop scrollable>
    <div style="height: auto;" id="modalGrafikStatusUsahaPerKomoditiContent">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped">
                    <tr>
                        <th width="150px" class="bg-light">Kode KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailKodeKbli"></span></td>
                    </tr>
                    <tr>
                        <th width="150px" class="bg-light">Nama KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailNamaKbli"></span></td>
                    </tr>
                    <tr>
                        <th class="bg-light">Jenis Barang</th>
                        <th class="bg-light">:</th>
                        <td><span id="detailNamaBarang"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped simpleDatatable">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nomor Legalitas Usaha</th>
                        <th>Nama Perusahaan</th>
                        <th>Alamat Perusahaan</th>
                        <th>Kecamatan</th>
                        <th>Nomor Telepon</th>
                        <th>Lokasi Usaha</th>
                        <th>Skala Usaha</th>
                        <th>Pemilik</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@push('js')
    <script>
        am4core.ready(function() {
            getPerusahaanByIdJenisBarangStatusUsaha({{ $id_jenis_barang }});
        });

        function getPerusahaanByIdJenisBarangStatusUsaha(id_jenis_barang) {
            $.ajax({
                data: { id_jenis_barang: id_jenis_barang },
                url: '<?=route('home.grafik-skala-usaha-per-komoditi');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const skala_usaha_per_komoditi = data.skala_usaha_per_komoditi;

                    showGrafikStatusUsahaPerKomoditi(skala_usaha_per_komoditi);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function showGrafikStatusUsahaPerKomoditi(data) {
            am4core.useTheme(am4themes_animated);

            // Create chart instance
            var chartStatusUsahaPerKomoditi = am4core.create("chartStatusUsahaPerKomoditi", am4charts.PieChart);

            chartStatusUsahaPerKomoditi.exporting.menu = new am4core.ExportMenu();
            chartStatusUsahaPerKomoditi.exporting.menu.items = [{
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "pdf", "label": "PDF" },
                    { "type": "print", "label": "Print", },
                ]
            }];

            // Create pie series
            var series = chartStatusUsahaPerKomoditi.series.push(new am4charts.PieSeries());
            series.dataFields.value = "jum";
            series.dataFields.category = "skala_usaha";
            series.dataFields.id_jenis_barang = "id_jenis_barang";

            series.labels.template.text = "{category}:\n{value}";
            series.slices.template.tooltipText = "{category}\n{value} pedagang";
            // series.legendSettings.valueText = "{value}";

            series.labels.template.maxWidth = 100;
            series.labels.template.wrap = true;
            series.labels.template.fontSize = 11;

            // Add data
            chartStatusUsahaPerKomoditi.data = data;

            // And, for a good measure, let's add a legend
            chartStatusUsahaPerKomoditi.legend = new am4charts.Legend();

            chartStatusUsahaPerKomoditi.legend.maxHeight = 100;
            chartStatusUsahaPerKomoditi.legend.scrollable = true;
            chartStatusUsahaPerKomoditi.legend.labels.template.text = "[bold {color}]{category}";

            series.slices.template.events.on("hit", function(ev) {
                let seriesItem = ev.target.dataItem.component;
                seriesItem.slices.each(function(item) {
                    if (item.isActive && item != ev.target) {
                        item.isActive = false;
                    }
                    if (!item.isActive && item == ev.target) {
                        item.isActive = true;
                    }

                    if(item == ev.target) {
                        showModalStatusUsahaPerKomoditiDetail(ev.target.dataItem.id_jenis_barang, ev.target.dataItem.category);
                    }
                });
            });
        }

        function showModalStatusUsahaPerKomoditiDetail(id_jenis_barang, skala_usaha) {
            const modal = $('#modalGrafikStatusUsahaPerKomoditi');
            const table = $('#modalGrafikStatusUsahaPerKomoditiContent .simpleDatatable tbody');
            const detailNamaBarang = $('#modalGrafikStatusUsahaPerKomoditiContent #detailNamaBarang');
            const detailKodeKbli = $('#modalGrafikStatusUsahaPerKomoditiContent #detailKodeKbli');
            const detailNamaKbli = $('#modalGrafikStatusUsahaPerKomoditiContent #detailNamaKbli');

            $.ajax({
                data: { id_jenis_barang: id_jenis_barang, skala_usaha: skala_usaha },
                url: '<?=route('home.grafik-skala-usaha-per-komoditi-detail');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const skala_usaha_per_komoditi = data.skala_usaha_per_komoditi;
                    const kode_kbli = data.kode_kbli;
                    const nama_kbli = data.nama_kbli;
                    const nama_barang = data.nama_barang;
                    const jenis_barang = data.jenis_barang;

                    detailKodeKbli.html(kode_kbli);
                    detailNamaKbli.html(nama_kbli);
                    detailNamaBarang.html(nama_barang + ' - ' + jenis_barang);

                    let tableBody = '';
                    skala_usaha_per_komoditi.forEach((item, index) => {
                        tableBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${item.nomor_legalitas_usaha}</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.alamat_perusahaan}</td>
                                <td>${item.nama_kecamatan}</td>
                                <td>${item.nomor_telepon}</td>
                                <td>${item.lokasi_usaha.replaceAll('_', ' ')}</td>
                                <td>${item.skala_usaha.replaceAll('_', ' ')}</td>
                                <td>${item.nama_pemilik}</td>
                            </tr>
                        `;
                    });

                    table.html('');
                    table.append(tableBody);

                    simpleDatatable();

                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush