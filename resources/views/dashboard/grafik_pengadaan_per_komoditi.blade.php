<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 16/09/2021
 * Time: 11:57
 */

?>

<div class="card card-outline card-blue">
    <div class="card-header">{!! __('Pengadaan Per Komoditi <small>Di Tahun '.$tahun.'</small>') !!}</div>

    <div class="card-body" style="overflow: auto;">
        <div style="width: 100%; height: 400px; display: flex; align-content: center;">
            <div id="chartKomoditiPengadaanPertahun" style="width: 100%; height: 100%;"></div>
        </div>
    </div>
</div>

<x-adminlte-modal id="modalGrafikPengadaanPerKomoditi" title="Grafik Pengadaan Per Komoditi" size="lg" theme="light" v-centered static-backdrop scrollable>
    <div style="height: auto;" id="modalGrafikPengadaanPerKomoditiContent">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped">
                    <tr>
                        <th width="150px" class="bg-light">Tahun</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailTahun"></span></td>

                        <th width="150px" class="bg-light">Kode KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailKodeKbli"></span></td>
                    </tr>
                    <tr>
                        <th width="150px" class="bg-light">Nama KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td colspan="4"><span id="detailNamaKbli"></span></td>
                    </tr>
                    <tr>
                        <th class="bg-light">Jenis Barang</th>
                        <th class="bg-light">:</th>
                        <td colspan="4"><span id="detailNamaBarang"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped simpleDatatable">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Bulan</th>
                        <th>Nama Perusahaan</th>
                        <th>Skala Usaha</th>
                        <th>Volume</th>
                        <th>Sumber Pembelian</th>
                        <th>Lokasi Pembelian</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@push('js')
    <script>
        am4core.ready(function() {
            am4core.useTheme(am4themes_animated);

            // Create chart instance
            var chartKomoditiPengadaanPertahun = am4core.create("chartKomoditiPengadaanPertahun", am4charts.PieChart);

            chartKomoditiPengadaanPertahun.exporting.menu = new am4core.ExportMenu();
            chartKomoditiPengadaanPertahun.exporting.menu.items = [{
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "pdf", "label": "PDF" },
                    { "type": "print", "label": "Print", },
                ]
            }];

            // Create pie series
            var series = chartKomoditiPengadaanPertahun.series.push(new am4charts.PieSeries());
            series.dataFields.value = "jum";
            series.dataFields.category = "nama_barang";
            series.dataFields.id_jenis_barang = "id_jenis_barang";
            series.dataFields.satuan = "satuan";

            series.labels.template.text = "{category}:\n{value} {satuan}";
            series.slices.template.tooltipText = "{category}\n{value} {satuan}";
            series.legendSettings.valueText = "{value} {satuan}";

            series.labels.template.maxWidth = 100;
            series.labels.template.wrap = true;
            series.labels.template.fontSize = 11;

            // Add data
            chartKomoditiPengadaanPertahun.data = <?php if(empty($pengadaan_get_per_komoditi_per_tahun)){ ?> [] <?php } else { echo json_encode($pengadaan_get_per_komoditi_per_tahun); } ?>;

            // And, for a good measure, let's add a legend
            chartKomoditiPengadaanPertahun.legend = new am4charts.Legend();

            chartKomoditiPengadaanPertahun.legend.maxHeight = 100;
            chartKomoditiPengadaanPertahun.legend.scrollable = true;
            chartKomoditiPengadaanPertahun.legend.labels.template.text = "[bold {color}]{category}";

            /*chartKomoditiPengadaanPertahun.legend.itemContainers.template.events.on("hit", function(ev) {
                alert("Clicked on" + ev.target);
            });*/
            series.slices.template.events.on("hit", function(ev) {
                let seriesItem = ev.target.dataItem.component;
                seriesItem.slices.each(function(item) {
                    if (item.isActive && item != ev.target) {
                        item.isActive = false;
                    }
                    if (!item.isActive && item == ev.target) {
                        item.isActive = true;
                    }

                    if(item == ev.target) {
                        showGrafikPengadaanPerKomoditiDetail(ev.target.dataItem.id_jenis_barang);
                    }
                });
            });
        });

        function showGrafikPengadaanPerKomoditiDetail(id_jenis_barang) {
            const modal = $('#modalGrafikPengadaanPerKomoditi');
            const table = $('#modalGrafikPengadaanPerKomoditiContent .simpleDatatable tbody');
            const detailNamaBarang = $('#modalGrafikPengadaanPerKomoditiContent #detailNamaBarang');
            const detailTahun = $('#modalGrafikPengadaanPerKomoditiContent #detailTahun');
            const detailKodeKbli = $('#modalGrafikPengadaanPerKomoditiContent #detailKodeKbli');
            const detailNamaKbli = $('#modalGrafikPengadaanPerKomoditiContent #detailNamaKbli');

            $.ajax({
                data: { id_jenis_barang: id_jenis_barang, tahun: '<?=$tahun;?>' },
                url: '<?=route('home.grafik-pengadaan-per-komoditi-detail');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const pengadaan_get_per_komoditi_per_tahun = data.pengadaan_get_per_komoditi_per_tahun;
                    const kode_kbli = data.kode_kbli;
                    const nama_kbli = data.nama_kbli;
                    const nama_barang = data.nama_barang;
                    const jenis_barang = data.jenis_barang;
                    const tahun = data.tahun;

                    detailTahun.html(tahun);
                    detailKodeKbli.html(kode_kbli);
                    detailNamaKbli.html(nama_kbli);
                    detailNamaBarang.html(nama_barang + ' - ' + jenis_barang);

                    let tableBody;
                    pengadaan_get_per_komoditi_per_tahun.forEach((item, index) => {
                        tableBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${bulans()[item.bulan]}</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.skala_usaha.replaceAll('_', ' ')}</td>
                                <td>${item.volume} ${item.satuan}</td>
                                <td>${item.nama_sumber_pembelian}</td>
                                <td>${item.nama_lokasi_pembelian}</td>
                            </tr>
                        `;
                    });

                    table.html('');
                    table.append(tableBody);

                    simpleDatatable();

                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush