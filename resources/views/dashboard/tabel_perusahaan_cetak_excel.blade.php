<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 18/10/2021
 * Time: 15:21
 */

?>

<table class="table table-borderless mt-3 mb-3">
    <tr>
        <th colspan="14" class="text-center" style="font-size: 22px">
            Laporan Pelaku Usaha Distribusi Bahan Pokok dan Kebutuhan Penting
            <br>
            Kota Bogor
        </th>
    </tr>
</table>

<table class="table table-bordered">
    <tr class="bg-light">
        <th class="text-center" style="vertical-align: middle;">No</th>
        <th class="text-center" style="vertical-align: middle;">Nama Usaha</th>
        <th class="text-center" style="vertical-align: middle;">No TDPUD</th>
        <th class="text-center" style="vertical-align: middle;">Alamat Usaha</th>
        <th class="text-center" style="vertical-align: middle;">Kelurahan</th>
        <th class="text-center" style="vertical-align: middle;">Kecamatan</th>
        <th class="text-center" style="vertical-align: middle;">Lama Usaha<br>(Tahun)</th>
        <th class="text-center" style="vertical-align: middle;">Lokasi Usaha</th>
        <th class="text-center" style="vertical-align: middle;">Status Usaha</th>
        <th class="text-center" style="vertical-align: middle;">Status Toko</th>
        <th class="text-center" style="vertical-align: middle;">Skala Usaha</th>
        <th class="text-center" style="vertical-align: middle;">Luas Gudang<br>(m<sup>2</sup>)</th>
        <th class="text-center" style="vertical-align: middle;">Komoditas</th>
        <th class="text-center" style="vertical-align: middle;">Koordinat</th>
    </tr>
    <tr style="font-size: 11px;">
        <th class="text-center">(1)</th>
        <th class="text-center">(2)</th>
        <th class="text-center">(3)</th>
        <th class="text-center">(4)</th>
        <th class="text-center">(5)</th>
        <th class="text-center">(6)</th>
        <th class="text-center">(7)</th>
        <th class="text-center">(8)</th>
        <th class="text-center">(9)</th>
        <th class="text-center">(10)</th>
        <th class="text-center">(11)</th>
        <th class="text-center">(12)</th>
        <th class="text-center">(13)</th>
        <th class="text-center">(14)</th>
    </tr>
    @foreach($perusahaan as $index => $row)
        <tr>
            <td class="text-center">{{ $index+1 }}.</td>
            <td>{{ $row->nama_perusahaan }}</td>
            <td>{{ $row->nomor_legalitas_usaha }}</td>
            <td>{{ $row->alamat_perusahaan }}</td>
            <td>{{ $row->nama_kelurahan }}</td>
            <td>{{ $row->nama_kecamatan }}</td>
            <td class="text-center">{{ $row->lama_usaha }}</td>
            <td>{{ ucfirst(str_replace('_', ' ', $row->lokasi_usaha)) }}</td>
            <td>{{ ucfirst(str_replace('_', ' ', $row->status_usaha)) }}</td>
            <td>{{ ucfirst(str_replace('_', ' ', $row->status_toko)) }}</td>
            <td>{{ ucfirst(str_replace('_', ' ', $row->skala_usaha)) }}</td>
            <td>{{ $row->luas_gudang }}</td>
            <td>{{ trim($row->nama_barangs) }}</td>
            <td>{{ round($row->latitude, 6) }},<br>{{ round($row->longitude, 6) }}</td>
        </tr>
    @endforeach
</table>
