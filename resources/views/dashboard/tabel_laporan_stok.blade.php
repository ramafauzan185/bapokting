<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 07/10/2021
 * Time: 14:58
 */

$columns = 7;
?>

@if($is_distributor)
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Info!</strong>
                <br>
                Entri Data Pelaporan Pelaku Usaha Distribusi, klik <a href="{{ route('distribusi-bapokting.create') }}" class="btn btn-sm btn-info"><i class="fa fa-plus-square"></i> Tambah</a>
                <br>
                <small>*Data diisi oleh Pelaku Usaha Distribusi yang memiliki TDPUD.</small>
            </div>
        </div>
    </div>
@endif

<div class="card">
    <div class="card-header">
        {!! __('Pelaporan Stok Barang Kebutuhan Pokok dan Barang Penting') !!}

        @if(!$is_distributor)
            <a href="{{ route('home.tabel-stok-barang') }}" class="btn btn-success btn-xs float-right" data-toggle="tooltip" data-placement="bottom" title="Cetak laporan stok distribusi bapokting" onclick="printDataStokBarang(event, this)">
                <i class="fa fa-print"></i> &nbsp;Print
            </a>

            <a href="{{ route('home.tabel-stok-barang') }}" class="btn btn-success btn-xs float-right mr-1" data-toggle="tooltip" data-placement="bottom" title="Cetak excel laporan stok distribusi bapokting" onclick="printDataStokBarang(event, this, true)">
                <i class="fa fa-file-excel"></i> &nbsp;Excel
            </a>
        @endif
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-stripped simpleDatatable2" id="tabelStokBarang">
                        <thead>
                        <tr>
                            <th rowspan="2" class="text-center" style="vertical-align: middle;">No.</th>
                            @if($is_distributor)
                                @php $columns += 1; @endphp
                                <th rowspan="2" class="text-center" style="vertical-align: middle;">Jenis Barang</th>
                            @endif
                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Tahun</th>
                            <th rowspan="2" class="text-center" style="vertical-align: middle;">Bulan</th>
                            <th class="text-center">Stok Awal</th>
                            <th class="text-center">Pengadaan</th>
                            <th class="text-center">Penyaluran</th>
                            <th class="text-center">Stok Akhir</th>
                        </tr>
                        <tr>
                            <th class="text-center">Jumlah</th>
                            <th class="text-center">Jumlah</th>
                            <th class="text-center">Jumlah</th>
                            <th class="text-center">Jumlah</th>
                        </tr>
                        <tr style="font-size: 10px;">
                            <?php
                                for($i = 0; $i < $columns; $i++) {
                                    $no = $i + 1;
                                    $noAdd = '';
                                    if($no == $columns) {
                                        ?>
                                        <th class="text-center">({{ $no }}) = ({{ $no - 3 }}) + ({{ $no - 2 }}) - ({{ $no - 1 }})</th>
                                        <?php
                                    } else {
                                        ?>
                                        <th class="text-center">({{ $no }})</th>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                }
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($laporan_stok)) {
                            $stok_awal = 0;
                            $id = 0;
                            foreach($laporan_stok as $k=>$row) {
                                $jumlah_pengadaan = $row->jumlah_pengadaan;
                                $jumlah_pemasaran = $row->jumlah_pemasaran;

                                if($k > 0 && $id != $row->id) {
                                    $stok_awal = 0;
                                }

                                $stok_akhir = $stok_awal + $jumlah_pengadaan - $jumlah_pemasaran;
                        ?>
                        <tr>
                            <td class="text-center">{{ $k+1 }}.</td>
                            @if($is_distributor)
                                <td class="text-center">{{ $row->nama_barang }}</td>
                            @endif
                            <td class="text-center">{{ $row->tahun }}</td>
                            <td class="text-center">{{ bulans()[$row->bulan] }}</td>
                            <td class="text-center">{{ $stok_awal }}</td>
                            <td class="text-center">{{ $jumlah_pengadaan }}</td>
                            <td class="text-center">{{ $jumlah_pemasaran }}</td>
                            <td class="text-center">{{ $stok_akhir }}</td>
                        </tr>
                        <?php
                                $id = $row->id;
                                $stok_awal = $stok_akhir;
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="{{ $columns }}">Data tidak ditemukan..</td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        function printDataStokBarang(event, el, excel = false) {
            event.preventDefault();

            const id_jenis_barang = $('#formFilterTabel #filter_id_jenis_barang').val();
            const urlQuery = `?id_jenis_barang=${id_jenis_barang}&cetak=true&xls=${excel}`;

            const url = $(el).attr('href') + urlQuery;

            window.open(url, '_blank').focus();
        }
    </script>
@endpush
