<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 07/11/2021
 * Time: 20:34
 */

?>

<table class="table table-borderless mt-3 mb-3">
    <tr>
        <th colspan="14" class="text-center" style="font-size: 22px">
            Laporan Stok Distribusi Bahan Pokok dan Kebutuhan Penting
            <br>
            Kota Bogor
        </th>
    </tr>
</table>

<table class="table table-bordered">
    <tr class="bg-light">
        <th rowspan="2" class="text-center" style="vertical-align: middle;">No.</th>
        <th rowspan="2" class="text-center" style="vertical-align: middle;">Tahun</th>
        <th rowspan="2" class="text-center" style="vertical-align: middle;">Bulan</th>
        <th class="text-center">Stok Awal</th>
        <th class="text-center">Pengadaan</th>
        <th class="text-center">Penyaluran</th>
        <th class="text-center">Stok Akhir</th>
    </tr>
    <tr class="bg-light">
        <th class="text-center">Jumlah</th>
        <th class="text-center">Jumlah</th>
        <th class="text-center">Jumlah</th>
        <th class="text-center">Jumlah</th>
    </tr>
    <tr style="font-size: 11px;">
        <th class="text-center">(1)</th>
        <th class="text-center">(2)</th>
        <th class="text-center">(3)</th>
        <th class="text-center">(4)</th>
        <th class="text-center">(5)</th>
        <th class="text-center">(6)</th>
        <th class="text-center">(7) = (4) + (5) - (6)</th>
    </tr>
    <?php
    if(!empty($laporan_stok)) {
        $stok_awal = 0;
        $id = 0;
        foreach($laporan_stok as $k=>$row) {
            $jumlah_pengadaan = $row->jumlah_pengadaan;
            $jumlah_pemasaran = $row->jumlah_pemasaran;

            if($k > 0 && $id != $row->id) {
                $stok_awal = 0;
            }

            $stok_akhir = $stok_awal + $jumlah_pengadaan - $jumlah_pemasaran;
    ?>
            <tr>
                <td class="text-center">{{ $k+1 }}.</td>
                <td class="text-center">{{ $row->tahun }}</td>
                <td class="text-center">{{ bulans()[$row->bulan] }}</td>
                <td class="text-center">{{ $stok_awal }}</td>
                <td class="text-center">{{ $jumlah_pengadaan }}</td>
                <td class="text-center">{{ $jumlah_pemasaran }}</td>
                <td class="text-center">{{ $stok_akhir }}</td>
            </tr>
    <?php
            $id = $row->id;
            $stok_awal = $stok_akhir;
        }
    } else {
    ?>
    <tr>
        <td colspan="7">Data tidak ditemukan..</td>
    </tr>
    <?php
    }
    ?>
</table>
