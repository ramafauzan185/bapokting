<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 17/09/2021
 * Time: 14:35
 */

$id_jenis_barang = '';

?>

<div class="card card-outline card-blue">
    <div class="card-header">{!! __('Aktivitas Pengadaan Per Komoditi <small>Berdasarkan sumber pembelian</small>') !!}</div>

    <div class="card-body" style="overflow: auto;">
        <div class="form-group row">
            <label for="id_jenis_barang" class="col-sm-4 col-form-label">Pilih Komoditi</label>
            <div class="col-sm-8">
                <select class="form-control @error('id_jenis_barang') is-invalid @enderror" id="id_jenis_barang" name="id_jenis_barang" onchange="getPengadaanByIdJenisBarangSumberPembelian(this.value)">
                    @foreach($jenis_barang as $key => $row)
                        @if($key == 0) @php $id_jenis_barang = $row->id @endphp @endif
                        <option value="{{ $row->id }}">{{ $row->nama_barang }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div style="width: 100%; height: 400px; display: flex; align-content: center;">
            <div id="chartSumberPembelianPerKomoditi" style="width: 100%; height: 100%;"></div>
        </div>
    </div>
</div>

<x-adminlte-modal id="modalGrafikPengadaanBySumberPembelianPerKomoditi" title="Grafik Aktivitas Pengadaan Per Komoditi" size="lg" theme="light" v-centered static-backdrop scrollable>
    <div style="height: auto;" id="modalGrafikPengadaanBySumberPembelianPerKomoditiContent">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped">
                    <tr>
                        <th width="150px" class="bg-light">Kode KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailKodeKbli"></span></td>
                    </tr>
                    <tr>
                        <th width="150px" class="bg-light">Nama KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailNamaKbli"></span></td>
                    </tr>
                    <tr>
                        <th class="bg-light">Jenis Barang</th>
                        <th class="bg-light">:</th>
                        <td><span id="detailNamaBarang"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped simpleDatatable">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tahun</th>
                        <th>Bulan</th>
                        <th>Nomor Legalitas Usaha</th>
                        <th>Nama Perusahaan</th>
                        <th>Volume</th>
                        <th>Sumber Pembelian</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@push('js')
    <script>
        am4core.ready(function() {
            getPengadaanByIdJenisBarangSumberPembelian({{ $id_jenis_barang }});
        });

        function getPengadaanByIdJenisBarangSumberPembelian(id_jenis_barang) {
            $.ajax({
                data: { id_jenis_barang: id_jenis_barang },
                url: '<?=route('home.grafik-pengadaan-by-sumber-pembelian-per-komoditi');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const pengadaan_get_per_komoditi_by_sumber_pembelian = data.pengadaan_get_per_komoditi_by_sumber_pembelian;

                    showGrafikPengadaanBySumberPembelianPerKomoditi(pengadaan_get_per_komoditi_by_sumber_pembelian);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function showGrafikPengadaanBySumberPembelianPerKomoditi(data) {
            am4core.useTheme(am4themes_animated);

            // Create chart instance
            var chartSumberPembelianPerKomoditi = am4core.create("chartSumberPembelianPerKomoditi", am4charts.PieChart);

            chartSumberPembelianPerKomoditi.exporting.menu = new am4core.ExportMenu();
            chartSumberPembelianPerKomoditi.exporting.menu.items = [{
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "pdf", "label": "PDF" },
                    { "type": "print", "label": "Print", },
                ]
            }];

            // Create pie series
            var series = chartSumberPembelianPerKomoditi.series.push(new am4charts.PieSeries());
            series.dataFields.value = "jum";
            series.dataFields.category = "sumber_pembelian";
            series.dataFields.id_jenis_barang = "id_jenis_barang";
            series.dataFields.id_sumber_pembelian = "id_sumber_pembelian";

            series.labels.template.text = "{category}:\n{value}";
            series.slices.template.tooltipText = "{category}\n{value}";
            // series.legendSettings.valueText = "{value}";

            series.labels.template.maxWidth = 100;
            series.labels.template.wrap = true;
            series.labels.template.fontSize = 11;

            // Add data
            chartSumberPembelianPerKomoditi.data = data;

            // And, for a good measure, let's add a legend
            chartSumberPembelianPerKomoditi.legend = new am4charts.Legend();

            chartSumberPembelianPerKomoditi.legend.maxHeight = 100;
            chartSumberPembelianPerKomoditi.legend.scrollable = true;
            chartSumberPembelianPerKomoditi.legend.labels.template.text = "[bold {color}]{category}";
            chartSumberPembelianPerKomoditi.legend.labels.template.fontSize = 12;

            series.slices.template.events.on("hit", function(ev) {
                let seriesItem = ev.target.dataItem.component;
                seriesItem.slices.each(function(item) {
                    if (item.isActive && item != ev.target) {
                        item.isActive = false;
                    }
                    if (!item.isActive && item == ev.target) {
                        item.isActive = true;
                    }

                    if(item == ev.target) {
                        showModalPengadaanBySumberPembelianPerKomoditiDetail(ev.target.dataItem.id_jenis_barang, ev.target.dataItem.id_sumber_pembelian);
                    }
                });
            });
        }

        function showModalPengadaanBySumberPembelianPerKomoditiDetail(id_jenis_barang, id_sumber_pembelian) {
            const modal = $('#modalGrafikPengadaanBySumberPembelianPerKomoditi');
            const table = $('#modalGrafikPengadaanBySumberPembelianPerKomoditiContent .simpleDatatable tbody');
            const detailNamaBarang = $('#modalGrafikPengadaanBySumberPembelianPerKomoditiContent #detailNamaBarang');
            const detailKodeKbli = $('#modalGrafikPengadaanBySumberPembelianPerKomoditiContent #detailKodeKbli');
            const detailNamaKbli = $('#modalGrafikPengadaanBySumberPembelianPerKomoditiContent #detailNamaKbli');

            $.ajax({
                data: { id_jenis_barang: id_jenis_barang, id_sumber_pembelian: id_sumber_pembelian },
                url: '<?=route('home.grafik-pengadaan-by-sumber-pembelian-per-komoditi-detail');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const pengadaan_per_komoditi_by_sumber_pembelian = data.pengadaan_per_komoditi_by_sumber_pembelian;
                    const kode_kbli = data.kode_kbli;
                    const nama_kbli = data.nama_kbli;
                    const nama_barang = data.nama_barang;
                    const jenis_barang = data.jenis_barang;

                    detailKodeKbli.html(kode_kbli);
                    detailNamaKbli.html(nama_kbli);
                    detailNamaBarang.html(nama_barang + ' - ' + jenis_barang);

                    let tableBody = '';
                    pengadaan_per_komoditi_by_sumber_pembelian.forEach((item, index) => {
                        tableBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${item.tahun}</td>
                                <td>${bulans()[item.bulan]}</td>
                                <td>${item.nomor_legalitas_usaha}</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.volume} ${item.satuan}</td>
                                <td>${item.nama_sumber_pembelian}</td>
                            </tr>
                        `;
                    });

                    table.html('');
                    table.append(tableBody);

                    simpleDatatable();

                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush