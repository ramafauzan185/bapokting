<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 16/09/2021
 * Time: 16:51
 */

?>

<div class="card card-outline card-blue">
    <div class="card-header">{!! __('Rata-rata lama usaha pedagang <small>Berdasarkan jenis produk yang dijual</small>') !!}</div>

    <div class="card-body" style="overflow: auto;">
        <div id="chartRerataLamaUsaha" style="width: 100%; height: 400px;"></div>
    </div>
</div>

<x-adminlte-modal id="modalGrafikRerataLamaUsahaPedagangByJenisProduk" title="Grafik Lama Usaha Pedagang Per Jenis Produk" size="lg" theme="light" v-centered static-backdrop scrollable>
    <div style="height: auto;" id="modalGrafikRerataLamaUsahaPedagangByJenisProdukContent">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped">
                    <tr>
                        <th width="150px" class="bg-light">Kode KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailKodeKbli"></span></td>
                    </tr>
                    <tr>
                        <th width="150px" class="bg-light">Nama KBLI</th>
                        <th width="10px" class="bg-light">:</th>
                        <td><span id="detailNamaKbli"></span></td>
                    </tr>
                    <tr>
                        <th class="bg-light">Jenis Barang</th>
                        <th class="bg-light">:</th>
                        <td><span id="detailNamaBarang"></span></td>
                    </tr>
                </table>
            </div>
            <div class="col-12">
                <table class="table table-hover table-bordered table-stripped simpleDatatable">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nomor Legalitas Usaha</th>
                        <th>Nama Perusahaan</th>
                        <th>NIK</th>
                        <th>Pemilik</th>
                        <th>Jenis Kelamin</th>
                        <th>Alamat</th>
                        <th>Umur</th>
                        <th>Lama Usaha</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@push('js')
    <script>
        am4core.ready(function() {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chartRerataLamaUsaha = am4core.create("chartRerataLamaUsaha", am4charts.XYChart);
            chartRerataLamaUsaha.scrollbarX = new am4core.Scrollbar();

            chartRerataLamaUsaha.exporting.menu = new am4core.ExportMenu();
            chartRerataLamaUsaha.exporting.menu.align = "left";
            chartRerataLamaUsaha.exporting.menu.verticalAlign = "top";
            chartRerataLamaUsaha.exporting.menu.items = [{
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "pdf", "label": "PDF" },
                    { "type": "print", "label": "Print", },
                ]
            }];

            // Add data
            chartRerataLamaUsaha.data = <?php if(!empty($rerata_lama_usaha_per_komoditi)) { echo json_encode($rerata_lama_usaha_per_komoditi); } else { ?> [] <?php } ?>;

            // Create axes
            var categoryAxis = chartRerataLamaUsaha.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "nama_barang";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.renderer.labels.template.rotation = 340;
            // categoryAxis.tooltip.disabled = true;
            categoryAxis.renderer.minHeight = 110;

            categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                if (target.dataItem && target.dataItem.index & 2 == 2) {
                    return 0;//dy + 25;
                }
                return 0;//dy;
            });

            var valueAxis = chartRerataLamaUsaha.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.minWidth = 50;
            valueAxis.tooltip.disabled = true;

            // Create series
            var series = chartRerataLamaUsaha.series.push(new am4charts.ColumnSeries());
            series.sequencedInterpolation = true;
            series.dataFields.valueY = "lama_usaha";
            series.dataFields.categoryX = "nama_barang";
            series.dataFields.id_jenis_barang = "id_jenis_barang";
            series.tooltipText = "[{categoryX}: bold]{valueY} per tahun[/]";
            series.columns.template.strokeWidth = 0;

            series.tooltip.pointerOrientation = "vertical";

            series.columns.template.column.cornerRadiusTopLeft = 10;
            series.columns.template.column.cornerRadiusTopRight = 10;
            series.columns.template.column.fillOpacity = 0.8;

            // on hover, make corner radiuses bigger
            var hoverState = series.columns.template.column.states.create("hover");
            hoverState.properties.cornerRadiusTopLeft = 0;
            hoverState.properties.cornerRadiusTopRight = 0;
            hoverState.properties.fillOpacity = 1;

            series.columns.template.adapter.add("fill", function(fill, target) {
                return chartRerataLamaUsaha.colors.getIndex(target.dataItem.index);
            });

            // Cursor
            chartRerataLamaUsaha.cursor = new am4charts.XYCursor();

            series.columns.template.events.on("hit", function(ev) {
                showGrafikRerataLamaUsahaPedagangByJenisProdukDetail(ev.target.dataItem.id_jenis_barang);
            }, this);
        });

        function showGrafikRerataLamaUsahaPedagangByJenisProdukDetail(id_jenis_barang) {
            const modal = $('#modalGrafikRerataLamaUsahaPedagangByJenisProduk');
            const table = $('#modalGrafikRerataLamaUsahaPedagangByJenisProdukContent .simpleDatatable tbody');
            const detailNamaBarang = $('#modalGrafikRerataLamaUsahaPedagangByJenisProdukContent #detailNamaBarang');
            const detailKodeKbli = $('#modalGrafikRerataLamaUsahaPedagangByJenisProdukContent #detailKodeKbli');
            const detailNamaKbli = $('#modalGrafikRerataLamaUsahaPedagangByJenisProdukContent #detailNamaKbli');

            $.ajax({
                data: { id_jenis_barang: id_jenis_barang },
                url: '<?=route('home.grafik-rerata-lama-usaha-per-komoditi-detail');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const rerata_lama_usaha_per_komoditi = data.rerata_lama_usaha_per_komoditi;
                    const kode_kbli = data.kode_kbli;
                    const nama_kbli = data.nama_kbli;
                    const nama_barang = data.nama_barang;
                    const jenis_barang = data.jenis_barang;

                    detailKodeKbli.html(kode_kbli);
                    detailNamaKbli.html(nama_kbli);
                    detailNamaBarang.html(nama_barang + ' - ' + jenis_barang);

                    let tableBody;
                    rerata_lama_usaha_per_komoditi.forEach((item, index) => {
                        tableBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${item.nomor_legalitas_usaha}</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.nik}</td>
                                <td>${item.nama_pemilik}</td>
                                <td>${get_jenis_kelamin(item.jenis_kelamin)}</td>
                                <td>${item.alamat}</td>
                                <td>${item.umur} tahun</td>
                                <td>${item.lama_usaha} tahun</td>
                            </tr>
                        `;
                    });

                    table.html('');
                    table.append(tableBody);

                    simpleDatatable();

                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush