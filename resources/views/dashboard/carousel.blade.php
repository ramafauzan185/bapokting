<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 07/10/2021
 * Time: 15:00
 */

?>

<div class="carousel-wrapper">
    {{--<h2>&nbsp;</h2>--}}
    <div class="carousel">
        @foreach($slider as $row)
            <div class="carousel-item">
                <img src="{{ !empty($row->image)? asset('images/slider/'.$row->image) : $row->image_url }}">
                <div class="carousel-caption">
                    <h3>{{ !empty($row->title)? $row->title : '' }}</h3>
                    <p>{{ !empty($row->caption)? $row->caption : '' }}</p>
                </div>
            </div>
        @endforeach
    </div>
</div>

@push('js')
    <script>
        $(document).ready(function(){
            $('.carousel').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                centerMode: true,
                autoplay: true,
                autoplaySpeed: 2000,
            });
        });
    </script>
@endpush