<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 04/10/2021
 * Time: 14:27
 */

?>

<div class="row">
    <div class="col-12">
        <x-adminlte-card title="Tampilkan Data Tabel Berdasarkan" icon="fas fa-sm fa-filter" theme="lightblue" collapsible="{{ !isset($_GET['btnFilter'])? 'collapsed' : '' }}">
            <form action="" id="formFilterTabel" method="get">
                @include('dashboard/form_filter')
            </form>
        </x-adminlte-card>
    </div>
</div>

<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Tabel Pelaku Distribusi</a>
        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Tabel Stok Barang</a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="card">
            <div class="card-header">
                {!! __('Pelaporan Pelaku Usaha Distribusi Bahan Pokok dan Kebutuhan Penting Kota Bogor') !!}

                <a href="{{ route('home.tabel-perusahaan') }}" class="btn btn-success btn-xs float-right" data-toggle="tooltip" data-placement="bottom" title="Cetak laporan pelaku usaha distribusi bapokting" onclick="printDataPerusahaan(event, this)">
                    <i class="fa fa-print"></i> &nbsp;Print
                </a>

                <a href="{{ route('home.tabel-perusahaan') }}" class="btn btn-success btn-xs float-right mr-1" data-toggle="tooltip" data-placement="bottom" title="Cetak excel laporan pelaku usaha distribusi bapokting" onclick="printDataPerusahaan(event, this, true)">
                    <i class="fa fa-file-excel"></i> &nbsp;Excel
                </a>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered table-stripped simpleDatatable" id="tabelPerusahaan">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Usaha</th>
                                    <th>No. TDPUD</th>
                                    <th>Alamat Usaha</th>
                                    <th>Kelurahan</th>
                                    <th>Kecamatan</th>
                                    <th>Lama Usaha (Tahun)</th>
                                    <th>Lokasi Usaha</th>
                                    <th>Status Usaha</th>
                                    <th>Status Toko</th>
                                    <th>Skala Usaha</th>
                                    <th>Luas Gudang (m<sup>2</sup>)</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
        @include('dashboard/tabel_laporan_stok')
    </div>
</div>

@push('js')
    <script>
        function showDataPerusahaan(data) {
            $.ajax({
                data: data,
                url: '<?=route('home.tabel-perusahaan');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const tabelPerusahaan = $('#tabelPerusahaan tbody');
                    let tabelPerusahaanBody = '';

                    data.perusahaan.forEach((item, index) => {
                        tabelPerusahaanBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.nomor_legalitas_usaha}</td>
                                <td>${item.alamat_perusahaan}</td>
                                <td>${item.nama_kelurahan}</td>
                                <td>${item.nama_kecamatan}</td>
                                <td>${item.lama_usaha}</td>
                                <td>${item.lokasi_usaha}</td>
                                <td>-</td>
                                <td>${item.status_toko}</td>
                                <td>${item.skala_usaha}</td>
                                <td>${item.luas_gudang}</td>
                            </tr>
                        `;
                    });

                    tabelPerusahaan.html('');
                    tabelPerusahaan.append(tabelPerusahaanBody);

                    simpleDatatable();

                    hideLoader();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function showLaporanStok(data) {
            $.ajax({
                data: data,
                url: '<?=route('home.tabel-stok-barang');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const bulans = data.bulans;
                    const tabelStokBarang = $('#tabelStokBarang tbody');
                    let tabelStokBarangBody = '';

                    let stok_awal = 0;
                    let id = 0;

                    if(data.laporan_stok.length > 0) {
                        data.laporan_stok.forEach((item, index) => {
                            const jumlah_pengadaan = parseInt(item.jumlah_pengadaan);
                            const jumlah_pemasaran = parseInt(item.jumlah_pemasaran);

                            if(index > 0 && id !== item.id) {
                                stok_awal = 0;
                            }

                            const stok_akhir = stok_awal + jumlah_pengadaan - jumlah_pemasaran;

                            tabelStokBarangBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${item.tahun}</td>
                                <td>${bulans[item.bulan]}</td>
                                <td>${stok_awal}</td>
                                <td>${jumlah_pengadaan}</td>
                                <td>${jumlah_pemasaran}</td>
                                <td>${stok_akhir}</td>
                            </tr>
                        `;

                            id = item.id;
                            stok_awal = stok_akhir;
                        });
                    } else {
                        tabelStokBarangBody += '<tr><td colspan="7">Data tidak ditemukan..</td></tr>';
                    }

                    tabelStokBarang.html('');
                    tabelStokBarang.append(tabelStokBarangBody);

                    hideLoader();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        showDataPerusahaan({});

        $('#formFilterTabel #filter_jenis_barang').on('change', function() {
            getKomoditiByJenisBarang('formFilterTabel', this.value);
        });

        $('#formFilterTabel #filter_id_kecamatan').on('change', function() {
            getKelurahanByKecamatan('formFilterTabel', this.value);
        });

        $('#formFilterTabel #btnFilter').on('click', () => {
            const jenis_barang = $('#formFilterTabel #filter_jenis_barang').val();
            const id_jenis_barang = $('#formFilterTabel #filter_id_jenis_barang').val();
            const id_kecamatan = $('#formFilterTabel #filter_id_kecamatan').val();
            const id_kelurahan = $('#formFilterTabel #filter_id_kelurahan').val();

            const data = {
                jenis_barang,
                id_jenis_barang,
                id_kecamatan,
                id_kelurahan,
            };

            showLoader();

            showDataPerusahaan(data);

            showLaporanStok(data);
        });

        function printDataPerusahaan(event, el, excel = false) {
            event.preventDefault();

            const jenis_barang = $('#formFilterTabel #filter_jenis_barang').val();
            const id_jenis_barang = $('#formFilterTabel #filter_id_jenis_barang').val();
            const id_kecamatan = $('#formFilterTabel #filter_id_kecamatan').val();
            const id_kelurahan = $('#formFilterTabel #filter_id_kelurahan').val();
            const urlQuery = `?jenis_barang=${jenis_barang}&id_jenis_barang=${id_jenis_barang}&id_kecamatan=${id_kecamatan}&id_kelurahan=${id_kelurahan}&cetak=true&xls=${excel}`;

            const url = $(el).attr('href') + urlQuery;

            window.open(url, '_blank').focus();
        }
    </script>
@endpush