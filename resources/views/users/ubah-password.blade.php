<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 06/10/2021
 * Time: 11:18
 */

$title = 'Ubah Password';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{ route('users.ubah-password-proses') }}" method="post">
        @method('PUT')
        @csrf

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="password_lama">Password Lama</label>
                            <input type="password" class="form-control @error('password_lama') is-invalid @enderror" id="password_lama" placeholder="Password Lama" name="password_lama" required>
                            @error('password_lama') <span class="text-danger">{{$message}}</span> @enderror
                        </div>

                        <div class="form-group">
                            <label for="password">Password Baru</label>
                            <input type="password" class="form-control" id="password" placeholder="Password Baru" name="password" required>
                            @error('password') <span class="text-danger">{{$message}}</span> @enderror
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation">Konfirmasi Password Baru</label>
                            <input type="password" class="form-control" id="password_confirmation" placeholder="Konfirmasi Password" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class="card-footer">
                        {!! button_save() !!}
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop