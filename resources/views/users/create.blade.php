<?php

$title = 'Tambah User';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{ route('users.store') }}" method="post">
        @csrf
        @include('users.form')
    </form>
@stop
