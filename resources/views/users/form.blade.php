<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 08/09/2021
 * Time: 14:47
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Nama lengkap" name="name" value="{{ (isset($user))? $user->name : old('name') }}" required>
                    @error('name') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Masukkan Email" name="email" value="{{ (isset($user))? $user->email : old('email') }}" required>
                    @error('email') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" name="password">
                    @error('password') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Konfirmasi Password</label>
                    <input type="password" class="form-control" id="password_confirmation" placeholder="Konfirmasi Password" name="password_confirmation">
                </div>

                <div class="form-group">
                    <label for="role">Role</label>
                    <select class="form-control @error('role') is-invalid @enderror" id="role" name="role" required>
                        <option value="">-- Pilih Role --</option>
                        @foreach(roles() as $k=>$v)
                            <option value="{{ $v }}" {{ (isset($user) && $user->role == $v || old('role') == $v)? 'selected' : '' }}>{{ ucfirst($v) }}</option>
                        @endforeach
                    </select>
                    @error('role') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('users') !!}
            </div>
        </div>
    </div>
</div>
