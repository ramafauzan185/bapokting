<?php

$title = 'Edit User';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('users.update', $user)}}" method="post">
        @method('PUT')
        @csrf
        @include('users/form');
    </form>
@stop
