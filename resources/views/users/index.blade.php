<?php

$title = 'Pengelolaan Data Pengguna';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {!! button_add('users') !!}
                </div>
                <div class="card-body">
                    <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $key => $user)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role}}</td>
                                <td class="status">{!! status($user->status) !!}</td>
                                <td class="text-center">
                                    {!! button_status('users', $user) !!}
                                    {!! button_edit('users', $user) !!}
                                    {!! button_delete('users', $user) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    @include('layouts/js')
@endpush
