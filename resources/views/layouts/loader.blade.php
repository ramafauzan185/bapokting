<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 04/10/2021
 * Time: 11:26
 */

?>

<div class="page-loader-wrapper d-none">
    <div class="loader text-center">
        <div class="spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>
