<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 08/10/2021
 * Time: 10:16
 */

?>

<li class="nav-item">
    <a class="nav-link" style="padding-top: 0.35rem;" href="{{ config('adminlte.dashboard_url', 'home') }}" role="button" target="_blank">
        <span class="badge badge-warning pl-2 pr-2 pt-2 pb-2"><i class="fa fa-globe-asia"></i>&nbsp; Website Disperindag</span>
    </a>
</li>
