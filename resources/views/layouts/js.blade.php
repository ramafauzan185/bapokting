<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 09/09/2021
 * Time: 4:02
 */

?>

@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)

<form action="" id="delete-form" method="post">
    @method('delete')
    @csrf
</form>

<script>

    $('[data-toggle="tooltip"]').tooltip();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#simpleDatatable').DataTable({
        "responsive": true,
        "columnDefs": [
            { "orderable": false, targets: -1 }
        ],
    });

    function simpleDatatable() {
        $('.simpleDatatable').DataTable({
            retrieve: true,
            "responsive": true,
        });
    }

    function notificationBeforeDelete(event, el) {
        event.preventDefault();
        Swal.fire({
            title: 'Anda yakin akan menghapus data ? ',
            text: "Data anda tidak dapat digunakan kembali.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus data!',
            cancelButtonText: 'Batal',
        }).then((result) => {
            if (result.value) {
                $("#delete-form").attr('action', $(el).attr('href'));
                $("#delete-form").submit();
            }
        });
    }

    function updateStatus(event, el) {
        event.preventDefault();
        $(el).tooltip('hide');

        $.ajax({
            data: {},
            url: $(el).attr('href'),
            type: "PUT",
            dataType: 'json',
            success: function (data) {
                const status = data.status;
                const buttons = data.buttons;
                const td_opsi = $(el).parent();
                const td_status = td_opsi.parent().find('.status');

                td_opsi.html(buttons);
                td_status.html(status);

                $('[data-toggle="tooltip"]').tooltip('update');
            },
            error: function (data) {
                $('[data-toggle="tooltip"]').tooltip('update');
                console.log('Error:', data);
            }
        });
    }

    function bulans() {
        return [
            '',
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember",
        ]
    }

    function get_jenis_kelamin(jenis_kelamin) {
        if(jenis_kelamin.toLowerCase() === 'l') {
            return 'Laki-laki';
        } else {
            return 'Perempuan';
        }
    }

    function showLoader() {
        $('.page-loader-wrapper').removeClass('d-none');
    }

    function hideLoader() {
        $('.page-loader-wrapper').addClass('d-none');
    }

</script>
