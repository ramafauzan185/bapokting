<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 13/10/2021
 * Time: 10:15
 */

?>

<li>
    <div class="logo-custom">
        <div class="logo-custom__logo">
            <img src="{{ asset('images/icon/xxhdpi.png') }}">
        </div>
        <div class="logo-custom__title">
            <h3>Dinas Perdagangan dan Perindustrian Kota Bogor</h3>
            <h5>Bidang Pengembangan Perdagangan Dalam Negeri, Perlindungan Konsumen dan Tertib Niaga</h5>
        </div>
    </div>
</li>
