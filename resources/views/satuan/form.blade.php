<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 14:39
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="satuan">Satuan</label>
                    <input type="text" class="form-control @error('satuan') is-invalid @enderror" id="satuan" placeholder="Kg" name="satuan" value="{{ isset($satuan)? $satuan->satuan : old('satuan') }}" required>
                    <span class="form-text text-muted">Contoh: Kg, M, Pack, Pcs</span>
                    @error('satuan') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="nama_satuan">Nama Satuan</label>
                    <input type="text" class="form-control @error('nama_satuan') is-invalid @enderror" id="nama_satuan" placeholder="Kilogram" name="nama_satuan" value="{{ isset($satuan)? $satuan->nama_satuan : old('nama_satuan') }}" required>
                    <span class="form-text text-muted">Contoh: Kilogram, Meter, Pack, Piece</span>
                    @error('nama_satuan') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('satuan') !!}
            </div>
        </div>
    </div>
</div>
