<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 10/09/2021
 * Time: 14:38
 */

$title = 'Edit Perusahaan & Pemilik TDPUD';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('perusahaan.update', $perusahaan)}}" method="post">
        @method('PUT')
        @csrf
        @include('perusahaan/form')
    </form>
@stop