<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 10/09/2021
 * Time: 2:56
 */

?>

@section('plugins.Select2', true)

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="nib">NIB</label>
                    <input type="text" class="form-control @error('nib') is-invalid @enderror" id="nib" placeholder="Nomor induk berusaha" name="nib" value="{{ isset($perusahaan)? $perusahaan->nib : old('nib') }}">
                    <span class="form-text text-muted">Apabila NIB tidak ada, silahkan isi form dengan tanda <i>minus</i> (<b>-</b>).</span>
                    @error('nib') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="nomor_legalitas_usaha">Nomor TDPUD</label>
                    <input type="text" class="form-control @error('nomor_legalitas_usaha') is-invalid @enderror" id="nomor_legalitas_usaha" placeholder="Nomor legalitas usaha" name="nomor_legalitas_usaha" value="{{ isset($perusahaan)? $perusahaan->nomor_legalitas_usaha : old('nomor_legalitas_usaha') }}">
                    <span class="form-text text-muted">Apabila Nomor Legalitas tidak ada, silahkan isi form dengan tanda <i>minus</i> (<b>-</b>).</span>
                    @error('nomor_legalitas_usaha') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="nama_perusahaan">Nama Perusahaan</label>
                    <input type="text" class="form-control @error('nama_perusahaan') is-invalid @enderror" id="nama_perusahaan" placeholder="Nama perusahaan" name="nama_perusahaan" value="{{ isset($perusahaan)? $perusahaan->nama_perusahaan : old('nama_perusahaan') }}" required>
                    @error('nama_perusahaan') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="alamat_perusahaan">Alamat Perusahaan</label>
                    <textarea class="form-control @error('alamat_perusahaan') is-invalid @enderror" id="alamat_perusahaan" name="alamat_perusahaan" required>{{ isset($perusahaan)? $perusahaan->alamat_perusahaan : old('alamat_perusahaan') }}</textarea>
                    @error('nama_perusahaan') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="id_kecamatan">Kecamatan</label>
                            <select class="form-control @error('id_kecamatan') is-invalid @enderror" id="id_kecamatan" name="id_kecamatan" onchange="showKelurahan(this.value)" required>
                                <option value="">-- Pilih Kecamatan --</option>
                                @foreach($kecamatan as $row)
                                    <option value="{{ $row->id }}" {{ (isset($kelurahan) && $kelurahan->id_kecamatan == $row->id || old('id_kecamatan') == $row->id)? 'selected' : '' }}>{{ $row->nama_kecamatan }}</option>
                                @endforeach
                            </select>
                            @error('id_kecamatan') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="id_kelurahan">Kelurahan</label>
                            <select class="form-control @error('id_kelurahan') is-invalid @enderror" id="id_kelurahan" name="id_kelurahan" {{ isset($kelurahans)? '' : 'disabled' }} required>
                                <option value="">-- Pilih Kelurahan --</option>
                                @if(isset($kelurahans))
                                    @foreach($kelurahans as $row)
                                        <option value="{{ $row->id }}" {{ (@$kelurahan->id == $row->id || old('id_kelurahan') == $row->id)? 'selected' : '' }}>{{ $row->nama_kelurahan }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('id_kelurahan') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nomor_telepon">Nomor Telepon</label>
                    <input type="text" class="form-control @error('nomor_telepon') is-invalid @enderror" id="nomor_telepon" placeholder="No telp" name="nomor_telepon" value="{{ isset($perusahaan)? $perusahaan->nomor_telepon : old('nomor_telepon') }}" required>
                    @error('nomor_telepon') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="lokasi_usaha">Lokasi Usaha</label>

                    <div class="form-row">
                        <div class="col-md-12">
                            @foreach(lokasi_usahas() as $k=>$v)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="lokasi_usaha" id="lokasi_usaha__{{ $v }}" value="{{ $v }}" {{ (isset($perusahaan) && $perusahaan->lokasi_usaha == $v || old('lokasi_usaha') == $v)? 'checked' : '' }}>
                                    <label class="form-check-label" for="lokasi_usaha__{{ $v }}">{{ ucwords(str_replace('_', ' ', $v)) }}</label>
                                </div>
                            @endforeach
                            @error('lokasi_usaha') <br><span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="status_usaha">Status Usaha</label>

                    <div class="form-row">
                        <div class="col-md-12">
                            @foreach(status_usahas() as $k=>$v)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status_usaha" id="status_usaha__{{ $v }}" value="{{ $v }}"  {{ (isset($perusahaan) && $perusahaan->status_usaha == $v || old('status_usaha') == $v)? 'checked' : '' }}>
                                    <label class="form-check-label" for="status_usaha__{{ $v }}">{{ ucwords(str_replace('_', ' ', $v)) }}</label>
                                </div>
                            @endforeach
                            @error('status_usaha') <br><span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="status_toko">Status Toko</label>

                    <div class="form-row">
                        <div class="col-md-12">
                            @foreach(status_tokos() as $k=>$v)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status_toko" id="status_toko__{{ $v }}" value="{{ $v }}"  {{ (isset($perusahaan) && $perusahaan->status_toko == $v || old('status_toko') == $v)? 'checked' : '' }}>
                                    <label class="form-check-label" for="status_toko__{{ $v }}">{{ ucwords(str_replace('_', ' ', $v)) }}</label>
                                </div>
                            @endforeach
                            @error('status_toko') <br><span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="skala_usaha">Skala Usaha</label>

                    <div class="form-row">
                        <div class="col-md-12">
                            @foreach(skala_usahas() as $k=>$v)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="skala_usaha" id="skala_usaha__{{ $v }}" value="{{ $v }}" {{ (isset($perusahaan) && $perusahaan->skala_usaha == $v || old('skala_usaha') == $v)? 'checked' : '' }}>
                                    <label class="form-check-label" for="skala_usaha__{{ $v }}">{{ ucwords(str_replace('_', ' ', $v)) }}</label>
                                </div>
                            @endforeach
                            @error('skala_usaha') <br><span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="luas_gudang">Luas Gudang</label>

                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="number" class="form-control @error('luas_gudang') is-invalid @enderror" id="luas_gudang" placeholder="Luas" name="luas_gudang" aria-describedby="inputGroupPrepend" value="{{ isset($perusahaan)? $perusahaan->luas_gudang : old('luas_gudang') }}" required>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="inputGroupPrepend">m<sup>2</sup></span>
                                </div>
                            </div>
                            @error('luas_gudang') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="koordinat">Koordinat (lat, lang)</label>

                    <div id="myMap" style="height: 200px;"></div>

                    <input type="text" class="form-control @error('koordinat') is-invalid @enderror" id="koordinat" placeholder="Latitude,Longitude" name="koordinat" value="{{ isset($perusahaan)? $perusahaan->koordinat : old('koordinat') }}">
                    @error('koordinat') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="id_jenis_barang">Jenis Barang - Bidang Usaha</label>
                    <select multiple class="form-control @error('id_jenis_barang') is-invalid @enderror" id="id_jenis_barang" name="id_jenis_barang[]" size="10" required>
                        @foreach($jenis_barang as $row)
                            <option value="{{ $row->id }}" {{ (in_array($row->id, $id_jenis_barangs))? 'selected' : '' }}>> {{ $row->nama_barang }} - {{ $row->nama_kbli }}</option>
                        @endforeach
                    </select>
                    <span class="form-text text-muted">Pilih komoditi jenis barang dari <b>box sebelah kiri</b> ke <b>box sebelah kanan</b>.</span>
                    @error('id_jenis_barang') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <fieldset class="border p-2">
                    <legend  class="w-auto">&nbsp;Pemilik / Penanggung Jawab Perusahaan&nbsp;</legend>

                    <div  class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="number" class="form-control @error('nik') is-invalid @enderror" id="nik" placeholder="Nomor induk kependudukan" name="nik" value="{{ isset($pemilik)? $pemilik->nik : old('nik') }}" required>
                                @error('nik') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama_pemilik">Nama Lengkap</label>
                                <input type="text" class="form-control @error('nama_pemilik') is-invalid @enderror" id="nama_pemilik" placeholder="Nama lengkap" name="nama_pemilik" value="{{ isset($pemilik)? $pemilik->nama_pemilik : old('nama_pemilik') }}" required>
                                @error('nama_pemilik') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div  class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tempat_lahir">Tempat Lahir</label>
                                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" placeholder="Tempat lahir" name="tempat_lahir" value="{{ isset($pemilik)? $pemilik->tempat_lahir : old('tempat_lahir') }}" required>
                                @error('tempat_lahir') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jenis_kelamin">Jenis Kelamin</label>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        @foreach(jenis_kelamins() as $k=>$v)
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin__{{ $k }}" value="{{ $k }}" {{ (isset($pemilik) && $pemilik->jenis_kelamin == $k || old('jenis_kelamin') == $k)? 'checked' : '' }}>
                                                <label class="form-check-label" for="jenis_kelamin__{{ $k }}">{{ $v }}</label>
                                            </div>
                                        @endforeach
                                        @error('jenis_kelamin') <br><span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div  class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal_lahir">Tanggal Lahir</label>
                                <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" placeholder="Tanggal lahir" name="tanggal_lahir" value="{{ isset($pemilik)? $pemilik->tanggal_lahir : old('tanggal_lahir') }}" required>
                                @error('tanggal_lahir') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal_join">Tanggal Join</label>
                                <input type="date" class="form-control @error('tanggal_join') is-invalid @enderror" id="tanggal_join" placeholder="Tanggal join di perusahaan" name="tanggal_join" value="{{ isset($pemilik)? $pemilik->tanggal_join : old('tanggal_join') }}" required>
                                @error('tanggal_join') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div  class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control @error('alamat') is-invalid @enderror" id="alamat" name="alamat" required>{{ isset($pemilik)? $pemilik->alamat : old('alamat') }}</textarea>
                                @error('alamat') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nomor_handphone">Nomor HP</label>
                                <input type="number" class="form-control @error('nomor_handphone') is-invalid @enderror" id="nomor_handphone" placeholder="Nomor handphone" name="nomor_handphone" value="{{ isset($pemilik)? $pemilik->nomor_handphone : old('nomor_handphone') }}" required>
                                @error('nomor_handphone') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="border p-2 mt-3">
                    <legend  class="w-auto">&nbsp;Akun Pengguna&nbsp;</legend>

                    <div  class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Email aktif" name="email" value="{{ isset($user)? $user->email : old('email') }}" required>
                                <span class="form-text text-muted">Email akan digunakan pada saat login ke aplikasi.</span>
                                @error('email') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="*****" name="password" value="">
                                <span class="form-text text-muted">Default password <code>12345!@</code>, apabila password dikosongkan.</span>
                                @error('password') <span class="text-danger">{{$message}}</span> @enderror
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('perusahaan') !!}
            </div>
        </div>
    </div>
</div>

@section('plugins.Bootstrap4duallistbox', true)
@push('js')

    <script>
        const koordinatCenter = [-6.599618330790078, 106.79976888250381];
        var map = L.map('myMap').setView(koordinatCenter, 13);
        var marks;

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: '{{ env('MAPBOX_ACCESS_TOKEN') }}'
        }).addTo(map);

        function addKoordinat(latlng) {
            $('#koordinat').val(`${latlng.lat},${latlng.lng}`);
        }

        function onMapClick(e) {
            addKoordinat(e.latlng);

            map.removeLayer(marks);

            marks = new L.Marker(e.latlng, {draggable:true});
            marks.on('drag', onMarkerDrag);
            map.addLayer(marks);
        }

        function onMarkerDrag(e) {
            addKoordinat(e.latlng);
        }

        marks = new L.Marker(koordinatCenter, {draggable:true});
        marks.on('drag', onMarkerDrag);
        map.addLayer(marks);
        marks.bindPopup("Klik atau Drag icon marker pada area map untuk mendapatkan koordinat (latitude, longitude).").openPopup();

        map.on('click', onMapClick);

        function showKelurahan(id_kecamatan) {
            $.ajax({
                data: { id_kecamatan: id_kecamatan },
                url: '<?=route('kelurahan.by-kecamatan');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const kelurahanForm = $('#id_kelurahan');
                    kelurahanForm.prop('disabled', false);
                    kelurahanForm.html('');

                    data.kelurahan.forEach((item) => {
                        kelurahanForm.append(`<option value="${item.id}">${item.nama_kelurahan}</option>`);
                    })
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        var listboxIdJenisBarang = $('select[name="id_jenis_barang[]"]').bootstrapDualListbox();
    </script>
@endpush