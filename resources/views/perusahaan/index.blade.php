<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:48
 */

$title = 'Pengelolaan Data Perusahaan & Pemilik TDPUD';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {!! button_add('perusahaan') !!}
                </div>
                <div class="card-body">
                    <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>No. TDPUD</th>
                            <th>Nama Perusahaan</th>
                            <th>Alamat</th>
                            <th>No Telp</th>
                            <th>Skala Usaha</th>
                            <th>Pemilik TDPUD</th>
                            <th>Bidang Usaha</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($perusahaan as $key => $row)
                            @php
                                $bidangUsaha = \App\Models\PerusahaanBidangUsahaView::where(['id_perusahaan' => $row->id, 'status' => '1'])->get()->toArray();
                                $namaBarang = array_column($bidangUsaha, 'nama_barang');
                                $namaBarang = implode(', ', $namaBarang);

                                $users = \App\Models\User::find($row->id_user);
                            @endphp
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$row->nomor_legalitas_usaha}}</td>
                                <td>
                                    {{$row->nama_perusahaan}}
                                    <br>
                                    <small><b>NIB:</b> {{ $row->nib }}</small>
                                </td>
                                <td>{{$row->alamat_perusahaan}}</td>
                                <td>{{$row->nomor_telepon}}</td>
                                <td>{{ ucfirst(str_replace('_', ' ', $row->skala_usaha)) }}</td>
                                <td>{{$row->nama_pemilik}}</td>
                                <td>{{ $namaBarang }}</td>
                                <td width="80px" class="text-center">
                                    {!! button_edit('perusahaan', $row) !!}
                                    {!! button_delete('perusahaan', $row) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    @include('layouts/js')
@endpush