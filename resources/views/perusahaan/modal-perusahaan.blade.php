<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 06/10/2021
 * Time: 13:12
 */

?>

<x-adminlte-modal id="modalPerusahaan" title="Data Perusahaan" size="lg" theme="light" v-centered static-backdrop scrollable>
    <div style="height: auto;" id="modalAktivitasContent">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-stripped simpleDatatable" id="tableModalPerusahaan">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nomor Legalitas Usaha</th>
                            <th>Nama Perusahaan</th>
                            <th>Skala Usaha</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@push('js')
    <script>
        function showModalPerusahaan() {
            const modal = $('#modalPerusahaan');
            const tableModalPerusahaan = $('#tableModalPerusahaan tbody');

            $.ajax({
                data: {},
                url: '{{ route('perusahaan.all') }}',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const perusahaan = data.perusahaan;
                    let tableBody = '';

                    perusahaan.forEach((item, index) => {
                        tableBody += `
                            <tr>
                                <td>${index+1}</td>
                                <td>${item.nomor_legalitas_usaha}</td>
                                <td>${item.nama_perusahaan}</td>
                                <td>${item.skala_usaha}</td>
                                <td class="text-center">
                                    <a href="javascript:;" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="bottom" title="Pilih data" data-id="${item.id}" data-nama_perusahaan="${item.nama_perusahaan}" onclick="pilihData(this)">
                                        <i class="fa fa-check-double"></i>
                                    </a>
                                </td>
                            </tr>
                        `;
                    });

                    tableModalPerusahaan.html('');
                    tableModalPerusahaan.append(tableBody);

                    simpleDatatable();

                    $('[data-toggle="tooltip"]').tooltip();

                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush