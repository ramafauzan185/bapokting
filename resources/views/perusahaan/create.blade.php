<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:51
 */

$title = 'Tambah Perusahaan & Pemilik TDPUD';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('perusahaan.store')}}" method="post">
        @csrf
        @include('perusahaan/form')
    </form>
@stop