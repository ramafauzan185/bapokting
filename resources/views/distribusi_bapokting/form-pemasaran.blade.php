<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 13/09/2021
 * Time: 19:58
 */

?>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <fieldset class="border p-2 mb-3">
        <legend  class="w-auto">&nbsp;Aktivitas Pemasaran&nbsp;</legend>

        <div class="form-group">
            <label for="id_konsumen">Konsumen</label>
            <select class="form-control @error('id_konsumen') is-invalid @enderror" id="id_konsumen" name="id_konsumen">
                <option value="">-- Pilih Konsumen --</option>
                @foreach($konsumen as $row)
                    <option value="{{ $row['id'] }}">{{ $row['nama_konsumen'] }}</option>
                @endforeach
            </select>
            @error('id_konsumen') <span class="text-danger">{{$message}}</span> @enderror
        </div>

        <div class="form-row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="volume_pemasaran">Volume</label>
                    <input type="number" class="form-control @error('volume_pemasaran') is-invalid @enderror" id="volume_pemasaran" name="volume_pemasaran" value="{{ old('volume_pemasaran') }}">
                    @error('volume_pemasaran') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="id_satuan_pemasaran">Satuan</label>
                    <select class="form-control @error('id_satuan_pemasaran') is-invalid @enderror" id="id_satuan_pemasaran" name="id_satuan_pemasaran">
                        <option value="">-- Pilih Satuan --</option>
                        @foreach($satuan as $row)
                            <option value="{{ $row['id'] }}">{{ $row['satuan'] }}</option>
                        @endforeach
                    </select>
                    @error('id_satuan_pemasaran') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="id_asal_konsumen">Asal Konsumen</label>
            <select class="form-control @error('id_asal_konsumen') is-invalid @enderror" id="id_asal_konsumen" name="id_asal_konsumen">
                <option value="">-- Pilih Asal Konsumen --</option>
                @foreach($asal_konsumen as $row)
                    <option value="{{ $row['id'] }}">{{ $row['nama_asal_konsumen'] }}</option>
                @endforeach
            </select>
            @error('id_asal_konsumen') <span class="text-danger">{{$message}}</span> @enderror
        </div>

        <div class="form-group text-right">
            <hr>
            <button type="button" class="btn btn-xs btn-success" onclick="createPemasaran(this)" data-toggle="tooltip" data-placement="bottom" title="Tambah pemasaran"><i class="fa fa-plus-square"></i> &nbsp; Tambah</button>
        </div>

        <div class="form-group">
            <hr>
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-stripped simpleDatatable" id="tablePemasaran">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Konsumen</th>
                        <th>Volume</th>
                        <th>Asal Konsumen</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($pemasaran))
                        @foreach($pemasaran as $k => $row)
                            <tr>
                                <td>{{ $k+1 }}.</td>
                                <td>{{ $row->nama_konsumen }}</td>
                                <td>{{ $row->volume }} {{ $row->satuan }}</td>
                                <td>{{ $row->nama_asal_konsumen }}</td>
                                <td class="text-center">
                                    <a href="javascript:;" onclick="deletePemasaranExist(this, '{{ $k }}', '{{ $row->id }}')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Hapus pemasaran">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

        @error('pemasaran') <span class="text-danger">{{$message}}</span> @enderror
    </fieldset>
</div>

@push('js')
    <script>
        let data_pemasaran = [];
        let data_pemasaran_exist = <?php if(isset($pemasaran)) { echo json_encode($pemasaran); } else { ?> [] <?php } ?>;
        let data_pemasaran_delete = [];

        function createPemasaran(el) {
            const id_konsumen = $('#id_konsumen').val();
            const nama_konsumen = $(`#id_konsumen option[value="${id_konsumen}"]`).text();
            const volume = $('#volume_pemasaran').val();
            const id_satuan = $('#id_satuan_pemasaran').val();
            const satuan = $(`#id_satuan_pemasaran option[value="${id_satuan}"]`).text();
            const id_asal_konsumen = $('#id_asal_konsumen').val();
            const nama_asal_konsumen = $(`#id_asal_konsumen option[value="${id_asal_konsumen}"]`).text();

            if(id_konsumen === '' ||
                volume === '' ||
                id_satuan === '' ||
                id_asal_konsumen === '') {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.warning('Data tidak boleh kosong!', 'Info')
            } else {
                data_pemasaran.unshift({
                    'id_konsumen': id_konsumen,
                    'nama_konsumen': nama_konsumen,
                    'volume': volume,
                    'id_satuan': id_satuan,
                    'satuan': satuan,
                    'id_asal_konsumen': id_asal_konsumen,
                    'nama_asal_konsumen': nama_asal_konsumen,
                });

                storePemasaran();
            }

            $(el).tooltip('hide');
        }

        function showItemPemasaran(item, index)
        {
            let indexExist;
            let opsi;
            if(item.hasOwnProperty('id')) {
                indexExist = 0;
                opsi = `
                        <a href="javascript:;" onclick="deletePemasaranExist(this, ${index}, ${item.id})" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Hapus pemasaran">
                            <i class="fa fa-trash"></i>
                        </a>
                    `;
            } else {
                indexExist = data_pemasaran_exist.length;
                opsi = `
                        <a href="javascript:;" onclick="deletePemasaran(this, ${index})" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Hapus pemasaran">
                            <i class="fa fa-trash"></i>
                        </a>
                    `;
            }

            return `
                <tr>
                    <td>${index+1+indexExist}.</td>
                    <td>${item.nama_konsumen}</td>
                    <td>${item.volume} ${item.satuan}</td>
                    <td>${item.nama_asal_konsumen}</td>
                    <td class="text-center">
                        ${opsi}
                    </td>
                </tr>
            `
        }

        function storePemasaran() {
            const tablePemasaran = $('#tablePemasaran tbody');
            const pemasaran = $('#pemasaran');
            let tableBody = '';
            tablePemasaran.html('');

            data_pemasaran.forEach((item, index) => {
                tableBody += showItemPemasaran(item, index);
            });

            storePemasaranExist();

            tablePemasaran.append(tableBody);

            pemasaran.val(JSON.stringify(data_pemasaran));

            $('[data-toggle="tooltip"]').tooltip();
        }

        function storePemasaranExist() {
            const tablePemasaran = $('#tablePemasaran tbody');
            let tableBody = '';

            data_pemasaran_exist.forEach((item, index) => {
                tableBody += showItemPemasaran(item, index);
            });

            tablePemasaran.append(tableBody);
        }

        function deletePemasaran(el, indexStart) {
            $(el).tooltip('hide');

            data_pemasaran.splice(indexStart, 1);

            storePemasaran();
        }

        function deletePemasaranExist(el, indexStart, id_pemasaran) {
            $(el).tooltip('hide');

            data_pemasaran_exist.splice(parseInt(indexStart), 1);

            storePemasaran();

            data_pemasaran_delete.unshift(parseInt(id_pemasaran));

            $('#pemasaran_delete').val(JSON.stringify(data_pemasaran_delete));
        }
    </script>
@endpush