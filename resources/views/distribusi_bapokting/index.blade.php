<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 6:15
 */

$title = 'Data Distribusi Bapokting';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-12">
            <x-adminlte-card title="Tampilkan Data Berdasarkan" icon="fas fa-sm fa-filter" collapsible="{{ !isset($_GET['btnFilter'])? 'collapsed' : '' }}">
                <form action="{{route('distribusi-bapokting.index')}}" method="get">
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tahun">Tahun :</label>
                                <select class="form-control" id="tahun" name="tahun">
                                    @foreach(tahuns() as $k=>$v)
                                        <option value="{{ $v }}" {{ ($filter['tahun'] == $v)? 'selected' : '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bulan">Bulan :</label>
                                <select class="form-control" id="bulan" name="bulan">
                                    <option value="">All</option>
                                    @foreach(bulans() as $k=>$v)
                                        <option value="{{ $k }}" {{ ($filter['bulan'] == $k)? 'selected' : '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if(is_superadmin())
                            <div class="col-md-3">
                                <label for="id_perusahaan">Perusahaan :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $filter['nama_perusahaan'] }}" placeholder="-- Pilih Perusahaan --" onclick="showModalPerusahaan()" autocomplete="off">
                                    <input type="hidden" id="id_perusahaan" name="id_perusahaan" value="{{ $filter['id_perusahaan'] }}">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" onclick="clearDataPerusahaan()"><i class="fa fa-eraser"></i></button>
                                </span>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="jenis_barang">Jenis Kebutuhan :</label>
                                <select class="form-control" id="jenis_barang" name="jenis_barang">
                                    <option value="">All</option>
                                    @foreach(jenis_barangs() as $k=>$v)
                                        <option value="{{ $k }}" {{ ($filter['jenis_barang'] == $k)? 'selected' : '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="id_jenis_barang">Jenis Komoditi :</label>
                                <select class="form-control" id="id_jenis_barang" name="id_jenis_barang">
                                    <option value="">All</option>
                                    @foreach($jenis_barang as $row)
                                        <option value="{{ $row->id }}" {{ ($filter['id_jenis_barang'] == $row->id)? 'selected' : '' }}>{{ $row->nama_barang }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-12">
                            <hr>
                            <button type="submit" name="btnFilter" class="btn btn-primary"><i class="fa fa-check-square"></i> &nbsp; Tampilkan</button>
                        </div>
                    </div>
                </form>
            </x-adminlte-card>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    {{--<a href="{{route('asal-konsumen.create')}}" class="btn btn-primary mb-2">
                        Tambah
                    </a>--}}

                    <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tahun</th>
                            <th>Bulan</th>
                            <th>Perusahaan</th>
                            <th>Bidang Usaha</th>
                            <th>Jenis Kebutuhan</th>
                            <th>Skala Usaha</th>
                            <th>Status</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($distribusi_bapokting as $key => $row)
                            <tr>
                                <td>{{$key+1}}.</td>
                                <td>{{$row->tahun}}</td>
                                <td>{{ bulans()[$row->bulan] }}</td>
                                <td>{{$row->nama_perusahaan}}</td>
                                <td>{{ $row->nama_barang }} - {{ $row->nama_kbli }}</td>
                                <td>{{ jenis_barangs()[$row->jenis_barang] }}</td>
                                <td>{{ ucwords(str_replace('_', ' ', $row->skala_usaha)) }}</td>
                                <td>{!! status_laporan($row->status_laporan) !!}</td>
                                <td class="text-center">
                                    @if(is_distributor() && $row->status_laporan == 'draft')
                                        {!! button_edit('distribusi-bapokting', $row) !!}
                                        {!! button_delete('distribusi-bapokting', $row) !!}
                                    @else
                                        {!! button_aktivitas($row) !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <x-adminlte-modal id="modalAktivitas" title="Pengadaan dan Pemasaran" size="lg" theme="light" v-centered static-backdrop scrollable>
        <div style="height: auto;" id="modalAktivitasContent">
            <div class="row">
                <div class="col-12">
                    <fieldset class="border p-2 mb-3">
                        <legend  class="w-auto">&nbsp;Aktivitas Pengadaan&nbsp;</legend>
                        <div class="form-group">
                            <table class="table table-hover table-bordered table-stripped simpleDatatable" id="tablePengadaan">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Pembelian</th>
                                    <th>Volume</th>
                                    <th>Lokasi</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>

                <div class="col-12">
                    <fieldset class="border p-2 mb-3">
                        <legend  class="w-auto">&nbsp;Aktivitas Pemasaran&nbsp;</legend>
                        <div class="form-group">
                            <table class="table table-hover table-bordered table-stripped simpleDatatable" id="tablePemasaran">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Konsumen</th>
                                    <th>Volume</th>
                                    <th>Asal Konsumen</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <x-slot name="footerSlot">
            <x-adminlte-button theme="light" label="Tutup" icon="fa fa-xs fa-eye-slash" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>

    @include('perusahaan/modal-perusahaan')
@stop

@push('css')
    <style type="text/css">
        legend {
            font-size: 16px;
            font-weight: bold;
        }
    </style>
@endpush

@push('js')
    @include('layouts/js')

    <script>
        function showAktivitasDistribusi(event, el, items) {
            event.preventDefault();
            $(el).tooltip('hide');

            $.ajax({
                data: {},
                url: $(el).attr('href'),
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    const modal = $('#modalAktivitas');
                    const tablePengadaan = $('#tablePengadaan tbody');
                    const tablePemasaran = $('#tablePemasaran tbody');
                    const pengadaan = data.pengadaan;
                    const pemasaran = data.pemasaran;
                    const item = JSON.parse(items);

                    let tablePengadaanBody = '';
                    pengadaan.forEach((row, index) => {
                        tablePengadaanBody += `
                            <tr>
                                <td width="10px">${index+1}.</td>
                                <td>${row.nama_sumber_pembelian}</td>
                                <td>${row.volume} ${row.satuan}</td>
                                <td>${row.nama_lokasi_pembelian}</td>
                            </tr>
                        `;
                    });

                    let tablePemasaranBody = '';
                    pemasaran.forEach((row, index) => {
                        tablePemasaranBody += `
                            <tr>
                                <td>${index+1}.</td>
                                <td>${row.nama_konsumen}</td>
                                <td>${row.volume} ${row.satuan}</td>
                                <td>${row.nama_asal_konsumen}</td>
                            </tr>
                        `;
                    });

                    tablePengadaan.html('');
                    tablePemasaran.html('');
                    tablePengadaan.append(tablePengadaanBody);
                    tablePemasaran.append(tablePemasaranBody);

                    simpleDatatable();

                    $('.modal-title').html(`${item.nama_barang}`);
                    modal.modal('show');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }

        function pilihData(el) {
            const modal = $('#modalPerusahaan');
            const id = $(el).data('id');
            const nama_perusahaan = $(el).data('nama_perusahaan');

            $('#nama_perusahaan').val(nama_perusahaan);
            $('#id_perusahaan').val(id);

            modal.modal('hide');
        }

        function clearDataPerusahaan() {
            $('#nama_perusahaan').val('');
            $('#id_perusahaan').val('');
        }
    </script>
@endpush