<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 14/09/2021
 * Time: 10:18
 */

$title = 'Edit Laporan Distribusi Bapokting';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('distribusi-bapokting.update', $distribusi_bapokting)}}" method="post" id="distribusi-bapokting-form">
        @method('PUT')
        @csrf
        @include('distribusi_bapokting/form')
    </form>
@stop