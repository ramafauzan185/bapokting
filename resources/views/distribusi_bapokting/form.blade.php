<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 13/09/2021
 * Time: 20:00
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div  class="form-row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="nib">NIB / Nomor TDPUD</label>
                            <input type="text" class="form-control" id="nib" name="nib" value="{{ $perusahaan->nib }} / {{ $perusahaan->nomor_legalitas_usaha }}" disabled="disabled">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="nama_perusahaan">Nama Perusahaan</label>
                            <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $perusahaan->nama_perusahaan }}" disabled="disabled">
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="tahun">Tahun</label>
                            <select class="form-control @error('tahun') is-invalid @enderror" id="tahun" name="tahun" onchange="cekDistribusiBapokting(this)" required>
                                <option value="">-- Pilih Tahun --</option>
                                @foreach(tahuns() as $k=>$v)
                                    @if(isset($distribusi_bapokting))
                                        @php $selected = ($distribusi_bapokting->tahun == $v && old('tahun') == '' || old('tahun') == $v)? 'selected' : ''; @endphp
                                    @else
                                        @php $selected = (date("Y") == $v && old('tahun') == '' || old('tahun') == $v)? 'selected' : '' @endphp
                                    @endif
                                    <option value="{{ $v }}" {{ $selected }}>{{ $v }}</option>
                                @endforeach
                            </select>
                            @error('tahun') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="bulan">Bulan</label>
                            <select class="form-control @error('bulan') is-invalid @enderror" id="bulan" name="bulan" onchange="cekDistribusiBapokting(this)" required>
                                <option value="">-- Pilih Bulan --</option>
                                @foreach(bulans() as $k=>$v)
                                    @php
                                        $month = ($k <= 9)? '0'.$k : $k;
                                    @endphp

                                    @if(isset($distribusi_bapokting))
                                        @php $selected = ($distribusi_bapokting->bulan == $k && old('bulan') == '' || old('bulan') == $k)? 'selected' : ''; @endphp
                                    @else
                                        @php $selected = (date("m") == $month && old('bulan') == '' || old('bulan') == $k)? 'selected' : '' @endphp
                                    @endif

                                    <option value="{{ $k }}" {{ $selected }}>{{ $v }}</option>
                                @endforeach
                            </select>
                            @error('bulan') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="id_jenis_barang">Jenis Barang - Bidang Usaha</label>
                            <select class="form-control @error('id_jenis_barang') is-invalid @enderror" id="id_jenis_barang" name="id_jenis_barang" onchange="cekDistribusiBapokting(this)" required>
                                <option value="">-- Pilih Jenis Barang - Bidang Usaha --</option>
                                @foreach($perusahaan_bidang_usaha as $row)
                                    @if(isset($distribusi_bapokting))
                                        @php $selected = ($distribusi_bapokting->id_jenis_barang == $row->id_jenis_barang && old('id_jenis_barang') == '' || old('id_jenis_barang') == $row->id_jenis_barang)? 'selected' : ''; @endphp
                                    @else
                                        @php $selected = (old('id_jenis_barang') == $row->id_jenis_barang)? 'selected' : ''; @endphp
                                    @endif
                                    <option value="{{ $row->id_jenis_barang }}" {{ $selected }}>{{ $row->nama_barang }} - {{ $row->nama_kbli }}</option>
                                @endforeach
                            </select>
                            @error('id_jenis_barang') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputName">Skala Usaha</label>
                            <div class="form-row">
                                <div class="col-12">
                                    @foreach(skala_usahas() as $k=>$v)
                                        @if(isset($distribusi_bapokting))
                                            @php $checked = ($distribusi_bapokting->skala_usaha == $v && old('skala_usaha') == '' || old('skala_usaha') == $v)? 'checked' : ''; @endphp
                                        @else
                                            @php $checked = ($perusahaan->skala_usaha == $v && old('skala_usaha') == '' || old('skala_usaha') == $v)? 'checked' : ''; @endphp
                                        @endif
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="skala_usaha" id="skala_usaha__{{ $v }}" value="{{ $v }}" {{ $checked }}>
                                            <label class="form-check-label" for="skala_usaha__{{ $v }}">{{ ucwords(str_replace('_', ' ', $v)) }}</label>
                                        </div>
                                    @endforeach
                                    @error('skala_usaha') <span class="text-danger">{{$message}}</span> @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    @include('distribusi_bapokting/form-pengadaan')

                    @include('distribusi_bapokting/form-pemasaran')
                </div>
            </div>

            <div class="card-footer">
                <input type="hidden" name="status_laporan" id="status_laporan" value="send">
                <input type="hidden" name="pengadaan" id="pengadaan" value="">
                <input type="hidden" name="pemasaran" id="pemasaran" value="">
                <input type="hidden" name="pengadaan_delete" id="pengadaan_delete" value="">
                <input type="hidden" name="pemasaran_delete" id="pemasaran_delete" value="">

                {!! button_save() !!}
                {!! button_draft() !!}
                {!! button_cancel('distribusi-bapokting') !!}
            </div>
        </div>
    </div>
</div>

@section('plugins.Toastr', true)
@section('plugins.Sweetalert2', true)

@push('css')
    <style lang="scss" type="text/css">
        #tablePengadaan, #tablePemasaran {
            font-size: 12px !important;
        }
    </style>
@endpush

@push('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('[data-toggle="tooltip"]').tooltip();

        function submitDraft() {
            const form = $('#distribusi-bapokting-form');
            const status_laporan = $('#status_laporan');

            status_laporan.val('draft');
            form.submit();
        }

        function cekDistribusiBapokting(el) {
            const tahun = $('#distribusi-bapokting-form #tahun').val();
            const bulan = $('#distribusi-bapokting-form #bulan').val();
            const id_jenis_barang = $('#distribusi-bapokting-form #id_jenis_barang').val();
            const is_form = '<?php echo \Illuminate\Support\Facades\Request::segment(3);?>';

            const data = {
                tahun,
                bulan,
                id_jenis_barang,
                is_form,
            };

            $.ajax({
                data: data,
                url: '<?=route('distribusi-bapokting.cek-data-sama');?>',
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    if(data.distribusi_bapokting.length > 0) {
                        Swal.fire({
                            title: 'Tambah Distribusi Bapokting',
                            text: "Data sudah ada. Silahkan pilih tahun/bulan/jenis barang lain !",
                            type: 'warning',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok',
                        }).then((result) => {
                            $(el).val('');
                        });
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endpush