<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 13/09/2021
 * Time: 19:55
 */

?>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <fieldset class="border p-2 mb-3">
        <legend  class="w-auto">&nbsp;Aktivitas Pengadaan&nbsp;</legend>

        <div class="form-group">
            <label for="id_sumber_pembelian">Sumber Pembelian</label>
            <select class="form-control @error('id_sumber_pembelian') is-invalid @enderror" id="id_sumber_pembelian" name="id_sumber_pembelian">
                <option value="">-- Pilih Sumber Pembelian --</option>
                @foreach($sumber_pembelian as $row)
                    <option value="{{ $row['id'] }}">{{ $row['nama_sumber_pembelian'] }}</option>
                @endforeach
            </select>
            @error('id_sumber_pembelian') <span class="text-danger">{{$message}}</span> @enderror
        </div>

        <div class="form-row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="volume_pengadaan">Volume</label>
                    <input type="number" class="form-control @error('volume_pengadaan') is-invalid @enderror" id="volume_pengadaan" name="volume_pengadaan" value="{{ old('volume_pengadaan') }}">
                    @error('volume_pengadaan') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="id_satuan_pengadaan">Satuan</label>
                    <select class="form-control @error('id_satuan_pengadaan') is-invalid @enderror" id="id_satuan_pengadaan" name="id_satuan_pengadaan">
                        <option value="">-- Pilih Satuan --</option>
                        @foreach($satuan as $row)
                            <option value="{{ $row['id'] }}">{{ $row['satuan'] }}</option>
                        @endforeach
                    </select>
                    @error('id_satuan_pengadaan') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="id_lokasi_pembelian">Lokasi Pembelian</label>
            <select class="form-control @error('id_lokasi_pembelian') is-invalid @enderror" id="id_lokasi_pembelian" name="id_lokasi_pembelian">
                <option value="">-- Pilih Lokasi Pembelian --</option>
                @foreach($lokasi_pembelian as $row)
                    <option value="{{ $row['id'] }}">{{ $row['nama_lokasi_pembelian'] }}</option>
                @endforeach
            </select>
            @error('id_lokasi_pembelian') <span class="text-danger">{{$message}}</span> @enderror
        </div>

        <div class="form-group text-right">
            <hr>
            <button type="button" class="btn btn-xs btn-success" onclick="createPengadaan(this)" data-toggle="tooltip" data-placement="bottom" title="Tambah pengadaan"><i class="fa fa-plus-square"></i> &nbsp; Tambah</button>
        </div>

        <div class="form-group">
            <hr>
            <div class="table-responsive">
                <table class="table table-hover table-bordered table-stripped simpleDatatable" id="tablePengadaan">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Pembelian</th>
                        <th>Volume</th>
                        <th>Lokasi</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($pengadaan))
                        @foreach($pengadaan as $k => $row)
                            <tr>
                                <td>{{ $k+1 }}.</td>
                                <td>{{ $row->nama_sumber_pembelian }}</td>
                                <td>{{ $row->volume }} {{ $row->satuan }}</td>
                                <td>{{ $row->nama_lokasi_pembelian }}</td>
                                <td class="text-center">
                                    <a href="javascript:;" onclick="deletePengadaanExist(this, '{{ $k }}', '{{ $row->id }}')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Hapus pengadaan">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

        @error('pengadaan') <span class="text-danger">{{$message}}</span> @enderror
    </fieldset>
</div>

@push('js')
    <script>
        let data_pengadaan = [];
        let data_pengadaan_exist = <?php if(isset($pengadaan)) { echo json_encode($pengadaan); } else { ?> [] <?php } ?>;
        let data_pengadaan_delete = [];

        function createPengadaan(el) {
            const id_sumber_pembelian = $('#id_sumber_pembelian').val();
            const nama_sumber_pembelian = $(`#id_sumber_pembelian option[value="${id_sumber_pembelian}"]`).text();
            const volume = $('#volume_pengadaan').val();
            const id_satuan = $('#id_satuan_pengadaan').val();
            const satuan = $(`#id_satuan_pengadaan option[value="${id_satuan}"]`).text();
            const id_lokasi_pembelian = $('#id_lokasi_pembelian').val();
            const nama_lokasi_pembelian = $(`#id_lokasi_pembelian option[value="${id_lokasi_pembelian}"]`).text();

            if(id_sumber_pembelian === '' ||
                volume === '' ||
                id_satuan === '' ||
                id_lokasi_pembelian === '') {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-bottom-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.warning('Data tidak boleh kosong!', 'Info')
            } else {
                data_pengadaan.unshift({
                    'id_sumber_pembelian': id_sumber_pembelian,
                    'nama_sumber_pembelian': nama_sumber_pembelian,
                    'volume': volume,
                    'id_satuan': id_satuan,
                    'satuan': satuan,
                    'id_lokasi_pembelian': id_lokasi_pembelian,
                    'nama_lokasi_pembelian': nama_lokasi_pembelian,
                });

                storePengadaan();
            }

            $(el).tooltip('hide');
        }

        function showItemPengadaan(item, index)
        {
            let indexExist;
            let opsi;
            if(item.hasOwnProperty('id')) {
                indexExist = 0;
                opsi = `
                        <a href="javascript:;" onclick="deletePengadaanExist(this, ${index}, ${item.id})" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Hapus pengadaan">
                            <i class="fa fa-trash"></i>
                        </a>
                    `;
            } else {
                indexExist = data_pengadaan_exist.length;
                opsi = `
                        <a href="javascript:;" onclick="deletePengadaan(this, ${index})" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="bottom" title="Hapus pengadaan">
                            <i class="fa fa-trash"></i>
                        </a>
                    `;
            }

            return `
                <tr>
                    <td>${index+1+indexExist}.</td>
                    <td>${item.nama_sumber_pembelian}</td>
                    <td>${item.volume} ${item.satuan}</td>
                    <td>${item.nama_lokasi_pembelian}</td>
                    <td class="text-center">
                        ${opsi}
                    </td>
                </tr>
            `
        }

        function storePengadaan() {
            const tablePengadaan = $('#tablePengadaan tbody');
            const pengadaan = $('#pengadaan');
            let tableBody = '';
            tablePengadaan.html('');

            data_pengadaan.forEach((item, index) => {
                tableBody += showItemPengadaan(item, index);
            });

            storePengadaanExist();

            tablePengadaan.append(tableBody);

            pengadaan.val(JSON.stringify(data_pengadaan));

            $('[data-toggle="tooltip"]').tooltip();
        }

        function storePengadaanExist() {
            const tablePengadaan = $('#tablePengadaan tbody');
            let tableBody = '';

            data_pengadaan_exist.forEach((item, index) => {
                tableBody += showItemPengadaan(item, index);
            });

            tablePengadaan.append(tableBody);
        }

        function deletePengadaan(el, indexStart) {
            $(el).tooltip('hide');

            data_pengadaan.splice(parseInt(indexStart), 1);

            storePengadaan();
        }

        function deletePengadaanExist(el, indexStart, id_pengadaan) {
            $(el).tooltip('hide');

            data_pengadaan_exist.splice(parseInt(indexStart), 1);

            storePengadaan();

            data_pengadaan_delete.unshift(parseInt(id_pengadaan));

            $('#pengadaan_delete').val(JSON.stringify(data_pengadaan_delete));
        }
    </script>
@endpush