<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 6:29
 */

$title = 'Tambah Laporan Distribusi Bapokting';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    <form action="{{route('distribusi-bapokting.store')}}" method="post" id="distribusi-bapokting-form">
        @csrf
        @include('distribusi_bapokting/form')
    </form>
@stop