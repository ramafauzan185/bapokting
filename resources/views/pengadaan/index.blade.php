<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 6:21
 */

$title = 'Data Pengadaan';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <x-adminlte-card title="Tampilkan Data Berdasarkan" icon="fas fa-sm fa-filter" collapsible="{{ !isset($_GET['btnFilter'])? 'collapsed' : '' }}">
                <form action="{{route('pengadaan.index')}}" method="get">
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tahun">Tahun :</label>
                                <select class="form-control" id="tahun" name="tahun">
                                    @foreach(tahuns() as $k=>$v)
                                        <option value="{{ $v }}" {{ ($filter['tahun'] == $v)? 'selected' : '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bulan">Bulan :</label>
                                <select class="form-control" id="bulan" name="bulan">
                                    <option value="">All</option>
                                    @foreach(bulans() as $k=>$v)
                                        <option value="{{ $k }}" {{ ($filter['bulan'] == $k)? 'selected' : '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if(is_superadmin())
                            <div class="col-md-3">
                                <label for="id_perusahaan">Perusahaan :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="{{ $filter['nama_perusahaan'] }}" placeholder="-- Pilih Perusahaan --" onclick="showModalPerusahaan()" autocomplete="off">
                                    <input type="hidden" id="id_perusahaan" name="id_perusahaan" value="{{ $filter['id_perusahaan'] }}">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" onclick="clearDataPerusahaan()"><i class="fa fa-eraser"></i></button>
                                </span>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="jenis_barang">Jenis Kebutuhan :</label>
                                <select class="form-control" id="jenis_barang" name="jenis_barang">
                                    <option value="">All</option>
                                    @foreach(jenis_barangs() as $k=>$v)
                                        <option value="{{ $k }}" {{ ($filter['jenis_barang'] == $k)? 'selected' : '' }}>{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="id_jenis_barang">Jenis Komoditi :</label>
                                <select class="form-control" id="id_jenis_barang" name="id_jenis_barang">
                                    <option value="">All</option>
                                    @foreach($jenis_barang as $row)
                                        <option value="{{ $row->id }}" {{ ($filter['id_jenis_barang'] == $row->id)? 'selected' : '' }}>{{ $row->nama_barang }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-12">
                            <hr>
                            <button type="submit" name="btnFilter" class="btn btn-primary"><i class="fa fa-check-square"></i> &nbsp; Tampilkan</button>
                        </div>
                    </div>
                </form>
            </x-adminlte-card>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tahun</th>
                            <th>Bulan</th>
                            <th>Perusahaan</th>
                            <th>Jenis Barang</th>
                            <th>Sumber Pembelian</th>
                            <th>Volume</th>
                            <th>Lokasi Pembelian</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pengadaan as $key => $row)
                            @php
                            $distribusi_bapokting = \App\Models\DistribusiBapoktingView::where('id', $row->id_distribusi_bapokting)->first();
                            @endphp
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$distribusi_bapokting->tahun}}</td>
                                <td>{{ bulans()[$distribusi_bapokting->bulan] }}</td>
                                <td>{{ $distribusi_bapokting->nama_perusahaan }}</td>
                                <td>
                                    {{ $distribusi_bapokting->nama_barang }}
                                    <br>
                                    <small>- {{ jenis_barangs()[$distribusi_bapokting->jenis_barang] }}</small>
                                </td>
                                <td>{{ $row->nama_sumber_pembelian }}</td>
                                <td>{{ $row->volume }} {{ $row->satuan }}</td>
                                <td>{{ $row->nama_lokasi_pembelian }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    @include('perusahaan/modal-perusahaan')
@stop

@push('js')
    @include('layouts/js')

    <script>
        function pilihData(el) {
            const modal = $('#modalPerusahaan');
            const id = $(el).data('id');
            const nama_perusahaan = $(el).data('nama_perusahaan');

            $('#nama_perusahaan').val(nama_perusahaan);
            $('#id_perusahaan').val(id);

            modal.modal('hide');
        }

        function clearDataPerusahaan() {
            $('#nama_perusahaan').val('');
            $('#id_perusahaan').val('');
        }
    </script>
@endpush