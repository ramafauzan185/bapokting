<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 09/10/2021
 * Time: 15:04
 */

$title = 'Pengelolaan Data Image Slider';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {!! button_add('slider') !!}
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Judul</th>
                                <th>Caption</th>
                                <th>Image</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($slider as $key => $row)
                                @php
                                    $image = !empty($row->image)? asset('images/slider/'.$row->image) : $row->image_url;
                                @endphp
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$row->title}}</td>
                                    <td>{{$row->caption}}</td>
                                    <td>
                                        <a href="{{ $image }}" target="_blank">
                                            <img src="{{ $image }}" class="img-fluid img-rounded img-thumbnail" style="width: 50px; object-fit: cover;">
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        {!! button_edit('slider', $row) !!}
                                        {!! button_delete('slider', $row) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    @include('layouts/js')
@endpush