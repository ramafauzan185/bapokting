<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 08/10/2021
 * Time: 14:07
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Judul image slider" name="title" value="{{ isset($slider)? $slider->title : old('title') }}">
                    @error('title') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="caption">Caption</label>
                    <textarea class="form-control @error('caption') is-invalid @enderror" id="caption" name="caption">{{ isset($slider)? $slider->caption : old('caption') }}</textarea>
                    @error('caption') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="upload_image">Pilih Gambar</label>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="upload_image" id="upload_image__image" value="image" {{ (isset($slider) && !empty($slider->image) || old('upload_image') == 'image')? 'checked' : '' }} onclick="showImageForm()">
                                <label class="form-check-label" for="upload_image__image">Upload Gambar</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="upload_image" id="upload_image__image_url" value="image_url" {{ (isset($slider) && !empty($slider->image_url) || old('upload_image') == 'image_url')? 'checked' : '' }} onclick="showImageUrlForm()">
                                <label class="form-check-label" for="upload_image__image_url">Url Gambar</label>
                            </div>
                            @error('upload_image') <br><span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="row {{ (isset($slider) && !empty($slider->image) || old('upload_image') == 'image')? '' : 'd-none' }}" id="showImageForm">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="image">Upload Gambar</label>
                            <input type="file" class="form-control @error('image') is-invalid @enderror" id="formImage" name="image" onchange="previewFile('formImage', ['png', 'jpeg', 'jpg'])">
                            <span class="form-text text-muted">*Format file : png/jpeg/jpg | Ukuran max : 2 MB | Resolusi(optional): 1000px X 500px</span>
                            @error('image') <span class="text-danger">{{$message}}</span> @enderror
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div id="preview__formImage" class="preview-image">
                            @if(isset($slider) && !empty($slider->image))
                                <img src="{{ asset('images/slider/'.$slider->image) }}" class="img-thumbnail img-rounded">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group {{ (isset($slider) && !empty($slider->image_url) || old('upload_image') == 'image_url')? '' : 'd-none' }}" id="showImageUrlForm">
                    <label for="image_url">Url Gambar</label>
                    <input type="text" class="form-control @error('image_url') is-invalid @enderror" id="title" placeholder="https://" name="image_url" value="{{ (isset($slider) && !empty($slider->image_url))? $slider->image_url : old('image_url') }}">
                    <span class="form-text text-muted">*Contoh: https://picsum.photos/1000/500?random=1</span>
                    @error('image_url') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('slider') !!}
            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        function previewFile(id, exts = ['']) {
            const preview   = document.querySelector('#preview__'+id);
            const file      = document.querySelector('#'+id).files[0];
            const imgPath   = document.querySelector('#'+id).value;
            const extn      = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            const reader    = new FileReader();

            if(!exts.includes(extn.toLowerCase())) {
                alert('Format file/lampiran yang dibolehkan: '+ exts.toString().toUpperCase() +' !');
                $('#'+id).val('');
                $('#preview__'+id).html('');
            } else {
                if(extn === 'jpg' || extn === 'jpeg' || extn === 'png') {
                    reader.addEventListener("load", function () {
                        preview.innerHTML = `<img src="${reader.result}" class="img-responsive img-thumbnail">`;
                    }, false);
                } else {
                    preview.innerHTML = '';
                }
            }

            if (file) {
                reader.readAsDataURL(file);
            }
        }

        function showImageForm() {
            $('#showImageForm').removeClass('d-none');
            $('#showImageUrlForm').addClass('d-none');
        }

        function showImageUrlForm() {
            $('#showImageForm').addClass('d-none');
            $('#showImageUrlForm').removeClass('d-none');
        }
    </script>
@endpush