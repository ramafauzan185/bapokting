<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 08/10/2021
 * Time: 14:06
 */

$title = 'Tambah Slider';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('slider.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        @include('slider/form')
    </form>
@stop