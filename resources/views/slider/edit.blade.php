<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 09/10/2021
 * Time: 17:15
 */

$title = 'Edit Slider';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('slider.update', $slider)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        @include('slider/form');
    </form>
@stop