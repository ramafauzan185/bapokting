<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 15:07
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="nama_sumber_pembelian">Sumber Pembelian</label>
                    <input type="text" class="form-control @error('nama_sumber_pembelian') is-invalid @enderror" id="nama_sumber_pembelian" placeholder="Nama sumber pembelian" name="nama_sumber_pembelian" value="{{ isset($sumber_pembelian)? $sumber_pembelian->nama_sumber_pembelian : old('nama_sumber_pembelian') }}" required>
                    @error('nama_sumber_pembelian') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('sumber-pembelian') !!}
            </div>
        </div>
    </div>
</div>
