<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 15:19
 */

$title = 'Edit Sumber Pembelian';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('sumber-pembelian.update', $sumber_pembelian)}}" method="post">
        @method('PUT')
        @csrf
        @include('sumber_pembelian/form');
    </form>
@stop