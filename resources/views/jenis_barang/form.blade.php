<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 12:00
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="nama_barang">Nama Barang</label>
                    <input type="text" class="form-control @error('nama_barang') is-invalid @enderror" id="nama_barang" placeholder="Nama Barang" name="nama_barang" value="{{ isset($jenis_barang)? $jenis_barang->nama_barang : old('nama_barang') }}" required>
                    @error('nama_barang') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="id_bidang_usaha">KBLI</label>
                    <select class="form-control @error('id_bidang_usaha') is-invalid @enderror" id="id_bidang_usaha" name="id_bidang_usaha" required>
                        <option value="">-- Pilih KBLI --</option>
                        @foreach($bidang_usaha as $row)
                            <option value="{{ $row->id }}" {{ (isset($jenis_barang) && $jenis_barang->id_bidang_usaha == $row->id || old('id_bidang_usaha') == $row->id)? 'selected' : old('id_bidang_usaha') }}>{{ $row->kode_kbli }} - {{ $row->nama_kbli }}</option>
                        @endforeach
                    </select>
                    @error('id_bidang_usaha') <span class="text-danger">{{ $message }}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('jenis-barang') !!}
            </div>
        </div>
    </div>
</div>
