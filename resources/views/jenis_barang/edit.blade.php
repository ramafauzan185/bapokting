<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 14:21
 */

$title = 'Edit Jenis Barang';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('jenis-barang.update', $jenis_barang)}}" method="post">
        @method('PUT')
        @csrf
        @include('jenis_barang/form');
    </form>
@stop
