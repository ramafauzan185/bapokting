<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:33
 */

$title = 'Tambah Jenis Barang';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <form action="{{route('jenis-barang.store')}}" method="post">
        @csrf
        @include('jenis_barang/form')
    </form>
@stop