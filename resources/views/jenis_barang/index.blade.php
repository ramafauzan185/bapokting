<?php
/**
 * Created by PhpStorm.
 * User: rizki
 * Date: 31/08/2021
 * Time: 4:29
 */

$title = 'Pengelolaan Data Jenis Barang';

?>

@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <h1 class="m-0 text-dark">{{ $title }}</h1>
@stop

@section('content')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    {!! button_add('jenis-barang') !!}
                </div>
                <div class="card-body">
                    <table class="table table-hover table-bordered table-stripped" id="simpleDatatable">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Barang</th>
                            <th>KBLI</th>
                            <th>Jenis Barang</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jenis_barang as $key => $row)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$row->nama_barang}}</td>
                                <td>{{$row->kode_kbli}} - {{ $row->nama_kbli }}</td>
                                <td>{{ jenis_barangs()[$row->jenis_barang] }}</td>
                                <td class="text-center">
                                    {!! button_edit('jenis-barang', $row) !!}
                                    {!! button_delete('jenis-barang', $row) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    @include('layouts/js')
@endpush