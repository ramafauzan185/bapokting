<?php
/**
 * Created by PhpStorm.
 * User: rizky
 * Date: 09/09/2021
 * Time: 16:12
 */

?>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="nama_lokasi_pembelian">Lokasi Pembelian</label>
                    <input type="text" class="form-control @error('nama_lokasi_pembelian') is-invalid @enderror" id="nama_lokasi_pembelian" placeholder="Nama lokasi pembelian" name="nama_lokasi_pembelian" value="{{ isset($lokasi_pembelian)? $lokasi_pembelian->nama_lokasi_pembelian : old('nama_lokasi_pembelian') }}" required>
                    @error('nama_lokasi_pembelian') <span class="text-danger">{{$message}}</span> @enderror
                </div>
            </div>

            <div class="card-footer">
                {!! button_save() !!}
                {!! button_cancel('lokasi-pembelian') !!}
            </div>
        </div>
    </div>
</div>
