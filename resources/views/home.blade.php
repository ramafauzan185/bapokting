@php
    $title = 'Dashboard';
@endphp

@extends('adminlte::page')
@section('title', $title)

@section('content_header')
    {{--<h1 class="m-0 text-dark">{{ $title }}</h1>--}}
@stop

@section('content')
    @include('layouts/loader')
    @include('layouts/flash-message')

    <div class="row">
        <div class="col-md-12 mt-2 mb-2">
            @include('dashboard/carousel')
        </div>
    </div>

    @if($is_superadmin || $is_manajemen)
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="pills-grafik-tab" data-toggle="pill" href="#pills-grafik" role="tab" aria-controls="pills-grafik" aria-selected="false">Grafik</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-peta-tab" data-toggle="pill" href="#pills-peta" role="tab" aria-controls="pills-peta" aria-selected="true">Peta</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-tabel-tab" data-toggle="pill" href="#pills-tabel" role="tab" aria-controls="pills-tabel" aria-selected="false">Tabel</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade" id="pills-grafik" role="tabpanel" aria-labelledby="pills-grafik-tab">
                @include('dashboard/grafik_dashboard')
            </div>

            <div class="tab-pane fade show active" id="pills-peta" role="tabpanel" aria-labelledby="pills-peta-tab">
                <div class="row justify-content-left">
                    <div class="col-md-12">
                        @include('dashboard/map_koordinat_perusahaan')
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="pills-tabel" role="tabpanel" aria-labelledby="pills-tabel-tab">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        @include('dashboard/tabel_perusahaan')
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($is_distributor)
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('dashboard/tabel_laporan_stok')
            </div>
        </div>
    @endif
@stop

@section('css')
@stop

@section('js')
    @include('layouts/js')
@stop